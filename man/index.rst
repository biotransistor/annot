.. Annotamentum documentation master file, created by
   sphinx-quickstart on Wed Apr 26 12:14:35 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Annotamentum's Documentation.
========================================

.. image:: img/annotlogo06.png
           :height: 128
           :alt: An! official annot logo
           :align: center

*Django admin based sample, reagent and experiment metadata tracking.*

**Summary:**
`Annot`_ is a web application, developed for biological wetlab experiment layout, sample and reagent logging,
so that data is ready for sharing and analysis.
On its core annot makes use of the `acpipe_anjson`_ library and acjson - assay coordinate `json`_ - file format.
The use of controlled vocabulary from ontologies for sample and reagent annotation is enforced.
Annot's modular implementation can be adapted to a variety of experimental paradigms.

.. _Annot: https://gitlab.com/biotransistor/annot
.. _acpipe_anjson: https://gitlab.com/biotransistor/acpipe_acjson
.. _json: http://json.org/

**Implementation:**
The annot web application is implemented in python3 utilizing roughly
`django`_, `postgresql`_, `nginx`_.
Annot is deployed via `docker`_ container platform.
Annot code it self is thereby packed into a `debian`_ docker container.

.. _django: https://www.djangoproject.com/
.. _postgresql: https://www.postgresql.org/
.. _nginx: https://nginx.org/
.. _docker: https://www.docker.com/
.. _debian: https://www.debian.org/

Annot was developed using for browser the `firefox`_ developer edition.
However, all major browsers are supported.

.. _firefox: https://www.mozilla.org/en-US/firefox/developer/

Source code is available under `GNU AGPLv3`_ license.
This manual is written under the `GNU FDLv1.3`_ license.

.. _GNU AGPLv3: https://www.gnu.org/licenses/licenses.html#AGPL
.. _GNU FDLv1.3: https://www.gnu.org/licenses/licenses.html#FDL

This are **links** to `source code`_ and
a `poster presentation`_ at PyCon 2015 Montreal, Canada at the very beginning of project.

.. _source code: https://gitlab.com/biotransistor/annot
.. _poster presentation: https://us.pycon.org/2015/schedule/presentation/461/
.. _publication: https://www.youtube.com/watch?v=dQw4w9WgXcQ

**This user manual:** is structured in tutorial, how-tos, reference and discussion.

.. toctree::
   :maxdepth: 3

   man_tutorial
   man_howto
   man_reference
   man_discussion
   fdl-1_3
