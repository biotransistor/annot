# theAnnot Project

+ [user manual](https://annot.readthedocs.io/en/latest/) at Read the Docs.
+ [source code](https://gitlab.com/biotransistor/annot) at GitLab.


## Version Log

+ **v0.5.02 - autumn alpha release 2019-10-31:** publication.
+ **v0.5.01 - autumn alpha release 2018-11-01:** publication submission.
+ **v0.5 - spring alpha release 2018-03-21:** acpipe acjson based reimplementation. more solid more flexible.
+ **v0.4 - midsummer alpha release 2017-06-21:** this is anNot.
+ **v0.3 - spring alpha release 2016-03-21:** used for lincs mema assay annotation.
+ **v0.2 - midsummer beta release 2015-09-09:** useful, definitely maybe.
+ **v0.1 - PyCon 2015 Montreal gamma release 2015-04-15:** still under development.
+ **v0.0 - first push 2014-09-12:** and a poked full of ideas to be implemented.
