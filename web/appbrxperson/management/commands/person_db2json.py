# import form django
from django.core import management
from django.core.management.base import BaseCommand, CommandError

# import from python
from datetime import datetime
import glob
import io
import json

# import from annot
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest


class Command(BaseCommand):
    args = "<no args...>"
    help="generates a json backup form the berson brick."

    def handle(self, *args, **options):
        if (options['verbosity'] > 0):
            self.stdout.write("\ndb2json being processed ...")
        # find latest file version and load as json obj
        s_latesregex = "{}brick/oo/person_brick_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]_oo.json".format(
            MEDIA_ROOT
        )
        ls_annotfile = glob.glob(s_latesregex)
        s_latestpathfile = annotfilelatest(ls_annotfile=ls_annotfile)
        if not(s_latestpathfile is None):
            with open(s_latestpathfile, 'r') as f_json:
                d_jsonbackup = json.load(f_json)

        # generate new data json obj
        o_out = io.StringIO()
        management.call_command(
            "dumpdata",
            "appbrxperson.personbrick",
            stdout=o_out,
            verbosity=0
        )
        s_jsonrecord = o_out.getvalue()
        s_jsonrecord = s_jsonrecord.strip()
        d_jsonrecord = json.loads(s_jsonrecord)

        # backup if necessary
        if (s_latestpathfile is None) or (d_jsonrecord != d_jsonbackup):
            # generate filename
            s_filename = "person_brick_{}{}_oo.json".format(
                datetime.now().strftime("%Y%m%d"),
                datetime.now().strftime("_%H%M%S")
            )
            s_filepath = "{}brick/oo/{}".format(
                MEDIA_ROOT,
                s_filename
            )
            # generate file (if no arg as annot id exist raise error)
            with open(s_filepath, 'w') as f_json:
                json.dump(
                    d_jsonrecord,
                    f_json,
                    sort_keys=True,
                )  # indent=4,
        else:
            self.stdout.write(
                "Info: no new file produced. data object defined in latest file is the same as new generated: {}.".format(
                    s_latestpathfile
                )
            )
        if (options['verbosity'] > 0):
            self.stdout.write("ok")
