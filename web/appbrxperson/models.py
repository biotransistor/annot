from django.db import models
# import from python
import re


# Create your models here.
# constant
ls_column_personbrick = [
    "annot_id",
    "email",
    "first_name",
    "middle_initials",
    "last_name"
]

# data structure
class PersonBrick(models.Model):
    annot_id = models.SlugField(
        max_length=128,
        primary_key=True,
        verbose_name="Annot Identifier",
        help_text="Unique identifier. Automatically generated."
    )
    first_name =  models.SlugField(
        max_length=32,
        help_text="Your first name."
    )
    middle_initials =  models.SlugField(
        max_length=32,
        help_text="The middle initials, if you own one.",
        blank=True
    )
    last_name =  models.SlugField(
        max_length=32,
        help_text="Your last name."
    )
    email = models.EmailField(
        default="not@yet.specified",
        help_text="The email address of a person."
    )
    # primary key generator
    def save(self, *args, **kwargs):
        s_firstn = re.sub(r'[^a-zA-Z0-9]', '', self.first_name)
        s_middle = re.sub(r'[^a-zA-Z0-9]', '', self.middle_initials)
        s_lastn = re.sub(r'[^a-zA-Z0-9]', '', self.last_name)
        s_annot_id = "{}_{}_{}".format(s_firstn, s_middle, s_lastn)
        self.annot_id = s_annot_id
        super().save(*args, **kwargs)

    # output
    def __repr__(self):
        #__repr__ = __str__
        s_out = "\nPersonBrick:\n"
        for s_column in ls_column_personbrick:
            s_out = "{}{}: {}\n".format(
                s_out,
                s_column,
                str(getattr(self, s_column))
            )
        return(s_out)

    def __str__(self):
        return(self.annot_id)

    class Meta:
        unique_together =(("first_name","middle_initials","last_name"),)
        ordering = ["annot_id"]
        verbose_name_plural = "Staff" #"People"
