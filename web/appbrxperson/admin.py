# import form django
from django.contrib import admin
from django.http import HttpResponseRedirect

# import from annot
from appbrxperson.models import PersonBrick


# Register your models here.
#admin.site.register(PersonBrick)
class PersonBrickAdmin(admin.ModelAdmin):
    search_fields = (
        "annot_id",
        "first_name",
        "middle_initials",
        "last_name",
        "email"
    )
    list_display = (
        "annot_id",
        "first_name",
        "middle_initials",
        "last_name",
        "email"
    )
    save_on_top = True
    fieldsets = [
        ("Primary key", {"fields": [
            "annot_id",
            "first_name",
            "middle_initials",
            "last_name"
        ]}),
        ("Detail", {"fields": [
            "email"
        ]})
    ]
    readonly_fields = ("annot_id",)

    # drop down menu
    actions = [
        "delete_selected",
        "download_brick_json",
        "download_brick_txt",
    ]

    ### json ###
    def download_brick_json( self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=brick&choice=person"
        ))
    download_brick_json.short_description = "Download human page as json file (item selection irrelevant)"

    ## tsv ##
    def download_brick_txt(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=brick&choice=person"
        ))
    download_brick_txt.short_description = "Download human page as tsv file (item selection irrelevant)"

# register
admin.site.register(PersonBrick, PersonBrickAdmin)
