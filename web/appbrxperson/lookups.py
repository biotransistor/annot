# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appbrxperson.models import PersonBrick

# code
class PersonBrickLookup(ModelLookup):
    model = PersonBrick
    search_fields = ('annot_id__icontains',)
registry.register(PersonBrickLookup)
