from django.apps import AppConfig


class ApponproviderOwnConfig(AppConfig):
    name = 'apponprovider_own'
