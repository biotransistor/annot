# import django
from django.core import management
from django.core.management.base import BaseCommand  #,CommandError

# import python
from datetime import datetime
import glob
import json

# import annot
from appbrreagentantibody.models import ls_column_antibody1brick, antibody1brick_db2objjson
from appsabrick.structure import objjson2tsvoo, tsvoo2objjson
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest


class Command(BaseCommand):
    args = "<no args...>"
    help = "Write primary antibody annotation content from database into a tsv file in human readable form."

    def handle(self, *args, **options):
        if (options['verbosity'] > 0):
            self.stdout.write("\ndb2tsvhuman being processed ...")
        # find latest file version and load as json obj
        s_latesregex = "{}brick/human/antibody1_brick_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]_human.txt".format(
            MEDIA_ROOT
        )
        ls_annotfile = glob.glob(s_latesregex)
        s_latestpath = annotfilelatest(ls_annotfile=ls_annotfile)
        if not (s_latestpath is None):
            dd_latestjson = tsvoo2objjson(
                s_latestpath,
                ls_column=ls_column_antibody1brick
            )
        # generate new data json obj
        dd_json = antibody1brick_db2objjson()
        # check for diff json objs
        if (s_latestpath is None) or (dd_latestjson != dd_json):
            # generate new filename
            s_filename = "antibody1_brick_{}{}_human.txt".format(
                datetime.now().strftime("%Y%m%d"),
                datetime.now().strftime("_%H%M%S")
            )
            s_filepath = "{}brick/human/{}".format(
                MEDIA_ROOT,
                s_filename
            )
            # generate now file (if no arg as annot id exist raise error)
            objjson2tsvoo(
                s_filename=s_filepath,
                ls_column=ls_column_antibody1brick,
                dd_json=dd_json
            )
        else:
            self.stdout.write(
                "Conclusion: no new file produced. data object defined in latest file found is the same as new generated: ".format(
                    s_latestpath
                )
            )
        if (options['verbosity'] > 0):
            self.stdout.write("ok")
