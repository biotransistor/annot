# import django
from django.core import management
from django.core.management.base import BaseCommand  #,CommandError

# import python
from datetime import datetime
import glob
import json

# import annot
from appbrreagentantibody.models import antibody2brick_db2objjson
from appsabrick.structure import objjson2json, json2objjson
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest


class Command(BaseCommand):
    args = "<no args...>"
    help = "Write secondary antibody annotation content into database into a json file."

    def handle(self, *args, **options):
        if (options['verbosity'] > 0):
            self.stdout.write("\ndb2json being processed ...")
        # find latest file version and load as json obj
        s_latesregex = "{}brick/oo/antibody2_brick_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]_oo.json".format(
            MEDIA_ROOT
        )
        ls_annotfile = glob.glob(s_latesregex)
        s_latestpath = annotfilelatest(ls_annotfile=ls_annotfile)
        if not(s_latestpath is None):
            dd_latestjson = json2objjson(s_latestpath)
        # generate new data json obj
        dd_json = antibody2brick_db2objjson()
        # check for diff json objs
        if (s_latestpath is None) or (dd_latestjson != dd_json):
            # generate filename
            s_filename = "antibody2_brick_{}{}_oo.json".format(
                datetime.now().strftime("%Y%m%d"),
                datetime.now().strftime("_%H%M%S")
            )
            s_filepath = "{}brick/oo/{}".format(MEDIA_ROOT, s_filename)
            # generate file (if no arg as annot id exist raise error)
            objjson2json(
                s_filename=s_filepath,
                dd_json=dd_json
            )
        else:
            self.stdout.write(
                "Info: no new file produced. data object defined in latest file is the same as new generated: {}.".format(
                    s_latestpath
                )
            )
        if (options['verbosity'] > 0):
            self.stdout.write("ok")
