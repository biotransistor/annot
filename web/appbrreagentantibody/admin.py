# import from django
from django import forms
from django.contrib import admin
from django.contrib import messages
from django.core import management
from django.http import HttpResponseRedirect
from appbrreagentantibody.models import Antibody1Brick, Antibody2Brick

# python
import io
import re

# import from django selectable prj
from selectable.forms import AutoComboboxSelectWidget, AutoComboboxSelectMultipleWidget

# import from annot
from appbrxperson.lookups import PersonBrickLookup
from apponantibodypart_bioontology.lookups import AntibodyPartLookup
from apponclonality_bioontology.lookups import ClonalityLookup
from apponcompound_ebi.lookups import CompoundLookup
from appondye_own.lookups import DyeLookup
from apponimmunologyisotype_bioontology.lookups import ImmunologyIsotypeLookup
from apponorganism_bioontology.lookups import OrganismLookup
from apponprotein_uniprot.lookups import ProteinLookup
from apponprovider_own.lookups import ProviderLookup
from apponunit_bioontology.lookups import UnitLookup
from apponyieldfraction_own.lookups import YieldFractionLookup

# Register your models here.
#admin.site.register(Antibody1Brick)
class Antibody1BrickForm(forms.ModelForm):
    class Meta:
        model = Antibody1Brick
        fields = [
            "antigen",
            "host_organism",
            "isotype",
            "provider",
            "crossabsorption",
            "clonality",
            "immunogen_source_organism",
            "antibodypart_provided",
            "purity",
            "stocksolution_buffer_compound",
            "stocksolution_concentration_value",
            "stocksolution_concentration_unit",
            "final_concentration_unit",
            "time_unit",
            "responsible",
        ]
        widgets = {
            "antigen": AutoComboboxSelectWidget(ProteinLookup),
            "host_organism": AutoComboboxSelectWidget(OrganismLookup),
            "isotype": AutoComboboxSelectWidget(ImmunologyIsotypeLookup),
            "provider": AutoComboboxSelectWidget(ProviderLookup),
            "crossabsorption": AutoComboboxSelectMultipleWidget(ProteinLookup),
            "clonality": AutoComboboxSelectWidget(ClonalityLookup),
            "immunogen_source_organism":AutoComboboxSelectWidget(OrganismLookup),
            "antibodypart_provided": AutoComboboxSelectMultipleWidget(AntibodyPartLookup),
            "purity": AutoComboboxSelectWidget(YieldFractionLookup),
            "stocksolution_buffer_compound": AutoComboboxSelectMultipleWidget(CompoundLookup),
            "stocksolution_concentration_unit": AutoComboboxSelectWidget(UnitLookup),
            "final_concentration_unit": AutoComboboxSelectWidget(UnitLookup),
            "time_unit": AutoComboboxSelectWidget(UnitLookup),
            "responsible": AutoComboboxSelectWidget(PersonBrickLookup),
        }

class Antibody1BrickAdmin(admin.ModelAdmin):
    form = Antibody1BrickForm
    search_fields = (
        "annot_id",
        "lincs_identifier",
        "common_name",
        "provider__annot_id",
        "provider_catalog_id",
        "provider_batch_id",
        "antigen__annot_id",
        "host_organism__annot_id",
        "isotype__annot_id",
        "crossabsorption__annot_id",
        "notes"
    )
    list_display = (
        "annot_id",
        "lincs_identifier",
        "antigen",
        "common_name",
        "host_organism",
        "isotype",
        "provider",
        "provider_catalog_id",
        "provider_batch_id",
        "available",
        "notes",
        "crossabsorption_display",
        "clonality",
        "clone_id",
        "epitop_sequence_note",
        "epitop_sequence",
        "immunogen_sequence_note",
        "immunogen_sequence",
        "immunogen_source_organism_note",
        "immunogen_source_organism",
        "antibodypart_provided_display",
        "purity",
        "lyophilized",
        "stocksolution_buffer_compound_display",
        "stocksolution_concentration_value",
        "stocksolution_concentration_unit",
        "final_concentration_unit",
        "time_unit",
        "reference_url",
        "responsible"
    )
    list_editable = ("available",) # "notes"
    save_on_top = True
    fieldsets = [
        ("Primary key", {"fields": [
            "annot_id",
            "lincs_identifier",
            "antigen",
            "common_name",
            "host_organism",
            "isotype",
            "provider",
            "provider_catalog_id",
            "provider_batch_id"
        ]}),
        ("Gory detail", {"fields": [
            "crossabsorption_display",
            "crossabsorption",
            "clonality",
            "clone_id",
            "epitop_sequence_note",
            "epitop_sequence",
            "immunogen_sequence_note",
            "immunogen_sequence",
            "immunogen_source_organism_note",
            "immunogen_source_organism",
            "antibodypart_provided_display",
            "antibodypart_provided",
            "purity",
            "lyophilized"
        ]}),
        ("Laboratory", {"fields": [
            "available",
            "stocksolution_buffer_compound_display",
            "stocksolution_buffer_compound",
            "stocksolution_concentration_value",
            "stocksolution_concentration_unit",
            "final_concentration_unit",
            "time_unit",
        ]}),
        ("Reference", {"fields" : [
            "reference_url",
            "responsible",
            "notes"
        ]})
    ]
    readonly_fields = (
        "annot_id",
        "crossabsorption_display",
        "antibodypart_provided_display",
        "stocksolution_buffer_compound_display",
    )
    actions = [
        "delete_selected",
        "download_brick_json",
        "download_brick_txt",
        "brick_load"
    ]

    ## json ##
    def download_brick_json(self, request, queryset):
        # call appsabrick view export function
        return(
            HttpResponseRedirect(
                "/appsavocabulary/export?filetype=json&layer=brick&choice=antibody1"
            )
        )
    download_brick_json.short_description = "Download antibody1 page as json file (item selection irrelevant)"

    ## tsv ##
    def download_brick_txt(self, request, queryset):
        # call appsabrick view export function
        return(
            HttpResponseRedirect(
                "/appsavocabulary/export?filetype=tsv&layer=brick&choice=antibody1"
            )
        )
    download_brick_txt.short_description = "Download antibody1 page as tsv file (item selection irrelevant)"


    # brick ##
    def brick_load(self, request, queryset):  # self = modeladmin
        # python manage.py brick_pull s_brick_type
        o_out = io.StringIO()
        management.call_command(
            "brick_load",
            "antibody1",
            stdout=o_out,
            verbosity=0
        )
        s_out = o_out.getvalue()
        s_out = s_out.strip()
        if (s_out == ""):
            self.message_user(
                request,
                "Antibody1 # successfully bricked.",
                level=messages.SUCCESS
            )
        elif re.match(".*Error.*", s_out):
            self.message_user(
                request,
                "Antibody1 # {}".format(s_out),
                level=messages.ERROR
            )
        elif re.match(".*Warning.*", s_out):
            self.message_user(
                request,
                "Antibody1 # {}".format(s_out),
                level=messages.WARNING
            )
        else:
            self.message_user(
                request,
                "Antibody1 # {}".format(s_out),
                level=messages.INFO
            )
    brick_load.short_description = "Upload antibody1 bricks (item selection irrelevant)"

# register
admin.site.register(Antibody1Brick, Antibody1BrickAdmin)


#admin.site.register(Antibody2Brick)
class Antibody2BrickForm(forms.ModelForm):
    class Meta:
        model = Antibody2Brick
        fields = [
            "host_organism",
            "target_organism",
            "isotype",
            "dye",
            "provider",
            "crossabsorption",
            "clonality",
            "immunogen_source_organism",
            "antibodypart_provided",
            "purity",
            "stocksolution_buffer_compound",
            "stocksolution_concentration_value",
            "stocksolution_concentration_unit",
            "final_concentration_unit",
            "time_unit",
            "responsible",
        ]
        widgets = {
            "host_organism": AutoComboboxSelectWidget(OrganismLookup),
            "target_organism": AutoComboboxSelectWidget(OrganismLookup),
            "isotype": AutoComboboxSelectWidget(ImmunologyIsotypeLookup),
            "dye": AutoComboboxSelectWidget(DyeLookup),
            "provider": AutoComboboxSelectWidget(ProviderLookup),
            "crossabsorption": AutoComboboxSelectMultipleWidget(OrganismLookup),
            "clonality": AutoComboboxSelectWidget(ClonalityLookup),
            "antibodypart_target": AutoComboboxSelectMultipleWidget(AntibodyPartLookup),
            "immunogen_source_organism":AutoComboboxSelectWidget(OrganismLookup),
            "antibodypart_provided": AutoComboboxSelectMultipleWidget(AntibodyPartLookup),
            "purity": AutoComboboxSelectWidget(YieldFractionLookup),
            "stocksolution_buffer_compound": AutoComboboxSelectMultipleWidget(CompoundLookup),
            "stocksolution_concentration_unit": AutoComboboxSelectWidget(UnitLookup),
            "final_concentration_unit": AutoComboboxSelectWidget(UnitLookup),
            "time_unit": AutoComboboxSelectWidget(UnitLookup),
            "responsible": AutoComboboxSelectWidget(PersonBrickLookup),
        }

class Antibody2BrickAdmin(admin.ModelAdmin):
    form = Antibody2BrickForm
    search_fields = (
        "annot_id",
        "lincs_identifier",
        "common_name",
        "provider__annot_id",
        "provider_catalog_id",
        "provider_batch_id",
        "host_organism__annot_id",
        "target_organism__annot_id",
        "isotype__annot_id",
        "dye__annot_id",
        "crossabsorption__annot_id",
        "notes"
    )
    list_display = (
        "annot_id",
        "lincs_identifier",
        "common_name",
        "host_organism",
        "target_organism",
        "isotype",
        "dye",
        "provider",
        "provider_catalog_id",
        "provider_batch_id",
        "available",
        "notes",
        "crossabsorption_display",
        "clonality",
        "antibodypart_target_display",
        "immunogen_sequence_note",
        "immunogen_sequence",
        "immunogen_source_organism_note",
        "immunogen_source_organism",
        "antibodypart_provided_display",
        "purity",
        "lyophilized",
        "stocksolution_buffer_compound_display",
        "stocksolution_concentration_value",
        "stocksolution_concentration_unit",
        "final_concentration_unit",
        "time_unit",
        "microscopy_channel",
        "wavelength_nm_nominal_excitation",
        "wavelength_nm_excitation",
        "wavelength_nm_emission",
        "reference_url",
        "responsible"
    )
    list_editable = ("available",)  #"notes"
    save_on_top = True
    fieldsets = [
        ("Primary key", {"fields": [
            "annot_id",
            "lincs_identifier",
            "common_name",
            "host_organism",
            "target_organism",
            "isotype",
            "dye",
            "provider",
            "provider_catalog_id",
            "provider_batch_id"
        ]}),
        ("Gory detail", {"fields": [
            "crossabsorption_display",
            "crossabsorption",
            "clonality",
            "antibodypart_target_display",
            "antibodypart_target",
            "immunogen_sequence_note",
            "immunogen_sequence",
            "immunogen_source_organism_note",
            "immunogen_source_organism",
            "antibodypart_provided_display",
            "antibodypart_provided",
            "purity",
            "lyophilized"
        ]}),
        ("Laboratory", {"fields": [
            "available",
            "stocksolution_buffer_compound_display",
            "stocksolution_buffer_compound",
            "stocksolution_concentration_value",
            "stocksolution_concentration_unit",
            "final_concentration_unit",
            "time_unit",
        ]}),
        ("Microscopy", {"fields": [
            "microscopy_channel",
            "wavelength_nm_nominal_excitation",
            "wavelength_nm_excitation",
            "wavelength_nm_emission"
        ]}),
        ("Reference", {"fields":[
            "reference_url",
            "responsible",
            "notes"
        ]})
    ]
    readonly_fields = (
        "annot_id",
        "crossabsorption_display",
        "antibodypart_target_display",
        "antibodypart_provided_display",
        "stocksolution_buffer_compound_display",
    )
    actions = [
        "delete_selected",
        "download_brick_json",
        "download_brick_txt",
        "brick_load"
    ]

    ## json ##
    def download_brick_json(self, request, queryset):
        # call appsabrick view export function
        return(
            HttpResponseRedirect(
                "/appsavocabulary/export?filetype=json&layer=brick&layer=brick&choice=antibody2"
            )
        )
    download_brick_json.short_description = "Download antibody2 page as json file (item selection irrelevant)"

    ## tsv ##
    def download_brick_txt(self, request, queryset):
        # call appsabrick view export function
        return(
            HttpResponseRedirect(
                "/appsavocabulary/export?filetype=tsv&layer=brick&choice=antibody2"
            )
        )
    download_brick_txt.short_description = "Download antibody2 page as tsv file (item selection irrelevant)"

    ## brick ##
    def brick_load(self, request, queryset):  # self = modeladmin
        # python manage.py brick_pull s_brick_type
        o_out = io.StringIO()
        management.call_command(
            "brick_load",
            "antibody2",
            stdout=o_out,
            verbosity=0
        )
        s_out = o_out.getvalue()
        s_out = s_out.strip()
        if (s_out == ""):
            self.message_user(
                request,
                "Antibody2 # successfully bricked.",
                level=messages.SUCCESS
            )
        elif re.match(".*Error.*", s_out):
            self.message_user(
                request,
                "Antibody2 # {}".format(s_out) ,
                level=messages.ERROR
            )
        elif re.match(".*Warning.*", s_out):
            self.message_user(
                request,
                "Antibody2 # {}".format(s_out),
                level=messages.WARNING
            )
        else:
            self.message_user(
                request,
                "Antibody2 # {}".format(s_out),
                level=messages.INFO
            )
    brick_load.short_description = "Upload antibody2 bricks (item selection irrelevant)"

# register
admin.site.register(Antibody2Brick, Antibody2BrickAdmin)
