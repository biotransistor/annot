# descrioption:
#    variables, constants and trafo functoin
#    realted to the 2017 liincs strabadard


# imoport from annot
from appbrreagentprotein.models import  proteinsetbrick_db2objjson  # ProteinSetBrick, proteinsetbrick_db2d,
dd_proteinset = proteinsetbrick_db2objjson()

# import form python
import os
import re


ls_column_antibodylincs = [
    "AR_LINCS_ID",
    "AR_Name",
    "AR_Alternative_Name",
    "AR_Alternative_ID",
    "AR_Center_Canonical_ID",
    "AR_Relevant_Citations",
    "AR_Center_Name",
    "AR_Center_Batch_ID",
    "AR_Provider_Name",
    "AR_Provider_Catalog_ID",
    "AR_Provider_Batch_ID",
    "AR_Comments",
    "AR_RRID",
    "AR_Clone_Name",
    "AR_Antibody_Type",
    "AR_Target_Protein",
    "AR_Target_Protein_ID",
    "AR_Target_Protein_LINCS_ID",
    "AR_Target_Protein_Center_ID",
    "AR_Non-Protein_Target",
    "AR_Target_Organism",
    "AR_Immunogen",
    "AR_Immunogen_Sequence",
    "AR_Antibody_Species",
    "AR_Antibody_Clonality",
    "AR_Antibody_Isotype",
    "AR_Antibody_Production_Source_Organism",
    "AR_Antibody_Production_Details",
    "AR_Antibody_Labeling",
    "AR_Antibody_Labeling_Details",
    "AR_Antibody_Purity",
]

ls_column_celllinelincs = [
    "CL_LINCS_ID",
    "CL_Name",
    "CL_Alternative_Name",
    "CL_Alternative_ID",
    "CL_Center_Canonical_ID",
    "CL_Relevant_Citations",
    "CL_Center_Name",
    "CL_Center_Batch_ID",
    "CL_Provider_Name",
    "CL_Provider_Catalog_ID",
    "CL_Provider_Batch_ID",
    "CL_Comments",
    "CL_Organism",
    "CL_Organ",
    "CL_Tissue",
    "CL_Cell_Type",
    "CL_Cell_Type_Detail",
    "CL_Donor_Sex",
    "CL_Donor_Age",
    "CL_Donor_Ethnicity",
    "CL_Donor_Health_Status",
    "CL_Disease",
    "CL_Disease_Detail",
    "CL_Known_Mutations",
    "CL_Mutation_Citations",
    "CL_Molecular_Features",
    "CL_Genetic_Modification",
    "CL_Growth_Properties",
    "CL_Recommended_Culture_Conditions",
    "CL_Related_Projects",
    "CL_Verification_Reference_Profile",
    "CL_Reference_Source",
    "CL_Cell_Markers",
    "CL_Gonosome_Code",
    "CL_Disease_Site_Onset",
    "CL_Disease_Age_Onset",
    "CL_Donor_Age_Death",
    "CL_Donor_Disease_Duration",
    "CL_Precursor_Cell_Name",
    "CL_Precursor_Cell_LINCS_ID",
    "CL_Precursor_Cell_Center_Batch_ID",
    "CL_Production_Details",
    "CL_Quality_Verification",
    "CL_Transient_Modification",
    "CL_Passage_Number",
    "CL_Source_Information",
    "CL_Date_Received",
    "CL_Center_Specific_Code",
]

ls_column_otherreagentlincs = [
    "OR_LINCS_ID",
    "OR_Name",
    "OR_Center_Canonical_ID",
    "OR_Center_Name",
    "OR_Center_Batch_ID",
    "OR_Provider_Name",
    "OR_Provider_Catalog_ID",
    "OR_Provider_Batch_ID",
    "OR_Comments",
]

ls_column_proteinlincs = [
    "PR_LINCS_ID",
    "PR_Name",
    "PR_Alternative_Name",
    "PR_Alternative_ID",
    "PR_Center_Canonical_ID",
    "PR_Relevant_Citations",
    "PR_Center_Name",
    "PR_Center_Batch_ID",
    "PR_Provider_Name",
    "PR_Provider_Catalog_ID",
    "PR_Provider_Batch_ID",
    "PR_Comments",
    "PR_PLN",
    "PR_UniProt_ID",
    "PR_Mutations",
    "PR_Modifications",
    "PR_Protein_Complex_Known_Component_LINCS_IDs",
    "PR_Protein_Complex_Known_Component_UniProt_IDs",
    "PR_Protein_Complex_Known_Component_Center_Protein_IDs",
    "PR_Protein_Complex_Details",
    "PR_Protein_Complex_Stoichiometry",
    "PR_Amino_Acid_Sequence",
    "PR_Production_Source_Organism",
    "PR_Production_Method",
    "PR_Protein_Purity",
]

ls_column_smallmoleculelincs = [
    "SM_LINCS_ID",
    "SM_Name",
    "SM_Alternative_Name",
    "SM_Alternative_ID",
    "SM_Center_Canonical_ID",
    "SM_Relevant_Citations",
    "SM_Center_Name",
    "SM_Center_Batch_ID",
    "SM_Provider_Name",
    "SM_Provider_Catalog_ID",
    "SM_Provider_Batch_ID",
    "SM_Comments",
    "SM_PubChem_CID",
    "SM_ChEBI_ID",
    "SM_InChI_Parent",
    "SM_InChI_Key_Parent",
    "SM_SMILES_Parent",
    "SM_Salt",
    "SM_SMILES_Batch",
    "SM_InChI_Batch",
    "SM_InChI_Key_Batch",
    "SM_Molecular_Mass",
    "SM_Purity",
    "SM_Purity_Method",
    "SM_Aqueous_Solubility",
    "SM_LogP",
]


# annot objjson to lincs dcic dataformat transformation
def idtrafo(s_id):
    if (s_id not in {"not_available", "not_yet_specified"}):
        ls_id = s_id.split("_")
        ls_id.pop(-1)
        s_id = "_".join(ls_id)
    return(s_id)

def consistent_update(d_dict, s_key, s_value):
    try:
        if not (d_dict[s_key] == s_value):
            raise ValueError(
                "Error @ appsabrick.lincsstandard : value inconsistency for  s_id {} {} {}.".format(
                    s_key,
                    s_value,
                    d_dict[s_key],
                )
            )
    except KeyError:
        d_dict.update({s_key : s_value})

def objjson2lincs(s_filename, dd_json, ls_column, b_verbose=True):
    """
    description:
        write lincs dcic compatible brick files out of json obj.
    """
    if (b_verbose):
        print(f"put json obj into lincs dcic compatible file: {s_filename}")
    # header
    if not (os.path.isfile(s_filename)):
        with open(s_filename, 'w') as f:
            f.write(",".join(ls_column) + "\n")
    # data row
    dd_out = {}

    # get record in alphabetical order
    ls_annotid = list(dd_json.keys())
    ls_annotid.sort()
    for s_annotid in ls_annotid:
        d_record = dd_json[s_annotid]

        # identifier manipulation
        try:
            s_proteinset = d_record["proteinset_annot_id"]
            if (s_proteinset == "not_available-notavailable_notavailable_notavailable"):
                # protein gent
                s_id = s_annotid
            else:
                # proteinset gent
                s_id = s_proteinset
        except KeyError:
            # non protein proteinset gents
            s_id = s_annotid
        try:
            d_out = dd_out[s_id]  #copy.deepcopy(dd_out[s_id])
        except KeyError:
            d_out = {}

        if s_id in {"air_Own-notavailable_notavailable_notavailable",}:
            continue

        # handle each lincs metdata standard 2017 column case
        for s_column in ls_column:
            # antibody
            if (s_column == "AR_LINCS_ID"):
                d_out.update({"AR_LINCS_ID": d_record["lincs_identifier"]})  # ok

            elif (s_column == "AR_Name"):
                d_out.update({"AR_Name": d_record["common_name"]})  # ok

            elif (s_column == "AR_Alternative_Name"):
                d_out.update({"AR_Alternative_Name": ""})  # nop

            elif (s_column == "AR_Alternative_ID"):
                d_out.update({"AR_Alternative_ID": ""})  # nop

            elif (s_column == "AR_Center_Canonical_ID"):
                ls_canonical = d_record["annot_id"].split("-")
                ls_canonical.pop(-1)
                s_canonical = "-".join(ls_canonical)
                d_out.update({"AR_Center_Canonical_ID": s_canonical})  # split

            elif (s_column == "AR_Relevant_Citations"):
                d_out.update({"AR_Relevant_Citations": d_record["reference_url"]})  # ok

            elif (s_column == "AR_Center_Name"):
                d_out.update({"AR_Center_Name": "MEP_LINCS"})  # ok

            elif (s_column == "AR_Center_Batch_ID"):
                d_out.update({"AR_Center_Batch_ID": d_record["annot_id"]})  # ok

            elif (s_column == "AR_Provider_Name"):
                d_out.update({"AR_Provider_Name": idtrafo(d_record["provider"])})  # trafo

            elif (s_column == "AR_Provider_Catalog_ID"):
                d_out.update({"AR_Provider_Catalog_ID": d_record["provider_catalog_id"]})  # ok

            elif (s_column == "AR_Provider_Batch_ID"):
                d_out.update({"AR_Provider_Batch_ID": d_record["provider_batch_id"]})  # ok

            elif (s_column == "AR_Comments"):
                # bue: AR_Comments is deald with at AR_Target_Organism
                pass

            elif (s_column == "AR_RRID"):
                d_out.update({"AR_RRID": ""})  # nop

            elif (s_column == "AR_Clone_Name"):
                try:
                    d_out.update({"AR_Clone_Name": d_record["clone_id"]})  # ok primary
                except KeyError:
                    d_out.update({"AR_Clone_Name": "not_available"})  # nop secondary

            elif (s_column == "AR_Antibody_Type"):
                d_out.update({"AR_Antibody_Type": d_record["immunogen_source_organism_note"]})  # ok

            elif (s_column == "AR_Target_Protein"):
                try:
                    d_out.update({"AR_Target_Protein": idtrafo(d_record["antigen"])})  # trafo primary
                except KeyError:
                    d_out.update({"AR_Target_Protein": "not_available"})  # nop secondary

            elif (s_column == "AR_Target_Protein_ID"):
                try:
                    d_out.update({"AR_Target_Protein_ID": d_record["antigen"].split("_")[-1]})  # split primary
                except KeyError:
                    d_out.update({"AR_Target_Protein_ID": "not_available"})  # nop secondary

            elif (s_column == "AR_Target_Protein_LINCS_ID"):
                d_out.update({"AR_Target_Protein_LINCS_ID": ""})  # nop

            elif (s_column == "AR_Target_Protein_Center_ID"):
                try:
                    d_out.update({"AR_Target_Protein_Center_ID": d_record["antigen"]})  # ok primary
                except KeyError:
                    d_out.update({"AR_Target_Protein_Center_ID": "not_available"})  # nop secondary

            elif (s_column == "AR_Non-Protein_Target"):
                d_out.update({"AR_Non-Protein_Target": ""})  # nop

            elif (s_column == "AR_Target_Organism"):
                try:
                    d_out.update({"AR_Target_Organism": idtrafo(d_record["target_organism"])})  # trafo secondary
                    d_out.update({"AR_Comments": "secondary antibody"})  # comment secondary antibody
                except KeyError:
                    d_out.update({"AR_Target_Organism": "not_available"})  # nop primary
                    d_out.update({"AR_Comments": "primary antibody"})  # comment primary antibody

            elif (s_column == "AR_Immunogen"):
                d_out.update({"AR_Immunogen": d_record["immunogen_sequence_note"]})  # ok

            elif (s_column == "AR_Immunogen_Sequence"):
                d_out.update({"AR_Immunogen_Sequence": d_record["immunogen_sequence"]})  # ok

            elif (s_column == "AR_Antibody_Species"):
                d_out.update({"AR_Antibody_Species": idtrafo(d_record["immunogen_source_organism"])})  # trafo

            elif (s_column == "AR_Antibody_Clonality"):
                d_out.update({"AR_Antibody_Clonality": idtrafo(d_record["clonality"])})  # trafo

            elif (s_column == "AR_Antibody_Isotype"):
                d_out.update({"AR_Antibody_Isotype": idtrafo(d_record["isotype"])})  # trafo

            elif (s_column == "AR_Antibody_Production_Source_Organism"):
                d_out.update({"AR_Antibody_Production_Source_Organism": idtrafo(d_record["host_organism"])})  # trafo

            elif (s_column == "AR_Antibody_Production_Details"):
                d_out.update({"AR_Antibody_Production_Details": ""})  # nop

            elif (s_column == "AR_Antibody_Labeling"):
                try:
                    d_out.update({"AR_Antibody_Labeling": idtrafo(d_record["dye"])})  # trafo secondary
                except KeyError:
                    d_out.update({"AR_Antibody_Labeling": "not_available"}) # nop primary

            elif (s_column == "AR_Antibody_Labeling_Details"):
                d_out.update({"AR_Antibody_Labeling_Details": ""})  # nop

            elif (s_column == "AR_Antibody_Purity"):
                d_out.update({"AR_Antibody_Purity": idtrafo(d_record["purity"])})  # trafo

            # celllline
            elif (s_column == "CL_LINCS_ID"):
                d_out.update({"CL_LINCS_ID": d_record["lincs_identifier"]})  # ok

            elif (s_column == "CL_Name"):
                d_out.update({"CL_Name": d_record["common_name"]})  # ok

            elif (s_column == "CL_Alternative_Name"):
                d_out.update({"CL_Alternative_Name": idtrafo(d_record["sample"])})  # trafo

            elif (s_column == "CL_Alternative_ID"):
                d_out.update({"CL_Alternative_ID": d_record["sample"].split("_")[-1]})  # split

            elif (s_column == "CL_Center_Canonical_ID"):
                ls_canonical = d_record["annot_id"].split("-")
                ls_canonical.pop(-1)
                s_canonical = "-".join(ls_canonical)
                d_out.update({"CL_Center_Canonical_ID": s_canonical})  # split

            elif (s_column == "CL_Relevant_Citations"):
                d_out.update({"CL_Relevant_Citations": d_record["reference_url"]})  # ok

            elif (s_column == "CL_Center_Name"):
                d_out.update({"CL_Center_Name": "MEP_LINCS"})  # ok

            elif (s_column == "CL_Center_Batch_ID"):
                d_out.update({"CL_Center_Batch_ID": d_record["annot_id"]})  # ok

            elif (s_column == "CL_Provider_Name"):
                d_out.update({"CL_Provider_Name": idtrafo(d_record["provider"])})  # trafo

            elif (s_column == "CL_Provider_Catalog_ID"):
                d_out.update({"CL_Provider_Catalog_ID": d_record["provider_catalog_id"]})  # ok

            elif (s_column == "CL_Provider_Batch_ID"):
                d_out.update({"CL_Provider_Batch_ID": d_record["provider_batch_id"]})  # ok

            elif (s_column == "CL_Comments"):
                d_out.update({"CL_Comments": ""})  # nop

            elif (s_column == "CL_Organism"):
                d_out.update({"CL_Organism": idtrafo(d_record["organism"])})  # trafo

            elif (s_column == "CL_Organ"):
                d_out.update({"CL_Organ": idtrafo(d_record["organ"])})  # trafo

            elif (s_column == "CL_Tissue"):
                d_out.update({"CL_Tissue": idtrafo(d_record["tissue"])})  # trafo

            elif (s_column == "CL_Cell_Type"):
                d_out.update({"CL_Cell_Type": idtrafo(d_record["cell_type"])})  # trafo

            elif (s_column == "CL_Cell_Type_Detail"):
                d_out.update({"CL_Cell_Type_Detail": ""})  # nop

            elif (s_column == "CL_Donor_Sex"):
                d_out.update({"CL_Donor_Sex": idtrafo(d_record["sex"])})  # trafo

            elif (s_column == "CL_Donor_Age"):
                d_out.update({"CL_Donor_Age": d_record["age_year"]}) # ok

            elif (s_column == "CL_Donor_Ethnicity"):
                d_out.update({"CL_Donor_Ethnicity": "; ".join(
                    [idtrafo(s_ethnecity) for s_ethnecity  in d_record["ethnicity"]]
                )})  # trafo list

            elif (s_column == "CL_Donor_Health_Status"):
                d_out.update({"CL_Donor_Health_Status": idtrafo(d_record["health_status"])})  # trafo

            elif (s_column == "CL_Disease"):
                d_out.update({"CL_Disease": "; ".join(
                    [idtrafo(s_disease) for s_disease in d_record["disease"]]
                )})  # trafo list

            elif (s_column == "CL_Disease_Detail"):
                d_out.update({"CL_Disease_Detail": ""})  # nop

            elif (s_column == "CL_Known_Mutations"):
                d_out.update({"CL_Known_Mutations": ""})  # nop

            elif (s_column == "CL_Mutation_Citations"):
                d_out.update({"CL_Mutation_Citations": ""})  # nop

            elif (s_column == "CL_Molecular_Features"):
                d_out.update({"CL_Molecular_Features": ""})  # nop

            elif (s_column == "CL_Genetic_Modification"):
                d_out.update({"CL_Genetic_Modification": "; ".join(
                    [idtrafo(s_genemod) for s_genemod in d_record["genetic_modification"]]
                )})  # trafo list

            elif (s_column == "CL_Growth_Properties"):
                d_out.update({"CL_Growth_Properties": "; ".join(
                    [idtrafo(s_growthprop) for s_growthprop in d_record["growth_propertie"]]
                )})  # trafo list

            elif (s_column == "CL_Recommended_Culture_Conditions"):
                d_out.update({"CL_Recommended_Culture_Conditions": d_record["reference_url"]})  # ok

            elif (s_column == "CL_Related_Projects"):
                d_out.update({"CL_Related_Projects": ""})  # nop

            elif (s_column == "CL_Verification_Reference_Profile"):
                d_out.update({"CL_Verification_Reference_Profile": "; ".join(
                    d_record["verification_profile_reference"]
                )})  # ok list

            elif (s_column == "CL_Reference_Source"):
                d_out.update({"CL_Reference_Source": d_record["reference_url"]})  # ok

            elif (s_column == "CL_Cell_Markers"):
                d_out.update({"CL_Cell_Markers": d_record["cell_marker_reference"]})  # ok

            elif (s_column == "CL_Gonosome_Code"):
                d_out.update({"CL_Gonosome_Code": ""})  # nop

            elif (s_column == "CL_Disease_Site_Onset"):
                d_out.update({"CL_Disease_Site_Onset": ""})  # nop

            elif (s_column == "CL_Disease_Age_Onset"):
                d_out.update({"CL_Disease_Age_Onset": ""})  # nop

            elif (s_column == "CL_Donor_Age_Death"):
                d_out.update({"CL_Donor_Age_Death": ""})  # nop

            elif (s_column == "CL_Donor_Disease_Duration"):
                d_out.update({"CL_Donor_Disease_Duration": ""})  # nop

            elif (s_column == "CL_Precursor_Cell_Name"):
                d_out.update({"CL_Precursor_Cell_Name": d_record["sampleparent"].split("-")[0]})  # split

            elif (s_column == "CL_Precursor_Cell_LINCS_ID"):
                d_out.update({"CL_Precursor_Cell_LINCS_ID": ""})  # nop

            elif (s_column == "CL_Precursor_Cell_Center_Batch_ID"):
                d_out.update({"CL_Precursor_Cell_Center_Batch_ID": d_record["sampleparent"]})  # ok

            elif (s_column == "CL_Production_Details"):
                d_out.update({"CL_Production_Details": idtrafo(d_record["sampleentity"])})  # trafo

            elif (s_column == "CL_Quality_Verification"):
                d_out.update({"CL_Quality_Verification": "; ".join(
                    d_record["verification_profile"]
                )})  # ok list

            elif (s_column == "CL_Transient_Modification"):
                d_out.update({"CL_Transient_Modification": "; ".join(
                    [idtrafo(s_transmod) for s_transmod in d_record["transient_modification"]]
                )})  # trafo list

            elif (s_column == "CL_Passage_Number"):
                #d_out.update({"CL_Passage_Number": d_record["passage_use"]})  # ok
                d_out.update({"CL_Passage_Number": ""})  # nop

            elif (s_column == "CL_Source_Information"):
                d_out.update({"CL_Source_Information": ""})  # nop

            elif (s_column == "CL_Date_Received"):
                d_out.update({"CL_Date_Received": ""})  # nop

            elif (s_column == "CL_Center_Specific_Code"):
                d_out.update({
                    "CL_Center_Specific_Code": idtrafo(d_record["annot_id"])
                })  # idtrafo will give cellline annot_id cellline wihtout passage numbers

            # other reagent
            elif (s_column == "OR_LINCS_ID"):
                d_out.update({"OR_LINCS_ID": d_record["lincs_identifier"]})  # ok

            elif (s_column == "OR_Name"):
                d_out.update({"OR_Name": d_record["common_name"]})  # ok

            elif (s_column == "OR_Center_Canonical_ID"):
                ls_canonical = d_record["annot_id"].split("-")
                ls_canonical.pop(-1)
                s_canonical = "-".join(ls_canonical)
                d_out.update({"OR_Center_Canonical_ID": s_canonical})  # split

            elif (s_column == "OR_Center_Name"):
                d_out.update({"OR_Center_Name": "MEP_LINCS"})  # ok

            elif (s_column == "OR_Center_Batch_ID"):
                d_out.update({"OR_Center_Batch_ID": d_record["annot_id"]})  # ok

            elif (s_column == "OR_Provider_Name"):
                d_out.update({"OR_Provider_Name": idtrafo(d_record["provider"])})  # trafo

            elif (s_column == "OR_Provider_Catalog_ID"):
                d_out.update({"OR_Provider_Catalog_ID": d_record["provider_catalog_id"]})  # ok

            elif (s_column == "OR_Provider_Batch_ID"):
                d_out.update({"OR_Provider_Batch_ID": d_record["provider_batch_id"]})  # ok

            elif (s_column == "OR_Comments"):
                d_out.update({"OR_Comments": d_record["reference_url"]})  # ok, cause no relevant citaion field

            # protein and proteinset handles
            elif (s_column == "PR_Relevant_Citations"):
                consistent_update(
                    d_dict = d_out,
                    s_key = "PR_Relevant_Citations",
                    s_value = d_record["reference_url"],
                )  # ok

            elif (s_column == "PR_Center_Name"):
                d_out.update({"PR_Center_Name": "MEP_LINCS"})  # ok

            elif (s_column == "PR_PLN"):
                d_out.update({"PR_PLN": ""})  # nop

            elif (s_column == "PR_Mutations"):
                d_out.update({"PR_Mutations": ""})  # nop

            elif (s_column == "PR_Modifications"):
                d_out.update({"PR_Modifications": ""})  # nop

            elif (s_column == "PR_Protein_Complex_Known_Component_LINCS_IDs"):
                d_out.update({"PR_Protein_Complex_Known_Component_LINCS_IDs": ""})  # nop

            elif (s_column == "PR_Protein_Complex_Details"):
                d_out.update({"PR_Protein_Complex_Details": ""})  # nop

            elif (s_column == "PR_Production_Source_Organism"):
                consistent_update(
                    d_dict = d_out,
                    s_key = "PR_Production_Source_Organism",
                    s_value = idtrafo(d_record["source_organism"]),
                )  # trafo

            elif (s_column == "PR_Production_Method"):
                consistent_update(
                    d_dict = d_out,
                    s_key = "PR_Production_Method",
                    s_value = d_record["source_organism_note"],
                )  # ok

            elif (s_column == "PR_Protein_Purity"):
                consistent_update(
                    d_dict = d_out,
                    s_key = "PR_Protein_Purity",
                    s_value = d_record["purity"],
                )  # ok

            # protein only handles
            elif (s_column == "PR_LINCS_ID") and (d_record["proteinset_annot_id"] == "not_available-notavailable_notavailable_notavailable"):
                d_out.update({"PR_LINCS_ID": d_record["lincs_identifier"]})  # ok

            elif (s_column == "PR_Name") and (d_record["proteinset_annot_id"] == "not_available-notavailable_notavailable_notavailable"):
                d_out.update({"PR_Name": d_record["common_name"]})  # ok

            elif (s_column == "PR_Alternative_Name") and (d_record["proteinset_annot_id"] == "not_available-notavailable_notavailable_notavailable"):
                d_out.update({"PR_Alternative_Name": "not_available"})  # nop

            elif (s_column == "PR_Alternative_ID") and (d_record["proteinset_annot_id"] == "not_available-notavailable_notavailable_notavailable"):
                d_out.update({"PR_Alternative_ID": "not_available"})  # nop

            elif (s_column == "PR_Center_Canonical_ID") and (d_record["proteinset_annot_id"] == "not_available-notavailable_notavailable_notavailable"):
                ls_canonical = d_record["annot_id"].split("-")
                ls_canonical.pop(-1)
                s_canonical = "-".join(ls_canonical)
                d_out.update({"PR_Center_Canonical_ID": s_canonical})  # split

            elif (s_column == "PR_Center_Batch_ID") and (d_record["proteinset_annot_id"] == "not_available-notavailable_notavailable_notavailable"):
                d_out.update({"PR_Center_Batch_ID": d_record["annot_id"]})  # ok

            elif (s_column == "PR_Provider_Name") and (d_record["proteinset_annot_id"] == "not_available-notavailable_notavailable_notavailable"):
                d_out.update({"PR_Provider_Name": idtrafo(d_record["provider"])})  # trafo

            elif (s_column == "PR_Provider_Catalog_ID") and (d_record["proteinset_annot_id"] == "not_available-notavailable_notavailable_notavailable"):
                d_out.update({"PR_Provider_Catalog_ID": d_record["provider_catalog_id"]})  # ok

            elif (s_column == "PR_Provider_Batch_ID") and (d_record["proteinset_annot_id"] == "not_available-notavailable_notavailable_notavailable"):
                d_out.update({"PR_Provider_Batch_ID": d_record["provider_batch_id"]})  # ok

            elif (s_column == "PR_Comments") and (d_record["proteinset_annot_id"] == "not_available-notavailable_notavailable_notavailable"):
                d_out.update({"PR_Comments": "single protein"})  # protein

            elif (s_column == "PR_UniProt_ID") and (d_record["proteinset_annot_id"] == "not_available-notavailable_notavailable_notavailable"):
                d_out.update({"PR_UniProt_ID": d_record["protein"].split("_")[1].replace("|","-")})  # split

            elif (s_column == "PR_Protein_Complex_Known_Component_UniProt_IDs") and (d_record["proteinset_annot_id"] == "not_available-notavailable_notavailable_notavailable"):
                d_out.update({"PR_Protein_Complex_Known_Component_UniProt_IDs": "not_available"})  # nop

            elif (s_column == "PR_Protein_Complex_Known_Component_Center_Protein_IDs") and (d_record["proteinset_annot_id"] == "not_available-notavailable_notavailable_notavailable"):
                d_out.update({"PR_Protein_Complex_Known_Component_Center_Protein_IDs": "not_available"})  # nop

            elif (s_column == "PR_Protein_Complex_Stoichiometry") and (d_record["proteinset_annot_id"] == "not_available-notavailable_notavailable_notavailable"):
                d_out.update({"PR_Protein_Complex_Stoichiometry": "not_available"})  # nop

            elif (s_column == "PR_Amino_Acid_Sequence") and (d_record["proteinset_annot_id"] == "not_available-notavailable_notavailable_notavailable"):
                d_out.update({"PR_Amino_Acid_Sequence": d_record["sequence_real"]})  # ok

            # proteinset only handles
            elif (s_column == "PR_LINCS_ID") and (d_record["proteinset_annot_id"] != "not_available-notavailable_notavailable_notavailable"):
                d_out.update({
                    "PR_LINCS_ID": dd_proteinset[d_record["proteinset_annot_id"]]["lincs_identifier"]
                })  # ok

            elif (s_column == "PR_Name") and (d_record["proteinset_annot_id"] != "not_available-notavailable_notavailable_notavailable"):
                d_out.update({
                     "PR_Name": dd_proteinset[d_record["proteinset_annot_id"]]["common_name"]
                })  # ok

            elif (s_column == "PR_Alternative_Name") and (d_record["proteinset_annot_id"] != "not_available-notavailable_notavailable_notavailable"):
                d_out.update({
                    "PR_Alternative_Name": idtrafo(
                        dd_proteinset[d_record["proteinset_annot_id"]]["proteinset"]
                    )
                })  # trafo

            elif (s_column == "PR_Alternative_ID") and (d_record["proteinset_annot_id"] != "not_available-notavailable_notavailable_notavailable"):
                d_out.update({
                    "PR_Alternative_ID" : dd_proteinset[d_record["proteinset_annot_id"]]["proteinset"].split("_")[-1]
                })  # split

            elif (s_column == "PR_Center_Canonical_ID") and (d_record["proteinset_annot_id"] != "not_available-notavailable_notavailable_notavailable"):
                ls_canonical = dd_proteinset[d_record["proteinset_annot_id"]]["annot_id"].split("-")
                ls_canonical.pop(-1)
                s_canonical = "-".join(ls_canonical)
                d_out.update({
                    "PR_Center_Canonical_ID": s_canonical
                })  # split

            elif (s_column == "PR_Center_Batch_ID") and (d_record["proteinset_annot_id"] != "not_available-notavailable_notavailable_notavailable"):
                d_out.update({
                    "PR_Center_Batch_ID": dd_proteinset[d_record["proteinset_annot_id"]]["annot_id"]
                })  # ok

            elif (s_column == "PR_Provider_Name") and (d_record["proteinset_annot_id"] != "not_available-notavailable_notavailable_notavailable"):
                d_out.update({
                    "PR_Provider_Name": idtrafo(
                        dd_proteinset[d_record["proteinset_annot_id"]]["provider"]
                    )
                 })  # trafo

            elif (s_column == "PR_Provider_Catalog_ID") and (d_record["proteinset_annot_id"] != "not_available-notavailable_notavailable_notavailable"):
                d_out.update({
                    "PR_Provider_Catalog_ID": dd_proteinset[d_record["proteinset_annot_id"]]["provider_catalog_id"]
                })  # ok

            elif (s_column == "PR_Provider_Batch_ID") and (d_record["proteinset_annot_id"] != "not_available-notavailable_notavailable_notavailable"):
                d_out.update({
                    "PR_Provider_Batch_ID": dd_proteinset[d_record["proteinset_annot_id"]]["provider_batch_id"]
                })  # ok

            elif (s_column == "PR_Comments") and (d_record["proteinset_annot_id"] != "not_available-notavailable_notavailable_notavailable"):
                d_out.update({"PR_Comments": "protein complex"})  # proteinset

            elif (s_column == "PR_UniProt_ID") and (d_record["proteinset_annot_id"] != "not_available-notavailable_notavailable_notavailable"):
                d_out.update({"PR_UniProt_ID": "not_available"}) # nop

            elif (s_column == "PR_Protein_Complex_Known_Component_UniProt_IDs") and (d_record["proteinset_annot_id"] != "not_available-notavailable_notavailable_notavailable"):
                try:
                    s_value = d_out["PR_Protein_Complex_Known_Component_UniProt_IDs"]
                    s_value += "; {}".format(
                        d_record["protein"].split("_")[-1].replace("|",".")
                    )
                except KeyError:
                    s_value = d_record["protein"].split("_")[-1].replace("|",".")
                d_out.update({"PR_Protein_Complex_Known_Component_UniProt_IDs": s_value})  # append

            elif (s_column == "PR_Protein_Complex_Known_Component_Center_Protein_IDs") and (d_record["proteinset_annot_id"] != "not_available-notavailable_notavailable_notavailable"):
                try:
                    s_value = d_out["PR_Protein_Complex_Known_Component_Center_Protein_IDs"]
                    s_value += "; {}".format(
                        d_record["protein"]
                    )
                except KeyError:
                    s_value = d_record["protein"]
                d_out.update({"PR_Protein_Complex_Known_Component_Center_Protein_IDs": s_value})  # append

            elif (s_column == "PR_Protein_Complex_Stoichiometry") and (d_record["proteinset_annot_id"] != "not_available-notavailable_notavailable_notavailable"):
                try:
                    s_value = d_out["PR_Protein_Complex_Stoichiometry"]
                    s_value += "; {}".format(
                        str(d_record["proteinset_ratio"])
                    )
                except KeyError:
                    s_value = str(d_record["proteinset_ratio"])
                d_out.update({"PR_Protein_Complex_Stoichiometry": s_value})  # append

            elif (s_column == "PR_Amino_Acid_Sequence") and (d_record["proteinset_annot_id"] != "not_available-notavailable_notavailable_notavailable"):
                try:
                    s_value = d_out["PR_Amino_Acid_Sequence"]
                    s_value += "; {}".format(
                        d_record["sequence_real"]
                    )
                except KeyError:
                    s_value = d_record["sequence_real"]
                d_out.update({"PR_Amino_Acid_Sequence": s_value})  # append

            # small molecule
            elif (s_column == "SM_LINCS_ID"):
                d_out.update({"SM_LINCS_ID": d_record["lincs_identifier"]})  # ok

            elif (s_column == "SM_Name"):
                d_out.update({"SM_Name":  d_record["common_name"]})  # ok

            elif (s_column == "SM_Alternative_Name"):
                d_out.update({"SM_Alternative_Name": d_record["annot_id"].split("-")[0].split("_")[0]})  # split

            elif (s_column == "SM_Alternative_ID"):
                # pubmed_sid and own
                if not (re.search("_chebi", d_record["annot_id"].lower()) or re.search("_pubmedcid", d_record["annot_id"].lower())):
                    try:
                        s_identifier = d_record["compound"].split("_")[-1]
                    except KeyError:
                        s_identifier = d_record["cstain"].split("_")[-1]
                    d_out.update({"SM_Alternative_ID": s_identifier})  # split
                else:
                    d_out.update({"SM_Alternative_ID": "not_available"})  # nop

            elif (s_column == "SM_Center_Canonical_ID"):
                ls_canonical = d_record["annot_id"].split("-")
                ls_canonical.pop(-1)
                s_canonical = "-".join(ls_canonical)
                d_out.update({"SM_Center_Canonical_ID": s_canonical})  # split

            elif (s_column == "SM_Relevant_Citations"):
                d_out.update({"SM_Relevant_Citations": d_record["reference_url"]})  # ok

            elif (s_column == "SM_Center_Name"):
                d_out.update({"SM_Center_Name": "MEP_LINCS"})  # ok

            elif (s_column == "SM_Center_Batch_ID"):
                d_out.update({"SM_Center_Batch_ID": d_record["annot_id"]})  # ok

            elif (s_column == "SM_Provider_Name"):
                d_out.update({"SM_Provider_Name": idtrafo(d_record["provider"])})  # trafo

            elif (s_column == "SM_Provider_Catalog_ID"):
                d_out.update({"SM_Provider_Catalog_ID": d_record["provider_catalog_id"]})  # ok

            elif (s_column == "SM_Provider_Batch_ID"):
                d_out.update({"SM_Provider_Batch_ID": d_record["provider_batch_id"]})  # ok

            elif (s_column == "SM_Comments"):
                d_out.update({"SM_Comments": ""})  # nop

            elif (s_column == "SM_PubChem_CID"):
                # pubmed_cid
                if re.search("_pubchemcid", d_record["annot_id"].lower()):
                    try:
                        s_identifier = d_record["compound"].split("_")[-1]
                    except KeyError:
                        s_identifier = d_record["cstain"].split("_")[-1]
                    d_out.update({"SM_PubChem_CID": s_identifier})  # split
                else:
                    d_out.update({"SM_PubChem_CID": "not_available"})  # nop

            elif (s_column == "SM_ChEBI_ID"):
                # chebi
                if re.search("_chebi", d_record["annot_id"].lower()):
                    try:
                        s_identifier = d_record["compound"].split("_")[-1]
                    except KeyError:
                        s_identifier = d_record["cstain"].split("_")[-1]
                    d_out.update({"SM_ChEBI_ID": s_identifier})  # split
                else:
                    d_out.update({"SM_ChEBI_ID": "not_available"})  # nop

            elif (s_column == "SM_InChI_Parent"):
                d_out.update({"SM_InChI_Parent": ""})  # nop

            elif (s_column == "SM_InChI_Key_Parent"):
                d_out.update({"SM_InChI_Key_Parent": ""})  # nop

            elif (s_column == "SM_SMILES_Parent"):
                d_out.update({"SM_SMILES_Parent": ""})  # nop

            elif (s_column == "SM_Salt"):
                d_out.update({"SM_Salt": ""})  # nop

            elif (s_column == "SM_SMILES_Batch"):
                d_out.update({"SM_SMILES_Batch": ""})  # nop

            elif (s_column == "SM_InChI_Batch"):
                d_out.update({"SM_InChI_Batch": ""})  # nop

            elif (s_column == "SM_InChI_Key_Batch"):
                d_out.update({"SM_InChI_Key_Batch": ""})  # nop

            elif (s_column == "SM_Molecular_Mass"):
                d_out.update({"SM_Molecular_Mass": ""})  # nop

            elif (s_column == "SM_Purity"):
                d_out.update({"SM_Purity": d_record["purity"]})  # ok

            elif (s_column == "SM_Purity_Method"):
                d_out.update({"SM_Purity_Method": ""})  # nop

            elif (s_column == "SM_Aqueous_Solubility"):
                d_out.update({"SM_Aqueous_Solubility": ""})  # nop

            elif (s_column == "SM_LogP"):
                d_out.update({"SM_LogP": ""})  # nop

            # error
            else:
                raise ValueError(
                    "Error @ appsabrick.structure.objjson2lincs : can not handle unknowen lincs standard column {}.".format(
                        s_column
                    )
                )

        # update output
        dd_out.update({s_id : d_out})

    # append to output file
    for _, d_out in dd_out.items():
        s_out = None
        for s_column in ls_column:
            #try:
            o_value = d_out[s_column]
            if (type(o_value) == str) and (o_value.find(",") > -1):
                o_value = '"{}"'.format(o_value)
            if (s_out == None):
                s_out = o_value
            else:
                s_out = "{},{}".format(s_out, o_value)
            #except KeyError:
            #    pass
        with open(s_filename, 'a') as f:
            f.write(s_out + "\n")
