from django.db import models

# import from python
import datetime

# constants
# record brick
# brick_module : brick_uberclass : brick_class == brick_type == label
tts_RECORDBRICK = (
    ("nop", "nop", "NotYetSpecified", "not_yet_specified", "not yet specified"),
    ("nop", "nop", "NotAvailable", "not_available", "not available"),
    ("appbrreagentantibody", "Endpoint", "Antibody1Brick", "antibody1", "Primary Antibody"),
    ("appbrreagentantibody", "Endpoint", "Antibody2Brick", "antibody2", "Secondary Antibody"),
    ("appbrreagentcompound", "Endpoint", "CstainBrick", "cstain", "Compound Stain"),
    ("appbrreagentcompound", "Perturbation", "CompoundBrick", "compound", "Compound"),
    ("appbrreagentprotein", "Perturbation", "ProteinBrick", "protein", "Protein"),
    ("appbrreagentprotein", "Perturbation", "ProteinSetBrick", "proteinset", "Proteinset"),
    ("appbrsamplehuman", "Sample", "HumanBrick", "human", "Human Homo sapiens"),
    ("appbrxperson", "Work", "PersonBrick", "person", "Person"),
)

# generate choice fields
lts_ENDPOINTTYPE = []
for ts_record in tts_RECORDBRICK:
    if (ts_record[1] == "Endpoint") or (ts_record[0] == "nop"):
        ts_endpointtype = (ts_record[-2], ts_record[-1])
        lts_ENDPOINTTYPE.append(ts_endpointtype)

lts_PERTURBATIONTYPE = []
for ts_record in tts_RECORDBRICK:
    if (ts_record[1] == "Perturbation") or (ts_record[0] == "nop"):
        ts_perturbationtype = (ts_record[-2], ts_record[-1])
        lts_PERTURBATIONTYPE.append(ts_perturbationtype)

lts_SAMPLETYPE = []
for ts_record in tts_RECORDBRICK:
    if (ts_record[1] == "Sample") or (ts_record[0] == "nop"):
        ts_sampletype = (ts_record[-2], ts_record[-1])
        lts_SAMPLETYPE.append(ts_sampletype)


# Create your models here.
# reagent brick entities
class EndpointBricked(models.Model):
    bricked_id = models.SlugField(
        max_length=256,
        primary_key=True,
        help_text="Unique bricked identifier. Automatically generated."
    )
    brick_type = models.CharField(
        max_length=32,
        choices=lts_ENDPOINTTYPE,
        default="not_yet_specified",
        help_text="Annot endpoint class. Run brick_load for library update."
    )
    annot_id = models.SlugField(
        max_length=256,
        default="not_yet_specified",
        help_text="Detailed endpoint reagent name. Run brick_load for library update."
    )
    available = models.NullBooleanField(
        default=None,
        help_text="Indicates if endpoint reagent is at stock in our lab. Indicator automatically updated every brick_load."
    )
    ok_brick = models.NullBooleanField(
        default=None,
        help_text="Indicates if endpoint reagent at the brick layer exist. Indicator automatically updated every brick_load."
    )

    # primary key generator
    def save(self, *args, **kwargs):
        self.bricked_id  = "{}-{}".format(self.brick_type, self.annot_id)
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.bricked_id)

    __repr__ = __str__

    class Meta:
        unique_together = (("brick_type", "annot_id"),)
        ordering = ["bricked_id"]
        verbose_name_plural = "uploaded endpoint reagent bricks"


class PerturbationBricked(models.Model):
    bricked_id = models.SlugField(
        max_length=256,
        primary_key=True,
        help_text="Unique bricked identifier. Automatically generated."
    )
    brick_type = models.CharField(
        max_length=32,
        choices=lts_PERTURBATIONTYPE,
        default="not_yet_specified",
        help_text="Annot perturbation class. Run brick_load for library update."
    )
    annot_id = models.SlugField(
        max_length=256,
        default="not_yet_specified",
        help_text="Detailed perturbation reagent name. Run brick_load for library update."
    )
    available = models.NullBooleanField(
        default=None,
        help_text="Indicates if perturbation reagent is at stock in our lab. Indicator automatically updated every brick_load."
    )
    ok_brick = models.NullBooleanField(
        default=None,
        help_text="Indicates if perturbation reagent at the brick layer exist. Indicator automatically updated every brick_load."
    )

    # primary key generator
    def save(self, *args, **kwargs):
        self.bricked_id  = "{}-{}".format(self.brick_type, self.annot_id)
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.bricked_id)

    __repr__ = __str__

    class Meta:
        unique_together = (("brick_type", "annot_id"),)
        ordering = ["bricked_id"]
        verbose_name_plural = "uploaded perturbation reagent bricks"


class SampleBricked(models.Model):
    bricked_id = models.SlugField(
        max_length=256,
        primary_key=True,
        help_text="Unique bricked identifier. Automatically generated."
    )
    brick_type = models.SlugField(
        max_length=32,
        choices=lts_SAMPLETYPE,
        default="not_yet_specified",
        help_text="Annot reagent class. Run samplebrick_load for library update."
    )
    annot_id = models.SlugField(
        max_length=256,
        default="not_yet_specified",
        help_text="Detailed sample name. Run brick_load for library update."
    )
    available = models.NullBooleanField(
        default=None,
        help_text="Indicates if reagent is at stock in our lab. Indicator automatically updated every brick_load."
    )
    ok_brick = models.NullBooleanField(
        default=None,
        help_text="Indicates if reagent at the brick layer exist. Indicator automatically updated every brick_load."
    )

    # primary key generator
    def save(self, *args, **kwargs):
        self.bricked_id  = "{}-{}".format(self.brick_type, self.annot_id)
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.bricked_id)

    __repr__ = __str__

    class Meta:
        unique_together = (("brick_type", "annot_id"),)
        ordering = ["bricked_id"]
        verbose_name_plural = "uploaded sample bricks"


# sys admin brick table
class SysAdminBrick(models.Model):
    brick_app = models.SlugField(
        default="nop"
    )
    brick_uberclass =  models.SlugField(
        default="nop"
    )
    brick_class = models.SlugField(
        unique=True
    )
    brick_type = models.SlugField(
        primary_key=True
    )
    item_count = models.PositiveSmallIntegerField(
        default=None,
        null=True,
        blank=True
    )
    last_brick_load = models.DateTimeField(
        default=datetime.datetime(year=1955, month=11, day=5)
    )

    def __str__(self):
        return(self.brick_type)

    class Meta:
        verbose_name_plural = "sys admin bricks"
        ordering = ["brick_uberclass","brick_app","brick_type"]
