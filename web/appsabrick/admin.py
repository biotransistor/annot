# import from django
from django.contrib import admin
from django.contrib import messages
from django.core import management
from django.http import HttpResponseRedirect
#from django.http import HttpResponse

# python
import io
import os
import re

# import from annot
from appsabrick.models import EndpointBricked, PerturbationBricked, SampleBricked, SysAdminBrick

# Register your models here.
#admin.site.register(EndpointBricked)
class EndpointBrickedAdmin(admin.ModelAdmin):
    search_fields = (
        "bricked_id",
        "brick_type",
        "annot_id"
    )
    list_display = (
        "bricked_id",
        "brick_type",
        "annot_id",
        "available",
        "ok_brick"
    )
    readonly_fields = (
        "bricked_id",
        "brick_type",
        "annot_id",
        "available",
        "ok_brick"
    )
    actions = [
        "delete_selected",
        "download_pipeelement_json",
        "download_pipeelement_tsv"
    ]

    ### json ###
    # action download bricked element json version
    def download_pipeelement_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=generic&choice=appsabrick.endpointbricked"
        ))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    ### tsv ###
    # action download bricked element tsv version
    def download_pipeelement_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=generic&choice=appsabrick.endpointbricked"
        ))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

admin.site.register(EndpointBricked, EndpointBrickedAdmin)


#admin.site.register(PerturbationBricked)
class PerturbationBrickedAdmin(admin.ModelAdmin):
    search_fields = (
        "bricked_id",
        "brick_type",
        "annot_id"
    )
    list_display = (
        "bricked_id",
        "brick_type",
        "annot_id",
        "available",
        "ok_brick"
    )
    readonly_fields = (
        "bricked_id",
        "brick_type",
        "annot_id",
        "available",
        "ok_brick"
    )
    actions = [
        "delete_selected",
        "download_pipeelement_json",
        "download_pipeelement_tsv"
    ]

    ### json ###
    # action download bricked element json version
    def download_pipeelement_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=generic&choice=appsabrick.perturbationbricked"
        ))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    ### tsv ###
    # action download bricked element tsv version
    def download_pipeelement_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=generic&choice=appsabrick.perturbationbricked"
        ))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

admin.site.register(PerturbationBricked, PerturbationBrickedAdmin)


#admin.site.register(SampleBricked)
class SampleBrickedAdmin(admin.ModelAdmin):
    search_fields = (
        "bricked_id",
        "brick_type",
        "annot_id"
    )
    list_display = (
        "bricked_id",
        "brick_type",
        "annot_id",
        "available",
        "ok_brick"
    )
    readonly_fields = (
        "bricked_id",
        "brick_type",
        "annot_id",
        "available",
        "ok_brick"
    )
    actions = [
        "delete_selected",
        "download_pipeelement_json",
        "download_pipeelement_tsv"
    ]

    ### json ###
    # action download bricked element json version
    def download_pipeelement_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=generic&choice=appsabrick.samplebricked"
        ))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    ### tsv ###
    # action download bricked element tsv version
    def download_pipeelement_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=generic&choice=appsabrick.samplebricked"
        ))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

admin.site.register(SampleBricked, SampleBrickedAdmin)


#admin.site.register(SysAdminBrick)
class SysAdminBrickAdmin(admin.ModelAdmin):
    # view
    search_fields = (
        "brick_app",
        "brick_uberclass",
        "brick_class",
        "brick_type"
    )
    list_display = (
        "brick_app",
        "brick_uberclass",
        "brick_class",
        "brick_type",
        "item_count",
        "last_brick_load"
    )
    list_display_links = (
        "brick_class",
    )
    readonly_fields = (
        "brick_app",
        "brick_uberclass",
        "brick_class",
        "brick_type",
        "item_count",
        "last_brick_load"
    )
    actions = [
        "download_pipeelement_json",
        "download_brick_json",
        "download_pipeelement_tsv",
        "download_brick_tsv",
        "cron_start",
        "cron_stop",
        "cron_status",
        "brick_load",
    ]

    ### json ###
    # action download pipe element json version
    def download_pipeelement_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=generic&choice=appsabrick.sysadminbrick"
        ))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    # action download brick json version
    def download_brick_json(self, request, queryset):
        ls_brick = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=brick&choice={}".format(
                "!".join(ls_brick)
            )
        ))
    download_brick_json.short_description = "Download brick as json file"

    ### tsv ###
    # action download pipe element tsv version
    def download_pipeelement_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=generic&choice=appsabrick.sysadminbrick"
        ))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

    # action download brick tsv version
    def download_brick_tsv(self, request, queryset):
        ls_brick = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=brick&choice={}".format(
                "!".join(ls_brick)
            )
        ))
    download_brick_tsv.short_description = "Download brick as tsv file"

    ### cron ###
    def cron_start(self, request, queryset):  # self = modeladmin
        s_out = os.popen("/etc/init.d/cron start").read().strip()
        self.message_user(
            request,
            s_out,
            level=messages.INFO
        )
    cron_start.short_description = "/etc/init.d/cron start (item selection irrelevant)"

    def cron_stop(self, request, queryset):  # self = modeladmin
        s_out = os.popen("/etc/init.d/cron stop").read().strip()
        self.message_user(
            request,
            s_out,
            level=messages.ERROR
        )
    cron_stop.short_description = "/etc/init.d/cron stop (item selection irrelevant)"

    def cron_status(self, request, queryset):  # self = modeladmin
        s_out = os.popen("/etc/init.d/cron status").read().strip()
        if s_out.endswith("running."):
            self.message_user(
                request,
                s_out,
                level=messages.SUCCESS
            )
        elif s_out.endswith("failed!"):
            self.message_user(
                request,
                s_out,
                level=messages.ERROR
            )
        else:
            self.message_user(
                request,
                s_out,
                level= messages.WARNING  # messages.INFO
            )
    cron_status.short_description = "/etc/initd/cron status (item selection irrelevant)"

    ### bricks ###
    def brick_load(self, request, queryset):  # self = modeladmin
        # python manage.py brick_pull s_brick_type
        for obj_n in queryset:
            o_out = io.StringIO()
            s_brick_type = str(obj_n.brick_type)
            management.call_command(
                "brick_load",
                s_brick_type,
                stdout=o_out,
                verbosity=0
            )
            s_out = o_out.getvalue()
            s_out = s_out.strip()
            if (s_out == ""):
                self.message_user(
                    request,
                    "{} # successfully bricked.".format(s_brick_type),
                    level=messages.SUCCESS
                )
            elif re.match(".*Error.*", s_out):
                self.message_user(
                    request,
                    "{} # {}".format(s_brick_type, s_out),
                    level=messages.ERROR
                )
            elif re.match(".*Warning.*", s_out):
                self.message_user(
                    request,
                    "{} # {}".format(s_brick_type,s_out),
                    level=messages.WARNING
                )
            else:
                self.message_user(
                    request,
                    "{} # {}".format(s_brick_type,s_out),
                    level=messages.INFO
                )
    brick_load.short_description = "Upload brick"

# register
admin.site.register(SysAdminBrick, SysAdminBrickAdmin)
