# bue 20150528: data structure for every brick
# import from django
from django.core.management.base import CommandError

# import from python
import csv
import glob
import json
import os
import re

# import from annot
from appsabrick.models import tts_RECORDBRICK, SysAdminBrick
from prjannot.settings import BASE_DIR
from prjannot.structure import retype

# const


# manipulations in brick model used
def scomma2sbr(s_text):
    """
    description:
        transforms a text string with commas into a text string with carriage return.
    """
    s_text = s_text.replace(",","\n")
    return(s_text)


def sbr2list(s_text):
    """
    description:
        transforms a text string with carriage return into a list.
    """
    lo_text = []
    s_text = re.sub("[\f\v]","", s_text) # eliminate: form feed, vertical tab
    s_text = re.sub("[\t\r\n]",",", s_text) # to comma: tab, carriage return, new line
    ls_split = s_text.split(",")  # string to list
    for s_split in ls_split:  # whitespace clean
        s_split = s_split.strip()
        if (len(s_split) > 0):
            o_split = retype(s_split)
            lo_text.append(o_split)
    return(lo_text)


def list2sbr(l_text):
    """
    description:
        transforms list into a text string with carriage return.
    """
    s_text = str(l_text)
    s_text = re.sub("\[\s*", "", s_text) # no square bracket
    s_text = re.sub("\s*]", "", s_text) # no square bracket
    s_text = re.sub("'", "", s_text) # no quotes
    s_text = re.sub(",\s*", "\n", s_text) # comma to new line
    s_text = s_text.strip()
    return(s_text)


# checks in brick model used
def checkfraction(r_number):
    """
    description:
        check if is fraction value is between 1 and 0, not bigger then 100 per cent, not smaller then 0 per cent.
    """
    if (r_number is not None):
        r_number = float(r_number)
        if (r_number > 1) or (r_number < 0):
            raise ValueError(
                "entered fraction value {} is not within range 1 to 0.".format(
                    r_number
                )
            )
    return(r_number)


def checkvoci(o_term, o_vocabulary, b_verbose=True):
    """
    description:
        check if term is controlled vocabulary.
    """
    try:
        o_exist = o_vocabulary.objects.get(annot_id=o_term)
        if (b_verbose):
            print(
                "Found ctrl voci term: {}.".format(o_term)
            )
    except o_vocabulary.DoesNotExist:
        raise o_vocabulary.DoesNotExist(
            "{} is not a controlled vocabulary term from the {} ontology.".format(
                o_term,
                o_vocabulary
            )
        )
    return(o_exist)

# functions
def makeannotid(s_base, s_provider, s_provider_catalog_id, s_provider_batch_id):
    """
    description:
        generate annot_id.
    """
    s_annot_id = "{}-{}_{}_{}".format(
        s_base,
        re.sub(r'[^a-zA-Z0-9]', '', s_provider),
        re.sub(r'[^a-zA-Z0-9]', '', s_provider_catalog_id),
        re.sub(r'[^a-zA-Z0-9]', '', s_provider_batch_id)
    )
    return(s_annot_id)


# obj json file json handles
def json2objjson(s_filename, es_filter=None, b_verbose=True):
    """
    description:
        read json file into json obj.
    """
    dd_json = {}
    # load data
    if (b_verbose):
        print(
            "get json oo file into json obj: {}".format(s_filename)
        )
    with open(s_filename, 'r') as f:
        dd_input = json.load(f)
    # filter
    if (es_filter == None):
        dd_json = dd_input
    else:
        for s_key in list(dd_input.keys()):
            if (s_key in es_filter):
                dd_json.update({s_key: dd_input[s_key]})
    return(dd_json)


def objjson2json(s_filename, dd_json, i_indent=None, b_verbose=True):
    """
    description:
        write json file out of json obj.
    """
    if (b_verbose):
        print("put json obj into json file oo: {}".format(s_filename))
    with open(s_filename, 'w') as f:
        if (type(i_indent) != int):
            json.dump(dd_json, f, sort_keys=True)
        else:
            json.dump(dd_json, f, sort_keys=True, indent=i_indent)


# obj json file tsv oo (human readable) handles
def tsvoo2objjson(s_filename, ls_column, b_verbose=True):
    """
    description:
        transforms tsv into json obj
    note:
        file can have commented line which have to start with a hash symbol (#)
        as first character.
        first non comment line have to be the header line.
        header line have to stick to the labels of ls_column
        files can have additional columns which will not be read by annot.
    """
    # set variables
    dd_json = {}

    with open(s_filename, newline="") as f:
        if (b_verbose):
            print(
                "get tsv oo file into json obj: {}".format(s_filename)
            )
        reader = csv.reader(f, delimiter="\t")  # quotechar=csv.QUOTE_NONE
        di_column = {}
        for ls_row in reader:
            # jump over commented out lines
            o_found = re.match(r"^#", ls_row[0])
            if (o_found is None):
                # get header info
                if (len(di_column) == 0):
                    for s_column in ls_column:
                        try:
                            i_column = ls_row.index(s_column)
                        except ValueError:
                            raise ValueError(
                                "Annot input file {} header {} is missing a column labeled {}. Import reject.".format(
                                    s_filename,
                                    ls_row,
                                    s_column
                                )
                            )
                        di_column.update({s_column: i_column})
                else:
                    # get row into json obj
                    d_entry = {}
                    for s_column in ls_column:
                        i_column = di_column[s_column]
                        s_value = ls_row[i_column].strip()
                        # array
                        if re.match("^\[.*]$", s_value):
                            o_value = []
                            s_value = re.sub("\[\s*", "", s_value)
                            s_value = re.sub("\s*]", "", s_value)
                            ls_value = s_value.split(",")
                            ls_lue = []
                            for o_lue in ls_value:
                                o_lue = retype(o_value=o_lue)
                                # string
                                if (type(o_lue) is str):
                                    if (len(o_lue) > 0):  # kick out empty str
                                        o_value.append(o_lue)
                                # not a string
                                else:
                                    o_value.append(o_lue)
                        # not an array
                        else:
                            o_value = retype(o_value=s_value)
                        # add each column to entry obj
                        d_entry.update({s_column: o_value})
                    # add entry obj to json  object
                    dd_json.update({d_entry["annot_id"]: d_entry})
    # output
    return(dd_json)


def objjson2tsvoo(s_filename, dd_json, ls_column, s_delimiter="\t", b_verbose=True):
    """
    description:
        transforms json into tsv obj
    """
    if (b_verbose):
        print("put json obj into tsv file oo:".format(s_filename))
    # empty output
    s_out = ""
    b_header = True
    # json obj to tsv oo row
    ls_key = list(dd_json.keys())
    ls_key.sort()
    for s_key in ls_key:
        # header
        if (b_header):
            s_out = "{}{}\n".format(s_out, s_delimiter.join(ls_column))
            b_header = False
        # data row
        d_json = dd_json[s_key]
        s_row = None
        for s_column in ls_column:
            s_value = str(d_json[s_column])
            s_value = s_value.replace("'", "")
            if (s_row is None):
                s_row = s_value
            else:
                s_row = "{}{}{}".format(s_row, s_delimiter, s_value)
        s_out = "{}{}\n".format(s_out, s_row)
    # write file
    with open(s_filename, 'w') as f:
        f.write(s_out)


# brick querryset check
def argsbrick(o_args):
    """
    description:
        check bricks in querryset args
    """
    # set output variables
    lo_queryset = []

    # get appbrick file system
    os.chdir(BASE_DIR)
    ls_appbr_fs = glob.glob("appbr*")
    es_appbr_fs = set(ls_appbr_fs)

    # get record dictionary brtype:appbrick
    ds_recordbr = {}
    for ts_record in tts_RECORDBRICK:
        s_appbr = ts_record[0]
        s_uberclassbr = ts_record[1]
        s_classbr = ts_record[2]
        s_typebr = ts_record[3]
        if (s_uberclassbr != "nop"):
            ds_recordbr.update(
                {s_typebr: (s_appbr, s_uberclassbr, s_classbr, s_typebr)}
            )

    # get appbrick obj
    es_appbr_obj = set()
    queryset = SysAdminBrick.objects.all()  # get queryset
    for o_appbr in queryset:
        es_appbr_obj.add(str(o_appbr.brick_app))

    # get loop list
    es_loop = set()
    # treat argument specified brick types
    if (len(o_args) > 0):
        for obj_n in o_args:
            # populate loop set
            if (type(obj_n) == str):
                es_loop.add(obj_n)
            else:
                es_loop.app(str(obj_n.brick_app))
    # treat complete brick types set
    else:
        es_loop = set(ds_recordbr.keys())

    # do the loop
    for s_loop in es_loop:
            # check against record
            try:
                s_appbr = ds_recordbr[s_loop][0]
                s_uberclassbr = ds_recordbr[s_loop][1]
                s_classbr = ds_recordbr[s_loop][2]
                s_typebr = ds_recordbr[s_loop][3]
            except KeyError:
                raise CommandError(
                    "Brick type {} not found in appsabrick.models tts_RECORDBRICK. Add record entry!".format(
                        s_loop
                    )
                )

            # check against file system
            if not(s_appbr in es_appbr_fs):
                raise CommandError(
                    "Brick module {} defined in appsabrick.models tts_RECORDBRICK through type {}, but module not found on file system.".format(
                        s_appbr,
                        s_typebr
                    )
                )

            # check against table
            try:
                SysAdminBrick.objects.get(
                    brick_app=s_appbr,
                    brick_type=s_typebr
                )
            # add if necessary, if appsabrick.models.DoesNotExist
            except :
                obj_i = SysAdminBrick(
                    brick_app=s_appbr,
                    brick_uberclass=s_uberclassbr,
                    brick_class=s_classbr,
                    brick_type=s_typebr
                )
                obj_i.save()

            # populate querysetlist
            obj_i = SysAdminBrick.objects.get(brick_type=s_typebr)
            lo_queryset.append(obj_i)

    # output
    return(lo_queryset)
