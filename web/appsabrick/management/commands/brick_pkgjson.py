# import from django
from django.core.management.base import BaseCommand  #,CommandError

# import from python
from datetime import datetime
import glob
import os
import shutil

# import from annot
from appsabrick.structure import argsbrick
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest

### main ###
class Command(BaseCommand):
    help = "Pack latest json bricks into YYYYMMDD_json_latestbrick folder."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument(
            "appbrick",
            nargs='*',
            type=str,
            help="Annot brick django apps <appbrick appbrick ...>"
        )

    def handle(self, *args, **options):
        ### generate latest out folder path ###
        s_outputpath = "{}brick/{}_json_latestbrick/".format(
            MEDIA_ROOT,
            datetime.now().strftime("%Y%m%d")
        )

        ### process each record  ###
        for o_queryset in argsbrick(o_args=options["appbrick"]):
            # extract form record
            s_brick_app = o_queryset.brick_app
            s_brick_uberclass = o_queryset.brick_uberclass
            s_brick_class = o_queryset.brick_class
            s_brick_type = o_queryset.brick_type
            # processing
            self.stdout.write(
                "\nProcessing brick pkg json for machine of type: {}".format(
                    s_brick_type
                )
            )

            ### get latest fs json ###
            s_latestregex = "{}brick/oo/{}_brick_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]_oo.json".format(
                MEDIA_ROOT,
                s_brick_type
            )
            ls_annotfile = glob.glob(s_latestregex)
            s_latestpath = annotfilelatest(ls_annotfile=ls_annotfile)
            if not(s_latestpath is None):
                ## make output path ##
                self.stdout.write(
                    "Copy {} to {}.".format(s_latestpath, s_outputpath)
                )
                if not(os.path.exists(s_outputpath)):
                    os.mkdir(s_outputpath)
                ## copy in to output path ##
                shutil.copy(s_latestpath, s_outputpath, follow_symlinks=False)
