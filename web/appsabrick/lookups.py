# import form django selectable prj
from selectable.base import LookupBase, ModelLookup
from selectable.registry import registry

# import from annot
from appsabrick.models import EndpointBricked, PerturbationBricked, SampleBricked

# code
class EndpointBrickedLookup(ModelLookup):
        model = EndpointBricked
        search_fields = ('bricked_id__icontains',)
registry.register(EndpointBrickedLookup)

class PerturbationBrickedLookup(ModelLookup):
        model = PerturbationBricked
        search_fields = ('bricked_id__icontains',)
registry.register(PerturbationBrickedLookup)

class SampleBrickedLookup(ModelLookup):
        model = SampleBricked
        search_fields = ('bricked_id__icontains',)
registry.register(SampleBrickedLookup)
