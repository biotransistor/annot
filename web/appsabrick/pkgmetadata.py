# import from django
from django.core.management.base import CommandError

# import from python
import copy
import json
import os

# import from acpipe
import acpipe_acjson.acjson as ac

# import from annot
from appbrreagentantibody.models import antibody1brick_db2objjson, antibody2brick_db2objjson, ls_column_antibody1brick, ls_column_antibody2brick
from appbrreagentcompound.models import compoundbrick_db2objjson, cstainbrick_db2objjson, ls_column_compoundbrick, ls_column_cstainbrick
from appbrreagentprotein.models import ProteinBrick, proteinbrick_db2objjson, proteinsetbrick_db2objjson, ls_column_proteinbrick, ls_column_proteinsetbrick
from appbrsamplehuman.models import humanbrick_db2objjson, ls_column_humanbrick
from appsabrick.structure import makeannotid, objjson2json, objjson2tsvoo
from appsabrick.lincsstandard import ls_column_antibodylincs, ls_column_celllinelincs, ls_column_otherreagentlincs, ls_column_proteinlincs, ls_column_smallmoleculelincs, objjson2lincs

# pack
def pkgbrick(ls_acjson, s_filetype, s_compilationname, s_opath="./"):
    """
    input:.
        ls_acjson: list of path to acjson files
        s_filetype: json, tsv, or lincsdcic
        s_opath: output path
        delimiter: for tsv and lincs dcic csv are fix
        s_compilationname: figures as filename extension

    output:
        ls_pathfile: list of generated acjson realated brickfiles
    """
    ls_pathfile = []

    # get acjson endpoint annot_ids
    es_endpoint = set()
    for s_acjson in ls_acjson:
        with open(s_acjson) as f_json:
            d_acjson = json.load(f_json)
        for s_well in d_acjson:
            if not (s_well in ac.ts_NOTCOOR) and not (d_acjson[s_well]["endpoint"] is None):
                for s_gent, d_value in d_acjson[s_well]["endpoint"].items():
                    s_annotid = makeannotid(
                        s_base=s_gent,
                        s_provider=d_acjson[s_well]["endpoint"][s_gent]["manufacture"],
                        s_provider_catalog_id=d_acjson[s_well]["endpoint"][s_gent]["catalogNu"],
                        s_provider_batch_id=d_acjson[s_well]["endpoint"][s_gent]["batch"],
                    )
                    try:
                        # secondary antibody patching
                        s_patch = "{}-{}-{}-{}".format(
                            d_acjson[s_well]["endpoint"][s_gent]["hostOrganism"],
                            d_acjson[s_well]["endpoint"][s_gent]["targetOrganism"],
                            d_acjson[s_well]["endpoint"][s_gent]["isotype"],
                            d_acjson[s_well]["endpoint"][s_gent]["dye"],
                        )
                        ls_annotid = s_annotid.split("-")[2:]
                        ls_annotid.insert(0, s_patch)
                        s_annotid = "-".join(ls_annotid)
                    except KeyError:
                        # primary antibody patching
                        try:
                            s_patch = "{}-{}".format(
                                d_acjson[s_well]["endpoint"][s_gent]["hostOrganism"],
                                d_acjson[s_well]["endpoint"][s_gent]["isotype"],
                            )
                            ls_annotid = s_annotid.split("-")
                            ls_annotid.insert(1, s_patch)
                            s_annotid = "-".join(ls_annotid)
                        except KeyError:
                            # compound stain
                            pass
                    # update resultset with relevant antibody1, antibody2, compound
                    es_endpoint.add(s_annotid)

    # get acjson perturbation annot_ids
    es_perturbation = set()
    for s_acjson in ls_acjson:
        with open(s_acjson) as f_json:
            d_acjson = json.load(f_json)
        for s_well in d_acjson:
            if not (s_well in ac.ts_NOTCOOR) and not (d_acjson[s_well]["perturbation"] is None):
                for s_gent, d_value in d_acjson[s_well]["perturbation"].items():
                    s_annotid = makeannotid(
                        s_base=s_gent,
                        s_provider=d_acjson[s_well]["perturbation"][s_gent]["manufacture"],
                        s_provider_catalog_id=d_acjson[s_well]["perturbation"][s_gent]["catalogNu"],
                        s_provider_batch_id=d_acjson[s_well]["perturbation"][s_gent]["batch"],
                    )
                    # update resultset with realted compound, protein, proteinset
                    es_perturbation.add(s_annotid)
                    # check for proteinset realted proteins
                    lo_protein = ProteinBrick.objects.filter(
                        proteinset_annot_id = s_annotid
                    )
                    # update resultset with related proteinset proteins
                    es_perturbation = es_perturbation.union(
                        set([o_protein.annot_id for o_protein in lo_protein])
                    )

    # get acjson sample annot_ids
    es_sample = set()
    for s_acjson in ls_acjson:
        with open(s_acjson) as f_json:
            d_acjson = json.load(f_json)
        for s_well in d_acjson:
            if not (s_well in ac.ts_NOTCOOR) and not (d_acjson[s_well]["sample"] is None):
                for s_gent, d_value in d_acjson[s_well]["sample"].items():
                    s_annotid = makeannotid(
                        s_base=s_gent,
                        s_provider=d_acjson[s_well]["sample"][s_gent]["manufacture"],
                        s_provider_catalog_id=d_acjson[s_well]["sample"][s_gent]["catalogNu"],
                        s_provider_batch_id=d_acjson[s_well]["sample"][s_gent]["batch"],
                    )
                    # cellline patching
                    #try:
                    #    s_patch = "{}x{}".format(
                    #        "|".join(d_acjson[s_well]["sample"][s_gent]["passageOrigin"]),
                    #        "|".join(d_acjson[s_well]["sample"][s_gent]["passageUsed"]),
                    #    )
                    #    s_annotid = "{}_{}".format(s_annotid, s_patch)

                    #except KeyError:
                    #    raise CommandError(
                    #        "Error @ appsabrick.structure.pkgbrick : Sample {}; {}; {} could not be found in any sample brick.".format(
                    #            s_annotid,
                    #            s_gent,
                    #            d_value
                    #        )
                    #    )

                    # update resultset with related human sample
                    es_sample.add(s_annotid)

    # get bricks
    d3_objjson = {}
    # get and filter endpoint bricks
    dd_antibody1 = antibody1brick_db2objjson()
    dd_antibody1pop = copy.deepcopy(dd_antibody1)
    for s_annotid in dd_antibody1:
        if not (s_annotid in es_endpoint):
            dd_antibody1pop.pop(s_annotid)
    d3_objjson.update({"antibody1": dd_antibody1pop})

    dd_antibody2 = antibody2brick_db2objjson()
    dd_antibody2pop = copy.deepcopy(dd_antibody2)
    for s_annotid in dd_antibody2:
        if not (s_annotid in es_endpoint):
            dd_antibody2pop.pop(s_annotid)
    d3_objjson.update({"antibody2": dd_antibody2pop})

    dd_cstain = cstainbrick_db2objjson()
    dd_cstainpop = copy.deepcopy(dd_cstain)
    for s_annotid in dd_cstain:
        if not (s_annotid in es_endpoint):
            dd_cstainpop.pop(s_annotid)
    d3_objjson.update({"cstain": dd_cstainpop})

    # get and filter perturbation bricks
    dd_compound = compoundbrick_db2objjson()
    dd_compoundpop = copy.deepcopy(dd_compound)
    for s_annotid in dd_compound:
        if not (s_annotid in es_perturbation):
            dd_compoundpop.pop(s_annotid)
    d3_objjson.update({"compound": dd_compoundpop})

    dd_protein = proteinbrick_db2objjson()
    dd_proteinpop = copy.deepcopy(dd_protein)
    for s_annotid in dd_protein:
        if not (s_annotid in es_perturbation):
            dd_proteinpop.pop(s_annotid)
    d3_objjson.update({"protein": dd_proteinpop})

    dd_proteinset = proteinsetbrick_db2objjson()
    dd_proteinsetpop = copy.deepcopy(dd_proteinset)
    for s_annotid in dd_proteinset:
        if not (s_annotid in es_perturbation):
            dd_proteinsetpop.pop(s_annotid)
    d3_objjson.update({"proteinset": dd_proteinsetpop})

    # get and filter sample bricks
    dd_human = humanbrick_db2objjson()
    dd_humanpop = copy.deepcopy(dd_human)
    for s_annotid in dd_human:
        if not (s_annotid in es_sample):
            dd_humanpop.pop(s_annotid)
    d3_objjson.update({"human": dd_humanpop})

    # file type specific code
    if (s_filetype == "json"):
        # get filenames
        ds_filename = {}
        ds_filename.update({"antibody1": "annotBrick_antibody1_{}.json"})
        ds_filename.update({"antibody2": "annotBrick_antibody2_{}.json"})
        ds_filename.update({"compound": "annotBrick_compound_{}.json"})
        ds_filename.update({"cstain": "annotBrick_cstain_{}.json"})
        ds_filename.update({"protein": "annotBrick_protein_{}.json"})
        ds_filename.update({"proteinset": "annotBrick_proteinset_{}.json"})
        ds_filename.update({"human": "annotBrick_human_{}.json"})
        # get column headers
        # bue: json file format has no headers
        # output bricks
        for s_bricktype, dd_objjson in d3_objjson.items():
            # get filename
            s_opathfile ="{}{}".format(
                s_opath,
                ds_filename[s_bricktype].format(s_compilationname),
            )
            # get file
            objjson2json(
                s_filename = s_opathfile,
                dd_json = dd_objjson,
                i_indent = 4,
                b_verbose = True,
            )
            # update function output
            if (os.stat(s_opathfile).st_size > 2):
                ls_pathfile.append(s_opathfile)
            else:
                os.remove(s_opathfile)

    elif(s_filetype == "tsv"):
        # get filenames
        ds_filename = {}
        ds_filename.update({"antibody1": "annotBrick_antibody1_{}.tsv"})
        ds_filename.update({"antibody2": "annotBrick_antibody2_{}.tsv"})
        ds_filename.update({"compound": "annotBrick_compound_{}.tsv"})
        ds_filename.update({"cstain": "annotBrick_cstain_{}.tsv"})
        ds_filename.update({"protein": "annotBrick_protein_{}.tsv"})
        ds_filename.update({"proteinset": "annotBrick_proteinset_{}.tsv"})
        ds_filename.update({"human": "annotBrick_human_{}.tsv"})
        # get column headers
        dls_header = {}
        dls_header.update({"antibody1": ls_column_antibody1brick})
        dls_header.update({"antibody2": ls_column_antibody2brick})
        dls_header.update({"compound": ls_column_compoundbrick})
        dls_header.update({"cstain": ls_column_cstainbrick})
        dls_header.update({"protein": ls_column_proteinbrick})
        dls_header.update({"proteinset": ls_column_proteinsetbrick})
        dls_header.update({"human": ls_column_humanbrick})
        # output bricks
        for s_bricktype, dd_objjson in d3_objjson.items():
            # get filename
            s_opathfile ="{}{}".format(
                s_opath,
                ds_filename[s_bricktype].format(s_compilationname),
            )
            # get file
            objjson2tsvoo(
                s_filename = s_opathfile,
                dd_json = dd_objjson,
                ls_column = dls_header[s_bricktype],
                b_verbose = True,
            )
            # update function output
            if (os.stat(s_opathfile).st_size > 0):
                ls_pathfile.append(s_opathfile)
            else:
                os.remove(s_opathfile)

    elif(s_filetype == "lincs"):
        es_pathfile = set()
        # get filenames
        ds_filename = {}
        ds_filename.update({"antibody1": "lincsDcic_AntibodyMetadata_{}.csv"})
        ds_filename.update({"antibody2": "lincsDcic_AntibodyMetadata_{}.csv"})
        ds_filename.update({"compound": "lincsDcic_SmallMoleculeMetadata_{}.csv"})
        ds_filename.update({"cstain": "lincsDcic_SmallMoleculeMetadata_{}.csv"})
        ds_filename.update({"protein": "lincsDcic_ProteinMetadata_{}.csv"})
        #ds_filename.update({"proteinset": "lincsDcic_ProteinMetadata_{}.csv"})
        ds_filename.update({"human": "lincsDcic_CellLineMetadata_{}.csv"})
        #"lincsDcic_OtherReagentMetadata_{}.csv"
        # get column headers
        dls_header = {}
        dls_header.update({"antibody1": ls_column_antibodylincs})
        dls_header.update({"antibody2": ls_column_antibodylincs})
        dls_header.update({"compound": ls_column_smallmoleculelincs})
        dls_header.update({"cstain": ls_column_smallmoleculelincs})
        dls_header.update({"protein": ls_column_proteinlincs})
        #dls_header.update({"proteinset": ls_column_proteinlincs})
        dls_header.update({"human": ls_column_celllinelincs})
        #ls_column_otherreagentlincs
        # delete possibly old lincs output brick files still hangiong around
        es_opathfilename = set()
        for  _, s_filename in ds_filename.items():
            es_opathfilename.add(
                "{}{}".format(
                    s_opath,
                    s_filename.format(s_compilationname),
                ),
            )
        for s_opathfilename in es_opathfilename:
            try:
                os.remove(s_opathfilename)
            except FileNotFoundError:
                pass

        # generate output bricks
        for s_bricktype, dd_objjson in d3_objjson.items():
            if (len(dd_objjson) > 0) and (s_bricktype != "proteinset"):
                # get filename
                s_opathfile ="{}{}".format(
                    s_opath,
                    ds_filename[s_bricktype].format(s_compilationname),
                )
                # get file
                objjson2lincs(
                    s_filename = s_opathfile,
                    dd_json = dd_objjson,
                    ls_column = dls_header[s_bricktype],
                    b_verbose = True,
                )
                # update function output
                es_pathfile.add(s_opathfile)
        # output
        ls_pathfile = list(es_pathfile)
    else:
        raise CommandError(
            "Error @ appsabrick.structure.pkgbrick : Unknowne s_filetype {}. Knowen are json, tsv, and lincsdcic.".format(
                s_filetype
            )
        )

    # output
    return(ls_pathfile)
