from django import forms
from django.contrib import admin
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.utils.html import format_html
from app1study.models import Study
from app3runset.models import RunSet

# import from django selectable prj
from selectable.forms import AutoComboboxSelectWidget, AutoComboboxSelectMultipleWidget

# import from annot
from app0investigation.lookups import InvestigationLookup
from appbrxperson.lookups import PersonBrickLookup


# Register your models here.
#admin.site.register(Study)
class StudyForm(forms.ModelForm):
    class Meta:
        model = Study
        fields = [
            "investigation",
            "responsible",
        ]
        widgets = {
            "investigation": AutoComboboxSelectMultipleWidget(InvestigationLookup),
            "responsible": AutoComboboxSelectMultipleWidget(PersonBrickLookup),
        }

class StudyAdmin(admin.ModelAdmin):
    form = StudyForm
    search_fields = (
        "annot_id",
        "study_name",
        "date_submission",
        "url_experimentaldesigne",
        "url_conclusion",
        "investigation__annot_id",
        "responsible__annot_id",
        "notes",
    )
    list_display = (
        "annot_id",
        "study_name",
        "date_submission",
        "url_experimentaldesigne_click",
        "url_conclusion_click",
        "investigation_display",
        "responsible_display",
        "notes",
    )
    save_on_top = True
    fieldsets = [
        ("Identification", { "fields" : [
            "annot_id",
            "study_name",
        ]}),
        ("Details", { "fields" : [
            "date_submission",
            "url_experimentaldesigne",
            "url_conclusion",
            "investigation_display",
            "investigation",
            "responsible_display",
            "responsible",
            "notes",
        ]}),
    ]
    readonly_fields = (
        "annot_id",
        "investigation_display",
        "responsible_display",
    )

    # url link clickable
    def url_experimentaldesigne_click (self, obj):
        return format_html("<a href='{url}'>{url}</a>", url=obj.url_experimentaldesigne)
    url_experimentaldesigne_click.short_description = "Clickable experimentaldesigne URL"

    def url_conclusion_click (self, obj):
        return format_html("<a href='{url}'>{url}</a>", url=obj.url_conclusion)
    url_conclusion_click.short_description = "Clickable conclusion URL"

    # actions
    actions = [
        "delete_selected",
        "download_json",
        "download_study2investigation_json",
        "download_brick_json",
        "download_acjson",
        "download_tsv",
        "download_study2investigation_tsv",
        "download_brick_tsv",
        "download_brick_lincs",
        "resave",
    ]

    ### json download action ###
    def download_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=generic&choice=app1study.study"
        )
    )
    download_json.short_description = "Download this page as json file (item selection irrelevant)"

    def download_study2investigation_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/app1study/export_study2investigation?filetype=json"
        ))
    download_study2investigation_json.short_description = "Download study investigation mapping as json \
file (item selection irrelevant)"

    def download_brick_json(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=study&filetype=json&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_json.short_description = "Download selected set releated bricks as json files"

    def download_acjson(self, request, queryset):
        es_runset = set()
        for s_study in request.POST.getlist(admin.ACTION_CHECKBOX_NAME):
            for o_runset in RunSet.objects.filter(study=s_study):
                es_runset.add(o_runset.runset_name)
        return(HttpResponseRedirect(
            "/appacaxis/export_acjson?model=runset&filetype=acjson&choice={}".format(
                "!".join(es_runset)
            )
        ))
    download_acjson.short_description = "Download selected sets as acjson file"

    ### tsv download action ###
    def download_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=generic&choice=app1study.study"
        ))
    download_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

    def download_study2investigation_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/app1study/export_study2investigation?filetype=tsv"
        ))
    download_study2investigation_tsv.short_description = "Download study investigation mapping as tsv fi\
le (item selection irrelevant)"

    def download_brick_tsv(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=study&filetype=tsv&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_tsv.short_description = "Download selected set releated bricks as tsv files"

    def download_brick_lincs(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=study&filetype=lincs&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_lincs.short_description = "Download selected set releated bricks as lincs data standard csv files"

    ### resave action ###
    def resave(self, request, queryset):
        # process
        ls_save = []
        for o_record in Study.objects.all():
            o_record.save()
            ls_save.append(o_record.study_name)
        # message
        messages.success(
            request,
            "{} # resaved".format(ls_save),
        )
    resave.short_description = "Resave all record (item selection irrelevant)"

admin.site.register(Study, StudyAdmin)
