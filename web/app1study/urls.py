# import form djnago
from django.conf.urls import url
# import from appsaontology
from app1study import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^export_study2investigation/$', views.export_study2investigation, name="export_study2investigation"),
]
