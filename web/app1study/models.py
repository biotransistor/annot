from django.db import models

# annot bricks
from app0investigation.models import Investigation
from appbrxperson.models import PersonBrick

# from python
import re

# Create your models here.
# study model
class Study(models.Model):
    annot_id = models.SlugField(
        max_length=256,
        primary_key=True,
        verbose_name="Study identifier",
        help_text="Automatically generated from input fields."
    )
    study_name = models.SlugField(
        max_length=256,
        default="not_yet_specified",
        help_text="A concise phrase used to encapsulate the purpose and goal of the study."
    )
    date_submission = models.DateField(
        default="1955-11-05",
        verbose_name="Study submission date",
        help_text="The date on which the study is submitted to an archive."
    )
    url_experimentaldesigne = models.URLField(
        max_length=256,
        default="https://not.yet.specified",
        verbose_name="Experimental designe document",
        help_text="Url link to the study related experimentaldesigne document."
    )
    url_conclusion = models.URLField(
        max_length=256,
        default="https://not.yet.specified",
        verbose_name="Conclusion document",
        help_text="Url link to the study related conclusion document."
    )
    investigation_display = models.TextField(
        help_text="Study related investigations.",
        blank=True
    )
    investigation = models.ManyToManyField(
        Investigation,
        help_text="Choose investigation.",
        blank=True
    )
    responsible_display = models.TextField(
        help_text="Study related principal investigaters.",
        blank=True
    )
    responsible = models.ManyToManyField(
        PersonBrick,
        help_text="Principal investigater associated with the study."
    )
    notes = models.TextField(
        help_text="Study related notes.",
        blank=True
    )

    # primary key generator
    def save(self, *args, **kwargs):
        self.annot_id = re.sub(r"[^a-z0-9-]", "", self.study_name.lower())
        # set content display
        self.investigation_display = str(
            [o_investigation.annot_id for o_investigation in self.investigation.all()]
        )
        self.responsible_display = str(
            [o_responsible.annot_id for o_responsible in self.responsible.all()]
        )
        # save
        super().save(*args, **kwargs)

    # output
    def __str__(self):
        return (self.study_name)

    __repr__ = __str__

    class Meta:
        ordering = ["-date_submission"]
        verbose_name_plural = "studies"
