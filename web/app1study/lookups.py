# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from app1study.models import Study

# code
class StudyLookup(ModelLookup):
    model = Study
    search_fields = ('annot_id__icontains',)
registry.register(StudyLookup)
