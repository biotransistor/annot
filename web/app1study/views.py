## appacaxis ###
# import form django
from django.http import HttpResponse
from django.contrib import messages

# import from python
import datetime
import io
import json
import os
import re
import zipfile

# import from annot
from app1study.models import Study
from prjannot.settings import MEDIA_ROOTQUARANTINE

# Create your views here.
# index
def index(request):
    """ hello world """
    return HttpResponse("Hello world! You're at the appacaxis.view index page")

# export
def export_study2investigation(request):
    #return HttpResponse("Hello world! You're at the app1study.export_study2investigation page")
    """
    description:
        make model realed json and tsv realted files downloadable.
    """

    # read out Study model
    d_investigation2study = {}
    for o_study in Study.objects.all():
        for o_investigation in o_study.investigation.all():
            try:
                ls_study = d_investigation2study[o_investigation.investigation_name]
                ls_study.append(o_study.study_name)
            except:
                ls_study = [o_study.study_name]
            d_investigation2study.update({o_investigation.investigation_name : ls_study})

    # Handle input
    s_filetype = request.GET.get("filetype")

    # set variables
    s_datetime = datetime.datetime.today().isoformat().split(".")[0].replace(":", "").replace("-", "")
    s_filezip = "{}_annot_{}_download.zip".format(s_datetime, s_filetype)
    s_pathfilezip = "{}{}".format(MEDIA_ROOTQUARANTINE, s_filezip)

    # open zip file handle
    with zipfile.ZipFile(s_pathfilezip, "w") as zipper:

        # stdout handle
        o_stdout = io.StringIO()

        # zip
        if (s_filetype in {"tsv"}):
            s_file = "annotCoordinate_investigation2study.tsv"
            s_pathfile = "{}{}".format(MEDIA_ROOTQUARANTINE, s_file)
            with open(s_pathfile, "w") as f_tsv:
                for s_investigation, ls_study in d_investigation2study.items():
                    ls_study.sort()
                    s_out = "{}\t{}\n".format(s_investigation, "; ".join(ls_study))
                    f_tsv.write(s_out)
            zipper.write(s_pathfile, arcname=s_file)
            os.remove(s_pathfile)

        else: # json
            s_file = "annotCoordinate_investigation2study.json"
            s_pathfile = "{}{}".format(MEDIA_ROOTQUARANTINE, s_file)
            with open(s_pathfile, "w") as f_json:
                json.dump(d_investigation2study, f_json, sort_keys=True) # indent=4
            zipper.write(s_pathfile, arcname=s_file)
            os.remove(s_pathfile)

    # push save as zip
    response = HttpResponse(content_type="application/zip")
    response["Content-Disposition"] = "attachment; filename={}".format(s_filezip)
    with open(s_pathfilezip, "rb") as f:
        response.write(f.read())

    # delete zip file
    os.system("rm {}".format(s_pathfilezip))

    # put stdout as message
    s_out = o_stdout.getvalue().strip()
    if (s_out == ""):
        messages.success(
            request,
            "{} study2investigation mapping # successfully zipped.".format(s_filetype)
        )
    elif re.match(".*Error.*", s_out):
        messages.error(
            request,
            "{} study2investigation mapping # {}".format(s_filetype, s_out)
        )
    elif re.match(".*Warning.*", s_out):
        messages.warning(
            request,
            "{} study2investigation mapping # {}".format(s_filetype, s_out)
        )
    else:
        messages.info(
            request,
            "{} study2investigation mapping # {}".format(s_filetype, s_out)
        )
    storage = messages.get_messages(request)

    # return
    return(response)
