# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appontissue_bioontology.models import Tissue

# code
class TissueLookup(ModelLookup):
    model = Tissue
    search_fields = ('annot_id__icontains',)
registry.register(TissueLookup)

