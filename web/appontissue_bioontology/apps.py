from django.apps import AppConfig


class AppontissueBioontologyConfig(AppConfig):
    name = 'appontissue_bioontology'
