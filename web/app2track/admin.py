from django import forms
from django.contrib import admin
from django.http import HttpResponseRedirect

# import from django selectable prj
from selectable.forms import AutoComboboxSelectWidget #, AutoComboboxSelectMultipleWidget

# import form annot
from app2track.models import MemaSuperSetTracking, MemaAssayTracking
from appbrxperson.models import PersonBrick
from app3runset.lookups import RunSetLookup
from app4superset.lookups import SuperSetLookup
from appbrxperson.lookups import PersonBrickLookup

# Register your models here.
#admin.site.register(SuperSetTracking)
# form
class MemaSuperSetTrackingForm(forms.ModelForm):
    class Meta:
        model = MemaSuperSetTracking
        fields = [
            "superset",
            "responsible",
        ]
        widgets = {
            "superset": AutoComboboxSelectWidget(SuperSetLookup),
            "responsible": AutoComboboxSelectWidget(PersonBrickLookup),
        }
# admin
class MemaSuperSetTrackingAdmin(admin.ModelAdmin):
    form = MemaSuperSetTrackingForm
    list_select_related = (
        "superset",
        "responsible",
    )
    search_fields = (
        "annot_id",
        "superset__annot_id",
    )
    # raw_id_fields = ()
    # list_per_page = 16
    list_display = (
        "annot_id",
        "superset",
        "protocol",
        "dday",
        "responsible",
    )
    # list_editable = ()
    save_on_top = True
    readonly_fields = ("annot_id",)
    fieldsets = [
        ("MEMA Superset", {"fields": [
            "annot_id",
            "superset"
        ]}),
        ("Track", {"fields": [
            "protocol",
            "dday",
            "responsible",
        ]}),
    ]
    # actions
    actions = [
        "delete_selected",
        "download_json",
        "download_tsv",
    ]

    ### json download action ###
    def download_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=generic&choice=app2track.supersettracking"
        )
    )
    download_json.short_description = "Download this page as json file (item selection irrelevant)"

    ### tsv download action ###
    def download_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=generic&choice=app2track.supersettracking"
        ))
    download_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

# register
admin.site.register(MemaSuperSetTracking, MemaSuperSetTrackingAdmin)




# mema assay
#admin.site.register(MemaAssayTracking)
# form
class MemaAssayTrackingForm(forms.ModelForm):
    class Meta:
        model = MemaAssayTracking
        fields = [
            "runset",
            "sample_addition_operator",
            "drug_addition_operator",
            "fixing_operator",
            "stain_addition_operator",
            "image_acquisition_operator",
        ]
        widgets = {
            "runset": AutoComboboxSelectWidget(RunSetLookup),
            "sample_addition_operator": AutoComboboxSelectWidget(PersonBrickLookup),
            "drug_addition_operator": AutoComboboxSelectWidget(PersonBrickLookup),
            "fixing_operator": AutoComboboxSelectWidget(PersonBrickLookup),
            "stain_addition_operator": AutoComboboxSelectWidget(PersonBrickLookup),
            "image_acquisition_operator": AutoComboboxSelectWidget(PersonBrickLookup),
        }
# admin
class MemaAssayTrackingAdmin(admin.ModelAdmin):
    form = MemaAssayTrackingForm
    list_select_related = (
        "runset",
        "sample_addition_operator",
        "drug_addition_operator",
        "fixing_operator",
        "stain_addition_operator",
        "image_acquisition_operator",
    )
    search_fields = (
        "annot_id",
        "runset__annot_id",
        "plate_batch_id",
    )
    # raw_id_fields = ()
    # list_per_page = 16
    list_display = (
        "annot_id",
        "runset",
        "plate_batch_id",
        "sample_addition_protocol",
        "sample_addition_dday",
        "sample_addition_operator",
        "drug_addition_day",
        "drug_addition_operator",
        "stain_addition_protocol",
        "fixing_day",
        "fixing_operator",
        "stain_addition_day",
        "stain_addition_operator",
        "image_acquisition_protocol",
        "image_acquisition_day",
        "image_acquisition_operator",
    )
    # list_editable = ()
    save_on_top = True
    readonly_fields = ("annot_id",)
    fieldsets = [
        ("MEMA assay run", {"fields": [
            "annot_id",
            "runset",
            "plate_batch_id",
        ]}),
        ("Sample and Treatment", {"fields": [
            "sample_addition_protocol",
            "sample_addition_dday",
            "sample_addition_operator",
            "drug_addition_day",
            "drug_addition_operator",
        ]}),
        ("Fixing and Staining", {"fields": [
            "stain_addition_protocol",
            "fixing_day",
            "fixing_operator",
            "stain_addition_day",
            "stain_addition_operator",
        ]}),
        ("Imaging", {"fields": [
            "image_acquisition_protocol",
            "image_acquisition_day",
            "image_acquisition_operator",
        ]}),
    ]
    # actions
    actions = [
        "delete_selected",
        "download_json",
        "download_tsv",
    ]

    ### json download action ###
    def download_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=generic&choice=app2track.supersettracking"
        )
    )
    download_json.short_description = "Download this page as json file (item selection irrelevant)"

    ### tsv download action ###
    def download_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=generic&choice=app2track.supersettracking"
        ))
    download_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

# register
admin.site.register(MemaAssayTracking, MemaAssayTrackingAdmin)
