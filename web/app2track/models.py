from django.db import models

# import from annot
from app3runset.models import RunSet
from app4superset.models import SuperSet
from appbrxperson.models import PersonBrick

# from python import
# https://www.youtube.com/watch?v=WlBiLNN1NhQ
import datetime

# Create your models here.
# super set
MEMASUPERSET_PROTOCOL = (
    ("Mema_step1-Protein_Preparation-v2", "Mema_step1-Protein_Preparation-v2"),
    ("Mema_step1-Protein_Preparation-v3", "Mema_step1-Protein_Preparation-v3"),
    ("Mema_step2-Plate_Preparation-v2", "Mema_step2-Plate_Preparation-v2"),
    ("Mema_step2-Plate_Preparation-v3", "Mema_step2-Plate_Preparation-v3"),
    ("Mema_step3-Aushon_Printing-v1", "Mema_step3-Aushon_Printing-v1"),
    ("Mema_step3-Aushon_Printing-v2", "Mema_step3-Aushon_Printing-v2"),
)

class MemaSuperSetTracking(models.Model):
    annot_id = models.SlugField(
        primary_key=True,
        #default = "automatically_generated",
        max_length=256,
        verbose_name = "Identifier",
        help_text="Automatically generated from input fields."
    )
    superset = models.ForeignKey(
        SuperSet,
        help_text="Choose superset.",
    )
    protocol = models.CharField(
        null=True,
        max_length = 128,
        choices = MEMASUPERSET_PROTOCOL,
        help_text="Choose SOP version used to gererate this superset."
    )
    dday = models.DateField(
        default=datetime.datetime(year=1955, month=11, day=5),
        verbose_name="Superset generation inital date"
    )
    responsible = models.ForeignKey(
        PersonBrick,
        default="not_yet_specified",
        help_text="Superset generation responsible."
    )

    # primary key generator
    def save(self, *args, **kwargs):
        # annot_id
        self.annot_id = "{}Day{}".format(
            self.superset.annot_id,
            self.dday.isoformat().replace("-","")
        )
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.annot_id)

    __repr__ =  __str__

    class Meta:
        ordering=["-dday","annot_id"]
        verbose_name_plural = "Superset trackings"

# mema
ASSAY_MEMASTEP4_PROTOCOL = (
    ("Mema_step4-Cell_Plating_And_Treatment-v2", "Mema_step4-Cell_Plating_And_Treatment-v2"),
    ("Mema_step4-Cell_Plating_And_Treatment-v3", "Mema_step4-Cell_Plating_And_Treatment-v3"),
    ("Mema_step4-Cell_Plating_And_Treatment-v4", "Mema_step4-Cell_Plating_And_Treatment-v4"),
)

ASSAY_MEMASTEP6_PROTOCOL = (
    ("Mema_step6-Cell_Fixing_And_Staining-v2", "Mema_step6-Cell_Fixing_And_Staining-v2"),
    ("Mema_step6-Cell_Fixing_And_Staining-v3", "Mema_step6-Cell_Fixing_And_Staining-v3"),
    ("Mema_step6-Cell_Fixing_And_Staining-v4", "Mema_step6-Cell_Fixing_And_Staining-v4"),
)

ASSAY_MEMASTEP7_PROTOCOL = (
    ("Mema_step7-7_Image_Acquisition-v1", "Mema_step7-7_Image_Acquisition-v1"),
    ("Mema_step7-7_Image_Acquisition-v2", "Mema_step7-7_Image_Acquisition-v2"),
)

class MemaAssayTracking(models.Model):
    annot_id = models.SlugField(
        primary_key=True,
        max_length=256,
        verbose_name = "Identifier",
        help_text="Automatically generated from input fields."
    )
    runset = models.ForeignKey(
        RunSet,
        help_text="Choose runset barcode.",
    )
    # plate lot number
    plate_batch_id = models.CharField(
        default="not_yet_specified",
        verbose_name="Plate batch id",
        max_length=256,
        help_text="Batch or lot number assigned to this well plate."
    )

    # sample and treatment
    sample_addition_protocol = models.CharField(
        null=True,
        max_length = 128,
        choices = ASSAY_MEMASTEP4_PROTOCOL,
        default = "Mema_step4-Cell_Plating_And_Treatment-v4",
        help_text="Choose SOP version used."
    )
    sample_addition_dday = models.DateField(
        default=datetime.datetime(year=1955, month=11, day=5),
    )
    sample_addition_operator = models.ForeignKey(
        PersonBrick,
        default="not_yet_specified",
        related_name="sample_operator"
    )
    # drug
    drug_addition_day = models.DateField(
        default=datetime.datetime(year=1955, month=11, day=5),
    )
    drug_addition_operator = models.ForeignKey(
        PersonBrick,
        default="not_yet_specified",
        related_name="drug_operator",
    )

    # fixing and staining
    stain_addition_protocol = models.CharField(
        null=True,
        max_length = 128,
        choices = ASSAY_MEMASTEP6_PROTOCOL,
        default = "Mema_step6-Cell_Fixing_And_Staining-v4",
        help_text="Choose SOP version used."
    )
    # fixing
    fixing_day = models.DateField(
        default=datetime.datetime(year=1955, month=11, day=5),
    )
    fixing_operator = models.ForeignKey(
        PersonBrick,
        default="not_yet_specified",
        related_name="fixing_operator",
    )
    # staining
    stain_addition_day = models.DateField(
        default=datetime.datetime(year=1955, month=11, day=5),
    )
    stain_addition_operator = models.ForeignKey(
        PersonBrick,
        default="not_yet_specified",
        related_name="stain_operator",
    )

    # imaging
    image_acquisition_protocol = models.CharField(
        null=True,
        max_length = 128,
        choices = ASSAY_MEMASTEP7_PROTOCOL,
        default = "Mema_step7-7_Image_Acquisition-v2",
        help_text="Choose SOP version used."
    )
    image_acquisition_day = models.DateField(
        default=datetime.datetime(year=1955, month=11, day=5),
    )
    image_acquisition_operator = models.ForeignKey(
        PersonBrick,
        default="not_yet_specified",
        related_name="image_operator",
    )

    # primary key generator
    def save(self, *args, **kwargs):
        # annot_id
        self.annot_id = "{}Day{}".format(
            self.runset.annot_id,
            self.sample_addition_dday.isoformat().replace("-","")
        )
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.annot_id)

    __repr__ =  __str__

    class Meta:
        ordering=["-sample_addition_dday","annot_id"]
