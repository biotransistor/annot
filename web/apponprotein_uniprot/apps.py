from django.apps import AppConfig


class ApponproteinUniprotConfig(AppConfig):
    name = 'apponprotein_uniprot'
