from django.db import models

# constants
TERM_SOURCE_NAME = "UNIPROT"
TERM_SOURCE_FILE = "http://www.uniprot.org/proteomes/"
TERM_SOURCE_DESCRIPTION = "Human reference uniprot proteome."
# rest source
REST_URL = "http://www.uniprot.org/uniprot/"
REST_QUERY = "(organism:9606) AND (keyword:Reference proteome)"
REST_FORMAT = "http"
REST_FILENAME = "uniprotbuffer.csv"
# build
PK_STRING_CASE = None
SWAP_TERMID = True

# Create your models here.
class Protein(models.Model):
    term_name = models.CharField(
        unique=True,
        max_length=254,
        verbose_name='Ontological term',
        help_text="Controlled vocabulary term."
    )
    term_id = models.CharField(
        blank=True,
        max_length=3072,
        verbose_name='Ontology identifier',
        help_text="Controlled vocabulary identifier."
    )
    annot_id = models.CharField(
        primary_key=True,
        max_length=254,
        verbose_name='Annot identifier',
        help_text="Internal identifier. This identifier is the primary key and should be descriptive. Choose this identifier carefully in accordance to the identifiers already in use."
    ) # bue 20151023: changed from slug to char this is a bit dangerous. but alphanumeric, underscores and pipes have to be allowed
    term_source_version_responsible = models.CharField(
        max_length=254,
        verbose_name="Responsible person",
        help_text="Your name."
    )
    term_source_version_update = models.DateField(
        verbose_name='Version update time stamp',
        auto_now=True
    )
    term_source_version = models.CharField(
        max_length=254,
        verbose_name='Ontology source file version',
        help_text="Version of controlled vocabulary source."
    )
    term_ok = models.NullBooleanField(
        default=None,
        null=True,
        verbose_name='Ontology term status',
        help_text='A term marked true is in the most recent ontology file version used in this database. A term marked false is internally generated or deprecated. A term marked null has not been checked against the most recent ontology file version.'
    )
    def __str__(self):
        return(self.annot_id)

    class Meta:
        ordering = ['annot_id']
