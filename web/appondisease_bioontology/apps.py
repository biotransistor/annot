from django.apps import AppConfig


class AppondiseaseBioontologyConfig(AppConfig):
    name = 'appondisease_bioontology'
