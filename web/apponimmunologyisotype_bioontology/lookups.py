# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponimmunologyisotype_bioontology.models import ImmunologyIsotype

# code
class ImmunologyIsotypeLookup(ModelLookup):
    model = ImmunologyIsotype
    search_fields = ('annot_id__icontains',)
registry.register(ImmunologyIsotypeLookup)

