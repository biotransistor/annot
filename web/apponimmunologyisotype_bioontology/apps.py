from django.apps import AppConfig


class ApponimmunologyisotypeBioontologyConfig(AppConfig):
    name = 'apponimmunologyisotype_bioontology'
