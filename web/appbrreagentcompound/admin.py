# import from django
from django import forms
from django.contrib import admin
from django.contrib import messages
from django.core import management
from django.http import HttpResponseRedirect
from appbrreagentcompound.models import CompoundBrick, CstainBrick

# python
import io
import re

# import from django selectable prj
from selectable.forms import AutoComboboxSelectWidget, AutoComboboxSelectMultipleWidget

# import from annot
from appbrxperson.lookups import PersonBrickLookup
from apponcompound_ebi.lookups import CompoundLookup
from appondye_own.lookups import DyeLookup
from appongeneontology_go.lookups import GeneOntologyLookup
from apponprovider_own.lookups import ProviderLookup
from apponunit_bioontology.lookups import UnitLookup


# Register your models here.
#admin.site.register(CompoundBrick)
class CompoundBrickForm(forms.ModelForm):
    class Meta:
        model = CompoundBrick
        fields = [
            "compound",
            "provider",
            "stocksolution_buffer_compound",
            "stocksolution_concentration_unit",
            "final_concentration_unit",
            "time_unit",
            "responsible"
        ]
        widgets = {
            "compound": AutoComboboxSelectWidget(CompoundLookup),
            "provider": AutoComboboxSelectWidget(ProviderLookup),
            "stocksolution_buffer_compound": AutoComboboxSelectMultipleWidget(CompoundLookup),
            "stocksolution_concentration_unit": AutoComboboxSelectWidget(UnitLookup),
            "final_concentration_unit": AutoComboboxSelectWidget(UnitLookup),
            "time_unit": AutoComboboxSelectWidget(UnitLookup),
            "responsible": AutoComboboxSelectWidget(PersonBrickLookup)
        }

class CompoundBrickAdmin(admin.ModelAdmin):
    form = CompoundBrickForm
    search_fields = (
        "annot_id",
        "lincs_identifier",
        "compound__annot_id",
        "common_name",
        "provider__annot_id",
        "provider_catalog_id",
        "provider_batch_id",
        "notes"
    )
    list_display = (
        "annot_id",
        "lincs_identifier",
        "compound",
        "common_name",
        "provider",
        "provider_catalog_id",
        "provider_batch_id",
        "available",
        "notes",
        "purity",
        "lyophilized",
        "stocksolution_buffer_compound_display",
        "stocksolution_concentration_value",
        "stocksolution_concentration_unit",
        "final_concentration_unit",
        "time_unit",
        "reference_url",
        "responsible"
    )
    list_editable = ("available",) # "notes"
    save_on_top = True
    fieldsets = [
        ("Primary key", {"fields": [
            "annot_id",
            "lincs_identifier",
            "compound",
            "common_name",
            "provider",
            "provider_catalog_id",
            "provider_batch_id"
        ]}),
        ("Detail", {"fields": [
            "purity",
            "lyophilized"
        ]}),
        ("Laboratory", {"fields": [
            "available",
            "stocksolution_buffer_compound_display",
            "stocksolution_buffer_compound",
            "stocksolution_concentration_value",
            "stocksolution_concentration_unit",
            "final_concentration_unit",
            "time_unit",
        ]}),
        ("Reference", {"fields" : [
            "reference_url",
            "responsible",
            "notes"
        ]}),
    ]
    readonly_fields = (
        "annot_id",
        "stocksolution_buffer_compound_display",
    )
    actions = [
        "delete_selected",
        "download_brick_json",
        "download_brick_txt",
        "brick_load"
    ]

    ## json ##
    def download_brick_json(self, request, queryset):
        # call appsabrick view export function
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=brick&choice=compound"
        ))
    download_brick_json.short_description = "Download compound page as json file (item selection irrelevant)"

    ## tsv ##
    def download_brick_txt(self, request, queryset):
        # call appsabrick view export function
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=brick&choice=compound"
        ))
    download_brick_txt.short_description = "Download compound page as tsv file (item selection irrelevant)"

    ## brick ##
    def brick_load(self, request, queryset):  # self = modeladmin
        # python manage.py brick_pull s_brick_type
        o_out = io.StringIO()
        management.call_command(
            "brick_load",
            "compound",
            stdout=o_out,
            verbosity=0
        )
        s_out = o_out.getvalue()
        s_out = s_out.strip()
        if (s_out == ""):
            self.message_user(
                request,
                "Compound # successfully bricked.",
                level=messages.SUCCESS
            )
        elif  re.match(".*Error.*", s_out):
            self.message_user(
                request,
                "Compound # {}".format(s_out),
                level=messages.ERROR
            )
        elif re.match(".*Warning.*", s_out):
            self.message_user(
                request,
                "Compound # {}".format(s_out),
                level=messages.WARNING
            )
        else:
            self.message_user(
                request,
                "Compound # {}".format(s_out),
                level=messages.INFO
            )
    brick_load.short_description = "Upload compound bricks (item selection irrelevant)"
# register
admin.site.register(CompoundBrick, CompoundBrickAdmin)


#admin.site.register(CstainBrick)
class CstainBrickForm(forms.ModelForm):
    class Meta:
        model = CstainBrick
        fields = [
            "cstain",
            "provider",
            "target",
            "crossabsorption",
            "dye",
            "stocksolution_buffer_compound",
            "stocksolution_concentration_unit",
            "final_concentration_unit",
            "time_unit",
            "responsible"
        ]
        widgets = {
            "cstain": AutoComboboxSelectWidget(CompoundLookup),
            "provider": AutoComboboxSelectWidget(ProviderLookup),
            "target": AutoComboboxSelectMultipleWidget(GeneOntologyLookup),
            "crossabsorption": AutoComboboxSelectMultipleWidget(GeneOntologyLookup),
            "dye": AutoComboboxSelectWidget(DyeLookup),
            "stocksolution_buffer_compound": AutoComboboxSelectMultipleWidget(CompoundLookup),
            "stocksolution_concentration_unit": AutoComboboxSelectWidget(UnitLookup),
            "final_concentration_unit": AutoComboboxSelectWidget(UnitLookup),
            "time_unit": AutoComboboxSelectWidget(UnitLookup),
            "responsible": AutoComboboxSelectWidget(PersonBrickLookup)
        }

class CstainBrickAdmin(admin.ModelAdmin):
    form = CstainBrickForm
    search_fields = (
        "annot_id",
        "lincs_identifier",
        "cstain__annot_id",
        "common_name",
        "provider__annot_id",
        "provider_catalog_id",
        "provider_batch_id",
        "target__annot_id",
        "crossabsorption__annot_id",
        "dye__annot_id",
        "notes"
    )
    list_display = (
        "annot_id",
        "lincs_identifier",
        "cstain",
        "common_name",
        "provider",
        "provider_catalog_id",
        "provider_batch_id",
        "available",
        "notes",
        "target_display",
        "crossabsorption_display",
        "dye",
        "purity",
        "lyophilized",
        "stocksolution_buffer_compound_display",
        "stocksolution_concentration_value",
        "stocksolution_concentration_unit",
        "final_concentration_unit",
        "time_unit",
        "microscopy_channel",
        "wavelength_nm_nominal_excitation",
        "wavelength_nm_excitation",
        "wavelength_nm_emission",
        "reference_url",
        "responsible"
    )
    list_editable = ("available",) # "notes"
    save_on_top = True
    fieldsets = [
        ("Primary key", {"fields": [
            "annot_id",
            "lincs_identifier",
            "cstain",
            "common_name",
            "provider",
            "provider_catalog_id",
            "provider_batch_id"
        ]}),
        ("Detail", {"fields": [
            "target_display",
            "target",
            "crossabsorption_display",
            "crossabsorption",
            "dye",
            "purity",
            "lyophilized"
        ]}),
        ("Laboratory", {"fields": [
            "available",
            "stocksolution_buffer_compound_display",
            "stocksolution_buffer_compound",
            "stocksolution_concentration_value",
            "stocksolution_concentration_unit",
            "final_concentration_unit",
            "time_unit",
        ]}),
        ("Microscopy", {"fields": [
            "microscopy_channel",
            "wavelength_nm_nominal_excitation",
            "wavelength_nm_excitation",
            "wavelength_nm_emission"
        ]}),
        ("Reference", {"fields" : [
            "reference_url",
            "responsible",
            "notes"
        ]}),
    ]
    readonly_fields = (
        "annot_id",
        "target_display",
        "crossabsorption_display",
        "stocksolution_buffer_compound_display",
    )
    actions = [
        "delete_selected",
        "download_brick_json",
        "download_brick_txt",
        "brick_load"
    ]

    ## json ##
    def download_brick_json(self, request, queryset):
        # call appsabrick view export function
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=brick&choice=cstain"
        ))
    download_brick_json.short_description = "Download cstain page as json file (item selection irrelevant)"

    ## tsv ##
    def download_brick_txt(self, request, queryset):
        # call appsabrick view export function
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=brick&choice=cstain"
        ))
    download_brick_txt.short_description = "Download cstain page as tsv file (item selection irrelevant)"

    # action pull latest bricks
    def brick_load(self, request, queryset):  # self = modeladmin
        # python manage.py brick_pull s_brick_type
        o_out = io.StringIO()
        management.call_command(
            "brick_load",
            "cstain",
            stdout=o_out,
            verbosity=0
        )
        s_out = o_out.getvalue()
        s_out = s_out.strip()
        if (s_out == ""):
            self.message_user(
                request,
                "Cstain # successfully bricked.",
                level=messages.SUCCESS
            )
        elif re.match(".*Error.*", s_out):
            self.message_user(
                request, "Cstain # {}".format(s_out),
                level=messages.ERROR
            )
        elif re.match(".*Warning.*", s_out):
            self.message_user(
                request, "Cstain # {}".format(s_out),
                level=messages.WARNING
            )
        else:
            self.message_user(
                request,
                "Cstain # {}".format(s_out),
                level=messages.INFO
            )
    brick_load.short_description = "Upload cstain bricks (item selection irrelevant)"
# register
admin.site.register(CstainBrick, CstainBrickAdmin)
