from django.db import models

# import form django
from django.core.management.base import CommandError

# import form annot
from appbrxperson.models import PersonBrick
from apponcompound_ebi.models import Compound
from appondye_own.models import Dye
from appongeneontology_go.models import GeneOntology
from apponprovider_own.models import Provider
from apponunit_bioontology.models import Unit
from appsabrick.structure import makeannotid, checkvoci, checkfraction, scomma2sbr, list2sbr, sbr2list
from prjannot.structure import retype

# constant
ls_column_compoundbrick = [
    "annot_id",
    "lincs_identifier",
    "compound",
    "common_name",
    "provider",
    "provider_catalog_id",
    "provider_batch_id",
    "purity",
    "lyophilized",
    "available",
    "stocksolution_buffer_compound",
    "stocksolution_concentration_value",
    "stocksolution_concentration_unit",
    "final_concentration_unit",
    "time_unit",
    "reference_url",
    "responsible",
    "notes"
]

# data structure
class CompoundBrick(models.Model):
    annot_id = models.SlugField(
        primary_key=True,
        max_length=256,
        verbose_name="Annot Identifier",
        help_text="Automatically generated."
    )
    lincs_identifier = models.CharField(
        max_length=256,
        verbose_name="Lincs identifier",
        help_text="Official lincs small molecule identifier for lincs prj related small molecule.",
        blank=True
    )
    compound = models.ForeignKey(
        Compound,
        default="not_yet_specified",
        verbose_name="Compound",
        help_text="Choose compound."
    )
    common_name = models.CharField(
        max_length=256,
        verbose_name="Descriptive name",
        help_text="Descriptive small molecule name.", blank=True
    )
    provider = models.ForeignKey(
        Provider,
        default="not_yet_specified",
        verbose_name="Provider",
        help_text="Choose provider."
    )
    provider_catalog_id = models.CharField(
        max_length=256,
        default="not_yet_specified",
        verbose_name="Provider catalog id",
        help_text="ID or catalogue number or name assigned to the compound by the vendor or provider."
    )
    provider_batch_id = models.CharField(
        max_length=256,
        default="not_yet_specified",
        verbose_name="Provider batch id",
        help_text="Batch or lot number assigned to the compound by the vendor or provider."
    )
    purity = models.FloatField(
        default=None,
        verbose_name="Purity",
        help_text="Purity fraction.", null=True,
        blank=True
    )
    lyophilized = models.NullBooleanField(
        default=None,
        verbose_name="Lyophilized deployment",
        help_text="Form in which the compound was shipped."
    )
    available = models.BooleanField(
        default=True,
        help_text="Is this reagent at stock in our laboratory?"
    )
    stocksolution_buffer_compound_display = models.TextField(
        help_text="Automatically generated from stocksolution_buffer_compound field.",
        null=True,
    )
    stocksolution_buffer_compound = models.ManyToManyField(
        Compound,
        related_name="CompoundBufferCompound",
        help_text="Choose all utilized buffer compounds."
    )
    stocksolution_concentration_value = models.FloatField(
        default=None,
        verbose_name="Stock solution concentration value",
        help_text="Value.",
        null=True,
        blank=True,
    )
    stocksolution_concentration_unit = models.ForeignKey(
        Unit,
        default="not_yet_specified",
        related_name="CompoundStockConcUnit",
        help_text="Default stocksolution concentration unit."
    )
    final_concentration_unit = models.ForeignKey(
        Unit,
        default="ug_per_mL_uo0000274",
        related_name="CompoundFinalConcUnit",
        help_text="Default incubation concentration unit."
    )
    time_unit = models.ForeignKey(
        Unit,
        default="hour_uo0000032",
        related_name="CompoundTimeUnit",
        help_text="Default incubation time unit."
    )
    reference_url = models.URLField(
        default="https://not.yet.specified",
        verbose_name="Compound reference URL",
        help_text="URL to the vendor or provider specific compound specification."
    )
    responsible = models.ForeignKey(
        PersonBrick,
        default="not_yet_specified",
        help_text="Annotator."
    )
    notes = models.TextField( # ph, storage temperature
        verbose_name="Notes",
        help_text="Additional this compound related notes. No commas but carriage return allowed.",
        blank=True
    )

    def save(self, *args, **kwargs):
        # primary key generator
        self.annot_id = makeannotid(
            s_base=str(self.compound.annot_id),
            s_provider=str(self.provider.annot_id),
            s_provider_catalog_id=str(self.provider_catalog_id),
            s_provider_batch_id=str(self.provider_batch_id)
        )
        # notes field
        self.purity = checkfraction(self.purity)
        if (type(self.notes) == list):
            self.notes = list2sbr(self.notes)
        elif (type(self.notes) == str):
            self.notes = scomma2sbr(self.notes)
        else:
            raise CommandError(
                "strange type {} for TextField transforantion".format(
                    type(self.notes)
                )
            )
        # many to many content display stocksolution_buffer_compound
        ls_stocksolution_buffer_compound = [o_stocksolution_buffer_compound.annot_id for o_stocksolution_buffer_compound in self.stocksolution_buffer_compound.all()]
        self.stocksolution_buffer_compound_display = str(ls_stocksolution_buffer_compound)
        # save
        super().save(*args, **kwargs)

    # output
    #__repr__ = __str__
    def __repr__(self):
        s_out = "\nCompoundBrick:\n"
        for s_column in ls_column_compoundbrick:
            s_out = "{}{}: {}\n".format(
                s_out,
                s_column,
                getattr(self, s_column)
            )
        return(s_out)

    def __str__(self):
        return(self.annot_id)

    class Meta:
        unique_together = ((
            "compound",
            "provider",
            "provider_catalog_id",
            "provider_batch_id"
        ),)
        ordering = ["annot_id"]
        verbose_name_plural = "Perturbation Compound"


# database dictionary handles
def compoundbrick_db2d(s_annot_id, ls_column=ls_column_compoundbrick):
    """
    annot_id is the only input needed
    """
    # empty output
    d_brick = {}

    # pull form annot db
    o_brick = CompoundBrick.objects.get(annot_id=s_annot_id)

    # write in to d obj and re-type
    for s_column in ls_column:
        s_value = str(getattr(o_brick, s_column))
        o_value = retype(s_value)
        d_brick.update({s_column: o_value})

    # many to many field handle
    ls_stocksolution_buffer_compound = [
        str(o_stocksolution_buffer.annot_id) for o_stocksolution_buffer in o_brick.stocksolution_buffer_compound.all()
    ]
    ls_stocksolution_buffer_compound.sort()
    d_brick.update({"stocksolution_buffer_compound": ls_stocksolution_buffer_compound})

    # notes handle
    lo_notes = sbr2list(o_brick.notes)
    d_brick.update({"notes": lo_notes})

    # out
    return(d_brick)


def compoundbrick_d2db(d_brick, ls_column=ls_column_compoundbrick):
    """
    internal methode
    """
    # get annot id
    print("\nCheck ctrl voci to be able to generate reagent annot_id.")
    o_compound = checkvoci(d_brick["compound"], Compound)
    o_provider = checkvoci(d_brick["provider"], Provider)
    s_provider_catalog_id = str(d_brick["provider_catalog_id"])
    s_provider_batch_id = str(d_brick["provider_batch_id"])
    s_annot_id = makeannotid(
        s_base=str(o_compound),
        s_provider=str(o_provider),
        s_provider_catalog_id=s_provider_catalog_id,
        s_provider_batch_id=s_provider_batch_id
    )
    print("ok", s_annot_id)

    # get annot db obj
    try:
        o_brick = CompoundBrick.objects.get(annot_id=s_annot_id)
    except CompoundBrick.DoesNotExist:
        o_brick = CompoundBrick(
            annot_id=s_annot_id,
            compound=o_compound,
            provider=o_provider,
            provider_catalog_id=s_provider_catalog_id,
            provider_batch_id=s_provider_batch_id
        )
        o_brick.save()

    # write dictionary into db obj
    for s_column in ls_column:
        print("Put into field:", s_column, ":", d_brick[s_column])
        # annot_id field
        if (s_column == "annot_id"):
            o_brick.annot_id = s_annot_id
        # foreigne key fields
        # get dictionary vocabulary controlled and into obj turned
        elif (s_column == "compound"):
            o_brick.compound = checkvoci(d_brick["compound"], Compound)
        elif (s_column == "provider"):
            o_brick.provider = checkvoci(d_brick["provider"], Provider)
        elif (s_column == "stocksolution_concentration_unit"):
            o_brick.stocksolution_concentration_unit = checkvoci(d_brick["stocksolution_concentration_unit"], Unit)
        elif (s_column == "final_concentration_unit"):
            o_brick.final_concentration_unit = checkvoci(d_brick["final_concentration_unit"], Unit)
        elif (s_column == "time_unit"):
            o_brick.time_unit = checkvoci(d_brick["time_unit"], Unit)
        elif (s_column == "responsible"):
            o_brick.responsible = checkvoci(d_brick["responsible"], PersonBrick)
        # many to many fields
        # get dictionary vocabulary controlled and into obj turned
        elif (s_column == "stocksolution_buffer_compound"):
            o_brick.stocksolution_buffer_compound.clear()
            [o_brick.stocksolution_buffer_compound.add(checkvoci(o_stocksolution_buffer, Compound)) for o_stocksolution_buffer in  d_brick["stocksolution_buffer_compound"]]
        else:
            # other fileds
            setattr(o_brick, s_column, d_brick[s_column])

    # save db obj into annot db
    o_brick.save()


# annot db json handels
def compoundbrick_db2objjson(ls_column=ls_column_compoundbrick):
    """
    this is main
    get json obj from the db. this is the main brick object for this reagent type.
    any brick file will be built form this object.
    """
    # set output variables
    dd_json = {}

    # get all annot_id
    ls_annotid =[]
    lo_brick = (CompoundBrick.objects.all())
    for o_brick in lo_brick:
        ls_annotid.append(str(o_brick.annot_id))
    ls_annotid.sort()

    # for each annot_id get json compatible dictionary with dbcontent
    for s_annotid in ls_annotid:
        # pull json compatible dictionary from annot db
        print("pull from database:", s_annotid)
        # get dictionary brick for annot_id
        d_brick = compoundbrick_db2d(s_annot_id=s_annotid, ls_column=ls_column)
        dd_json.update({s_annotid:d_brick})

    # output
    return(dd_json)


def compoundbrick_objjson2db(dd_json, ls_column=ls_column_compoundbrick):
    """
    this is main
    """
    # extract obj json into annot db
    for s_key in dd_json:
        d_json = dd_json[s_key]
        compoundbrick_d2db(d_brick=d_json, ls_column=ls_column)



#### c stain ####

# constant
ls_column_cstainbrick = [
    "annot_id",
    "lincs_identifier",
    "cstain",
    "common_name",
    "provider",
    "provider_catalog_id",
    "provider_batch_id",
    "target",
    "crossabsorption",
    "dye",
    "purity",
    "lyophilized",
    "available",
    "stocksolution_buffer_compound",
    "stocksolution_concentration_value",
    "stocksolution_concentration_unit",
    "final_concentration_unit",
    "time_unit",
    "microscopy_channel",
    "wavelength_nm_nominal_excitation",
    "wavelength_nm_excitation",
    "wavelength_nm_emission",
    "reference_url",
    "responsible",
    "notes"
]

# data structure
class CstainBrick(models.Model):
    annot_id = models.SlugField(
        primary_key=True,
        max_length=256,
        verbose_name="Annot Identifier",
        help_text="Automatically generated.",
        blank=True
    )
    lincs_identifier = models.CharField(
        max_length=256,
        verbose_name="Lincs identifier",
        help_text="Official lincs compound stain identifier for lincs prj related small molecule.",
        blank=True
    )
    cstain = models.ForeignKey(
        Compound,
        related_name="CstainBrickCstainCompound",
        default="not_yet_specified",
        verbose_name="Staining compound",
        help_text="Choose compound."
    )
    common_name = models.CharField(
        max_length=256,
        verbose_name="Descriptive name",
        help_text="Descriptive compound stain name.",
        blank=True
    )
    provider = models.ForeignKey(
        Provider,
        default="not_yet_specified",
        verbose_name="Provider",
        help_text="Choose provider."
    )
    provider_catalog_id = models.CharField(
        max_length=256,
        default="not_yet_specified",
        verbose_name="Provider catalog id",
        help_text="ID or catalogue number or name assigned to the staining compound by the vendor or provider."
    )
    provider_batch_id = models.CharField(
        max_length=256,
        default="not_yet_specified",
        verbose_name="Provider batch id",
        help_text="Batch or lot number assigned to the staining compound by the vendor or provider."
    )
    target_display = models.TextField(
        help_text="Automatically generated from target field.",
        null=True,
    )
    target = models.ManyToManyField(
        GeneOntology,
        related_name="CstainBrickTargetGeneOntology",
        verbose_name="Staining target",
        help_text="Choose."
    )
    crossabsorption_display = models.TextField(
        help_text="Automatically generated from crossabsorption field.",
        null=True,
    )
    crossabsorption = models.ManyToManyField(
        GeneOntology,
        related_name="CstainBrickCrossAbsorptionGeneOntology",
        verbose_name="Staining crossabsorption",
        help_text="Choose."
    )
    dye = models.ForeignKey(
        Dye,
        default="not_yet_specified",
        verbose_name="Staining dye",
        help_text="Choose fluophore. Terms should be compatible with http://www.spectra.arizona.edu/."
    )
    purity = models.FloatField(
        default=None,
        verbose_name="Purity",
        help_text="Purity fraction.",
        null=True,
        blank=True
    )
    lyophilized = models.NullBooleanField(
        default=None,
        verbose_name="Lyophilized deployment",
        help_text="Form in which the compound was shipped."
    )
    available = models.BooleanField(
        default=True,
        help_text="Is this reagent at stock in our laboratory?"
    )
    stocksolution_buffer_compound_display = models.TextField(
        help_text="Automatically generated from stocksolution_buffer_compound field.",
        null=True,
    )
    stocksolution_buffer_compound = models.ManyToManyField(
        Compound,
        related_name="CstainBufferCompound",
        help_text="Choose all utilized buffer compounds."
    )
    stocksolution_concentration_value = models.FloatField(
        default=None,
        verbose_name="Stock solution concentration value",
        help_text="Value.",
        null=True,
        blank=True,
    )
    stocksolution_concentration_unit = models.ForeignKey(
        Unit,
        default="not_yet_specified",
        related_name="CstainStockConcUnit",
        help_text="Default stocksolution concentration unit."
    )
    final_concentration_unit = models.ForeignKey(
        Unit,
        default="ug_per_mL_uo0000274",
        related_name="CstainFinalConcUnit",
        help_text="Default incubation concentration unit."
    )
    time_unit = models.ForeignKey(
        Unit,
        default="hour_uo0000032",
        related_name="CstainTimeUnit",
        help_text="Default incubation time unit."
    )
    microscopy_channel = models.SlugField(
        max_length=32,
        default="not_yet_specified",
        help_text="Default microscopy channel name. Usually simply an integer."
    )
    wavelength_nm_nominal_excitation = models.PositiveSmallIntegerField(
        default=None,
        verbose_name="Nominal excitation wavelength",
        help_text="Wavelength in nm.",
        null=True,
        blank=True
    )
    wavelength_nm_excitation = models.PositiveIntegerField(
        default=None,
        verbose_name="Excitation wavelength",
        help_text="Wave length in nm.",
        null=True,
        blank=True
    )
    wavelength_nm_emission = models.PositiveIntegerField(
        default=None,
        verbose_name="Emission wavelength",
        help_text="Wave length in nm.",
        null=True,
        blank=True
    )
    reference_url = models.URLField(
        default="https://not.yet.specified",
        verbose_name="Compound reference URL",
        help_text="URL to the vendor or provider specific staining compound specification."
    )
    responsible = models.ForeignKey(
        PersonBrick,
        default="not_yet_specified",
        help_text="Annotator."
    )
    notes = models.TextField(  # ph, stock concentration value, stock storage temperature
        verbose_name="Notes",
        help_text="Additional this compound related notes. Commas will be changed to carriage returns. Single quotes will be erased.",
        blank=True
    )
    def save(self, *args, **kwargs):
        # primary key generator
        self.annot_id = makeannotid(
            s_base=str(self.cstain.annot_id),
            s_provider=str(self.provider.annot_id),
            s_provider_catalog_id=str(self.provider_catalog_id),
            s_provider_batch_id=str(self.provider_batch_id)
        )
        # notes field
        self.purity = checkfraction(self.purity)
        if (type(self.notes) == list):
            self.notes = list2sbr(self.notes)
        elif (type(self.notes) == str):
            self.notes = scomma2sbr(self.notes)
        else:
            raise CommandError(
                "strange type {} for TextField transforantion".format(
                    type(self.notes)
                )
            )
        # many to many content display
        # target
        ls_target = [o_target.annot_id for o_target in self.target.all()]
        self.target_display = str(ls_target)
        # crossabsorption
        ls_crossabsorption = [o_crossabsorption.annot_id for o_crossabsorption in self.crossabsorption.all()]
        self.crossabsorption_display = str(ls_crossabsorption)
        # stocksolution_buffer_compound
        ls_stocksolution_buffer_compound = [o_stocksolution_buffer_compound.annot_id for o_stocksolution_buffer_compound in self.stocksolution_buffer_compound.all()]
        self.stocksolution_buffer_compound_display = str(ls_stocksolution_buffer_compound)
        # save
        super().save(*args, **kwargs)

    # output
    #__repr__ = __str__
    def __repr__(self):
        s_out = "\nCstainBrick:\n"
        for s_column in ls_column_cstainbrick:
            s_out = "{}{}: {}\n".format(
                s_out,
                s_column, getattr(self, s_column)
            )
        return(s_out)

    def __str__(self):
        return(self.annot_id)

    class Meta:
        unique_together = ((
            "cstain",
            "provider",
            "provider_catalog_id",
            "provider_batch_id"
        ),)
        ordering = ["annot_id"]
        verbose_name_plural = "Endpoint Compound Stain"


# database dictionary handles
def cstainbrick_db2d(s_annot_id, ls_column=ls_column_cstainbrick):
    """
    annot_id is the only input needed
    """
    #empty output
    d_brick = {}

    # pull form annot db
    o_brick = CstainBrick.objects.get(annot_id=s_annot_id)

    # write in to d obj and re-type
    for s_column in ls_column:
        s_value = str(getattr(o_brick, s_column))
        o_value = retype(s_value)
        d_brick.update({s_column: o_value})

    # many to many field handle
    ls_target = [
        str(o_geneontology.annot_id) for o_geneontology in o_brick.target.all()
    ]
    ls_target.sort()
    d_brick.update({"target": ls_target})
    ls_crossabsorption = [
        str(o_geneontology.annot_id) for o_geneontology in o_brick.crossabsorption.all()
    ]
    ls_crossabsorption.sort()
    d_brick.update({"crossabsorption": ls_crossabsorption})
    ls_stocksolution_buffer_compound = [
        str(o_stocksolution_buffer.annot_id) for o_stocksolution_buffer in o_brick.stocksolution_buffer_compound.all()
    ]
    ls_stocksolution_buffer_compound.sort()
    d_brick.update({"stocksolution_buffer_compound": ls_stocksolution_buffer_compound})

    # notes handle
    lo_notes = sbr2list(o_brick.notes)
    d_brick.update({"notes": lo_notes})

    # out
    return(d_brick)


def cstainbrick_d2db(d_brick, ls_column=ls_column_cstainbrick):
    """
    internal methode
    """
    # get annot id
    print("\nCheck ctrl voci to be able to generate reagent annot_id.")
    o_cstain = checkvoci(d_brick["cstain"], Compound)
    o_dye = checkvoci(d_brick["dye"], Dye)
    o_provider = checkvoci(d_brick["provider"], Provider)
    s_provider_catalog_id = str(d_brick["provider_catalog_id"])
    s_provider_batch_id = str(d_brick["provider_batch_id"])
    s_annot_id = makeannotid(
        s_base=str(o_cstain),
        s_provider=str(o_provider),
        s_provider_catalog_id=s_provider_catalog_id,
        s_provider_batch_id=s_provider_batch_id
    )
    print("ok", s_annot_id)

    # get annot db obj
    try:
        o_brick = CstainBrick.objects.get(annot_id=s_annot_id)
    except CstainBrick.DoesNotExist:
        o_brick = CstainBrick(
            annot_id=s_annot_id,
            cstain=o_cstain,
            dye=o_dye,
            provider=o_provider,
            provider_catalog_id=s_provider_catalog_id,
            provider_batch_id=s_provider_batch_id
        )
        o_brick.save()

    # write dictionary into db obj
    for s_column in ls_column:
        print("Put into field:", s_column, ":", d_brick[s_column])
        # annot_id field
        if (s_column == "annot_id"):
            o_brick.annot_id = s_annot_id
        # foreigne key fields
        # get dictionary vocabulary controlled and str into obj turned
        elif (s_column == "cstain"):
            o_brick.cstain = checkvoci(d_brick["cstain"], Compound)
        elif (s_column == "provider"):
            o_brick.provider = checkvoci(d_brick["provider"], Provider)
        elif (s_column == "dye"):
            o_brick.dye = checkvoci(d_brick["dye"], Dye)
        elif (s_column == "stocksolution_concentration_unit"):
            o_brick.stocksolution_concentration_unit = checkvoci(d_brick["stocksolution_concentration_unit"], Unit)
        elif (s_column == "final_concentration_unit"):
            o_brick.final_concentration_unit = checkvoci(d_brick["final_concentration_unit"], Unit)
        elif (s_column == "time_unit"):
            o_brick.time_unit = checkvoci(d_brick["time_unit"], Unit)
        elif (s_column == "responsible"):
            o_brick.responsible = checkvoci(d_brick["responsible"], PersonBrick)
        # get many to many
        # get dictionary vocabulary controlled and into obj turned
        elif (s_column == "target"):
            o_brick.target.clear()
            [o_brick.target.add(checkvoci(o_geneontology, GeneOntology)) for o_geneontology in d_brick["target"]]
        elif (s_column == "crossabsorption"):
            o_brick.crossabsorption.clear()
            [o_brick.crossabsorption.add(checkvoci(o_geneontology, GeneOntology)) for o_geneontology in d_brick["crossabsorption"]]
        elif (s_column == "stocksolution_buffer_compound"):
            o_brick.stocksolution_buffer_compound.clear()
            [o_brick.stocksolution_buffer_compound.add(checkvoci(o_stocksolution_buffer, Compound)) for o_stocksolution_buffer in  d_brick["stocksolution_buffer_compound"]]
        else:
            # other fields
            setattr(o_brick, s_column, d_brick[s_column])

    # save db obj into annot db
    o_brick.save()


# annot db json handels
def cstainbrick_db2objjson(ls_column=ls_column_cstainbrick):
    """
    this is main
    get json obj from the db. this is the main cstain brick object.
    any cstain brick file will be built form this object.
    """
    # set output variables
    dd_json = {}

    # get all annot_id
    ls_annotid =[]
    lo_brick = (CstainBrick.objects.all())
    for o_brick in lo_brick:
        ls_annotid.append(str(o_brick.annot_id))
    ls_annotid.sort()

    # for each annot_id get json compatible dictionary with dbcontent
    for s_annotid in ls_annotid:
        # pull json compatible dictionary from annot db
        print("pull from database:", s_annotid)
        # get dictionary brick for annot_id
        d_brick = cstainbrick_db2d(s_annot_id=s_annotid, ls_column=ls_column)
        dd_json.update({s_annotid:d_brick})

    # output
    return(dd_json)


def cstainbrick_objjson2db(dd_json, ls_column=ls_column_cstainbrick):
    """
    this is main
    """
    # extract obj json into annot db
    for s_key in dd_json:
        d_json = dd_json[s_key]
        cstainbrick_d2db(d_brick=d_json, ls_column=ls_column)
