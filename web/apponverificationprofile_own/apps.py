from django.apps import AppConfig


class ApponverificationprofileOwnConfig(AppConfig):
    name = 'apponverificationprofile_own'
