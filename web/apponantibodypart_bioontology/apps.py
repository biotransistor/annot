from django.apps import AppConfig


class ApponantibodypartBioontologyConfig(AppConfig):
    name = 'apponantibodypart_bioontology'
