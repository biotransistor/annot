from django import forms
from django.contrib import admin
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.utils.html import format_html
from app0investigation.models import Investigation
from app1study.models import Study
from app3runset.models import RunSet

# import from django selectable prj
from selectable.forms import AutoComboboxSelectMultipleWidget


# import form annot
from appbrxperson.lookups import PersonBrickLookup

# Register your models here.
#admin.site.register(Investigation)
class InvestigationForm(forms.ModelForm):
    class Meta:
        model = Investigation
        fields = ["responsible"]
        widgets = {
            "responsible": AutoComboboxSelectMultipleWidget(PersonBrickLookup),
        }

class InvestigationAdmin(admin.ModelAdmin):
    form = InvestigationForm
    search_fields = (
        "annot_id",
        "investigation_name",
        "url_release",
        "responsible__annot_id",
        "notes",
    )
    list_display = (
        "annot_id",
        "investigation_name",
        "date_submission",
        "url_release_click",
        "responsible_display",
        "notes",
    )
    save_on_top = True
    fieldsets = [
        ("Identification", { "fields" : [
            "annot_id",
            "investigation_name",
        ]}),
        ("Details", { "fields" : [
            "date_submission",
            "url_release",
            "responsible_display",
            "responsible",
            "notes",
        ]}),
    ]
    readonly_fields = (
        "annot_id",
        "responsible_display",
    )

    # url link clickable
    def url_release_click (self, obj):
        return format_html("<a href='{url}'>{url}</a>", url=obj.url_release)
    url_release_click.short_description = "Clickable experimentaldesigne URL"

    # action
    actions = [
        "delete_selected",
        "download_json",
        "download_brick_json",
        "download_acjson",
        "download_tsv",
        "download_brick_tsv",
        "download_brick_lincs",
        "resave",
    ]

    ### json download action ###
    def download_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=generic&choice=app0investigation.investigation"
        )
    )
    download_json.short_description = "Download this page as json file (item selection irrelevant)"

    def download_brick_json(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=investigation&filetype=json&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_json.short_description = "Download selected set releated bricks as json files"

    def download_acjson(self, request, queryset):
        es_runset = set()
        for s_investigation in request.POST.getlist(admin.ACTION_CHECKBOX_NAME):
            for s_study in Study.objects.filter(investigation=s_investigation):
                for o_runset in RunSet.objects.filter(study=s_study):
                    es_runset.add(o_runset.runset_name)
        return(HttpResponseRedirect(
            "/appacaxis/export_acjson?model=runset&filetype=acjson&choice={}".format(
                "!".join(es_runset)
            )
        ))
    download_acjson.short_description = "Download selected sets as acjson file"

    ### tsv download action ###
    def download_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=generic&choice=app0investigation.investigation"
        ))
    download_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

    def download_brick_tsv(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=investigation&filetype=tsv&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_tsv.short_description = "Download selected set releated bricks as tsv files"

    def download_brick_lincs(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=investigation&filetype=lincs&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_lincs.short_description = "Download selected set releated bricks as lincs data standard csv files"

    def resave(self, request, queryset):
        # process
        ls_save = []
        for o_record in Investigation.objects.all():
            o_record.save()
            ls_save.append(o_record.investigation_name)
        # message
        messages.success(
            request,
            "{} # resaved".format(ls_save),
        )
    resave.short_description = "Resave all record (item selection irrelevant)"

admin.site.register(Investigation, InvestigationAdmin)
