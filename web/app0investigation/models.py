from django.db import models

# import python
import re

# import from annot
from appbrxperson.models import PersonBrick

# create your models here.
# investigation model
class Investigation(models.Model):
    annot_id = models.SlugField(
        max_length=256,
        primary_key=True,
        verbose_name="Investigation identifier",
        help_text="Automatically generated from input fields."
    )
    investigation_name = models.SlugField (
        max_length=256,
        default="not_yet_specified",
        help_text="A concise name given to the investigation."
    )
    date_submission = models.DateField(
        default="1955-11-05",
        verbose_name="Investigation submission date",
        help_text="The date on which the investigation was reported to the repository."
    )
    url_release = models.URLField(
        max_length=256,
        default="https://not.yet.specified",
        verbose_name="Investigation release homepage",
        help_text="Main investigation related homepage."
    )
    responsible_display = models.TextField(
        help_text="Inverstigation related principal inverstigaters.",
        blank=True
    )
    responsible = models.ManyToManyField(
        PersonBrick,
        help_text="Person associated with the investigation."
    )
    notes = models.TextField(
        help_text="Investigation related notes.",
        blank=True
    )

    # primary key generator
    def save(self, *args, **kwargs):
        self.annot_id = re.sub(r"[^a-z0-9-]", "", self.investigation_name.lower())
        # set content display
        self.responsible_display = str(
            [o_responsible.annot_id for o_responsible in self.responsible.all()]
        )
        # save
        super().save(*args, **kwargs)

    # output
    def __str__(self):
        return(self.investigation_name)

    __repr__ = __str__

    class Meta:
        ordering = ["-date_submission"]
