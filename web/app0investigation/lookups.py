# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from app0investigation.models import Investigation

# code
class InvestigationLookup(ModelLookup):
    model = Investigation
    search_fields = ('annot_id__icontains',)
registry.register(InvestigationLookup)
