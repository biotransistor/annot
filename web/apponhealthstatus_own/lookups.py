# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponhealthstatus_own.models import HealthStatus

# code
class HealthStatusLookup(ModelLookup):
    model = HealthStatus
    search_fields = ('annot_id__icontains',)
registry.register(HealthStatusLookup)

