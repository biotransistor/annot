from django.db import models

# import form python
import os
import re

# import from annot
# ontologies
from apponsample_cellosaurus.models import Sample
from apponprovider_own.models import Provider
from apponsampleentity_own.models import SampleEntity
from apponorganism_bioontology.models import Organism
from apponsex_bioontology.models import Sex
from apponethnicity_bioontology.models import Ethnicity
from apponhealthstatus_own.models import HealthStatus
from appondisease_bioontology.models import Disease
from apponorgan_bioontology.models import Organ
from appontissue_bioontology.models import Tissue
from apponcelltype_bioontology.models import CellType
from appongrowthproperty_bioontology.models import GrowthProperty
from apponverificationprofile_own.models import VerificationProfile
from appongeneticmodification_bioontology.models import GeneticModification
from appontransientmodification_bioontology.models import TransientModification
from apponunit_bioontology.models import Unit
# bridges
from appbrxperson.models import PersonBrick
# others
from appsabrick.structure import makeannotid, checkvoci, scomma2sbr, list2sbr, sbr2list
from prjannot.settings import MEDIA_ROOT #, MEDIA_ROOTQUARANTINE, MEDIA_ROOTURL, MEDIA_UR
from prjannot.structure import retype


# Create your models here.
ls_column_humanbrick = [
    "annot_id",
    "lincs_identifier",
    "sampleparent",
    "sample",
    "common_name",
    "provider",
    "provider_catalog_id",
    "provider_batch_id",
    "sampleentity",
    "organism",
    "sex",
    "ethnicity",
    "age_year",
    "health_status",
    "disease",
    "organ",
    "tissue",
    "cell_type",
    "growth_propertie",
    "cell_marker_reference",
    "verification_profile_reference",
    "verification_profile",
    "genetic_modification",
    "transient_modification",
    "available",
    "concentration_unit",
    "time_unit",
    "date",
    "responsible",
    "reference_url",
    "publication_file",
    "notes"
]
#"passage_origin",
#"passage_use",

# sample
class HumanBrick(models.Model):
    # sample roots
    sampleparent = models.ForeignKey(
        "self",
        default="not_yet_specified-notyetspecified_notyetspecified_notyetspecified", # "not_yet_specified-notyetspecified_notyetspecified_notyetspecified_0xnot|yet|specified",
        verbose_name="Sample parent",
        help_text="Parent primary cell or cell line to bundle related cells.",
        null=True,
        blank=True
    )
    # label
    annot_id = models.SlugField(
        max_length=256,
        primary_key=True,
        help_text="Automatically generated unique identifier."
    )
    lincs_identifier = models.CharField(
        max_length=256,
        verbose_name="Lincs identifier",
        help_text="Official lincs sample identifier for lincs prj related samples.",
        blank=True
    )
    sample = models.ForeignKey(
        Sample,
        default="not_yet_specified",
        help_text="The name for the primary cells or cell line."
    )
    common_name = models.CharField(
        max_length=256,
        verbose_name="Descriptive name",
        help_text="Descriptive sample name.",
        blank=True
    )
    # source provider
    provider = models.ForeignKey(
        Provider,
        default="not_yet_specified",
        help_text="Choose provider."
    )
    provider_catalog_id = models.CharField(
        default="not_yet_specified",
        verbose_name="Provider catalog id",
        max_length=256,
        help_text="ID or catalogue number or name assigned to the cell by the vendor or provider."
    )
    provider_batch_id = models.CharField(
        default="not_yet_specified",
        verbose_name="Provider batch id",
        max_length=256,
        help_text="Batch or lot number assigned to the cell by the vendor or provider."
    )
    #passage_origin = models.CharField(
    #    max_length=32,
    #    default=0,
    #    verbose_name="Origin passage",
    #    help_text="The passage or passages, separated by pipe symbols, our lab received the sample."
    #)
    #passage_use = models.CharField(
    #    max_length=32,
    #    default="not_yet_specified",
    #    verbose_name="Used passages",
    #    help_text="The passage or passages used, separated by pipe symbols."
    #)
    # biology
    sampleentity = models.ForeignKey(
        SampleEntity,
        default="not_yet_specified",
        help_text="e.g. whole organism, organism part"
    )
    organism = models.ForeignKey(
        Organism,
        default="not_yet_specified",
        verbose_name="Organism",
        help_text="Organism of origin from which the cell was derived."
    )
    sex = models.ForeignKey(
        Sex,
        default="not_yet_specified",
        verbose_name="Sex",
        help_text="The sex of the organism from which the cell was obtained."
    )
    ethnicity_display = models.TextField(
        help_text="Automatically generated from ethnicity field.",
        null=True,
    )
    ethnicity = models.ManyToManyField(
        Ethnicity,
        verbose_name="Ethnicity",
        help_text="The ethnicity of the donor."
    )
    age_year = models.FloatField(
        default=None,
        verbose_name="Age [year]",
        help_text="The age of the donor.",
        null=True,
        blank=True
    )
    health_status = models.ForeignKey(
        HealthStatus,
        default="not_yet_specified",
        verbose_name="Health status",
        help_text="The health status: healthy sample from a healthy person, normal sample from a ill person, disease sample."
    )
    disease_display = models.TextField(
        help_text="Automatically generated from disease field.",
        null=True,
    )
    disease = models.ManyToManyField(
        Disease,
        verbose_name="Disease",
        help_text="If the primary cell came from a particular diseased tissue, the disease should be noted."
    )
    organ = models.ForeignKey(
        Organ,
        default="not_yet_specified",
        verbose_name="Organ",
        help_text="Organ of origin from which the cell was derived."
    )
    tissue = models.ForeignKey(
        Tissue, default="not_yet_specified",
        verbose_name="Tissue",
        help_text="Tissue of origin from which the cell was derived."
    )
    cell_type = models.ForeignKey(
        CellType,
        default="not_yet_specified",
        verbose_name="Cell type",
        help_text="The cell type from which a cell was derived, also sometimes referred to as cell morphology."
    )
    growth_propertie_display = models.TextField(
        help_text="Automatically generated from growth_propertie field.",
        null=True,
    )
    growth_propertie = models.ManyToManyField(
        GrowthProperty,
        verbose_name="Growth property",
        help_text="The growth properties of the cell."
    )
    # biological identification
    cell_marker_reference = models.URLField(
        default="https://not.yet.specified",
        verbose_name="Cell marker reference URL",
        help_text="Link to the markers used to isolate and identify the cell type."
    )
    verification_profile_reference_display= models.TextField(
        help_text="Automatically generated from verification_profile_reference field.",
        null=True,
    )
    verification_profile_reference = models.ManyToManyField(
        VerificationProfile,
        related_name="VerificationProfileReference",
        verbose_name="Verification profile reference",
        help_text="Information pertaining to expected STR (reference) profile of the cell based on provider information."
    )
    verification_profile_display= models.TextField(
        help_text="Automatically generated verification_profile field.",
        null=True,
    )
    verification_profile = models.ManyToManyField(
        VerificationProfile,
        related_name="VerificationProfile",
        verbose_name="Verification profile",
        help_text="Information pertaining to experimental verification of the primary cell identity STR profile."
    )
    # biological engineering
    genetic_modification_display= models.TextField(
        help_text="Automatically generated from genetic_modification field.",
        null=True,
    )
    genetic_modification = models.ManyToManyField(
        GeneticModification,
        verbose_name="Genetic modification",
        help_text="If the cell was  stable transfection or viral transduction, the modifications (e.g. expressing GFP-tagged protein) should be described and appropriate references provided."
    )
    transient_modification_display= models.TextField(
        help_text="Automatically generated from transient_modificationfield.",
        null=True,
    )
    transient_modification = models.ManyToManyField(
        TransientModification,
        verbose_name="Transient modification",
        help_text="Transient transfection or viral transduction."
    )
    # lab notes
    available = models.BooleanField(
        default=True,
        help_text="Is this sample at stock in our laboratory?"
    )
    concentration_unit = models.ForeignKey(
        Unit,
        default="cells_per_mL_uo0000201",
        related_name="HumanConcUnit",
        help_text="Default seeding concentration unit."
    )
    time_unit = models.ForeignKey(
        Unit,
        default="hour_uo0000032",
        related_name="HumanTimeUnit",
        help_text="Default incubation time unit."
    )
    date = models.DateField(
        default="1955-11-05",
        help_text="The last date when the verification profile or genetic or transient modification was done."
    )
    responsible = models.ForeignKey(
        PersonBrick,
        default="not_yet_specified",
        help_text="The person who did the verification profile or genetic or transient modification."
    )
    # literature
    reference_url = models.URLField(
        default="https://not.yet.specified",
        verbose_name="Reference URL",
        help_text="Link to provider input."
    )
    publication_file_overwrite = models.BooleanField(
        default= False,
        verbose_name="overwrite file",
        help_text = "if by upload a file with the same name already exist, should the exiting file be overwritten (True) or the existing file be linked (False)? Default is None which will throw an error with detailed informations about the files.",
    )
    publication_file = models.FileField(
        upload_to="upload/samplebrick/",
        help_text="Upload publication where the cell line for the fist time was mentioned.",
        blank=True
    )
    # notes
    notes = models.TextField(
        help_text="Any additional information worth to mention.",
        blank=True
    )

    def save(self, *args, **kwargs):
        # primary key generator
        #self.passage_use = re.sub(r'[^a-zA-Z0-9]', '|', str(self.passage_use))
        #s_annot_id = makeannotid(
        self.annot_id = makeannotid(
            s_base=str(self.sample.annot_id),
            s_provider=str(self.provider.annot_id),
            s_provider_catalog_id=str(self.provider_catalog_id),
            s_provider_batch_id=str(self.provider_batch_id)
        )
        #self.annot_id = "{}_{}x{}".format(
        #    s_annot_id,
        #    self.passage_origin,
        #    self.passage_use
        #)
        # file handling
        if not (self.publication_file.name is None):
            self.publication_file.name = re.sub(r"[^a-zA-Z0-9_./]", "", self.publication_file.name)
            if (os.path.isfile("{}upload/sample/{}".format(MEDIA_ROOT, self.publication_file.name))) \
               and not (self.publiaction_file_overwrite):
                raise CommandError(
                    "Error at appbrsamplehuman.models: Overwrite_file is set to False and a file with name upl\
oad/sample/{} alreday exist.".format(
                        self.publiaction_file.name
                    )
                )
            if (os.path.isfile("{}upload/sample/{}".format(MEDIA_ROOT, self.publication_file.name))):
                os.remove("{}upload/sample/{}".format(MEDIA_ROOT, self.publication_file.name))
            self.publication_file_overwrite = False
        # note field
        if (type(self.notes) == list):
            self.notes = list2sbr(self.notes)
        elif (type(self.notes) == str):
            self.notes = scomma2sbr(self.notes)
        else:
            raise CommandError(
                "strange type {} for TextField transformation".format(
                    type(self.notes)
                )
            )
        # many to many content display
        # ethnicity
        ls_ethnicity = [o_ethnicity.annot_id for o_ethnicity in self.ethnicity.all()]
        self.ethnicity_display = str(ls_ethnicity)
        # disease
        ls_disease = [o_disease.annot_id for o_disease in self.disease.all()]
        self.disease_display = str(ls_disease)
        # growth_propertie
        ls_growth_propertie = [o_growth_propertie.annot_id for o_growth_propertie in self.growth_propertie.all()]
        self.growth_propertie_display = str(ls_growth_propertie)
        # verification_profile_reference
        ls_verification_profile_reference = [o_verification_profile_reference.annot_id for o_verification_profile_reference in self.verification_profile_reference.all()]
        self.verification_profile_reference_display = str(ls_verification_profile_reference)
        # verification_profile
        ls_verification_profile = [o_verification_profile.annot_id for o_verification_profile in self.verification_profile.all()]
        self.verification_profile_display = str(ls_verification_profile)
        # genetic_modification
        ls_genetic_modification = [o_genetic_modification.annot_id for o_genetic_modification in self.genetic_modification.all()]
        self.genetic_modification_display = str(ls_genetic_modification)
        # transient_modification
        ls_transient_modification = [o_transient_modification.annot_id for o_transient_modification in self.transient_modification.all()]
        self.transient_modification_display = str(ls_transient_modification)
        # save
        super().save(*args, **kwargs)

    # output
    #__repr__ = __str__
    def __repr__(self):
        s_out = "\nHumanBrick:\n"
        for s_column in ls_column_humanbrick:
            s_out = "{}{}: {}\n".format(
                s_out,
                s_column,
                getattr(self, s_column)
            )
        return(s_out)

    def __str__(self):
        return(self.annot_id)

    class Meta:
        unique_together = ((
            "sample",
            "provider",
            "provider_catalog_id",
            "provider_batch_id"
        ),)
        #"passage_origin",
        #"passage_use"
        ordering = ["annot_id"]
        verbose_name_plural = "Sample Homo Sapiens"


# database dictionary handles
def humanbrick_db2d(s_annot_id, ls_column=ls_column_humanbrick):
    """
    annot_id is the only input needed
    """
    # empty output
    d_brick = {}

    # pull form annot db
    o_brick = HumanBrick.objects.get(annot_id=s_annot_id)

    # write in to d obj and re-type
    for s_column in ls_column:
        s_value = str(getattr(o_brick, s_column))
        o_value = retype(s_value)
        d_brick.update({s_column: o_value})

    # many to many field handle
    # ethnicity
    ls_ethnicity = [
        str(o_loop.annot_id) for o_loop in o_brick.ethnicity.all()
    ]
    ls_ethnicity.sort()
    d_brick.update({"ethnicity": ls_ethnicity})
    # disease
    ls_disease = [
        str(o_loop.annot_id) for o_loop in o_brick.disease.all()
    ]
    ls_disease.sort()
    d_brick.update({"disease": ls_disease})
    # growth_property
    ls_growth_propertie = [
        str(o_loop.annot_id) for o_loop in o_brick.growth_propertie.all()
    ]
    ls_growth_propertie.sort()
    d_brick.update({"growth_propertie": ls_growth_propertie})
    # verification_profile_reference
    ls_verification_profile_reference = [
        str(o_loop.annot_id) for o_loop in o_brick.verification_profile_reference.all()
    ]
    ls_verification_profile_reference.sort()
    d_brick.update({"verification_profile_reference": ls_verification_profile_reference})
    # verification_profile
    ls_verification_profile = [
        str(o_loop.annot_id) for o_loop in o_brick.verification_profile.all()
    ]
    ls_verification_profile.sort()
    d_brick.update({"verification_profile": ls_verification_profile})
    # genetic_modification
    ls_genetic_modification = [
        str(o_loop.annot_id) for o_loop in o_brick.genetic_modification.all()
    ]
    ls_genetic_modification.sort()
    d_brick.update({"genetic_modification": ls_genetic_modification})
    # transient_modification
    ls_transient_modification = [
        str(o_loop.annot_id) for o_loop in o_brick.transient_modification.all()
    ]
    ls_transient_modification.sort()
    d_brick.update({"transient_modification": ls_transient_modification})

    # notes handle
    lo_notes = sbr2list(o_brick.notes)
    d_brick.update({"notes": lo_notes})

    # out
    return(d_brick)


def humanbrick_d2db(d_brick, ls_column=ls_column_humanbrick):
    """
    internal method
    """
    # get annot id
    print("\nCheck ctrl voci to be able to generate reagent annot_id.")
    o_sample = checkvoci(d_brick["sample"], Sample)
    o_provider = checkvoci(d_brick["provider"], Provider)
    s_provider_catalog_id = str(d_brick["provider_catalog_id"])
    s_provider_batch_id = str(d_brick["provider_batch_id"])
    #s_passage_origin = str(d_brick["passage_origin"])
    #s_passage_use = re.sub(r'[^a-zA-Z0-9]', '|', str(d_brick["passage_use"]))
    s_annot_id = makeannotid(
        s_base=str(o_sample),
        s_provider=str(o_provider),
        s_provider_catalog_id=s_provider_catalog_id,
        s_provider_batch_id=s_provider_batch_id
    )
    #s_annot_id = "{}_{}x{}".format(
    #    s_annot_id,
    #    s_passage_origin,
    #    s_passage_use
    #)
    print("ok", s_annot_id)

    # get annot db obj
    try:
        o_brick = HumanBrick.objects.get(annot_id=s_annot_id)
    except HumanBrick.DoesNotExist:
        o_brick = HumanBrick(
            annot_id=s_annot_id,
            sample=o_sample,
            provider=o_provider,
            provider_catalog_id=s_provider_catalog_id,
            provider_batch_id=s_provider_batch_id,
        )
        #passage_origin=s_passage_origin,
        #passage_use=s_passage_use
        o_brick.save()

    # write dictionary into db obj
    for s_column in ls_column:
        print("Put into field:", s_column, ":", d_brick[s_column])
        # sampleparent
        if (s_column == "sampleparent"):
            o_brick.sampleparent = checkvoci(d_brick["sampleparent"], HumanBrick)
        # annot_id field
        elif (s_column == "annot_id"):
            o_brick.annot_id = s_annot_id
        # foreign key fields
        # get dictionary vocabulary controlled and into obj turned
        # sample
        elif (s_column == "sample"):
            o_brick.sample = checkvoci(d_brick["sample"], Sample)
        # provider
        elif (s_column == "provider"):
            o_brick.provider = checkvoci(d_brick["provider"], Provider)
        # entity
        elif (s_column == "sampleentity"):
            o_brick.sampleentity = checkvoci(d_brick["sampleentity"], SampleEntity)
        # organism
        elif (s_column == "organism"):
            o_brick.organism = checkvoci(d_brick["organism"], Organism)
        # sex
        elif (s_column == "sex"):
            o_brick.sex = checkvoci(d_brick["sex"], Sex)
        # health_status
        elif (s_column == "health_status"):
            o_brick.health_status = checkvoci(d_brick["health_status"], HealthStatus)
        # organ
        elif (s_column == "organ"):
            o_brick.organ = checkvoci(d_brick["organ"], Organ)
        # tissue
        elif (s_column == "tissue"):
            o_brick.tissue = checkvoci(d_brick["tissue"], Tissue)
        # cell_type
        elif (s_column == "cell_type"):
            o_brick.cell_type = checkvoci(d_brick["cell_type"], CellType)
        # concentration_unit
        elif (s_column == "concentration_unit"):
            o_brick.concentration_unit = checkvoci(d_brick["concentration_unit"], Unit)
        # time_unit
        elif (s_column == "time_unit"):
            o_brick.time_unit = checkvoci(d_brick["time_unit"], Unit)
        # responsible
        elif (s_column == "responsible"):
            o_brick.responsible = checkvoci(d_brick["responsible"], PersonBrick)
        # many to many fields
        # ethnicity
        elif (s_column == "ethnicity"):
            o_brick.ethnicity.clear()
            [o_brick.ethnicity.add(checkvoci(o_loop, Ethnicity)) for o_loop in  d_brick["ethnicity"]]
        # disease
        elif (s_column == "disease"):
            o_brick.disease.clear()
            [o_brick.disease.add(checkvoci(o_loop, Disease)) for o_loop in  d_brick["disease"]]
        # growth_property
        elif (s_column == "growth_propertie"):
            o_brick.growth_propertie.clear()
            [o_brick.growth_propertie.add(checkvoci(o_loop, GrowthProperty)) for o_loop in  d_brick["growth_propertie"]]
        # verification_profile_reference
        elif (s_column == "verification_profile_reference"):
            o_brick.verification_profile_reference.clear()
            [o_brick.verification_profile_reference.add(checkvoci(o_loop, VerificationProfile)) for o_loop in  d_brick["verification_profile_reference"]]
        # verification_profile
        elif (s_column == "verification_profile"):
            o_brick.verification_profile.clear()
            [o_brick.verification_profile.add(checkvoci(o_loop, VerificationProfile)) for o_loop in  d_brick["verification_profile"]]
        # genetic_modification
        elif (s_column == "genetic_modification"):
            o_brick.genetic_modification.clear()
            [o_brick.genetic_modification.add(checkvoci(o_loop, GeneticModification)) for o_loop in  d_brick["genetic_modification"]]
        # transient_modification
        elif (s_column == "transient_modification"):
            o_brick.transient_modification.clear()
            [o_brick.transient_modification.add(checkvoci(o_loop, TransientModification)) for o_loop in  d_brick["transient_modification"]]

        else:
            # other fields
            setattr(o_brick, s_column, d_brick[s_column])

    # save db obj into annot db
    o_brick.save()



# annot db json handels
def humanbrick_db2objjson(ls_column=ls_column_humanbrick):
    """
    this is main
    get json obj from the db. this is the main brick object for this sample type.
    any brick file will be built form this object.
    """
    # set output variables
    dd_json = {}

    # get all annot_id
    ls_annotid =[]
    lo_brick = (HumanBrick.objects.all())
    for o_brick in lo_brick:
        ls_annotid.append(str(o_brick.annot_id))
    ls_annotid.sort()

    # for each annot_id get json compatible dictionary with dbcontent
    for s_annotid in ls_annotid:
        # pull json compatible dictionary from annot db
        print("pull from database:", s_annotid)
        # get dictionary brick for annot_id
        d_brick = humanbrick_db2d(s_annot_id=s_annotid, ls_column=ls_column)
        dd_json.update({s_annotid:d_brick})

    # output
    return(dd_json)

def humanbrick_objjson2db(dd_json, ls_column=ls_column_humanbrick):
    """
    this is main
    """
    # extract obj json into annot db
    for s_key in dd_json:
        d_json = dd_json[s_key]
        humanbrick_d2db(d_brick=d_json, ls_column=ls_column)
