# import from django
from django import forms
from django.contrib import admin
from django.contrib import messages
from django.core import management
from django.http import HttpResponseRedirect

# python
import io
import re

# import from django selectable prj
from selectable.forms import AutoComboboxSelectWidget, AutoComboboxSelectMultipleWidget

# import from annot
from appbrsamplehuman.models import HumanBrick
from appbrsamplehuman.lookups import HumanBrickLookup
from appbrxperson.lookups import PersonBrickLookup
from apponcelltype_bioontology.lookups import CellTypeLookup
from appondisease_bioontology.lookups import DiseaseLookup
from apponethnicity_bioontology.lookups import EthnicityLookup
from appongrowthproperty_bioontology.lookups import GrowthPropertyLookup
from appongeneticmodification_bioontology.lookups import GeneticModificationLookup
from apponhealthstatus_own.lookups import HealthStatusLookup
from apponorgan_bioontology.lookups import OrganLookup
from apponorganism_bioontology.lookups import OrganismLookup
from apponprovider_own.lookups import ProviderLookup
from apponsample_cellosaurus.lookups import SampleLookup
from apponsampleentity_own.lookups import SampleEntityLookup
from apponsex_bioontology.lookups import SexLookup
from appontissue_bioontology.lookups import TissueLookup
from appontransientmodification_bioontology.lookups import TransientModificationLookup
from apponverificationprofile_own.lookups import VerificationProfileLookup
from apponunit_bioontology.lookups import UnitLookup

# Register your models here.
# sample
#admin.site.register(HumanBrick)
class HumanBrickForm(forms.ModelForm):
    class Meta:
        model = HumanBrick
        # "sampleparent"
        fields = [
            "sample",
            "provider",
            "sampleentity",
            "organism",
            "sex",
            "ethnicity",
            "health_status",
            "disease",
            "organ",
            "tissue",
            "cell_type",
            "growth_propertie",
            "verification_profile_reference",
            "verification_profile",
            "genetic_modification",
            "transient_modification",
            "concentration_unit",
            "time_unit",
            "responsible",

        ]
        #"sampleparent": AutoComboboxSelectWidget(HumanBrickLookup),
        widgets = {
            "sample": AutoComboboxSelectWidget(SampleLookup),
            "provider": AutoComboboxSelectWidget(ProviderLookup),
            "sampleentity": AutoComboboxSelectWidget(SampleEntityLookup),
            "organism": AutoComboboxSelectWidget(OrganismLookup),
            "sex": AutoComboboxSelectWidget(SexLookup),
            "ethnicity": AutoComboboxSelectMultipleWidget(EthnicityLookup),
            "health_status": AutoComboboxSelectWidget(HealthStatusLookup),
            "disease": AutoComboboxSelectMultipleWidget(DiseaseLookup),
            "organ": AutoComboboxSelectWidget(OrganLookup),
            "tissue": AutoComboboxSelectWidget(TissueLookup),
            "cell_type": AutoComboboxSelectWidget(CellTypeLookup),
            "growth_propertie": AutoComboboxSelectMultipleWidget(GrowthPropertyLookup),
            "verification_profile_reference": AutoComboboxSelectMultipleWidget(VerificationProfileLookup),
            "verification_profile": AutoComboboxSelectMultipleWidget(VerificationProfileLookup),
            "genetic_modification": AutoComboboxSelectMultipleWidget(GeneticModificationLookup),
            "transient_modification": AutoComboboxSelectMultipleWidget(TransientModificationLookup),
            "concentration_unit": AutoComboboxSelectWidget(UnitLookup),
            "time_unit": AutoComboboxSelectWidget(UnitLookup),
            "responsible": AutoComboboxSelectWidget(PersonBrickLookup),
        }

class HumanBrickAdmin(admin.ModelAdmin):
    form = HumanBrickForm
    search_fields = (
        "sampleparent__annot_id",
        "annot_id",
        "sample__annot_id",
        "common_name",
        "provider__annot_id",
        "provider_catalog_id",
        "provider_batch_id",
        "sampleentity__annot_id",
        "organism__annot_id",
        "sex__annot_id",
        "ethnicity__annot_id",
        "health_status__annot_id",
        "disease__annot_id",
        "organ__annot_id",
        "tissue__annot_id",
        "cell_type__annot_id",
        "growth_propertie__annot_id",
        "verification_profile_reference__annot_id",
        "verification_profile__annot_id",
        "concentration_unit__annot_id",
        "time_unit__annot_id",
        "notes"
    )
    #date_hierarchy = "date"
    list_display = (
        "annot_id",
        "lincs_identifier",
        "sampleparent",
        "sample",
        "common_name",
        "provider",
        "provider_catalog_id",
        "provider_batch_id",
        #"passage_origin",
        #"passage_use",
        "available",
        "sampleentity",
        "organism",
        "sex",
        "ethnicity_display",
        "age_year",
        "health_status",
        "disease_display",
        "organ",
        "tissue",
        "cell_type",
        "growth_propertie_display",
        "cell_marker_reference",
        "verification_profile_reference_display",
        "verification_profile_display",
        "genetic_modification_display",
        "transient_modification_display",
        "concentration_unit",
        "time_unit",
        "date",
        "responsible",
        "reference_url",
        "publication_file",
        "notes",
    )
    list_editable = ("available",)
    save_on_top = True
    fieldsets = [
        # sample roots
        ("Roots", { "fields":[
            "sampleparent"
        ]}),
        # sample name
        ("Primary key", {"fields":[
            "annot_id",
            "lincs_identifier",
            "sample",
            "common_name",
            "provider",
            "provider_catalog_id",
            "provider_batch_id",
            #"passage_origin",
            #"passage_use"
        ]}),
        # biology features
        ("Biology features", {"fields":[
            "sampleentity",
            "organism",
            "sex",
            "ethnicity_display",
            "ethnicity",
            "age_year",
            "health_status",
            "disease_display",
            "disease",
            "organ",
            "tissue",
            "cell_type",
            "growth_propertie_display",
            "growth_propertie"
        ]}),
        # biological identification
        ("Biological identification", {"fields":[
            "cell_marker_reference",
            "verification_profile_reference_display",
            "verification_profile_reference",
            "verification_profile_display",
            "verification_profile"
        ]}),
        # biological engineering
        ("Biological engineering", {"fields":[
            "genetic_modification_display",
            "genetic_modification",
            "transient_modification_display",
            "transient_modification"
        ]}),
        ("Laboratory", {"fields":[
            "available",
            "concentration_unit",
            "time_unit",
            "date",
            "responsible"
        ]}),
        # literature
        ("Reference", {"fields":[
            "reference_url",
            "publication_file_overwrite",
            "publication_file",
        ]}),
        # notes
        ("Notes", {"fields":[
            "notes"
        ]}),
    ]
    readonly_fields = (
        "annot_id",
        "ethnicity_display",
        "disease_display",
        "growth_propertie_display",
        "verification_profile_reference_display",
        "verification_profile_display",
        "genetic_modification_display",
        "transient_modification_display",
    )

    # drop down menu
    actions = [
        "delete_selected",
        "download_brick_json",
        "download_brick_txt",
        "brick_load"
    ]

    ## json ##
    def download_brick_json( self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=brick&choice=human"
        ))
    download_brick_json.short_description = "Download human page as json file (item selection irrelevant)"

    ## tsv ##
    def download_brick_txt(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=brick&choice=human"
        ))
    download_brick_txt.short_description = "Download human page as tsv file (item selection irrelevant)"

    ## brick ##
    def brick_load( self, request, queryset):  # self = modeladmin
        o_out = io.StringIO()
        management.call_command(
            "brick_load",
            "human",
            stdout=o_out,
            verbosity=0
        )
        s_out = o_out.getvalue()
        s_out = s_out.strip()
        if (s_out == ""):
            self.message_user(request, "Human # successfully bricked.", level=messages.SUCCESS)
        elif re.match(".*Error.*", s_out):
            self.message_user(request, "Human # {}".format(s_out) , level=messages.ERROR)
        elif re.match(".*Warning.*", s_out):
            self.message_user(request, "Human # {}".format(s_out), level=messages.WARNING)
        else:
            self.message_user(request, "Human # {}".format(s_out), level=messages.INFO)
    brick_load.short_description = "Upload human bricks (item selection irrelevant)"

# register
admin.site.register(HumanBrick, HumanBrickAdmin)
