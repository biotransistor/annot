from django.apps import AppConfig


class AppongeneEnsemblConfig(AppConfig):
    name = 'appongene_ensembl'
