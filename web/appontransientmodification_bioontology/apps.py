from django.apps import AppConfig


class AppontransientmodificationBioontologyConfig(AppConfig):
    name = 'appontransientmodification_bioontology'
