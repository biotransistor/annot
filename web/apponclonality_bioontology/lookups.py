# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponclonality_bioontology.models import Clonality

# code
class ClonalityLookup(ModelLookup):
    model = Clonality
    search_fields = ('annot_id__icontains',)
registry.register(ClonalityLookup)

