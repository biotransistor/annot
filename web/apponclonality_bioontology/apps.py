from django.apps import AppConfig


class ApponclonalityBioontologyConfig(AppConfig):
    name = 'apponclonality_bioontology'
