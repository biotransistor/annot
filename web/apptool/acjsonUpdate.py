###
# title: acjsonUpdater.py
#
# author: bue
# date: 2018-05-05
# license: GPLv3
#
# input:
#     the acpipeTemplateCode_*.py and superset files have to be palced into
#     the following folderstructure:
#     + acaxis/
#     + superset/
#     + supersetfile/
#     + runset/
#
#     the acjsonUpdater.py file it self have to be placed at
#     the root of the folder structur
#
# run:
#     python3 acjsonUpdater.py
#
# description:
#     offline code that re-generates and as such updates all acjson by
#     re-running all acjson generating acpipeTemplateCode_*.py scripts
###

# load libery
import glob
import os

# const
s_python3 = "python3"

# processing
print('\nprocess acAxes.')
os.chdir("./acaxis/")
os.system('rm *_ac.json')
ls_file = glob.glob('./acpipeTemplateCode_*.py')
for s_file in ls_file:
    print('process:', s_file )
    os.system(f"{s_python3} {s_file}")
os.chdir("..")

print('\nprocess superSets.')
os.chdir("./superset/")
os.system('rm *_ac.json')
ls_file = glob.glob('./acpipeTemplateCode_*.py')
for s_file in ls_file:
    print('process:', s_file )
    os.system(f"{s_python3} {s_file}")
os.chdir("..")

print('\nprocess runSets.')
os.chdir("./runset")
os.system('rm *_ac.json')
ls_file = glob.glob('./acpipeTemplateCode_*.py')
for s_file in ls_file:
    print('process:', s_file )
    os.system(f"{s_python3} {s_file}")
os.chdir("..")

# return
print('ok')
