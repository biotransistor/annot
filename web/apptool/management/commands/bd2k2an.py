"""
title: bd2k2an.py
language: python3
author: bue
date: 2016-06-30
license GPLv3

description:
    for bd2k big data to knowledge
    pulls meplincs related lincs_ids from
    the dcic data coordination and integration center
    and maps them via annnot_id to the corresponding brick entries.

input: nop
output: brick tables
run:
    python manage.py bd2k2an
"""

# django
from django.core.management.base import BaseCommand, CommandError

# python library
import requests

# annot
from appbrreagentantibody.models import Antibody1Brick, Antibody2Brick
from appbrreagentcompound.models import CompoundBrick, CstainBrick
from appbrreagentprotein.models import ProteinSetBrick, ProteinBrick
from appbrsamplehuman.models import HumanBrick

# main
class Command(BaseCommand):
    args = "<no args...>"
    help = "This is a LINCS project specific command. Command checks via dcic web api for lincs_id updates and updates the corresponding annot bricks."

    def handle(self, *args, **options):
        i_total = 0
        # for each brick model
        for o_model in [
                Antibody1Brick,
                Antibody2Brick,
                CompoundBrick,
                CstainBrick,
                ProteinSetBrick,
                ProteinBrick,
                HumanBrick
            ]:
            self.stdout.write("\nprosess next model {}...".format(o_model))
            # for each brick
            for o_record in o_model.objects.all():

                # get and put lincs_id
                s_url = "http://dev3.ccs.miami.edu:8080/dcic/api/batchTolsm"
                #d_param = {"searchTerm": o_record.annot_id} # bue: less specific but performs ok
                d_param = {"searchTerm": "batchid:{}".format(o_record.annot_id)}  # bue: more specific but perforems really poorly

                # ping api
                i_limit = 0
                d_param.update({"limit": i_limit})
                o_r = requests.get(s_url, params=d_param)
                if  (o_r.status_code != 200):
                    raise CommandError("Error bd2k2an: cound not reach api {} {}".format(
                        s_url,
                        d_param,
                    ))

                # featch form api
                i_limit = o_r.json()["results"]["totalDocuments"]
                i_total += i_limit
                if (i_limit > 1):
                    raise CommandError("Error bd2k2an: api provides more then 1 document for this annotid querry {} {} {}".format(
                        s_url,
                        d_param,
                        o_r.json()["results"]["documents"]
                    ))

                # process data fetched
                s_linkid = None
                d_param.update({"limit": i_limit})
                o_r = requests.get(s_url, params=d_param)
                for d_document in o_r.json()["results"]["documents"]:  # 0 oder 1 document
                    s_linkid = d_document["lincsidentifier"]
                    o_record.lincs_identifier =  s_linkid
                    o_record.save()

                # screen output
                self.stdout.write("mapped id: {} to {}".format(
                    o_record.annot_id,
                    s_linkid,
                ))

        # screern output
        self.stdout.write("total ids mapped: {}".format(i_total))
