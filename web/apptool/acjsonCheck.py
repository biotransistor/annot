####
# title: acjsonCheck.py
# language: python3
# author: bue
# date: 2018-06-07
# license GPLv3
#
# input:
#     command line argument
#
# output:
#     YYYYmmdd_acjsoncheck.log file
#
# run examples:
#     python3 acjsonCheck.py mema-LI8C00201
#     python3 acjsonCheck.py mema-LI8C00201 mema-LI8C00202
#     python3 acjsonCheck.py superset
#     python3 acjsonCheck.py runsert
#     python3 acjsonCheck.py
#
# description:
#     annot v5 compatible offline implementation for checking
#     runset and superset acjson for for consestency with their
#     origin acaxis and superset acjson
####

# library
from acpipe_acjson import acjson as ac
import argparse
from datetime import datetime
import json
import os
import re
import sys

# main
def acjsoncheck():
    """
    run dmc
    """
    b_out = False

    # command line input handle
    parser = argparse.ArgumentParser(description="acjsonCheck.py annot v5 compatible offline checks runset and superset acjson against their origin acjson.")
    parser.add_argument(
        "setname",
        nargs='*',
        default=None,
        type=str,
        help="Assay runset or superset name <set_name ...>\nWorking example: python3 acjsonCheck.py mema-LI8C00201 mema-LI8C00202"
    )
    args = parser.parse_args()

    # get acjson file names
    ds_filepath = {}
    for s_path in {"acaxis", "superset","runset"}:
        for s_file in os.listdir("./{}".format(s_path)):
            if re.search("_ac.json$", s_file):
                try:
                    ds_filepath[s_file]
                    sys.exit("ambiguous set name {}.\neven if it is possible to give two times the same name, this can fuck up things. please change one of the setnames.".format(s_file))
                except KeyError:
                    s_setname = re.sub("^annot_acaxis-|^annot_superset-|^annot_runset-","", re.sub("_ac.json$","", s_file))
                    ds_filepath.update({s_setname : "{}/{}".format(s_path, s_file)})

    # expand  command line input
    es_setname =  set(args.setname)
    # empty
    if (len(es_setname) == 0):
        es_setname = {"superset","runset"}
    # superset
    if ("superset" in es_setname):
        for s_file in os.listdir("./superset"):
            if re.search("_ac.json$", s_file):
                s_setname = re.sub("^annot_superset-","", re.sub("_ac.json$","", s_file))
                es_setname.add(s_setname)
        es_setname.discard("superset")
    # runset
    if ("runset" in es_setname):
        for s_file in os.listdir("./runset"):
            if re.search("_ac.json$", s_file):
                s_setname = re.sub("^annot_runset-","", re.sub("_ac.json$","", s_file))
                es_setname.add(s_setname)
        es_setname.discard("runset")

    # generate log file
    s_logfile = f"{datetime.today().strftime('%Y%m%d')}_acjsoncheck.log"
    with open (s_logfile, "w") as f_log:
        f_log.write(f"{datetime.now().isoformat()} python3 acjsonCheck.py {args.setname}\n")

    # loop through setanmes
    for s_setname in sorted(es_setname):
        # load acjson
        print(f"\nload {ds_filepath[s_setname]} ...")
        with open (ds_filepath[s_setname], "r") as f_acjson:
            d_acpop = json.load(f_acjson)
        # check acid to filename
        if (d_acpop["acid"] != ds_filepath[s_setname].split("/")[-1]):
            print(
                "Warning @ acjsoncheck : acid {} differs from filename {}.".format(
                    d_acpop["acid"],
                    ds_filepath[s_setname].split("/")[-1]
                )
            )
        # check runtype
        elif not (d_acpop["runtype"] in {"annot_superset", "annot_runset"}):
            print(
                "Warning @ acjsoncheck : runtype {} is neider annot_runset nor annot_superset.".format(
                    d_acpop["runtype"]
                )
            )
        # get origin acjson names
        es_acorigin = set()
        for s_file in d_acpop["log"].split("+"):
            if re.search("_ac.json$", s_file.strip()):
                s_acorigin = re.sub("^annot_acaxis-|^annot_superset-","", re.sub("_ac.json$","", s_file.strip()))
                es_acorigin.add(s_acorigin)
        es_error = set()
        for s_acorigin in es_acorigin:
            print(f"handle {ds_filepath[s_acorigin]} ...")
            # load origin acjson
            with open (ds_filepath[s_acorigin], "r") as f_acjson:
                d_acorigin = json.load(f_acjson)
            # pop
            (d_acpop, es_error) = ac.pop_ac(
                d_acjson=d_acpop,
                d_acrecord=d_acorigin,
                ts_axis=ac.ts_ACAXIS,
                b_leaf_intersection=True,
                b_recordset_major=False,
                es_error=es_error,
            )
        # append to logfie
        with open (s_logfile, "a") as f_log:
            if (len(es_error) == 0):
                f_log.write(f"\n{s_setname}: ok\n")
            else:
                f_log.write(f"\n{s_setname}:\n")
                for s_error in es_error:
                    f_log.write(f"{s_error}\n")
    # output
    b_out = True
    return(b_out)

# main call
if __name__ == '__main__':
    b_out = acjsoncheck()
    print(b_out)
