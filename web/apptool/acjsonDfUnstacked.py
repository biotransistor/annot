####
# title: acjsonDfUnstacked.py
# language: python3
# author: bue
# date: 2018-06-08
# license GPLv3
#
# input:
#     command line argument
#
# output:
#     LI8C00201_an2omero.csv
#     annot_runset-mema-LI8C00201dataframetsv_unstacked.tsv
#
# run examples:
#     python3 acjsonCheck.py mema-LI8C00201
#     python3 acjsonCheck.py mema-LI8C00201 mema-LI8C00202
#     python3 acjsonCheck.py runsert
#
# description:
#     annot v5 compatible offline implementation to retrieve from
#     the command line unstacked datafarmes. the generic filename
#     and column headers will be tranlated to the house internal
#     aggred output stanadard.
####

# library
from acpipe_acjson import acjson as ac
import argparse
import json
import os
import pandas as pd
import re

# const header manipulation dictionary
ds_header = {
    "^endpoint-ss": "Stain-{}",
    "^perturbation-ds": "Drug-{}",
    "^perturbation-es": "Ecm-{}",
    "^perturbation-ls": "Ligand-{}",
    "^perturbation-ms": "Medium-{}",
    "^sample-cl": "CellLine-{}",
}

# main
def acjson2dfunstacked():
    """
    input:
    output:
    desciption:
        pulls annot run as unstacked dataframe
        transforms header according inhouse standard
    """
    b_out = False

    # command line input handle
    parser = argparse.ArgumentParser(description="an2ome pulls unstacked run dataframnes form annot.")
    parser.add_argument(
        "setname",
        nargs='+',
        default=None,
        type=str,
        help="Assay runset name <set_name ...>\nWorking example: python3 an2om.py mema-LI8X00519\nWorking example: python3 an2om.py mema-LI8X00519 mema-LI8X00520"
    )
    args = parser.parse_args()

    # get acjson file names
    ds_filepath = {}
    for s_path in {"runset"}:
        for s_file in os.listdir("./{}".format(s_path)):
            if re.search("_ac.json$", s_file):
                s_setname = re.sub("^annot_acaxis-|^annot_superset-|^annot_runset-","", re.sub("_ac.json$","", s_file))
                ds_filepath.update({s_setname : "{}/{}".format(s_path, s_file)})

    # expand  command line input
    es_setname =  set(args.setname)
    # runset
    if ("runset" in es_setname):
        for s_file in os.listdir("./runset"):
            if re.search("_ac.json$", s_file):
                s_setname = re.sub("^annot_runset-","", re.sub("_ac.json$","", s_file))
                es_setname.add(s_setname)
        es_setname.discard("runset")

    # loop through setanmes
    for s_setname in sorted(es_setname):
        # load acjson
        print(f"\nprocess {ds_filepath[s_setname]} ...")
        with open (ds_filepath[s_setname], "r") as f_acjson:
            d_ajson = json.load(f_acjson)

        # get dataframe
        s_ftsv = ac.acjson2dataframetsv(
            d_acjson=d_ajson,
            es_dfleaf=ac.es_DATAFRAMELEAF,
            b_stacked=False,
            s_delimiter="\t",
            s_opath="./"
        )

        # load csv
        df_run = pd.read_csv(s_ftsv, sep="\t")

        # transform column headers
        ls_header = list(df_run.columns)
        for s_column in df_run.columns:
            for s_regex, s_substitute in ds_header.items():
                if (re.search(s_regex, s_column)):
                    s_header = s_substitute.format(
                        "-".join(s_column.split("-")[2:])
                    )
                    ls_header[ls_header.index(s_column)] = s_header
        # write df to csv
        # bue 20180530: Mark needs csv an standartisized filename
        s_filename = "{}_an2omero.csv".format(s_setname.split("-")[-1])
        df_run.columns = ls_header
        df_run.to_csv(s_filename, index=False)

    # output
    b_out = True
    return(b_out)

# main call
if __name__ == '__main__':
    b_out = acjson2dfunstacked()
    print(b_out)
