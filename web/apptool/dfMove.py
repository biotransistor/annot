####
# title: dfMove.py
# language: python3
# author: bue
# date: 2018-06-09
# license GPLv3
#
# input: nop
#
# output:
#     files moved to there corresponing analysis folders
#
# run:
#     python3 dfMove.py
#
# description:
#     moves dataframes produced with anjsonDfUnstacked.py
#     to the corresponding analysis folder.
####

# library
import os
import re
import shutil

# main
def dfmove():
    """
    run dmc
    """
    b_out = False

    # get dataframe file names
    for s_file in os.listdir("."):
        if re.search("_an2omero.csv$", s_file):
            s_path = "/data/share/lincs_user/{}/Analysis/".format(s_file.split('_')[0])
            print("move {} to {}...".format(s_file, s_path))
            if not os.path.isdir(s_path):
                os.mkdir(s_path)
            try:
                os.remove("{}{}".format(s_path, s_file))
            except FileNotFoundError:
                pass
            shutil.move(s_file, s_path)

    # output
    b_out = True
    return(b_out)

# main call
if __name__ == '__main__':
    b_out = dfmove()
    print(b_out)
