#!/usr/bin/python3

"""
title: an2ome.py
language: python3
author: bue
date: 2018-03-14
license GPLv3

input:
    command line argument

output:
    annot2omero.tsv file

run examples:
    python3 an2om.py mema-LI8X00519
    python3 an2om.py mema-LI8X00519  mema-LI8X00520

description:
    third, annot v5 compatible implementation to retrieve from the command line
    unstacked datafarnmes for annot runset ids. the generic column headers will
    be tranlated to the house internal aggred output stanadard.
"""

# library
import argparse
import os
import pandas as pd
import re
import requests
import sys
import zipfile

# import from annot prj
#URL = "http://192.168.99.100/"
from crowbar import URL

# const assay
# http://192.168.99.100/appacaxis/export_dftsv/?model=runset&filetype=dataframeunstacked&choice=bue
s_URLQUERY = "{}appacaxis/export_dftsv?model=runset&filetype=dataframeunstacked&choice={}"

# main
def runset2df():
    """
    input:
    output:
    desciption:
        pulls annot run as unstacked dataframe
        transforms header according inhouse standard
    """
    b_out = False

    ### input handle ###
    parser = argparse.ArgumentParser(description="an2ome pulls unstacked run dataframnes form annot.")
    parser.add_argument(
        "runset",
        nargs='+',
        default=None,
        type=str,
        help="Assay runset name <runset_name ...>\nWorking example: python3 an2om.py mema-LI8X00519\nWorking example: python3 an2om.py mema-LI8X00519 mema-LI8X00520"
    )
    args = parser.parse_args()

    # get and manipulate unstacked runset dataframes one by one
    for s_run in args.runset:
        s_ANNOTZIP = "./an2ome-{}.zip".format(s_run)
        # pull unstacked runset datafarme form annot
        s_url = s_URLQUERY.format(URL, s_run)
        print("run: {}".format(s_url))
        r = requests.get(s_url, stream=True)  # auth=('user', 'pass')
        with open(s_ANNOTZIP, 'wb') as f_zip:
            for o_chunk in r.iter_content(chunk_size=1024):
                if o_chunk: # filter out keep-alive new chunks
                    f_zip.write(o_chunk)
        with zipfile.ZipFile(s_ANNOTZIP, 'r') as f_zip:
            ls_ftsv = f_zip.namelist()
            f_zip.extractall()
        os.remove(s_ANNOTZIP)

        # check download
        if (len(ls_ftsv) != 1):
            sys.exit("Error an2om: non or more then one assay run file retrived via {}".format(s_url))
        else:
            s_ftsv = ls_ftsv[0]

        # header manipulation dictionary
        ds_header = {
            "^endpoint-ss": "Stain-{}",
            "^perturbation-ds": "Drug-{}",
            "^perturbation-es": "Ecm-{}",
            "^perturbation-ls": "Ligand-{}",
            "^perturbation-ms": "Medium-{}",
            "^sample-cl": "CellLine-{}",
        }
        # load tsv
        df_run = pd.read_csv(s_ftsv, sep="\t")
        # transform column headers
        ls_header = list(df_run.columns)
        for s_column in df_run.columns:
            for s_regex, s_substitute in ds_header.items():
                if (re.search(s_regex, s_column)):
                    s_header = s_substitute.format(
                        "-".join(s_column.split("-")[2:])
                    )
                    ls_header[ls_header.index(s_column)] = s_header
        # update column headers and write df to tsv
        # bue 20180530: Mark needs csv an standartisized filename
        s_filename = "{}_an2omero.csv".format(s_run.split("-")[-1])
        df_run.columns = ls_header
        df_run.to_csv(s_filename, index=False) # sep="\t"

    # populate output
    b_out = True
    return(b_out)

# main call
if __name__ == '__main__':
    b_out = runset2df()
    print(b_out)
