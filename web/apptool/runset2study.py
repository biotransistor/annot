#!/usr/bin/python3

"""
title: runset2study.py
language: python3
author: bue
date: 2018-03-16
license GPLv3

input:
    command line argument

output:
    runset2study.tsv file

run examples:
    python3 runset2study.py

description:
    annot v5 compatible implementation to retrieve from the command line a
    tsv in the house internal aggred output stanadard that
    links all runsets to their coresponding studies.
"""

# library
import argparse
import os
import requests
import sys
import zipfile

# import from annot prj
#URL = "http://192.168.99.100/"
from crowbar import URL

# const assay
# http://192.168.99.100/app3runset/export_runset2study?filetype=json
s_URLQUERYJSON = "{}app3runset/export_runset2study?filetype=json"
s_URLQUERYTSV = "{}app3runset/export_runset2study?filetype=tsv"
s_ANNOTZIP = "./runset2study.zip"

# main
def mapping():
    """
    input:
    output:
    desciption:
    """
    b_out = False

    ### input handle ###
    parser = argparse.ArgumentParser(description="mapping mapps annot runs to annot study and dumps the result into a tsv or json file.")
    parser.add_argument(
        "--ff",
        default="tsv",
        type=str,
        help="output file format. ether tsv or json."
    )
    args = parser.parse_args()

    # handle args
    if (args.ff == "tsv"):
        s_url = s_URLQUERYTSV.format(URL)
    else:
        s_url = s_URLQUERYJSON.format(URL)
    # pull result file form annot
    r = requests.get(s_url, stream=True)  # auth=('user', 'pass')
    with open(s_ANNOTZIP, 'wb') as f_zip:
        for o_chunk in r.iter_content(chunk_size=1024):
            if o_chunk: # filter out keep-alive new chunks
                f_zip.write(o_chunk)
    with zipfile.ZipFile(s_ANNOTZIP, 'r') as f_zip:
        ls_ftsv = f_zip.namelist()
        f_zip.extractall()
    os.remove(s_ANNOTZIP)

    # check download
    if (len(ls_ftsv) != 1):
        sys.exit("Error an2om: non or more then one assay run file retrived via {}".format(s_url))
    else:
        s_ftsv = ls_ftsv[0]

    # populate output
    b_out = True
    return(b_out)

# main call
if __name__ == '__main__':
    b_out = mapping()
    print(b_out)
