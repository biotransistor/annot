from django.apps import AppConfig


class ApptoolConfig(AppConfig):
    name = 'apptool'
