from django.db import models

# constants
TERM_SOURCE_NAME = "SNOMEDCT"
TERM_SOURCE_FILE = "http://ihtsdo.org"
TERM_SOURCE_DESCRIPTION = "Extract of  SNOMED CT Systematized Nomenclature of Medicine - Clinical Terms ontology."
# rest source
REST_URL = "http://data.bioontology.org"
REST_FILENAME = "bioontologybufferhttp.json"
REST_FORMAT = "http"
CLASS_URL = "http%3A%2F%2Fpurl.bioontology.org%2Fontology%2FSNOMEDCT%2F389109008"  # url or the start term e.g. Social_context/Group
#CLASS_URL = "http%3A%2F%2Fpurl.bioontology.org%2Fontology%2FSNOMEDCT%2F372148003"  # url of the start term e.g. Ethnic group
#CLASS_URL = "http%3A%2F%2Fpurl.bioontology.org%2Fontology%2FSNOMEDCT%2F415229000"  # url of the start term e.g. Racial group
GET_BRANCHCUT = False
# build
SWAP_TERMID = False
PK_STRING_CASE = str.lower

# Create your models here.
class Ethnicity(models.Model):
    term_name = models.CharField(
        unique=True,
        max_length=254,
        verbose_name='Ontological term',
        help_text="Controlled vocabulary term."
    )
    term_id = models.CharField(
        blank=True,
        max_length=254,
        verbose_name='Ontology identifier',
        help_text="Controlled vocabulary identifier."
    )
    annot_id = models.SlugField(
        primary_key=True,
        max_length=254,
        verbose_name='Annot identifier',
        help_text="Internal identifier. This identifier is the primary key and should be descriptive. Choose this identifier carefully in accordance to the identifiers already in use."
    )
    term_source_version_responsible = models.CharField(
        max_length=254,
        verbose_name="Responsible person",
        help_text="Your name."
    )
    term_source_version_update = models.DateField(
        verbose_name='Version update time stamp',
        auto_now=True
    )
    term_source_version = models.CharField(
        max_length=254,
        verbose_name='Ontology source file version',
        help_text="Version of controlled vocabulary source."
    )
    term_ok = models.NullBooleanField(
        default=None,
        null=True,
        verbose_name='Ontology term status',
        help_text='A term marked true is in the most recent ontology file version used in this database. A term marked false is internally generated or deprecated. A term marked null has not been checked against the most recent ontology file version.'
    )
    def __str__(self):
        return(self.annot_id)

    class Meta:
        ordering = ['annot_id']
        verbose_name_plural = 'ethnicities'
