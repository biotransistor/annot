# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponethnicity_bioontology.models import Ethnicity

# code
class EthnicityLookup(ModelLookup):
    model = Ethnicity
    search_fields = ('annot_id__icontains',)
registry.register(EthnicityLookup)
