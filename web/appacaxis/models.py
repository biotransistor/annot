from django.db import models
from django.contrib.auth.models import User
from django.core.management.base import CommandError

# import form prj
from appbrxperson.models import PersonBrick
from appsabrick.models import EndpointBricked, PerturbationBricked, SampleBricked
from prjannot.settings import MEDIA_ROOT

# python  library
import os
import re

# Create your models here.
class EndpointSet(models.Model):
    annot_id = models.SlugField(
        unique=True,
        max_length=256,
        help_text="Automatically generated."
    )
    setname = models.SlugField(
        primary_key=True,
        max_length=256,
        default="not_yet_specified",
        help_text="Endpoint set name."
    )
    acjson_file_overwrite = models.BooleanField(
        default=False,
        verbose_name="overwrite file",
        help_text="If file with same name aready exist, should it be overwritten?"
    )
    acjson_file = models.FileField(
        default=None,
        upload_to="upload/acaxis/",
        help_text="Upload set related, resulting acjson file.",
        blank=True
    )
    set_display = models.TextField(
        help_text="Set content.",
        blank=True
    )
    brick = models.ManyToManyField(
        EndpointBricked,
        help_text="Choose endpoint."
    )
    available = models.NullBooleanField(
        default=None,
        help_text="Is this endpoint set at stock in our laboratory?"
    )
    responsible = models.ForeignKey(
        PersonBrick,
        default="not_yet_specified",
        help_text="The wetlab scientist who put this set together."
    )
    # notes
    notes = models.TextField(
        help_text="Any additional information worth to mention.",
        blank=True
    )

    def save(self, *args, **kwargs):
        # annot_id generator
        self.annot_id = re.sub(r"[^a-z0-9-]", "", self.setname.lower())
        # set content display
        ls_brick = [o_brick.annot_id for o_brick in self.brick.all()]
        self.set_display = str(ls_brick)
        # file handling
        if not (self.acjson_file.name is None):
            self.acjson_file.name = re.sub(r"[^a-zA-Z0-9_\-./]", "", self.acjson_file.name)
            if (os.path.isfile("{}upload/acaxis/{}".format(MEDIA_ROOT, self.acjson_file.name))) \
               and not (self.acjson_file_overwrite):
                raise CommandError(
                    "Error at appacaxis.models: Overwrite_file is set to False and a file with name upload/acaxis/{} alreday exist.".format(
                        self.acjson_file.name
                    )
                )
            if (os.path.isfile("{}upload/acaxis/{}".format(MEDIA_ROOT, self.acjson_file.name))):
                os.remove("{}upload/acaxis/{}".format(MEDIA_ROOT, self.acjson_file.name))
            self.acjson_file_overwrite = False
        # available
        self.available = True
        for o_brick in self.brick.all():
            if not (o_brick.available):
                self.available = False
                break
        # save
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.setname)

    __repr__ = __str__

    class Meta:
        ordering = ["annot_id"]
        verbose_name_plural = "Set of Endpoints"



class PerturbationSet(models.Model):
    annot_id = models.SlugField(
        unique=True,
        max_length=256,
        help_text="Automatically generated."
    )
    setname = models.SlugField(
        primary_key=True,
        max_length=256,
        default="not_yet_specified",
        help_text="Perturbation set name."
    )
    acjson_file_overwrite = models.BooleanField(
        default=False,
        verbose_name="overwrite file",
        help_text="If file with same name aready exist, should it be overwritten?"
    )
    acjson_file = models.FileField(
        upload_to="upload/acaxis/",
        help_text="Upload set related, resulting acjson file.",
        blank=True
    )
    set_display = models.TextField(
        help_text="Set content.",
        blank=True
    )
    brick = models.ManyToManyField(
        PerturbationBricked,
        help_text="Choose sample."
    )
    available = models.NullBooleanField(
        default=None,
        help_text="Is this sample at stock in our laboratory?"
    )
    responsible = models.ForeignKey(
        PersonBrick,
        default="not_yet_specified",
        help_text="The wetlab scientist who put this set together."
    )
    # notes
    notes = models.TextField(
        help_text="Any additional information worth to mention.",
        blank=True
    )

    def save(self, *args, **kwargs):
        # annot_id generator
        s_annotid = re.sub(r"[^a-z0-9-]", "", self.setname.lower())
        self.annot_id = s_annotid
        # set content display
        ls_brick = [o_brick.annot_id for o_brick in self.brick.all()]
        self.set_display = str(ls_brick)
        # file handling
        if not (self.acjson_file.name is None):
            self.acjson_file.name = re.sub(r"[^a-zA-Z0-9_\-./]", "", self.acjson_file.name)
            if (os.path.isfile("{}upload/acaxis/{}".format(MEDIA_ROOT, self.acjson_file.name))) \
               and not (self.acjson_file_overwrite):
                raise CommandError(
                    "Error at appacaxis.models: Overwrite_file is set to False and a file with name upload/acaxis/{} alreday exist.".format(
                        self.acjson_file.name
                    )
                )
            if (os.path.isfile("{}upload/acaxis/{}".format(MEDIA_ROOT, self.acjson_file.name))):
                os.remove("{}upload/acaxis/{}".format(MEDIA_ROOT, self.acjson_file.name))
            self.acjson_file_overwrite = False
        # available
        self.available = True
        for o_brick in self.brick.all():
            if not (o_brick.available):
                self.available = False
                break
        # save
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.setname)

    __repr__ = __str__

    class Meta:
        ordering =["annot_id"]
        verbose_name_plural = "Set of Perturbation"



class SampleSet(models.Model):
    annot_id = models.SlugField(
        unique=True,
        max_length=256,
        help_text="Automatically generated."
    )
    setname = models.SlugField(
        primary_key=True,
        max_length=256,
        default="not_yet_specified",
        help_text="Sample set name."
    )
    acjson_file_overwrite = models.BooleanField(
        default=False,
        verbose_name="overwrite file",
        help_text="If file with same name aready exist, should it be overwritten?"
    )
    acjson_file = models.FileField(
        upload_to="upload/acaxis/",
        help_text="Upload set related, resulting acjson file.",
        blank=True
    )
    set_display = models.TextField(
        help_text="Set content.",
        blank=True
    )
    brick = models.ManyToManyField(
        SampleBricked,
        help_text="Choose sample."
    )
    available = models.NullBooleanField(
        default=None,
        help_text="Is this sample at stock in our laboratory?"
    )
    responsible = models.ForeignKey(
        PersonBrick,
        default="not_yet_specified",
        help_text="The wetlab scientist who put this set together."
    )
    # notes
    notes = models.TextField(
        help_text="Any additional information worth to mention.",
        blank=True
    )

    def save(self, *args, **kwargs):
        # annot_id generator
        s_annotid = re.sub(r"[^a-z0-9-]", "", self.setname.lower())
        self.annot_id = s_annotid
        # set content display
        ls_brick = [o_brick.annot_id for o_brick in self.brick.all()]
        self.set_display = str(ls_brick)
        # file handling
        if not (self.acjson_file.name is None):
            self.acjson_file.name = re.sub(r"[^a-zA-Z0-9_\-./]", "", self.acjson_file.name)
            if (os.path.isfile("{}upload/acaxis/{}".format(MEDIA_ROOT, self.acjson_file.name))) \
               and not (self.acjson_file_overwrite):
                raise CommandError(
                    "Error at appacaxis.models: Overwrite_file is set to False and a file with name upload/acaxis/{} alreday exist.".format(
                        self.acjson_file.name
                    )
                )
            if (os.path.isfile("{}upload/acaxis/{}".format(MEDIA_ROOT, self.acjson_file.name))):
                os.remove("{}upload/acaxis/{}".format(MEDIA_ROOT, self.acjson_file.name))
            self.acjson_file_overwrite = False
        # available
        self.available = True
        for o_brick in self.brick.all():
            if not (o_brick.available):
                self.available = False
                break
        # save
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.setname)

    __repr__ = __str__

    class Meta:
        ordering =["annot_id"]
        verbose_name_plural = "Set of Samples"


# Create your models here.
class AcjsonToTsv(models.Model):
    annot_user = models.OneToOneField(
        User,
        primary_key=True,
        blank=True,
        help_text="user realted setting."
    )
    # file
    tsv_delimiter = models.CharField(
        max_length=2,
        default="\\t",
        help_text="delimiter for layout and dataframe files. default is tab \t.",
    )
    # run
    wellborder = models.BooleanField(
        default=False,
        help_text="for layout tsvs, should there be put a space between wells? this especcialy helpfull for spotted arrays. default setting is False."
    )
    runid = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    welllayout = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    spotlayout = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    # coordiante
    sxi = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    ixi = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    iixii = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    iWell = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    iSpot = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    # collapses
    runCollapsed = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    axisCollapsed = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    recordSetCollapsed = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    recordSet = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    # sample perturbation endpoint
    conc = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    concUnit = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    cultType = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    timeBegin = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    timeEnd = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    timeUnit = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    longRecord = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    externalId = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    # sample
    passageOrigin = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    passageUsed = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    # perturbation
    galId = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    pinId = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    # endpoint
    micChannel = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    wavelengthNm = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    hostOrganism = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    targetOrganism = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    isotype = models.BooleanField(
        default=True,
        help_text="should this field be included in the stacked and unstacked dataframe tsv fiels and in the long version of the layout tsv file?"
    )
    def __str__(self):
        return(str(self.annot_user))

    __repr__ = __str__

    class Meta:
        ordering =["annot_user"]
        verbose_name_plural = "acjson to tsv setting"
