from django import forms
from django.contrib import admin, messages
from django.core import management
from django.http import HttpResponseRedirect
from appacaxis.models import AcjsonToTsv, EndpointSet, PerturbationSet, SampleSet

# import from django selectable prj
from selectable.forms import AutoComboboxSelectWidget, AutoComboboxSelectMultipleWidget

# import form annot
from appsabrick.lookups import EndpointBrickedLookup, PerturbationBrickedLookup, SampleBrickedLookup
from appbrxperson.lookups import PersonBrickLookup

# import from python
import io
import re

# Register your models here.
class AcjsonToTsvAdmin(admin.ModelAdmin):
    search_fields = (
        "annot_user__username",
    )
    list_display = (
        "annot_user",
        "tsv_delimiter",
        "wellborder",
        "runid",
        "welllayout",
        "spotlayout",
        "sxi",
        "ixi",
        "iixii",
        "iWell",
        "iSpot",
        "runCollapsed",
        "axisCollapsed",
        "recordSetCollapsed",
        "recordSet",
        "conc",
        "concUnit",
        "cultType",
        "timeBegin",
        "timeEnd",
        "timeUnit",
        "longRecord",
        "externalId",
        "passageOrigin",
        "passageUsed",
        "galId",
        "pinId",
        "micChannel",
        "wavelengthNm",
        "hostOrganism",
        "targetOrganism",
        "isotype",
    )
    save_on_top = True
    fieldsets = [
        ("Identity", { "fields" : [
            "annot_user",
        ]}),
        ("File", { "fields" : [
            "tsv_delimiter",
            "wellborder",
        ]}),
        ("Run", { "fields" : [
            "runid",
            "welllayout",
            "spotlayout",
        ]}),
        ("Coordiante", { "fields" : [
            "sxi",
            "ixi",
            "iixii",
            "iWell",
            "iSpot",
        ]}),
        ("Collapsed fields", { "fields" : [
            "runCollapsed",
            "axisCollapsed",
            "recordSetCollapsed",
        ]}),
        ("Sample and reagent basics", { "fields" : [
            "recordSet",
            "conc",
            "concUnit",
            "cultType",
            "timeBegin",
            "timeEnd",
            "timeUnit",
            "longRecord",
            "externalId",
        ]}),
        ("Sample specifics", { "fields" : [
            "passageOrigin",
            "passageUsed",
        ]}),
        ("Perturbation specifics", { "fields" : [
            "galId",
            "pinId",
        ]}),
        ("Endpoint specifics", { "fields" : [
            "micChannel",
            "wavelengthNm",
            "hostOrganism",
            "targetOrganism",
            "isotype",
        ]}),
    ]
    readonly_fields = ("annot_user",)
    actions = [
        "delete_selected",
    ]
    def save_model(self, request, obj, form, change):
        obj.annot_user = request.user
        super(AcjsonToTsvAdmin, self).save_model(request, obj, form, change)

admin.site.register(AcjsonToTsv, AcjsonToTsvAdmin)


#admin.site.register(EndpointSet)
class EndpointSetForm(forms.ModelForm):
    class Meta:
        model = EndpointSet
        fields = ["brick","responsible"]
        widgets = {
            "brick": AutoComboboxSelectMultipleWidget(EndpointBrickedLookup),
            "responsible": AutoComboboxSelectWidget(PersonBrickLookup),
        }

class EndpointSetAdmin(admin.ModelAdmin):
    form = EndpointSetForm
    search_fields = (
        "annot_id",
        "setname",
        "acjson_file",
        "brick__bricked_id",
        "responsible__annot_id",
        "notes"
    )
    list_display = (
        "annot_id",
        "available",
        "setname",
        "acjson_file",
        "set_display",
        "responsible",
        "notes"
    )
    save_on_top = True
    fieldsets = [
        ("Identity", { "fields" : [
            "annot_id",
            "setname",
        ]}),
        ("Acjson", { "fields" : [
            "acjson_file_overwrite",
            "acjson_file",
        ]}),
        ("Set", { "fields" : [
            "set_display",
            "brick"
        ]}),
        ("Laboratory", {"fields":[
            "available",
            "responsible",
            "notes"
        ]}),
    ]
    readonly_fields = ("annot_id", "set_display", "available")
    actions = [
        "delete_selected",
        "download_json",
        "download_brick_json",
        "download_acjson",
        "download_tsv",
        "download_brick_tsv",
        "download_brick_lincs",
        "download_layouttsv_short",
        "download_layouttsv_long",
        "download_dataframetsv_tidy",
        "download_dataframetsv_unstacked",
        "resave",
        "check_acjson_against_brick",
        "download_acpipe_template_py",
    ]

    ### json download action ###
    def download_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=generic&choice=appacaxis.endpointset"
        ))
    download_json.short_description = "Download this page as json file (item selection irrelevant)"

    def download_brick_json(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=endpointset&filetype=json&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_json.short_description = "Download selected set releated bricks as json files"

    def download_acjson(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_acjson?model=endpointset&filetype=acjson&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_acjson.short_description = "Download selected sets as acjson file"

    ### tsv download action ###
    def download_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=generic&choice=appacaxis.endpointset"
        ))
    download_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

    def download_brick_tsv(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=endpointset&filetype=tsv&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_tsv.short_description = "Download selected set releated bricks as tsv files"

    def download_brick_lincs(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=endpointset&filetype=lincs&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_lincs.short_description = "Download selected set releated bricks as lincs data standard csv files"

    def download_layouttsv_short(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=endpointset&filetype=layoutshort&choice={}&user={}".format(
                "!".join(ls_choice),
                request.user.id
            )
        ))
    download_layouttsv_short.short_description = "Download selected sets as tsv_short layout file"

    def download_layouttsv_long(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=endpointset&filetype=layoutlong&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_layouttsv_long.short_description = "Download selected sets as tsv_long layout file"

    def download_dataframetsv_tidy(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=endpointset&filetype=dataframetidy&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_dataframetsv_tidy.short_description = "Download selected sets as tsv tidy dataframe files"

    def download_dataframetsv_unstacked(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=endpointset&filetype=dataframeunstacked&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_dataframetsv_unstacked.short_description = "Download selected sets as tsv unstacked dataframe file"

    ### resave action ###
    def resave(self, request, queryset):
        # process
        ls_save = []
        for o_record in EndpointSet.objects.all():
            o_record.save()
            ls_save.append(o_record.setname)
        # message
        messages.success(
            request,
            "{} # resaved".format(ls_save),
        )
    resave.short_description = "Resave all record (item selection irrelevant)"
    ### acpipe ###
    # action check acpipe generated acjson against brick in database
    def check_acjson_against_brick(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        for s_choice in ls_choice:
            # stdout handle
            o_out = io.StringIO()
            # command call
            management.call_command(
                "acjson_endpointcheck",
                s_choice,
                stdout=o_out,
                verbosity=0
            )
            # put stdout as message
            s_out = o_out.getvalue().strip()
            if (s_out == ""):
                messages.success(
                    request,
                    "{} # successfully checked".format(s_choice),
                )
            elif re.match(".*Error.*", s_out):
                messages.error(
                    request,
                    "{} # {}".format(s_choice, s_out),
                )
            elif re.match(".*Warning.*", s_out):
                messages.warning(
                    request,
                    "{} # {}".format(s_choice, s_out),
                )
            else:
                messages.info(
                    request,
                    "{} # {}".format(s_choice, s_out),
                )
    check_acjson_against_brick.short_description = "Check selected set's acjson file against brick content"

    # action generate python3 acpipe template script
    def download_acpipe_template_py(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_pytemplate?axis=endpoint&set={}".format(
                "!".join(ls_choice)
            )
        ))
    download_acpipe_template_py.short_description = "Download selected set's python3 acpipe template script"

admin.site.register(EndpointSet, EndpointSetAdmin)



#admin.site.register(PerturbationSet)
class PerturbationSetForm(forms.ModelForm):
    class Meta:
        model = PerturbationSet
        fields = ["brick","responsible"]
        widgets = {
            "brick": AutoComboboxSelectMultipleWidget(PerturbationBrickedLookup),
            "responsible": AutoComboboxSelectWidget(PersonBrickLookup),
        }

class PerturbationSetAdmin(admin.ModelAdmin):
    form = PerturbationSetForm
    search_fields = (
        "annot_id",
        "setname",
        "acjson_file",
        "brick__bricked_id",
        "responsible__annot_id",
        "notes"
    )
    list_display = (
        "annot_id",
        "available",
        "setname",
        "acjson_file",
        "set_display",
        "responsible",
        "notes"
    )
    save_on_top = True
    fieldsets = [
        ("Identity", { "fields" : [
            "annot_id",
            "setname",
        ]}),
        ("Acjson", { "fields" : [
            "acjson_file_overwrite",
            "acjson_file",
        ]}),
        ("Set", { "fields" : [
            "set_display",
            "brick"
        ]}),
        ("Laboratory", {"fields":[
            "available",
            "responsible",
            "notes"
        ]}),
    ]
    readonly_fields = ("annot_id", "set_display", "available")
    actions = [
        "delete_selected",
        "download_json",
        "download_brick_json",
        "download_acjson",
        "download_tsv",
        "download_brick_tsv",
        "download_brick_lincs",
        "download_layouttsv_short",
        "download_layouttsv_long",
        "download_dataframetsv_tidy",
        "download_dataframetsv_unstacked",
        "resave",
        "check_acjson_against_brick",
        "download_acpipe_template_py",
    ]

    ### json download action ###
    def download_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=generic&choice=appacaxis.perturbationset"
        ))
    download_json.short_description = "Download this page as json file (item selection irrelevant)"

    def download_brick_json(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=perturbationset&filetype=json&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_json.short_description = "Download selected set releated bricks as json files"

    def download_acjson(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_acjson?model=perturbationset&filetype=acjson&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_acjson.short_description = "Download selected sets as acjson file"

    ### tsv download action ###
    def download_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=generic&choice=appacaxis.perturbationset"
        ))
    download_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

    def download_brick_tsv(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=perturbationset&filetype=tsv&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_tsv.short_description = "Download selected set releated bricks as tsv files"

    def download_brick_lincs(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=perturbationset&filetype=lincs&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_lincs.short_description = "Download selected set releated bricks as lincs data standard csv files"

    def download_layouttsv_short(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=perturbationset&filetype=layoutshort&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_layouttsv_short.short_description = "Download selected sets as tsv_short layout file"

    def download_layouttsv_long(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=perturbationset&filetype=layoutlong&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_layouttsv_long.short_description = "Download selected sets as tsv_long layout file"

    def download_dataframetsv_tidy(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=perturbationset&filetype=dataframetidy&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_dataframetsv_tidy.short_description = "Download selected sets as tsv tidy dataframe files"

    def download_dataframetsv_unstacked(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=perturbationset&filetype=dataframeunstacked&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_dataframetsv_unstacked.short_description = "Download selected sets as tsv unstacked dataframe file"

    ### resave action ###
    def resave(self, request, queryset):
        # process
        ls_save = []
        for o_record in PerturbationSet.objects.all():
            o_record.save()
            ls_save.append(o_record.setname)
        # message
        messages.success(
            request,
            "{} # resaved".format(ls_save),
        )
    resave.short_description = "Resave all record (item selection irrelevant)"

    ### acpipe ###
    # action check acpipe generated acjson against brick in database
    def check_acjson_against_brick(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        for s_choice in ls_choice:
            # stdout handle
            o_out = io.StringIO()
            # command call
            management.call_command(
                "acjson_perturbationcheck",
                s_choice,
                stdout=o_out,
                verbosity=0
            )
            # put stdout as message
            s_out = o_out.getvalue().strip()
            if (s_out == ""):
                messages.success(
                    request,
                    "{} # successfully checked".format(s_choice),
                )
            elif re.match(".*Error.*", s_out):
                messages.error(
                    request,
                    "{} # {}".format(s_choice, s_out),
                )
            elif re.match(".*Warning.*", s_out):
                messages.warning(
                    request,
                    "{} # {}".format(s_choice, s_out),
                )
            else:
                messages.info(
                    request,
                    "{} # {}".format(s_choice, s_out),
                )
    check_acjson_against_brick.short_description = "Check selected set's acjson file against brick content"

    # action generate python3 acpipe template script
    def download_acpipe_template_py(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_pytemplate?axis=perturbation&set={}".format(
                "!".join(ls_choice)
            )
        ))
    download_acpipe_template_py.short_description = "Download selected set's python3 acpipe template script"

admin.site.register(PerturbationSet, PerturbationSetAdmin)


#admin.site.register(SampleSet)
class SampleSetForm(forms.ModelForm):
    class Meta:
        model = SampleSet
        fields = ["brick","responsible"]
        widgets = {
            "brick": AutoComboboxSelectMultipleWidget(SampleBrickedLookup),
            "responsible": AutoComboboxSelectWidget(PersonBrickLookup),
        }

class SampleSetAdmin(admin.ModelAdmin):
    form = SampleSetForm
    search_fields = (
        "annot_id",
        "setname",
        "acjson_file",
        "brick__bricked_id",
        "responsible__annot_id",
        "notes"
    )
    list_display = (
        "annot_id",
        "available",
        "setname",
        "acjson_file",
        "set_display",
        "responsible",
        "notes"
    )
    save_on_top = True
    fieldsets = [
        ("Identity", { "fields" : [
            "annot_id",
            "setname",
        ]}),
        ("Set", { "fields" : [
            "acjson_file_overwrite",
            "acjson_file",
            "set_display",
            "brick"
        ]}),
        ("Laboratory", {"fields":[
            "available",
            "responsible",
            "notes"
        ]}),
    ]
    readonly_fields = ("annot_id", "set_display", "available")
    actions = [
        "delete_selected",
        "download_json",
        "download_brick_json",
        "download_acjson",
        "download_tsv",
        "download_brick_tsv",
        "download_brick_lincs",
        "download_layouttsv_short",
        "download_layouttsv_long",
        "download_dataframetsv_tidy",
        "download_dataframetsv_unstacked",
        "resave",
        "check_acjson_against_brick",
        "download_acpipe_template_py",
    ]

    ### json download action ###
    def download_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=generic&choice=appacaxis.sampleset"
        ))
    download_json.short_description = "Download this page as json file (item selection irrelevant)"

    def download_brick_json(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=sampleset&filetype=json&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_json.short_description = "Download selected set releated bricks as json files"

    def download_acjson(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_acjson?model=sampleset&filetype=acjson&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_acjson.short_description = "Download selected sets as acjson file"

    ### tsv download action ###
    def download_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=generic&choice=appacaxis.sampleset"
        ))
    download_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

    def download_brick_tsv(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=sampleset&filetype=tsv&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_tsv.short_description = "Download selected set releated bricks as tsv files"

    def download_brick_lincs(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=sampleset&filetype=lincs&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_lincs.short_description = "Download selected set releated bricks as lincs data standard csv files"

    def download_layouttsv_short(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=sampleset&filetype=layoutshort&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_layouttsv_short.short_description = "Download selected sets as tsv_short layout file"

    def download_layouttsv_long(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=sampleset&filetype=layoutlong&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_layouttsv_long.short_description = "Download selected sets as tsv_long layout file"

    def download_dataframetsv_tidy(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=sampleset&filetype=dataframetidy&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_dataframetsv_tidy.short_description = "Download selected sets as tsv tidy dataframe files"

    def download_dataframetsv_unstacked(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=sampleset&filetype=dataframeunstacked&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_dataframetsv_unstacked.short_description = "Download selected sets as tsv unstacked dataframe file"

    ### resave action ###
    def resave(self, request, queryset):
        # process
        ls_save = []
        for o_record in SampleSet.objects.all():
            o_record.save()
            ls_save.append(o_record.setname)
        # message
        messages.success(
            request,
            "{} # resaved".format(ls_save),
        )
    resave.short_description = "Resave all record (item selection irrelevant)"

    ### acpipe ###
    # action check acpipe generated acjson against brick in database
    def check_acjson_against_brick(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        for s_choice in ls_choice:
            # stdout handle
            o_out = io.StringIO()
            # command call
            management.call_command(
                "acjson_samplecheck",
                s_choice,
                stdout=o_out,
                verbosity=0
            )
            # put stdout as message
            s_out = o_out.getvalue().strip()
            if (s_out == ""):
                messages.success(
                    request,
                    "{} # successfully checked".format(s_choice),
                )
            elif re.match(".*Error.*", s_out):
                messages.error(
                    request,
                    "{} # {}".format(s_choice, s_out),
                )
            elif re.match(".*Warning.*", s_out):
                messages.warning(
                    request,
                    "{} # {}".format(s_choice, s_out),
                )
            else:
                messages.info(
                    request,
                    "{} # {}".format(s_choice, s_out),
                )
    check_acjson_against_brick.short_description = "Check selected set's acjson file against brick content"

    # action generate python3 acpipe template script
    def download_acpipe_template_py(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_pytemplate?axis=sample&set={}".format(
                "!".join(ls_choice)
            )
        ))
    download_acpipe_template_py.short_description = "Download selected set's python3 acpipe template script"

admin.site.register(SampleSet, SampleSetAdmin)
