## appacaxis ###
# import form django
from django.shortcuts import render
from django.http import HttpResponse
from django.core import management
from django.contrib import messages

# import from python
import datetime
import io
import os
import re
import sys
import zipfile

# import from acpipe
from acpipe_acjson import acjson as ac

# import from annot
from app3runset.models import RunSet
from app4superset.models import SuperSet, SuperSetFile
from appacaxis.models import SampleSet, PerturbationSet, EndpointSet
from prjannot.settings import MEDIA_ROOTQUARANTINE

# Create your views here.
# index
def index(request):
    """ hello world """
    return HttpResponse("Hello world! You're at the appacaxis.view index page")

# export
def export_acjson(request):
    #return HttpResponse("Hello world! You're at the appacaxis.view export_acjson page")
    """
    description:
        make acjson and original supersetfiles downloadable.
    """
    # handle input
    s_model = request.GET.get("model")
    s_filetype = request.GET.get("filetype")
    s_choice = request.GET.get("choice")
    ls_choice = s_choice.split("!")

    # set variables
    s_datetime = datetime.datetime.today().isoformat().split(".")[0].replace(":", "").replace("-", "")
    s_filezip = "{}_annot_{}_download.zip".format(s_datetime, s_filetype)
    s_pathfilezip = "{}{}".format(MEDIA_ROOTQUARANTINE, s_filezip)

    # open zip file handle
    with zipfile.ZipFile(s_pathfilezip, "w") as zipper:

        # for each choice
        for s_choice in ls_choice:

            # stdout handle
            o_stdout = io.StringIO()

            # zip acjson and original files
            if (s_model == "sampleset"):
                o_record = SampleSet.objects.get(setname=s_choice)
                s_ipathfile = o_record.acjson_file.path
            elif (s_model == "perturbationset"):
                o_record = PerturbationSet.objects.get(setname=s_choice)
                s_ipathfile = o_record.acjson_file.path
            elif (s_model == "endpointset"):
                o_record = EndpointSet.objects.get(setname=s_choice)
                s_ipathfile = o_record.acjson_file.path
            elif (s_model == "superset"):
                o_record = SuperSet.objects.get(superset_name=s_choice)
                s_ipathfile = o_record.acjson_file.path
            elif (s_model == "supersetfile"):
                o_record = SuperSetFile.objects.get(annot_id=s_choice)
                s_ipathfile= o_record.superset_file.path
            elif (s_model == "runset"):
                o_record = RunSet.objects.get(runset_name=s_choice)
                s_ipathfile = o_record.acjson_file.path
            else:
                sys.exit(
                    "Error @ appacaxis.view export_jsontsv : unknown model {} in http request. Implemented are sampleset, perturbationset, endpontset, superset, supersetfile, runset.".format(
                        s_model
                    )
                )
            s_file = s_ipathfile.split("/")[-1]
            try:
                zipper.write(s_ipathfile, arcname=s_file)
            except ValueError:
                pass # The 'acjson_file' attribute has no file associated with it.

    # push save as zip
    response = HttpResponse(content_type="application/zip")
    response["Content-Disposition"] = "attachment; filename={}".format(s_filezip)
    with open(s_pathfilezip, "rb") as f:
        response.write(f.read())

    # delete zip file
    os.system("rm {}".format(s_pathfilezip))

    # put stdout as message
    s_out = o_stdout.getvalue().strip()
    if (s_out == ""):
        messages.success(
            request,
            "{} {} # successfully zipped.".format(s_filetype, ls_choice)
        )
    elif re.match(".*Error.*", s_out):
        messages.error(
            request,
            "{} {} # {}".format(s_filetype, ls_choice, s_out)
        )
    elif re.match(".*Warning.*", s_out):
        messages.warning(
            request,
            "{} {} # {}".format(s_filetype, ls_choice, s_out)
        )
    else:
        messages.info(
            request,
            "{} {} # {}".format(s_filetype, ls_choice, s_out)
        )
    storage = messages.get_messages(request)

    # return
    return(response)


def export_dftsv(request):
    #return HttpResponse("Hello world! You're at the appacaxis.view export_dftsv page")
    """
    description:
        make model realed tsv layout and dataframe files downloadable.
    """
    # handle input
    s_model = request.GET.get("model")
    s_filetype = request.GET.get("filetype")
    s_choice = request.GET.get("choice")
    ls_choice = s_choice.split("!")

    # set variables
    s_datetime = datetime.datetime.today().isoformat().split(".")[0].replace(":", "").replace("-", "")
    s_filezip = "{}_annot_{}_download.zip".format(s_datetime, s_filetype)
    s_pathfilezip = "{}{}".format(MEDIA_ROOTQUARANTINE, s_filezip)

    # open zip file handle
    with zipfile.ZipFile(s_pathfilezip, "w") as zipper:

        # for each choice
        for s_choice in ls_choice:

            # stdout handle
            o_stdout = io.StringIO()

            # python manage.py command
            # bue 20180314:
            # if export_jsontsv is called with out specific user settings,
            # for example called via requests library form the command line,
            # then the whole ac.DATAFRAMELEAF is used as AcjsonToTsv setting
            # to generate the outputfile.
            if (request.user.id is None):
                request.user.id = 0
            management.call_command(
                "db2{}_{}".format(s_filetype, s_model),
                s_choice,
                "--user={}".format(request.user.id),
                stdout=o_stdout,
                verbosity=0
            )

            # zip layouts
            if (s_filetype in {"layoutshort", "layoutlong"}):
                s_stdout = re.sub("\s","",o_stdout.getvalue())
                for s_pathfile in s_stdout.split("!"):
                    s_file = s_pathfile.split("/")[-1].replace("acpipe_acjson-","annotCoordinate_runset_")
                    zipper.write(s_pathfile, arcname=s_file)
                    os.remove(s_pathfile)

            # zip dataframes
            elif (s_filetype in {"dataframetidy", "dataframeunstacked"}):
                s_pathfile = re.sub("\s","",o_stdout.getvalue())
                s_file = "annotCoordinate_{}_{}_{}.tsv".format(s_model, s_choice, s_filetype)
                zipper.write(s_pathfile, arcname=s_file)
                os.remove(s_pathfile)

            # error filetype
            else:
                sys.exit(
                    "Error @ appacaxis.view export_jsontsv : unknown filetype {} in http request. Implemented are acjson, dataframeshort, dataframelong, layoutshort, layoutlong, original.".format(
                        s_filetype
                    )
                )

    # push save as zip
    response = HttpResponse(content_type="application/zip")
    response["Content-Disposition"] = "attachment; filename={}".format(s_filezip)
    with open(s_pathfilezip, "rb") as f:
        response.write(f.read())

    # delete zip file
    os.system("rm {}".format(s_pathfilezip))

    # put stdout as message
    s_out = o_stdout.getvalue().strip()
    if (s_out == ""):
        messages.success(
            request,
            "{} {} # successfully zipped.".format(s_filetype, ls_choice)
        )
    elif re.match(".*Error.*", s_out):
        messages.error(
            request,
            "{} {} # {}".format(s_filetype, ls_choice, s_out)
        )
    elif re.match(".*Warning.*", s_out):
        messages.warning(
            request,
            "{} {} # {}".format(s_filetype, ls_choice, s_out)
        )
    else:
        messages.info(
            request,
            "{} {} # {}".format(s_filetype, ls_choice, s_out)
        )
    storage = messages.get_messages(request)

    # return
    return(response)


def export_pytemplate(request):
    #return HttpResponse("Hello world! You're at the appacaxis.view export_pytemplate page")
    """
    description:
        make ac axis set realted python3 acpipe template downloadable
    """
    # stdout handle
    o_stdout = io.StringIO()

    # handle input
    s_axis = request.GET.get("axis")
    s_set = request.GET.get("set")
    ls_set = s_set.split("!")

    # set variables
    s_datetime = datetime.datetime.today().isoformat().split(".")[0].replace(":", "").replace("-", "")
    s_filezip = "{}_annot_acpipe_{}template_download.zip".format(s_datetime, s_axis)
    s_pathfilezip = "{}{}".format(MEDIA_ROOTQUARANTINE, s_filezip)

    # open zip file handle
    with zipfile.ZipFile(s_pathfilezip, "w") as zipper:

        # for each axis set
        for s_set in ls_set:

            # python manage.py command
            management.call_command(
                "acpipe_{}template".format(s_axis),
                s_set,
                stdout=o_stdout,
                verbosity=0
            )

            # zip
            s_file = "acpipeTemplateCode_{}.py".format(s_set)
            s_pathfile = "{}{}".format(MEDIA_ROOTQUARANTINE, s_file)
            zipper.write(s_pathfile, arcname=s_file)
            os.remove(s_pathfile)

    # push save as zip
    response = HttpResponse(content_type="application/zip")
    response["Content-Disposition"] = "attachment; filename={}".format(s_filezip)
    with open(s_pathfilezip, "rb") as f:
        response.write(f.read())

    # delete zip file
    os.system("rm {}".format(s_pathfilezip))

    # put stdout as message
    s_out = o_stdout.getvalue().strip()
    if (s_out == ""):
        messages.success(
            request,
            "python3 {} template {} # successfully zipped.".format(s_axis, ls_set)
        )
    elif re.match(".*Error.*", s_out):
        messages.error(
            request,
            "python3 {} template {} # {}".format(s_axis, ls_set, s_out)
        )
    elif re.match(".*Warning.*", s_out):
        messages.warning(
            request,
            "python3 {} template {} # {}".format(s_axis, ls_set, s_out)
        )
    else:
        messages.info(
            request,
            "python3 {} template {} # {}".format(s_axis, ls_set, s_out)
        )
    storage = messages.get_messages(request)

    # return
    return(response)


def export_brick(request):
    #return HttpResponse("Hello world! You're at the appacaxis.view export_brick page")
    """
    description:
        make acjson realed brick files downloadable.
    """
    # handle input
    s_model = request.GET.get("model")
    s_filetype = request.GET.get("filetype")
    s_choice = request.GET.get("choice")
    ls_choice = s_choice.split("!")

    # set variables
    s_datetime = datetime.datetime.today().isoformat().split(".")[0].replace(":", "").replace("-", "")
    s_filezip = "{}_annot_{}_download.zip".format(s_datetime, s_filetype)
    s_pathfilezip = "{}{}".format(MEDIA_ROOTQUARANTINE, s_filezip)

    # open zip file handle
    with zipfile.ZipFile(s_pathfilezip, "w") as zipper:

        # for each choice
        for s_choice in ls_choice:

            # stdout handle
            o_stdout = io.StringIO()

            # python manage.py command
            management.call_command(
                "db2pkgbrick_{}".format(s_model),
                s_choice,
                "--filetype={}".format(s_filetype),
                stdout=o_stdout,
                verbosity=0
            )
            # zip
            for s_pathfile in o_stdout.getvalue().strip().split("!"):
                zipper.write(s_pathfile, arcname=s_pathfile.split("/")[-1])
                os.remove(s_pathfile)

    # push save as zip
    response = HttpResponse(content_type="application/zip")
    response["Content-Disposition"] = "attachment; filename={}".format(s_filezip)
    with open(s_pathfilezip, "rb") as f:
        response.write(f.read())

    # delete zip file
    os.system("rm {}".format(s_pathfilezip))

    # put stdout as message
    s_out = o_stdout.getvalue().strip()
    if (s_out == ""):  # bue: this will never happen.
        messages.success(
            request,
            "{} bricks in fromat {} # successfully zipped.".format(s_filetype, ls_choice)
        )
    elif re.match(".*Error.*", s_out):
        messages.error(
            request,
            "{} realted bricks in fromat {} # {}".format(s_filetype, ls_choice, s_out)
        )
    elif re.match(".*Warning.*", s_out):
        messages.warning(
            request,
            "{} realted bricks in fromat {} # {}".format(s_filetype, ls_choice, s_out)
        )
    else:
        messages.info(
            request,
            "{} related bricks for acjson {} # {} zipped".format(s_filetype, ls_choice, s_out)
        )
    storage = messages.get_messages(request)

    # return
    return(response)
