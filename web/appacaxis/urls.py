# import form djnago
from django.conf.urls import url
# import from appsaontology
from appacaxis import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^export_acjson/$', views.export_acjson, name="export_acjson"),
    url(r'^export_brick/$', views.export_brick, name="export_brick"),
    url(r'^export_dftsv/$', views.export_dftsv, name="export_dftsv"),
    url(r'^export_pytemplate/$', views.export_pytemplate, name="export_pytemplate"),
]
