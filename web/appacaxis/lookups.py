# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appacaxis.models import EndpointSet, PerturbationSet, SampleSet

# code
class EndpointSetLookup(ModelLookup):
    model = EndpointSet
    search_fields = (
        'annot_id__icontains',
    )
registry.register(EndpointSetLookup)

class PerturbationSetLookup(ModelLookup):
    model = PerturbationSet
    search_fields = (
        'annot_id__icontains',
    )
registry.register(PerturbationSetLookup)

class SampleSetLookup(ModelLookup):
    model = SampleSet
    search_fields = (
        'annot_id__icontains',
    )
registry.register(SampleSetLookup)
