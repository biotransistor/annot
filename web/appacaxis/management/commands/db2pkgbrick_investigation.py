# import frmom django
from django.core.management.base import BaseCommand, CommandError
from django.core import management

# import from python
import json
import sys

# imporfrom acpipe
import acpipe_acjson.acjson as ac

# import from annot
from app1study.models import Study
from app3runset.models import RunSet
from appsabrick import pkgmetadata
from prjannot.settings import MEDIA_ROOTQUARANTINE

### main ###
class Command(BaseCommand):
    help = "get absolute file path for requested runset realted brick dump."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument(
            "investigation",
            nargs='+',
            type=str,
            help="Annot investigation name"
        )
        # named (optional) arguments
        parser.add_argument(
            "--filetype",
            nargs="?",
            default="tsv",
            type=str,
            help="filetype in which the brick content should be outputed.",
        )

    def handle(self, *args, **options):
        ls_pathfile = []

        # process each axis set
        for s_investigation in options["investigation"]:
            print("\ndb2pkgbrick_investigarion {}".format(s_investigation))

            # load acjson file
            ls_acjson = []
            for o_study in Study.objects.filter(investigation=s_investigation):
                for o_runset in RunSet.objects.filter(study =o_study.annot_id):
                    if (o_runset.acjson_file.name != ""):
                        print("process: {} {} {}".format(s_investigation, o_study.annot_id, o_runset.acjson_file.name))
                        ls_acjson.append(o_runset.acjson_file.path)

            # process
            print("pkg metadata into {} ...".format(options["filetype"]))
            ls_brickfile = pkgmetadata.pkgbrick(
                ls_acjson=ls_acjson,
                s_filetype=options["filetype"],
                s_compilationname=s_investigation,
                s_opath=MEDIA_ROOTQUARANTINE,
            )
            # result
            ls_pathfile.extend(ls_brickfile)

        # output
        print(ls_pathfile)
        self.stdout.write("!".join(ls_pathfile))
