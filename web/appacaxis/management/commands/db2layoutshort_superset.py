# import frmom django
from django.core.management.base import BaseCommand, CommandError
from django.core import management

# import from python
import codecs
import io
import json

# imporfrom acpipe
import acpipe_acjson.acjson as ac

# import from annot
from appacaxis.models import AcjsonToTsv
from app4superset.models import SuperSet
from prjannot.settings import MEDIA_ROOTQUARANTINE

### main ###
class Command(BaseCommand):
    help = "get absolute file path for set requested super set json dump."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument(
            "axisset",
            nargs='+',
            type=str,
            help="Annot coordinate axis superset name"
        )
        # named (optional) arguments
        parser.add_argument(
            "--user",
            nargs="?",
            default=0,
            type=int,
            help="user id to fetch userspecific output settings.",
        )


    def handle(self, *args, **options):
        ls_pathfile_total = []

        # process
        for s_axisset in options["axisset"]:

            # load axisset obj
            lo_axisset = SuperSet.objects.filter(superset_name__regex=r'{}'.format(s_axisset))
            if (len(lo_axisset) < 1):
                raise CommandError(
                    "Error: axisset argument {} does not specify any axis set.".format(
                        s_axisset
                    )
                )

            # for each axisset obj
            for o_axisset in lo_axisset:

                # load acjson file
                with open (o_axisset.acjson_file.path, "r") as f_acjson:
                    d_acjson = json.load(f_acjson)

                if (options["user"] == 0):
                    s_delimiter = "\t"
                    b_wellborder = False
                else:
                    # load tsv setting
                    o_tsvsetting = AcjsonToTsv.objects.get(annot_user=options["user"])
                    s_delimiter = codecs.decode(o_tsvsetting.tsv_delimiter, "unicode_escape")
                    b_wellborder = o_tsvsetting.wellborder

                # process
                ls_pathfile = ac.acjson2layouttsv(
                    d_acjson=d_acjson,
                    ts_axis=ac.ts_ACAXIS,
                    s_mode="short",
                    b_wellborder=b_wellborder,
                    s_delimiter=s_delimiter,
                    s_opath=MEDIA_ROOTQUARANTINE
                )

                # result
                ls_pathfile_total.extend(ls_pathfile)

        # output
        self.stdout.write("!".join(ls_pathfile_total))
