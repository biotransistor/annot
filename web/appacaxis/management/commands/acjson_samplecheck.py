# import frmom django
from django.core.management.base import BaseCommand, CommandError

# import from python
import copy
import json

# import from annot
from appacaxis.models import SampleSet
from appbrsamplehuman.models import HumanBrick
from apponunit_bioontology.models import Unit

# const
es_unit = set((o_unit.annot_id for o_unit in Unit.objects.all()))

### main ###
class Command(BaseCommand):
    help = "Check sample acpipe json file against brick content."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument(
            "axisset",
            nargs='+',
            type=str,
            help="Annot coordinate axis set setname"
        )

    def handle(self, *args, **options):

        # process
        for s_axisset in options["axisset"]:

            # load axisset obj
            lo_axisset = SampleSet.objects.filter(setname__regex=r'{}'.format(s_axisset))
            if (len(lo_axisset) < 1):
                raise CommandError(
                    "Error: axisset argument {} does not specify any axis set.".format(
                        s_axisset
                    )
                )

            # for each axisset obj
            for o_axisset in lo_axisset:

                # load acjson file
                with open (o_axisset.acjson_file.path, "r") as f_acjson:
                    d_acjson = json.load(f_acjson)
                d_acpop = copy.deepcopy(d_acjson)

                # get a list of all the brick names
                es_brickpop = set([o_bricked.annot_id.split("-")[0] for o_bricked in o_axisset.brick.all()])

                # check acid, runid, runtype
                if (d_acjson["acid"] != o_axisset.acjson_file.path.split("/")[-1]):
                    self.stdout.write(
                        "Warning @ acjson_samplecheck : acid {} differs from filename {}.".format(
                            d_acjson["acid"],
                            o_axisset.acjson_file.path.split("/")[-1]
                        )
                    )
                elif (d_acjson["runid"] != o_axisset.setname):
                    self.stdout.write(
                        "Warning @ acjson_samplecheck : runid {} differs from setname {}.".format(
                            d_acjson["runid"],
                            o_axisset.setname
                        )
                    )
                elif (d_acjson["runtype"] != "annot_acaxis"):
                    self.stdout.write(
                        "Warning @ acjson_samplecheck : runtype {} is not annot_acaxis.".format(
                            d_acjson["runtype"]
                        )
                    )

                # handle each brick
                for o_bricked in o_axisset.brick.all():

                    # human
                    if (o_bricked.brick_type == "human"):
                        o_brick = HumanBrick.objects.get(annot_id=o_bricked.annot_id)
                        # loop through d_acjson
                        for s_key, o_well in d_acjson.items():
                            try:
                                for s_gent, d_gent in o_well["sample"].items():
                                    if (s_gent == o_brick.sample.annot_id):
                                        # check gent
                                        if (d_gent["manufacture"] != o_brick.provider.annot_id):
                                            self.stdout.write(
                                                "Error @ acjson_samplecheck : {} {} file manufacture {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["manufacture"],
                                                    o_brick.provider.annot_id,
                                                )
                                            )
                                        elif (d_gent["catalogNu"] != o_brick.provider_catalog_id):
                                            self.stdout.write(
                                                "Error @ acjson_samplecheck : {} {} file catalog number  {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["catalogNu"],
                                                    o_brick.provider_catalog_id,
                                                )
                                            )
                                        elif (d_gent["batch"] != o_brick.provider_batch_id):
                                            self.stdout.write(
                                                "Error @ acjson_samplecheck : {} {} file batch {} differs from the brick one {}".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["batch"],
                                                    o_brick.provider_batch_id,
                                                )
                                            )
                                        #elif (d_gent["passageOrigin"] != o_brick.passage_origin.split("|")):
                                        #    self.stdout.write(
                                        #        "Error @ acjson_samplecheck : {} {} file passage origin {} differs from the brick one {}.".format(
                                        #            o_axisset.setname,
                                        #            s_gent,
                                        #            d_gent["passageOrigin"],
                                        #            o_brick.passage_origin.split("|"),
                                        #        )
                                        #    )
                                        #elif (d_gent["passageUsed"] != o_brick.passage_use.split("|")):
                                        #    self.stdout.write(
                                        #        "Error @ acjson_samplecheck : {} {} file pasage used {} differs from the brick one {}.".format(
                                        #            o_axisset.setname,
                                        #            s_gent,
                                        #            d_gent["passageUsed"],
                                        #            o_brick.passage_use.split("|"),
                                        #        )
                                        #    )
                                        # bue 20190926: this is a list
                                        elif (d_gent["recordSet"][0].split("-")[0:2] != o_axisset.setname.split("-")[0:2]):
                                            self.stdout.write(
                                                "Error @ acjson_samplecheck : {} {} file record set name {} differs from appacaxis one.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["recordSet"],
                                                )
                                            )
                                        elif (d_gent["externalId"] != o_brick.lincs_identifier):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file external idenfifier {} differs from appacaxis one.".format(
                                                    o_brick.lincs_identifier,
                                                    s_gent,
                                                    d_gent["externalId"],
                                                )
                                            )
                                        elif not (d_gent["concUnit"] in es_unit):
                                            self.stdout.write(
                                                "Error @ acjson_samplecheck : {} {} concentration unit {} is not controlled vocabulary.".format(
                                                    o_axisset.acjson_file.path.split("/")[-1],
                                                    s_gent,
                                                    d_gent["concUnit"],
                                                )
                                            )
                                        elif not (d_gent["timeUnit"] != es_unit):
                                            self.stdout.write(
                                                "Error @ acjson_samplecheck : {} {} time unit {} is not controlled vocabulary.".format(
                                                    o_axisset.acjson_file.path.split("/")[-1],
                                                    s_gent,
                                                    d_gent["timeUnit"],
                                                )
                                            )
                                        # pop gent form acjson
                                        try:
                                            d_acpop[s_key]["sample"].pop(s_gent)
                                        except KeyError:
                                            if not (s_gent in d_acjson[s_key]["sample"].keys()):
                                                self.stdout.write(
                                                    "Error @ acjson_samplecheck : sample brick {} missing in acjson sample axis {}.".format(s_gent, set(d_acjson[s_key]["sample"].keys()))
                                                )
                                        es_brickpop.discard(s_gent)

                            except TypeError:
                                pass
                            except AttributeError:  # None case
                                d_acpop[s_key]["sample"] = {}

                    # strange brick
                    else:
                        self.stdout.write(
                            "Error @ acjson_samplecheck : unknown sample brick type {}. implemented is sample type human".format(o_bricked.brick_type)
                        )

                #  check if any sample gent left
                es_gent = set()
                for s_key, o_well in d_acpop.items():
                    try:
                        es_gent = es_gent.union(set(d_acpop[s_key]["sample"].keys()))
                    except TypeError:
                        pass
                if (len(es_gent) > 0):
                    self.stdout.write("Error @ acjson_samplecheck : sample {} found in file but not as brick.".format(es_gent))
                elif (len(es_brickpop) > 0):
                    self.stdout.write("Error @ acjson_samplecheck : sample {} found as brick but not in file.".format(es_brickpop))
                else:
                    print("checked {}: ok".format(s_axisset))
