# import frmom django
from django.core.management.base import BaseCommand, CommandError

# import from python
import copy
import json

# import from annot
from appacaxis.models import EndpointSet
from appbrreagentantibody.models import Antibody1Brick, Antibody2Brick
from appbrreagentcompound.models import CstainBrick
from apponunit_bioontology.models import Unit

# const
es_unit = set((o_unit.annot_id for o_unit in Unit.objects.all()))

### main ###
class Command(BaseCommand):
    help = "Check endpoint acpipe json file against brick content."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument(
            "axisset",
            nargs='+',
            type=str,
            help="Annot coordinate axis set setname"
        )

    def handle(self, *args, **options):

        # process
        for s_axisset in options["axisset"]:

            # load axisset obj
            lo_axisset = EndpointSet.objects.filter(setname__regex=r'{}'.format(s_axisset))
            if (len(lo_axisset) < 1):
                raise CommandError(
                    "Error: axisset argument {} does not specify any axis set.".format(
                        s_axisset
                    )
                )

            # for each axisset obj
            for o_axisset in lo_axisset:

                # load acjson file
                with open (o_axisset.acjson_file.path, "r") as f_acjson:
                    d_acjson = json.load(f_acjson)
                d_acpop = copy.deepcopy(d_acjson)

                # get a list of all the brick names
                es_brickpop = set([o_bricked.annot_id.split("-")[0] for o_bricked in o_axisset.brick.all() if o_bricked.brick_type != "antibody2"])
                es_brickantibody2 = set([
                    "{}-{}".format(o_bricked.annot_id.split("-")[1], o_bricked.annot_id.split("-")[2])
                    for o_bricked in o_axisset.brick.all() if o_bricked.brick_type == "antibody2"
                ])
                es_brickpop = es_brickpop.union(es_brickantibody2)

                # handle cstain not_available case for microscopy channel not in use
                for s_key in d_acjson.keys():
                    try:
                        d_acpop[s_key]["endpoint"]["not_available"]
                        d_acpop[s_key]["endpoint"].pop("not_available")
                    except KeyError:
                        pass
                    except TypeError:  # non well cases
                        pass
                es_brickpop.discard("not_available")

                # check acid, runid, runtype
                if (d_acjson["acid"] != o_axisset.acjson_file.path.split("/")[-1]):
                    self.stdout.write(
                        "Warning @ acjson_endpointcheck : acid {} differs from filename {}.".format(
                            d_acjson["acid"],
                            o_axisset.acjson_file.path.split("/")[-1]
                        )
                    )
                elif (d_acjson["runid"] != o_axisset.setname):
                    self.stdout.write(
                        "Warning @ acjson_endpointcheck : runid {} differs from setname {}.".format(
                            d_acjson["runid"],
                            o_axisset.setname
                        )
                    )
                elif (d_acjson["runtype"] != "annot_acaxis"):
                    self.stdout.write(
                        "Warning @ acjson_endpointcheck : runtype {} is not annot_acaxis.".format(
                            d_acjson["runtype"]
                        )
                    )

                # handle each brick
                for o_bricked in o_axisset.brick.all():

                    # antibody1
                    if (o_bricked.brick_type == "antibody1"):
                        o_brick = Antibody1Brick.objects.get(annot_id=o_bricked.annot_id)
                        # loop through d_acjson
                        for s_key, o_well in d_acjson.items():
                            try:
                                for s_gent, d_gent in o_well["endpoint"].items():
                                    if (s_gent == o_brick.antigen.annot_id):
                                        # check gent
                                        if (d_gent["manufacture"] != o_brick.provider.annot_id):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file manufacture {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["manufacture"],
                                                    o_brick.provider.annot_id,
                                                )
                                            )
                                        elif (d_gent["catalogNu"] != o_brick.provider_catalog_id):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file catalog number  {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["catalogNu"],
                                                    o_brick.provider_catalog_id,
                                                )
                                            )
                                        elif (d_gent["batch"] != o_brick.provider_batch_id):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file batch {} differs from the brick one {}".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["batch"],
                                                    o_brick.provider_batch_id,
                                                )
                                            )
                                        elif (d_gent["hostOrganism"] != o_brick.host_organism.annot_id):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file host organism {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["hostOrganism"],
                                                    o_brick.host_organism.annot_id,
                                                )
                                            )
                                        elif (d_gent["isotype"] != o_brick.isotype.annot_id):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file isotype {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["isotype"],
                                                    o_brick.isotype.annot_id,
                                                )
                                            )
                                        elif (d_gent["externalId"] != o_brick.lincs_identifier):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file external idenfifier {} differs from appacaxis one.".format(
                                                    o_brick.lincs_identifier,
                                                    s_gent,
                                                    d_gent["externalId"],
                                                )
                                            )
                                        elif not (d_gent["concUnit"] in es_unit):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} concentration unit {} is not controlled vocabulary.".format(
                                                    o_axisset.acjson_file.path.split("/")[-1],
                                                    s_gent,
                                                    d_gent["concUnit"],
                                                )
                                            )
                                        elif not (d_gent["timeUnit"] != es_unit):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} time unit {} is not controlled vocabulary.".format(
                                                    o_axisset.acjson_file.path.split("/")[-1],
                                                    s_gent,
                                                    d_gent["timeUnit"],
                                                )
                                            )
                                        # pop gent form acjson and brick
                                        d_acpop[s_key]["endpoint"].pop(s_gent)
                                        es_brickpop.discard(s_gent)
                            except TypeError:
                                pass
                            except AttributeError:  # None case
                                d_acpop[s_key]["endpoint"] = {}

                    # antibody2
                    elif (o_bricked.brick_type == "antibody2"):
                        o_brick = Antibody2Brick.objects.get(annot_id=o_bricked.annot_id)
                        s_secondary = "{}-{}".format(
                            o_brick.target_organism.annot_id,
                            o_brick.isotype.annot_id
                        )
                        # loop through d_acjson
                        for s_key, o_well in d_acjson.items():
                            try:
                                for s_gent, d_gent in o_well["endpoint"].items():
                                    if (s_gent == s_secondary):
                                        # check gent
                                        if (d_gent["manufacture"] != o_brick.provider.annot_id):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file manufacture {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["manufacture"],
                                                    o_brick.provider.annot_id,
                                                )
                                            )
                                        elif (d_gent["catalogNu"] != o_brick.provider_catalog_id):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file catalog number  {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["catalogNu"],
                                                    o_brick.provider_catalog_id,
                                                )
                                            )
                                        elif (d_gent["batch"] != o_brick.provider_batch_id):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file batch {} differs from the brick one {}".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["batch"],
                                                    o_brick.provider_batch_id,
                                                )
                                            )
                                        elif (d_gent["hostOrganism"] != o_brick.host_organism.annot_id):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file host organism {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["hostOrganism"],
                                                    o_brick.host_organism.annot_id,
                                                )
                                            )
                                        elif (d_gent["targetOrganism"] != o_brick.target_organism.annot_id):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file target organism {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["targetOrganism"],
                                                    o_brick.target_organism.annot_id,
                                                )
                                            )
                                        elif (d_gent["isotype"] != o_brick.isotype.annot_id):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file isotype {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["isotype"],
                                                    o_brick.isotype.annot_id,
                                                )
                                            )
                                        elif (d_gent["dye"] != o_brick.dye.annot_id):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file dye {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["dye"],
                                                    o_brick.dye.annot_id,
                                                )
                                            )
                                        elif (d_gent["wavelengthNm"] != o_brick.wavelength_nm_nominal_excitation):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file nominal excitation wavelength {}[nm] differs from the brick one {}[nm].".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["wavelengthNm"],
                                                    o_brick.wavelength_nm_nominal_excitation,
                                                )
                                            )
                                        # bue 20190926: this is a list!
                                        elif (d_gent["recordSet"][0].split("-")[0:2] != o_axisset.setname.split("-")[0:2]):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file record set name {} differs from appacaxis one.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["recordSet"],
                                                )
                                            )
                                        elif (d_gent["externalId"] != o_brick.lincs_identifier):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file external idenfifier {} differs from appacaxis one.".format(
                                                    o_brick.lincs_identifier,
                                                    s_gent,
                                                    d_gent["externalId"],
                                                )
                                            )
                                        elif not (d_gent["concUnit"] in es_unit):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} concentration unit {} is not controlled vocabulary.".format(
                                                    o_axisset.acjson_file.path.split("/")[-1],
                                                    s_gent,
                                                    d_gent["concUnit"],
                                                )
                                            )
                                        elif not (d_gent["timeUnit"] != es_unit):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} time unit {} is not controlled vocabulary.".format(
                                                    o_axisset.acjson_file.path.split("/")[-1],
                                                    s_gent,
                                                    d_gent["timeUnit"],
                                                )
                                            )
                                        # pop gent form acjson and brick
                                        d_acpop[s_key]["endpoint"].pop(s_gent)
                                        es_brickpop.discard(s_gent)
                            except TypeError:
                                pass
                            except AttributeError:  # None case
                                d_acpop[s_key]["endpoint"] = {}

                    # cstain
                    elif (o_bricked.brick_type == "cstain"):
                        # handle not_available case for microscopy channel not in use
                        if (o_bricked.annot_id == "not_available-notavailable_notavailable_notavailable"):
                            pass
                        else:
                            o_brick = CstainBrick.objects.get(annot_id=o_bricked.annot_id)
                            # loop through d_acjson
                            for s_key, o_well in d_acjson.items():
                                try:
                                    for s_gent, d_gent in o_well["endpoint"].items():
                                        if (s_gent == o_brick.cstain.annot_id):
                                            # check gent
                                            if (d_gent["manufacture"] != o_brick.provider.annot_id):
                                                self.stdout.write(
                                                    "Error @ acjson_endpointcheck : {} {} file manufacture {} differs from the brick one {}.".format(
                                                        o_axisset.setname,
                                                        s_gent,
                                                        d_gent["manufacture"],
                                                        o_brick.provider.annot_id,
                                                    )
                                                )
                                            elif (d_gent["catalogNu"] != o_brick.provider_catalog_id):
                                                self.stdout.write(
                                                    "Error @ acjson_endpointcheck : {} {} file catalog number  {} differs from the brick one {}.".format(
                                                        o_axisset.setname,
                                                        s_gent,
                                                        d_gent["catalogNu"],
                                                        o_brick.provider_catalog_id,
                                                    )
                                                )
                                            elif (d_gent["batch"] != o_brick.provider_batch_id):
                                                self.stdout.write(
                                                    "Error @ acjson_endpointcheck : {} {} file batch {} differs from the brick one {}".format(
                                                        o_axisset.setname,
                                                        s_gent,
                                                        d_gent["batch"],
                                                        o_brick.provider_batch_id,
                                                    )
                                                )
                                            elif (d_gent["wavelengthNm"] != o_brick.wavelength_nm_nominal_excitation):
                                                self.stdout.write(
                                                    "Error @ acjson_endpointcheck : {} {} file nominal excitation wavelength {}[nm] differs from the brick one {}[nm].".format(
                                                        o_axisset.setname,
                                                        s_gent,
                                                        d_gent["wavelengthNm"],
                                                        o_brick.wavelength_nm_nominal_excitation,
                                                    )
                                                )
                                            # bue 20190926: this is a list!
                                            elif (d_gent["recordSet"][0].split("-")[0:2] != o_axisset.setname.split("-")[0:2]):
                                                self.stdout.write(
                                                    "Error @ acjson_endpointcheck : {} {} file record set name {} differs from appacaxis one.".format(
                                                        o_axisset.setname,
                                                        s_gent,
                                                        d_gent["recordSet"],
                                                    )
                                                )
                                            elif (d_gent["externalId"] != o_brick.lincs_identifier):
                                                self.stdout.write(
                                                    "Error @ acjson_endpointcheck : {} {} file external idenfifier {} differs from appacaxis one.".format(
                                                        o_brick.lincs_identifier,
                                                        s_gent,
                                                        d_gent["externalId"],
                                                    )
                                                )
                                            elif not (d_gent["concUnit"] in es_unit):
                                                self.stdout.write(
                                                    "Error @ acjson_endpointcheck : {} {} concentration unit {} is not controlled vocabulary.".format(
                                                        o_axisset.acjson_file.path.split("/")[-1],
                                                        s_gent,
                                                        d_gent["concUnit"],
                                                    )
                                                )
                                            elif not (d_gent["timeUnit"] != es_unit):
                                                self.stdout.write(
                                                    "Error @ acjson_endpointcheck : {} {} time unit {} is not controlled vocabulary.".format(
                                                        o_axisset.acjson_file.path.split("/")[-1],
                                                        s_gent,
                                                        d_gent["timeUnit"],
                                                    )
                                                )
                                            # pop gent form acjson and brick
                                            d_acpop[s_key]["endpoint"].pop(s_gent)
                                            es_brickpop.discard(s_gent)
                                except TypeError:
                                    pass
                                except AttributeError:  # None case
                                    d_acpop[s_key]["endpoint"] = {}

                    # strange brick
                    else:
                        self.stdout.write(
                            "Error @ acjson_endpointcheck : unknown endpoint brick type {}. implemented are antibody1, antibody2 and cstain".format(o_bricked.brick_type)
                        )

                #  check if any endpoint gent left in d_acpop
                es_gent = set()
                for s_key, o_well in d_acpop.items():
                    try:
                        es_gent = es_gent.union(set(d_acpop[s_key]["endpoint"].keys()))
                    except TypeError:
                        pass
                if (len(es_gent) > 0):
                    self.stdout.write("Error @ acjson_endpointcheck : endpoint {} found in file but not as brick.".format(es_gent))
                elif (len(es_brickpop) > 0):
                    self.stdout.write("Error @ acjson_endpointcheck : endpoint {} found as brick but not in file.".format(es_brickpop))
                else:
                    print("checked {}: ok".format(s_axisset))
