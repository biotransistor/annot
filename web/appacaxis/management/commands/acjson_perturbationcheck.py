# import frmom django
from django.core.management.base import BaseCommand, CommandError

# import from python
import copy
import json

# import from annot
from appacaxis.models import PerturbationSet
from appbrreagentcompound.models import CompoundBrick
from appbrreagentprotein.models import ProteinSetBrick, ProteinBrick
from apponunit_bioontology.models import Unit

# const
es_unit = set((o_unit.annot_id for o_unit in Unit.objects.all()))

### main ###
class Command(BaseCommand):
    help = "Check perturbation acpipe json file against brick content."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument(
            "axisset",
            nargs='+',
            type=str,
            help="Annot coordinate axis set setname"
        )

    def handle(self, *args, **options):

        # process
        for s_axisset in options["axisset"]:

            # load axisset obj
            lo_axisset = PerturbationSet.objects.filter(setname__regex=r'{}'.format(s_axisset))
            if (len(lo_axisset) < 1):
                raise CommandError(
                    "Error: axisset argument {} does not specify any axis set.".format(
                        s_axisset
                    )
                )

            # for each axisset obj
            for o_axisset in lo_axisset:

                # load acjson file
                with open (o_axisset.acjson_file.path, "r") as f_acjson:
                    d_acjson = json.load(f_acjson)
                d_acpop = copy.deepcopy(d_acjson)

                # get a list of all the brick names
                es_brickpop = set([o_bricked.annot_id.split("-")[0] for o_bricked in o_axisset.brick.all()])

                # check acid, runid, runtype
                if (d_acjson["acid"] != o_axisset.acjson_file.path.split("/")[-1]):
                    self.stdout.write(
                        "Warning @ acjson_perturbationcheck : acid {} differs from filename {}.".format(
                            d_acjson["acid"],
                            o_axisset.acjson_file.path.split("/")[-1]
                        )
                    )
                elif (d_acjson["runid"] != o_axisset.setname):
                    self.stdout.write(
                        "Warning @ acjson_perturbationcheck : runid {} differs from setname {}.".format(
                            d_acjson["runid"],
                            o_axisset.setname
                        )
                    )
                elif (d_acjson["runtype"] != "annot_acaxis"):
                    self.stdout.write(
                        "Warning @ acjson_perturbationcheck : runtype {} is not annot_acaxis.".format(
                            d_acjson["runtype"]
                        )
                    )

                # handle each brick
                for o_bricked in o_axisset.brick.all():

                    # compound
                    if (o_bricked.brick_type == "compound"):
                        o_brick = CompoundBrick.objects.get(annot_id=o_bricked.annot_id)
                        # loop through d_acjson
                        for s_key, o_well in d_acjson.items():
                            try:
                                for s_gent, d_gent in o_well["perturbation"].items():
                                    if (s_gent == o_brick.compound.annot_id):
                                        # check gent
                                        if (d_gent["manufacture"] != o_brick.provider.annot_id):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} file manufacture {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["manufacture"],
                                                    o_brick.provider.annot_id,
                                                )
                                            )
                                        elif (d_gent["catalogNu"] != o_brick.provider_catalog_id):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} file catalog number  {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["catalogNu"],
                                                    o_brick.provider_catalog_id,
                                                )
                                            )
                                        elif (d_gent["batch"] != o_brick.provider_batch_id):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} file batch {} differs from the brick one {}".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["batch"],
                                                    o_brick.provider_batch_id,
                                                )
                                            )
                                        # bue 20190926: this is a list
                                        elif (d_gent["recordSet"][0].split("-")[0:2] != o_axisset.setname.split("-")[0:2]):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} file record set name {} differs from appacaxis one.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["recordSet"],
                                                )
                                            )
                                        elif (d_gent["externalId"] != o_brick.lincs_identifier):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file external idenfifier {} differs from appacaxis one.".format(
                                                    o_brick.lincs_identifier,
                                                    s_gent,
                                                    d_gent["externalId"],
                                                )
                                            )
                                        elif not (d_gent["concUnit"] in es_unit):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} concentration unit {} is not controlled vocabulary.".format(
                                                    o_axisset.acjson_file.path.split("/")[-1],
                                                    s_gent,
                                                    d_gent["concUnit"],
                                                )
                                            )
                                        elif not (d_gent["timeUnit"] != es_unit):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} time unit {} is not controlled vocabulary.".format(
                                                    o_axisset.acjson_file.path.split("/")[-1],
                                                    s_gent,
                                                    d_gent["timeUnit"],
                                                )
                                            )
                                        # pop gent form acjson and brick
                                        d_acpop[s_key]["perturbation"].pop(s_gent)
                                        es_brickpop.discard(s_gent)
                            except TypeError:
                                pass
                            except AttributeError:  # None case
                                d_acpop[s_key]["perturbation"] = {}

                    # proteinset
                    elif (o_bricked.brick_type == "proteinset"):
                        o_brick = ProteinSetBrick.objects.get(annot_id=o_bricked.annot_id)
                        # loop through d_acjson
                        for s_key, o_well in d_acjson.items():
                            try:
                                for s_gent, d_gent in o_well["perturbation"].items():
                                    if (s_gent == o_brick.proteinset.annot_id):
                                        # check gent
                                        if (d_gent["manufacture"] != o_brick.provider.annot_id):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} file manufacture {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["manufacture"],
                                                    o_brick.provider.annot_id,
                                                )
                                            )
                                        elif (d_gent["catalogNu"] != o_brick.provider_catalog_id):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} file catalog number  {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["catalogNu"],
                                                    o_brick.provider_catalog_id,
                                                )
                                            )
                                        elif (d_gent["batch"] != o_brick.provider_batch_id):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} file batch {} differs from the brick one {}".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["batch"],
                                                    o_brick.provider_batch_id,
                                                )
                                            )
                                        # bue 20190926: this is a list
                                        elif (d_gent["recordSet"][0].split("-")[0:2] != o_axisset.setname.split("-")[0:2]):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} file record set name {} differs from appacaxis one.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["recordSet"],
                                                )
                                            )
                                        elif (d_gent["externalId"] != o_brick.lincs_identifier):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file external idenfifier {} differs from appacaxis one.".format(
                                                    o_brick.lincs_identifier,
                                                    s_gent,
                                                    d_gent["externalId"],
                                                )
                                            )
                                        elif not (d_gent["concUnit"] in es_unit):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} concentration unit {} is not controlled vocabulary.".format(
                                                    o_axisset.acjson_file.path.split("/")[-1],
                                                    s_gent,
                                                    d_gent["concUnit"],
                                                )
                                            )
                                        elif not (d_gent["timeUnit"] != es_unit):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} time unit {} is not controlled vocabulary.".format(
                                                    o_axisset.acjson_file.path.split("/")[-1],
                                                    s_gent,
                                                    d_gent["timeUnit"],
                                                )
                                            )
                                        # pop gent form acjson and brick
                                        d_acpop[s_key]["perturbation"].pop(s_gent)
                                        es_brickpop.discard(s_gent)
                            except TypeError:
                                pass
                            except AttributeError:  # None case
                                d_acpop[s_key]["perturbation"] = {}

                    # protein
                    elif (o_bricked.brick_type == "protein"):
                        o_brick = ProteinBrick.objects.get(annot_id=o_bricked.annot_id)
                        # loop through d_acjson
                        for s_key, o_well in d_acjson.items():
                            try:
                                for s_gent, d_gent in o_well["perturbation"].items():
                                    if (s_gent == o_brick.protein.annot_id):
                                        # check gent
                                        if (d_gent["manufacture"] != o_brick.provider.annot_id):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} file manufacture {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["manufacture"],
                                                    o_brick.provider.annot_id,
                                                )
                                            )
                                        elif (d_gent["catalogNu"] != o_brick.provider_catalog_id):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} file catalog number  {} differs from the brick one {}.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["catalogNu"],
                                                    o_brick.provider_catalog_id,
                                                )
                                            )
                                        elif (d_gent["batch"] != o_brick.provider_batch_id):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} file batch {} differs from the brick one {}".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["batch"],
                                                    o_brick.provider_batch_id,
                                                )
                                            )
                                        # bue 20190926: this is a list
                                        elif (d_gent["recordSet"][0].split("-")[0:2] != o_axisset.setname.split("-")[0:2]):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} file record set name {} differs from appacaxis one.".format(
                                                    o_axisset.setname,
                                                    s_gent,
                                                    d_gent["recordSet"],
                                                )
                                            )
                                        elif (d_gent["externalId"] != o_brick.lincs_identifier):
                                            self.stdout.write(
                                                "Error @ acjson_endpointcheck : {} {} file external idenfifier {} differs from appacaxis one.".format(
                                                    o_brick.lincs_identifier,
                                                    s_gent,
                                                    d_gent["externalId"],
                                                )
                                            )
                                        elif not (d_gent["concUnit"] in es_unit):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} concentration unit {} is not controlled vocabulary.".format(
                                                    o_axisset.acjson_file.path.split("/")[-1],
                                                    s_gent,
                                                    d_gent["concUnit"],
                                                )
                                            )
                                        elif not (d_gent["timeUnit"] != es_unit):
                                            self.stdout.write(
                                                "Error @ acjson_perturbationcheck : {} {} time unit {} is not controlled vocabulary.".format(
                                                    o_axisset.acjson_file.path.split("/")[-1],
                                                    s_gent,
                                                    d_gent["timeUnit"],
                                                )
                                            )
                                        # pop gent form acjson
                                        d_acpop[s_key]["perturbation"].pop(s_gent)
                                        es_brickpop.discard(s_gent)
                            except TypeError:
                                pass
                            except AttributeError:  # None case
                                d_acpop[s_key]["perturbation"] = {}

                    # strange brick
                    else:
                        self.stdout.write(
                            "Error @ acjson_perturbationcheck : unknown perturbation brick type {}. implemented are antibody1, antibody2 and cstain".format(o_bricked.brick_type)
                        )

                #  check if any perturbation gent left
                es_gent = set()
                for s_key, o_well in d_acpop.items():
                    try:
                        es_gent = es_gent.union(set(d_acpop[s_key]["perturbation"].keys()))
                    except TypeError:
                        pass
                if (len(es_gent) > 0):
                    self.stdout.write("Error @ acjson_perturbationcheck : perturbation {} found in file but not as brick.".format(es_gent))
                elif (len(es_brickpop) > 0):
                    self.stdout.write("Error @ acjson_perturbationcheck : perturbation {} found as brick but not in file.".format(es_brickpop))
                else:
                    print("checked {}: ok".format(s_axisset))
