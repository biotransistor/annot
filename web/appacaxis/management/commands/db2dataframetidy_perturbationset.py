# import frmom django
from django.core.management.base import BaseCommand, CommandError
from django.core import management

# import from python
import codecs
import io
import json

# imporfrom acpipe
import acpipe_acjson.acjson as ac

# import from annot
from appacaxis.models import AcjsonToTsv, PerturbationSet
from prjannot.settings import MEDIA_ROOTQUARANTINE

### main ###
class Command(BaseCommand):
    help = "get absolute file path for set requested perturbation set json dump."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument(
            "axisset",
            nargs='+',
            type=str,
            help="Annot coordinate axis set setname"
        )
        # named (optional) arguments
        parser.add_argument(
            "--user",
            nargs="?",
            default=0,
            type=int,
            help="user id to fetch userspecific output settings.",
        )


    def handle(self, *args, **options):
        ls_pathfile = []

        # process
        for s_axisset in options["axisset"]:

            # load axisset obj
            lo_axisset = PerturbationSet.objects.filter(setname__regex=r'{}'.format(s_axisset))
            if (len(lo_axisset) < 1):
                raise CommandError(
                    "Error: axisset argument {} does not specify any axis set.".format(
                        s_axisset
                    )
                )

            # for each axisset obj
            for o_axisset in lo_axisset:

                # load acjson file
                with open (o_axisset.acjson_file.path, "r") as f_acjson:
                    d_acjson = json.load(f_acjson)

                if (options["user"] == 0):
                    s_delimiter = "\t"
                    es_leaf = ac.es_DATAFRAMELEAF
                else:
                    # load tsv setting
                    o_tsvsetting = AcjsonToTsv.objects.get(annot_user=options["user"])
                    s_delimiter = codecs.decode(o_tsvsetting.tsv_delimiter, "unicode_escape")
                    es_leaf = set()

                    # run
                    if (o_tsvsetting.runid):
                        es_leaf.add("runid")
                    if (o_tsvsetting.welllayout):
                        es_leaf.add("welllayout")
                    if (o_tsvsetting.spotlayout):
                        es_leaf.add("spotlayout")
                    # coordinate
                    if (o_tsvsetting.sxi):
                        es_leaf.add("sxi")
                    if (o_tsvsetting.ixi):
                        es_leaf.add("ixi")
                    if (o_tsvsetting.iixii):
                        es_leaf.add("iixii")
                    if (o_tsvsetting.iWell):
                        es_leaf.add("iWell")
                    if (o_tsvsetting.iSpot):
                        es_leaf.add("iSpot")
                    # collapsed
                    if (o_tsvsetting.runCollapsed):
                        es_leaf.add("runCollapsed")
                    if (o_tsvsetting.axisCollapsed):
                        es_leaf.add("axisCollapsed")
                    if (o_tsvsetting.recordSetCollapsed):
                        es_leaf.add("recordSetCollapsed")
                    if (o_tsvsetting.recordSet):
                        es_leaf.add("recordSet")
                    # sample perturbation endpoint
                    if (o_tsvsetting.conc):
                        es_leaf.add("conc")
                    if (o_tsvsetting.concUnit):
                        es_leaf.add("concUnit")
                    if (o_tsvsetting.timeBegin):
                        es_leaf.add("timeBegin")
                    if (o_tsvsetting.timeEnd):
                        es_leaf.add("timeEnd")
                    if (o_tsvsetting.timeUnit):
                        es_leaf.add("timeUnit")
                    if (o_tsvsetting.cultType):
                        es_leaf.add("cultType")
                    if (o_tsvsetting.longRecord):
                        es_leaf.add("longRecord")
                    if (o_tsvsetting.externalId):
                        es_leaf.add("externalId")
                    # sample
                    if (o_tsvsetting.passageOrigin):
                        es_leaf.add("passageOrigin")
                    if (o_tsvsetting.passageUsed):
                        es_leaf.add("passageUsed")
                    # perturbation
                    if (o_tsvsetting.galId):
                        es_leaf.add("galId")
                    if (o_tsvsetting.pinId):
                        es_leaf.add("pinId")
                    # endpoint
                    if (o_tsvsetting.micChannel):
                        es_leaf.add("micChannel")
                    if (o_tsvsetting.wavelengthNm):
                        es_leaf.add("wavelengthNm")
                    if (o_tsvsetting.hostOrganism):
                        es_leaf.add("hostOrganism")
                    if (o_tsvsetting.targetOrganism):
                        es_leaf.add("targetOrganism")
                    if (o_tsvsetting.isotype):
                        es_leaf.add("isotype")

                # process
                es_pathfile = ac.acjson2dataframetsv(
                    d_acjson=d_acjson,
                    es_dfleaf=es_leaf,
                    s_mode="tidy",
                    s_delimiter=s_delimiter,
                    s_opath=MEDIA_ROOTQUARANTINE
                )
                # result
                ls_pathfile.extend(es_pathfile)
                ls_pathfile.sort()

        # output
        self.stdout.write("!".join(ls_pathfile))
