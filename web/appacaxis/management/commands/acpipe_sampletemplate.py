# import frmom django
from django.core.management.base import BaseCommand, CommandError
from django.apps import apps

# import from python
import datetime

# import from annot
from appacaxis.models import SampleSet
from appbrsamplehuman.models import HumanBrick
from prjannot.settings import MEDIA_ROOTQUARANTINE


### main ###
class Command(BaseCommand):
    help = "Generate sample brick based acpipe template."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument(
            "axisset",
            nargs='+',
            type=str,
            help="Annot coordinate axis set setname"
        )

    def handle(self, *args, **options):

        # process
        for s_axisset in options["axisset"]:

            # load axisset obj
            lo_axisset = SampleSet.objects.filter(setname__regex=r'{}'.format(s_axisset))
            if (len(lo_axisset) < 1):
                raise CommandError(
                    "Error: axisset argument {} does not specify any axis set.".format(
                    s_axisset
                    )
                )

            # for each axisset obj
            for o_axisset in lo_axisset:
                # filename
                s_file = "acpipeTemplateCode_{}.py".format(o_axisset.setname)

                # generate python code
                ls_line = [
                    "###",
                    "# title: {}".format(s_file),
                    "#",
                    "# date: {}".format(datetime.date.today().isoformat()),
                    "# license: GPL>=3",
                    "# author: bue",
                    "#",
                    "# description:",
                    "#   acpipe script to generate a sampleset related acjson file.",
                    "#   template automatically generated by annot softawre.",
                    "#   check out: https://gitlab.com/biotransistor/annot",
                    "###",
                    "",
                    "# python library",
                    "import copy",
                    "import json",
                    "",
                    "# acpipe library",
                    "# check out: https://gitlab.com/biotransistor/acpipe_acjson",
                    "import acpipe_acjson.acjson as ac",
                    "",
                    "# build acjson",
                    "d_acjson = ac.acbuild(",
                    "    s_runid='{}',".format(o_axisset.setname),
                    "    s_runtype='annot_acaxis',",
                    "    s_welllayout='?x?',",
                    "    ti_spotlayout=(1,),",
                    "    s_log='from the bricks'",
                    ")",
                ]

                # handle each brick
                for o_bricked in o_axisset.brick.all():
                    if (o_bricked.brick_type == "human"):
                        o_brick = HumanBrick.objects.get(annot_id=o_bricked.annot_id)
                        ls_brick = [
                            "",
                            "# reagent: {}".format(o_bricked.bricked_id),
                            "s_gent = '{}'".format(o_brick.sample.annot_id),
                            "d_record = {s_gent: copy.deepcopy(ac.d_RECORDLONG)}",
                            "d_record[s_gent].update(copy.deepcopy(ac.d_SAMPLE))",
                            "d_record[s_gent].update(copy.deepcopy(ac.d_SET))",
                            "d_record[s_gent].update(copy.deepcopy(ac.d_EXTERNALID))",
                            "d_record[s_gent].update({'passageOrigin': ?})", #.format(o_brick.passage_origin.split("|")),
                            "d_record[s_gent].update({'passageUsed': ?})", #.format(o_brick.passage_use.split("|")),
                            "d_record[s_gent].update({{'manufacture': '{}'}})".format(o_brick.provider.annot_id),
                            "d_record[s_gent].update({{'catalogNu': '{}'}})".format(o_brick.provider_catalog_id),
                            "d_record[s_gent].update({{'batch': '{}'}})".format(o_brick.provider_batch_id),
                            "d_record[s_gent].update({'conc': ?})",
                            "d_record[s_gent].update({{'concUnit': '{}'}})".format(o_brick.concentration_unit.annot_id),
                            "d_record[s_gent].update({'cultType': '?'})",
                            "d_record[s_gent].update({'timeBegin': ?})",
                            "d_record[s_gent].update({'timeEnd': ?})",
                            "d_record[s_gent].update({{'timeUnit': '{}'}})".format(o_brick.time_unit.annot_id),
                            "d_record[s_gent].update({{'recordSet': ['{}']}})".format(o_axisset.setname),
                            "d_record[s_gent].update({{'externalId': '{}'}})".format(o_brick.lincs_identifier),
                            "d_acjson = ac.acfuseaxisrecord(",
                            "    d_acjson,",
                            "    s_coor='?',",
                            "    s_axis='sample',",
                            "    d_record=d_record",
                            ")",
                        ]

                    else:
                        raise CommandError(
                            "Error @ acpipe_sampletemplate.py : unknown sample brick type {}. implemented is sample type human.".format(o_bricked.brick_type)
                        )

                    # add brick related code
                    ls_line.extend(ls_brick)

                # handle write to json file
                ls_file = [
                    "",
                    "# write to json file",
                    "print('write file: {}'.format(d_acjson['acid']))",
                    "with open(d_acjson['acid'], 'w') as f_acjson:",
                    "    json.dump(d_acjson, f_acjson, sort_keys=True)",  # indent=4
                ]
                ls_line.extend(ls_file)

                # write file
                s_pathfile = "{}{}".format(MEDIA_ROOTQUARANTINE, s_file)
                with open(s_pathfile, "w", newline="") as f_out:
                    for s_line in ls_line:
                        s_line += "\n"
                        f_out.write(s_line)
                # output
                print("generated {} template: {}".format(o_axisset.setname, s_pathfile))
