# import from django
from django.core import management
from django.core.management.base import BaseCommand  #,CommandError

# import from python
from datetime import datetime
import os
import shutil

# import from annot
from app3runset.models import RunSet
from app4superset.models import SuperSet, SuperSetFile
from appacaxis.models import EndpointSet, PerturbationSet, SampleSet
from appbrsamplehuman.models import HumanBrick
from prjannot.settings import MEDIA_ROOT

# main
class Command(BaseCommand):
    help = "Pack latest acjson and superset files into YYYYMMDD_json_latestexperiment/upload folder."

    def handle(self, *args, **options):
        # generate latest out folder path
        s_output = "{}experiment/{}_acjson_latestexperiment/".format(
            MEDIA_ROOT,
            datetime.now().strftime("%Y%m%d"),
        )
        shutil.rmtree(s_output, ignore_errors=True)

        # get filepath sampleset
        ls_ipathfile = []

        self.stdout.write("\nexperiment_pkgacjson brickfile")
        # sample brick
        s_opath = "{}samplebrick/".format(s_output)
        os.makedirs(s_opath, exist_ok=True)
        ls_ipathfile = [o_record.publication_file.path for o_record in HumanBrick.objects.all() if (o_record.publication_file.name != "")]
        for s_ipathfile in ls_ipathfile:
            self.stdout.write("processing: {}".format(s_ipathfile))
            s_opathfile = "{}{}".format(s_opath, s_ipathfile.split("/")[-1])
            shutil.copy(s_ipathfile,s_opathfile)

        self.stdout.write("\nexperiment_pkgacjson acaxis")
        # sample
        s_opath = "{}acaxis/".format(s_output)
        os.makedirs(s_opath, exist_ok=True)
        ls_ipathfile = [o_record.acjson_file.path for o_record in SampleSet.objects.all() if (o_record.acjson_file.name != "")]
        for s_ipathfile in ls_ipathfile:
            self.stdout.write("processing: {}".format(s_ipathfile))
            s_opathfile = "{}{}".format(s_opath, s_ipathfile.split("/")[-1])
            shutil.copy(s_ipathfile,s_opathfile)

        # perturbation
        s_opath = "{}acaxis/".format(s_output)
        os.makedirs(s_opath, exist_ok=True)
        ls_ipathfile = [o_record.acjson_file.path for o_record in PerturbationSet.objects.all() if (o_record.acjson_file.name != "")]
        for s_ipathfile in ls_ipathfile:
            self.stdout.write("processing: {}".format(s_ipathfile))
            s_opathfile = "{}{}".format(s_opath, s_ipathfile.split("/")[-1])
            shutil.copy(s_ipathfile,s_opathfile)

        # endpoint
        s_opath = "{}acaxis/".format(s_output)
        os.makedirs(s_opath, exist_ok=True)
        ls_ipathfile = [o_record.acjson_file.path for o_record in EndpointSet.objects.all() if (o_record.acjson_file.name != "")]
        for s_ipathfile in ls_ipathfile:
            self.stdout.write("processing: {}".format(s_ipathfile))
            s_opathfile = "{}{}".format(s_opath, s_ipathfile.split("/")[-1])
            shutil.copy(s_ipathfile,s_opathfile)

        # superset
        self.stdout.write("\nexperiment_pkgacjson superset")
        s_opath = "{}superset/".format(s_output)
        os.makedirs(s_opath, exist_ok=True)
        ls_ipathfile = [o_record.acjson_file.path for o_record in SuperSet.objects.all() if (o_record.acjson_file.name != "")]
        for s_ipathfile in ls_ipathfile:
            self.stdout.write("processing: {}".format(s_ipathfile))
            s_opathfile = "{}{}".format(s_opath, s_ipathfile.split("/")[-1])
            shutil.copy(s_ipathfile,s_opathfile)

        # superset file
        self.stdout.write("\nexperiment_pkgacjson supersetfile")
        s_opath = "{}supersetfile/".format(s_output)
        os.makedirs(s_opath, exist_ok=True)
        ls_ipathfile = [o_record.superset_file.path for o_record in SuperSetFile.objects.all() if (o_record.superset_file.name != "")]
        for s_ipathfile in ls_ipathfile:
            self.stdout.write("processing: {}".format(s_ipathfile))
            s_opathfile = "{}{}".format(s_opath, s_ipathfile.split("/")[-1])
            shutil.copy(s_ipathfile,s_opathfile)

        # runset
        self.stdout.write("\nexperiment_pkgacjson runset")
        s_opath = "{}runset/".format(s_output)
        os.makedirs(s_opath, exist_ok=True)
        ls_ipathfile = [o_record.acjson_file.path for o_record in RunSet.objects.all() if (o_record.acjson_file.name != "")]
        for s_ipathfile in ls_ipathfile:
            self.stdout.write("processing: {}".format(s_ipathfile))
            s_opathfile = "{}{}".format(s_opath, s_ipathfile.split("/")[-1])
            shutil.copy(s_ipathfile,s_opathfile)
