# import frmom django
from django.core.management.base import BaseCommand, CommandError
from django.core import management

# import from python
import json

# imporfrom acpipe
import acpipe_acjson.acjson as ac

# import from annot
from app4superset.models import SuperSet
from appsabrick import pkgmetadata
from prjannot.settings import MEDIA_ROOTQUARANTINE

### main ###
class Command(BaseCommand):
    help = "get absolute file path for requested superset realted brick dump."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument(
            "axisset",
            nargs='+',
            type=str,
            help="Annot coordinate axis set setname"
        )
        # named (optional) arguments
        parser.add_argument(
            "--filetype",
            nargs="?",
            default="tsv",
            type=str,
            help="filetype in wich the brick content should be outputed.",
        )

    def handle(self, *args, **options):
        ls_pathfile = []

        # process each axis set
        for s_axisset in options["axisset"]:

            # load acjson file
            o_axisset = SuperSet.objects.get(superset_name__regex=r'{}'.format(s_axisset))
            # process
            ls_brickfile = pkgmetadata.pkgbrick(
                ls_acjson=[o_axisset.acjson_file.path],
                s_filetype=options["filetype"],
                s_compilationname=s_axisset,
                s_opath=MEDIA_ROOTQUARANTINE,
            )
            # result
            ls_pathfile.extend(ls_brickfile)

        # output
        print(ls_pathfile)
        self.stdout.write("!".join(ls_pathfile))
