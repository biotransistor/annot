# import from django
from django.core.management.base import BaseCommand  #,CommandError

# import from python
from datetime import datetime
import glob
import os
import shutil

# import from annot
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest

# const
es_table = {
    "acaxis_endpointset",
    "acaxis_perturbationset",
    "acaxis_sampleset",
    "superset_acpipe",
    "superset_supersetfile",
    "superset_superset",
    "runset_runset",
    "track_memasuperset",
    "track_memaassay",
    "study_study",
    "investigation_investigation",
}

### main ###
class Command(BaseCommand):
    help = "Pack latest experiment table content as json backup into YYYYMMDD_json_latestexperiment folder."

    def handle(self, *args, **options):
        # backup acaxis tables
        os.system(
            "python /usr/src/app/manage.py dumpdata appacaxis.EndpointSet > /usr/src/media/experiment/oo/acaxis_endpointset_$(date +%Y%m%d)_oo.json"
        )
        os.system(
            "python /usr/src/app/manage.py dumpdata appacaxis.PerturbationSet > /usr/src/media/experiment/oo/acaxis_perturbationset_$(date +%Y%m%d)_oo.json"
        )
        os.system(
            "python /usr/src/app/manage.py dumpdata appacaxis.SampleSet > /usr/src/media/experiment/oo/acaxis_sampleset_$(date +%Y%m%d)_oo.json"
        )
        # backup superset tables
        os.system(
            "python /usr/src/app/manage.py dumpdata app4superset.AcPipe > /usr/src/media/experiment/oo/superset_acpipe_$(date +%Y%m%d)_oo.json"
        )
        os.system(
            "python /usr/src/app/manage.py dumpdata app4superset.SuperSetFile > /usr/src/media/experiment/oo/superset_supersetfile_$(date +%Y%m%d)_oo.json"
        )
        os.system(
            "python /usr/src/app/manage.py dumpdata app4superset.SuperSet > /usr/src/media/experiment/oo/superset_superset_$(date +%Y%m%d)_oo.json"
        )
        # backup runset table
        os.system(
            "python /usr/src/app/manage.py dumpdata app3runset.RunSet > /usr/src/media/experiment/oo/runset_runset_$(date +%Y%m%d)_oo.json"
        )
        # backup track tables
        os.system(
            "python /usr/src/app/manage.py dumpdata app2track.MemaAssayTracking > /usr/src/media/experiment/oo/track_memaassay_$(date +%Y%m%d)_oo.json"
        )
        os.system(
            "python /usr/src/app/manage.py dumpdata app2track.MemaSuperSetTracking > /usr/src/media/experiment/oo/track_memasuperset_$(date +%Y%m%d)_oo.json"
        )
        # backup study table
        os.system(
            "python /usr/src/app/manage.py dumpdata app1study.Study > /usr/src/media/experiment/oo/study_study_$(date +%Y%m%d)_oo.json"
        )
        # basckup investigation table
        os.system(
            "python /usr/src/app/manage.py dumpdata app0investigation.investigation > /usr/src/media/experiment/oo/investigation_investigation_$(date +%Y%m%d)_oo.json"
        )
        print("yepa")

        # generate latest output folder path
        s_outputpath = "{}experiment/{}_json_latestexperiment/".format(
            MEDIA_ROOT,
            datetime.now().strftime("%Y%m%d")
        )
        shutil.rmtree(s_outputpath, ignore_errors=True)

        # process each backuped table
        for s_table in es_table:
            self.stdout.write(
                "\nProcessing experiment part: {}".format(
                    s_table
                )
            )
            # get latest fs json
            s_latestregex = "{}experiment/oo/{}_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_oo.json".format(
                MEDIA_ROOT,
                s_table
            )
            ls_annotfile = glob.glob(s_latestregex)
            s_latestpath = annotfilelatest(ls_annotfile=ls_annotfile)
            if not(s_latestpath is None):
                # copy in to output path
                self.stdout.write(
                    "Copy {} to {}.".format(s_latestpath, s_outputpath)
                )
                os.makedirs(s_outputpath, exist_ok=True)
                shutil.copy(s_latestpath, s_outputpath, follow_symlinks=False)
