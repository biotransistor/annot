from django.db import models

# import from annot
from appbrxperson.models import PersonBrick
from apponcompound_ebi.models import Compound
from apponorganism_bioontology.models import Organism
from apponprotein_uniprot.models import Protein
from apponcellularcomponent_go.models import CellularComponent
from apponprovider_own.models import Provider
from apponunit_bioontology.models import Unit
from appsabrick.structure import makeannotid, checkvoci, checkfraction, scomma2sbr, list2sbr, sbr2list
from prjannot.structure import retype


#### protein set ####

# constant
ls_column_proteinsetbrick = [
    "annot_id",
    "proteinset",
    "common_name",
    "lincs_identifier",
    "provider",
    "provider_catalog_id",
    "provider_batch_id",
    "available",
    "final_concentration_unit",
    "time_unit",
]

# data structure
class ProteinSetBrick(models.Model):
    annot_id = models.SlugField(
        primary_key=True,
        max_length=256,
        verbose_name="Annot Identifier",
        help_text="Automatically generated."
    )
    proteinset = models.ForeignKey(
        CellularComponent,
        default="not_yet_specified",
        verbose_name="Protein set",
        help_text="Choose protein set."
    )
    common_name = models.CharField(
        max_length=256,
        verbose_name="Descriptive name",
        help_text="Descriptive protein set name.",
        blank=True
    )
    lincs_identifier = models.CharField(
        max_length=256,
        verbose_name="Lincs identifier",
        help_text="Official lincs protein set identifier for lincs prj related protein set.",
        blank=True
    )
    provider = models.ForeignKey(
        Provider,
        default="not_yet_specified",
        verbose_name="Provider",
        help_text="Choose protein set provider."
    )
    provider_catalog_id = models.CharField(
        default="not_yet_specified",
        verbose_name="Provider catalog protein set id",
        max_length=256,
        help_text="Identifier or catalogue number or name assigned to the protein set by the vendor or provider."
    )
    provider_batch_id = models.CharField(
        default="not_yet_specified",
        verbose_name="Provider batch id",
        max_length=256,
        help_text="Batch or lot number assigned to the protein set by the vendor or provider."
    )
    available = models.BooleanField(
        default=True,
        help_text="Is this reagent at stock in our laboratory?"
    )
    final_concentration_unit = models.ForeignKey(
        Unit,
        default="ug_per_mL_uo0000274",
        related_name="ProteinSetFinalConcUnit",
        help_text="Default incubation concentration unit."
    )
    time_unit = models.ForeignKey(
        Unit,
        default="hour_uo0000032",
        related_name="ProteinSetTimeUnit",
        help_text="Default incubation time unit."
    )
    # primary key generator
    def save(self, *args, **kwargs):
        s_annot_id = makeannotid(
            s_base=str(self.proteinset.annot_id),
            s_provider=str(self.provider.annot_id),
            s_provider_catalog_id=str(self.provider_catalog_id),
            s_provider_batch_id=str(self.provider_batch_id)
        )
        self.annot_id = s_annot_id
        super().save(*args, **kwargs)

    # output
    #__repr__ = __str__
    def __repr__(self):
        s_out = "\nProteinSetBrick:\n"
        for s_column in ls_column_proteinsetbrick:
            s_out = s_out + s_column + "{}{}: {}\n".format(
                s_out,
                s_column,
                getattr(self, s_column)
            )
        return(s_out)

    def __str__(self):
        return(self.annot_id)

    class Meta:
        unique_together = ((
            "proteinset",
            "provider",
            "provider_catalog_id",
            "provider_batch_id"
        ),)
        ordering = ["annot_id"]
        verbose_name_plural = "Perturbation Proteinset"


# database dictionary handles
def proteinsetbrick_db2d(s_annot_id, ls_column=ls_column_proteinsetbrick):
    """
    annot_id is the only input needed
    """
    #empty output
    d_brick = {}

    # pull form annot db
    o_brick = ProteinSetBrick.objects.get(annot_id=s_annot_id)

    # write in to d obj and re-type
    for s_column in ls_column:
        s_value = str(getattr(o_brick, s_column))
        o_value = retype(s_value)
        d_brick.update({s_column: o_value})

    # many to many field handle
    # nop

    # notes handle
    # nop

    # out
    return(d_brick)


def proteinsetbrick_d2db(d_brick, ls_column=ls_column_proteinsetbrick):
    """
    """
    # get annot id
    print("\nCheck ctrl voci for proteinset and provider to be able to generate reagent annot_id.")
    o_proteinset = checkvoci(d_brick["proteinset"], CellularComponent)
    o_provider = checkvoci(d_brick["provider"], Provider)
    s_provider_catalog_id = str(d_brick["provider_catalog_id"])
    s_provider_batch_id = str(d_brick["provider_batch_id"])
    s_annot_id = makeannotid(
        s_base=str(o_proteinset),
        s_provider=str(o_provider),
        s_provider_catalog_id=s_provider_catalog_id,
        s_provider_batch_id=s_provider_batch_id
    )
    print("ok", s_annot_id)

    # get annot db obj
    try:
        o_brick = ProteinSetBrick.objects.get(annot_id=s_annot_id)
    except ProteinSetBrick.DoesNotExist:
        o_brick = ProteinSetBrick(
            annot_id=s_annot_id,
            proteinset=o_proteinset,
            provider=o_provider,
            provider_catalog_id=s_provider_catalog_id,
            provider_batch_id=s_provider_batch_id
        )
        o_brick.save()

    # write dictionary into db obj
    for s_column in ls_column:
        print("Put into field:", s_column, ":", d_brick[s_column])
        # annot_id field
        if (s_column == "annot_id"):
            o_brick.annot_id = s_annot_id
        # foreigne key fields
        # get dictionary vocabulary controlled and str into obj turned
        elif (s_column == "proteinset"):
            o_brick.proteinset = checkvoci(d_brick["proteinset"], CellularComponent)
        elif (s_column == "provider"):
            o_brick.provider = checkvoci(d_brick["provider"], Provider)
        elif (s_column == "final_concentration_unit"):
            o_brick.final_concentration_unit = checkvoci(d_brick["final_concentration_unit"], Unit)
        elif (s_column == "time_unit"):
            o_brick.time_unit = checkvoci(d_brick["time_unit"], Unit)
        # many to many fields
        # nop
        else:
            # other fields
            setattr(o_brick, s_column, d_brick[s_column])

    # save db obj into annot db
    o_brick.save()


# annot db json handels
def proteinsetbrick_db2objjson(ls_column=ls_column_proteinsetbrick):
    """
    this is main
    get json obj from the db. this is the main proteinset brick object.
    any proteinset brick file will be built form this object.
    """
    # set output variables
    dd_json = {}

    # get all annot_id
    ls_annotid =[]
    lo_brick = (ProteinSetBrick.objects.all())
    for o_brick in lo_brick:
        ls_annotid.append(str(o_brick.annot_id))
    ls_annotid.sort()

    # for each annot_id get json compatible dictionary with dbcontent
    for s_annotid in ls_annotid:
        # pull json compatible dictionary from annot db
        print("pull from database:", s_annotid)
        # get dictionary brick for annot_id
        d_brick = proteinsetbrick_db2d(s_annot_id=s_annotid, ls_column=ls_column)
        dd_json.update({s_annotid:d_brick})

    # output
    return(dd_json)


def proteinsetbrick_objjson2db(dd_json, ls_column=ls_column_proteinsetbrick):
    """
    this is main
    """
    # extract obj json into annot db
    for s_key in dd_json:
        d_json = dd_json[s_key]
        proteinsetbrick_d2db(d_brick=d_json, ls_column=ls_column)



#### protein ####

# constant
ls_column_proteinbrick = [
    "annot_id",
    "protein",
    "common_name",
    "lincs_identifier",
    "proteinset_annot_id",
    "proteinset_ratio",
    "provider",
    "provider_catalog_id",
    "provider_batch_id",
    "native",
    "code_organism",
    "source_organism_note",
    "source_organism",
    "isoform_explicite",
    "sequence_real_note",
    "sequence_real",
    "length_real",
    "mass_Da_real",
    "purity",
    "carryer_free",
    "lyophilized",
    "available",
    "stocksolution_buffer_compound",
    "stocksolution_concentration_value",
    "stocksolution_concentration_unit",
    "final_concentration_unit",
    "time_unit",
    "reference_url",
    "responsible",
    "notes"
]

# data structure
class ProteinBrick(models.Model):
    annot_id = models.SlugField(
        primary_key=True,
        max_length=256,
        verbose_name="Annot Identifier",
        help_text="Automatically generated."
    )
    protein = models.ForeignKey(
        Protein,
        default="not_yet_specified",
        verbose_name="Protein",
        help_text="Choose reference protein."
    )
    common_name = models.CharField(
        max_length=256,
        verbose_name="Descriptive name",
        help_text="Descriptive protein name.",
        blank=True
    )
    lincs_identifier = models.CharField(
        max_length=256,
        verbose_name="Lincs identifier",
        help_text="Official lincs protein identifier for lincs prj related protein.",
        blank=True
    )
    provider = models.ForeignKey(
        Provider,
        default="not_yet_specified",
        verbose_name="Provider",
        help_text="Choose protein provider."
    )
    provider_catalog_id = models.CharField(
        default="not_yet_specified",
        verbose_name="Provider catalog protein id",
        max_length=256,
        help_text="Identifier or catalogue number or name assigned to the protein by the vendor or provider."
    )
    provider_batch_id = models.CharField(
        default="not_yet_specified",
        verbose_name="Provider batch id",
        max_length=256,
        help_text="Batch or lot number assigned to the protein by the vendor or provider."
    )
    proteinset_annot_id = models.ForeignKey(
        ProteinSetBrick,
        default="not_yet_specified-notyetspecified_notyetspecified_notyetspecified",
        verbose_name="Protein set",
        help_text="If this protein was provided as part of a proteinset choose the corresponding proteinset else choose not_available-notavailable_notavailable_notavailable."
    )
    proteinset_ratio = models.PositiveIntegerField(
        default=None,
        verbose_name="Protein set ratio",
        help_text="Proteinset specific protein ratio integer.",
        null=True,
        blank=True
    )
    native = models.NullBooleanField(
        default=None,
        verbose_name="Native protein",
        help_text="Is this as native or recombinant protein?"
    )
    code_organism = models.ForeignKey(
        Organism,
        related_name="ProteinBrickCodeOrganism",
        default="not_yet_specified",
        verbose_name="Protein DNA code source",
        help_text="Specify protein DNA template source organism."
    )
    source_organism_note = models.CharField(
        max_length=256,
        default="not_yet_specified",
        verbose_name="Protein production source organism note",
        help_text="Information about the production source other the just the species.",
        blank=True
    )
    source_organism = models.ForeignKey(
        Organism,
        related_name="ProteinBrickSourceOrganism",
        default="not_yet_specified",
        verbose_name="Protein production source",
        help_text="Specify protein production source organism or cell line."
    )
    isoform_explicite = models.NullBooleanField(
        default=None,
        verbose_name="Isoform explicitness",
        help_text="According to the data sheet, is this only speific isoform of the protein or is the canonical protein and its sequence assumed?"
    )
    sequence_real_note = models.CharField(
        default="not_yet_specified",
        verbose_name="Real protein sequence note",
        max_length=256,
        help_text="Protein sequence provider note."
    )
    sequence_real = models.TextField(
        default="not_yet_specified",
        verbose_name="Real protein sequence",
        help_text="Exact fasta format sequence of protein used in the lab."
    )
    length_real = models.PositiveIntegerField(
        default=None,
        verbose_name="Real protein length",
        help_text="Real protein length in number amino acids.",
        null=True,
        blank=True
    )
    mass_Da_real = models.FloatField(
        default=None,
        verbose_name="Real protein mass",
        help_text="Real protein mass in Da. If a mass range is given, take the mean value.",
        null=True,
        blank=True
    )
    purity = models.FloatField(
        default=None,
        verbose_name="Purity",
        help_text="Protein purity in fraction. If caused by different purity detection methodes more then one purity value is given, choose the worst purity value.",
        null=True,
        blank=True
    )
    carryer_free = models.NullBooleanField(
        default=None,
        verbose_name="Carrier free",
        help_text="Was the protein produced without carrier agences?"
    )
    lyophilized = models.NullBooleanField(
        default=None,
        verbose_name="Lyophilized deployment",
        help_text="Form in which the protein was shipped."
    )
    available = models.BooleanField(
        default=True,
        help_text="Is this reagent at stock in our laboratory?"
    )
    stocksolution_buffer_compound_display = models.TextField(
        help_text="Automatically generated from stocksolution_buffer_compound field.",
        null=True,
    )
    stocksolution_buffer_compound = models.ManyToManyField(
        Compound,
         help_text="Choose all utilized buffer compounds."
    )
    stocksolution_concentration_value = models.FloatField(
        default=None,
        verbose_name="Stock solution concentration value",
        help_text="Value.",
        null=True,
        blank=True,
    )
    stocksolution_concentration_unit = models.ForeignKey(
        Unit,
        default="not_yet_specified",
        related_name="ProteinStockConcUnit",
        help_text="Default stocksolution concentration unit."
    )
    final_concentration_unit = models.ForeignKey(
        Unit,
        default="ug_per_mL_uo0000274",
        related_name="ProteinFinalConcUnit",
        help_text="Default incubation concentration unit."
    )
    time_unit = models.ForeignKey(
        Unit,
        default="hour_uo0000032",
        related_name="ProteinTimeUnit",
        help_text="Default incubation time unit."
    )
    reference_url = models.URLField(
        default="https://not.yet.specified",
        verbose_name="Protein reference URL",
        help_text="URL to the vendor or provider specific protein specification."
    )
    responsible = models.ForeignKey(
        PersonBrick,
        default="not_yet_specified",
        help_text="Annotator."
    )
    notes = models.TextField(
        verbose_name="Notes",
        help_text="Additional this protein related notes. No commas but carriage return allowed.",
        blank=True
    )

    def save(self, *args, **kwargs):
        # primary key generator
        self.annot_id = makeannotid(
            s_base=str(self.protein.annot_id),
            s_provider=str(self.provider.annot_id),
            s_provider_catalog_id=str(self.provider_catalog_id),
            s_provider_batch_id=str(self.provider_batch_id)
        )
        # note field
        self.purity = checkfraction(self.purity)
        if (type(self.notes) == list):
            self.notes = list2sbr(self.notes)
        elif (type(self.notes) == str):
            self.notes = scomma2sbr(self.notes)
        else:
            raise CommandError(
                "strange type {} for TextField transformation".format(
                    type(self.notes)
                ))
        # many to many content display stocksolution_buffer_compound
        ls_stocksolution_buffer_compound = [o_stocksolution_buffer_compound.annot_id for o_stocksolution_buffer_compound in self.stocksolution_buffer_compound.all()]
        self.stocksolution_buffer_compound_display = str(ls_stocksolution_buffer_compound)
        # save
        super().save(*args, **kwargs)

    # output
    #__repr__ = __str__
    def __repr__(self):
        s_out = "\nProteinBrick:\n"
        for s_column in ls_column_proteinbrick:
            s_out = "{}{}: {}\n".format(
                s_out,
                s_column,
                getattr(self, s_column)
            )
        return(s_out)

    def __str__(self):
        return(self.annot_id)

    class Meta:
        unique_together = ((
            "protein",
            "provider",
            "provider_catalog_id",
            "provider_batch_id"
        ),)
        ordering = ["annot_id"]
        verbose_name_plural = "Perturbation Protein"


# database dictionary handles
def proteinbrick_db2d(s_annot_id, ls_column=ls_column_proteinbrick):
    """
    annot_id is the only input needed
    """
    #empty output
    d_brick = {}

    # pull form annot db
    o_brick = ProteinBrick.objects.get(annot_id=s_annot_id)

    # write in to d obj and re-type
    for s_column in ls_column:
        s_value = str(getattr(o_brick, s_column))
        o_value = retype(s_value)
        d_brick.update({s_column: o_value})

    # many to many field handle
    ls_stocksolution_buffer_compound = [
        str(o_buffer.annot_id) for o_buffer in o_brick.stocksolution_buffer_compound.all()
    ]
    ls_stocksolution_buffer_compound.sort()
    d_brick.update({"stocksolution_buffer_compound": ls_stocksolution_buffer_compound})

    # notes handle
    lo_notes = sbr2list(o_brick.notes)
    d_brick.update({"notes": lo_notes})

    # out
    return(d_brick)


def proteinbrick_d2db(d_brick, ls_column=ls_column_proteinbrick):
    """
    internal methode
    """
    # get annot id
    print("\nCheck ctrl voci for protein and provider to be able to generate reagent annot_id.")
    #print("Line:", d_brick)
    o_protein = checkvoci(d_brick["protein"], Protein)
    o_provider = checkvoci(d_brick["provider"], Provider)
    s_provider_catalog_id = str(d_brick["provider_catalog_id"])
    s_provider_batch_id = str(d_brick["provider_batch_id"])
    s_annot_id = makeannotid(
        s_base=str(o_protein),
        s_provider=str(o_provider),
        s_provider_catalog_id=s_provider_catalog_id,
        s_provider_batch_id=s_provider_batch_id
    )
    print("ok", s_annot_id)

    # get annot db obj
    try:
        o_brick = ProteinBrick.objects.get(annot_id=s_annot_id)
    except ProteinBrick.DoesNotExist:
        o_brick = ProteinBrick(
            annot_id=s_annot_id,
            protein=o_protein,
            provider=o_provider,
            provider_catalog_id=s_provider_catalog_id,
            provider_batch_id=s_provider_batch_id
        )
        o_brick.save()

    # write dictionary into db obj
    for s_column in ls_column:
        print("Put into field:", s_column, ":", d_brick[s_column])
        # annot_id field
        if (s_column == "annot_id"):
            o_brick.annot_id = s_annot_id
        # foreign key fields
        # get dictionary vocabulary controlled and str into obj turned
        elif (s_column == "protein"):
            o_brick.protein = checkvoci(d_brick["protein"], Protein)
        elif (s_column == "provider"):
            o_brick.provider = checkvoci(d_brick["provider"], Provider)
        elif (s_column == "proteinset_annot_id"):
            o_brick.proteinset_annot_id = checkvoci(d_brick["proteinset_annot_id"], ProteinSetBrick)
        elif (s_column == "code_organism"):
            o_brick.code_organism = checkvoci(d_brick["code_organism"], Organism)
        elif (s_column == "source_organism"):
            o_brick.source_organism = checkvoci(d_brick["source_organism"], Organism)
        elif (s_column == "stocksolution_concentration_unit"):
            o_brick.stocksolution_concentration_unit = checkvoci(d_brick["stocksolution_concentration_unit"], Unit)
        elif (s_column == "final_concentration_unit"):
            o_brick.final_concentration_unit = checkvoci(d_brick["final_concentration_unit"], Unit)
        elif (s_column == "time_unit"):
            o_brick.time_unit = checkvoci(d_brick["time_unit"], Unit)
        elif (s_column == "responsible"):
            o_brick.responsible = checkvoci(d_brick["responsible"], PersonBrick)
        # many to many fields
        # get dictionary vocabulary controlled and into obj turned
        elif (s_column == "stocksolution_buffer_compound"):
            o_brick.stocksolution_buffer_compound.clear()
            [o_brick.stocksolution_buffer_compound.add(checkvoci(o_buffer, Compound)) for o_buffer in  d_brick["stocksolution_buffer_compound"]]
        else:
            # other fields
            setattr(o_brick, s_column, d_brick[s_column])

    # save db obj into annot db
    o_brick.save()


# annot db json handels
def proteinbrick_db2objjson(ls_column=ls_column_proteinbrick):
    """
    this is main
    get json obj from the db. this is the main protein brick object.
    any protein brick file will be built form this object.
    """
    # set output variables
    dd_json = {}

    # get all annot_id
    ls_annotid =[]
    lo_brick = (ProteinBrick.objects.all())
    for o_brick in lo_brick:
        ls_annotid.append(str(o_brick.annot_id))
    ls_annotid.sort()

    # for each annot_id get json compatible dictionary with dbcontent
    for s_annotid in ls_annotid:
        # pull json compatible dictionary from annot db
        print("pull from database:", s_annotid)
        # get dictionary brick for annot_id
        d_brick = proteinbrick_db2d(s_annot_id=s_annotid, ls_column=ls_column)
        dd_json.update({s_annotid:d_brick})

    # output
    return(dd_json)


def proteinbrick_objjson2db(dd_json, ls_column=ls_column_proteinbrick):
    """
    this is main
    """
    # extract obj json into annot db
    for s_key in dd_json:
        d_json = dd_json[s_key]
        proteinbrick_d2db(d_brick=d_json, ls_column=ls_column)
