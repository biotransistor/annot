# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appbrreagentprotein.models import ProteinSetBrick

# code
class ProteinSetBrickLookup(ModelLookup):
    model = ProteinSetBrick
    search_fields = ('annot_id__icontains',)
registry.register(ProteinSetBrickLookup)
