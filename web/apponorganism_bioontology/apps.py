from django.apps import AppConfig


class ApponorganismBioontologyConfig(AppConfig):
    name = 'apponorganism_bioontology'
