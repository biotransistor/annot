from django.contrib import admin
from django.http import HttpResponseRedirect
from apponyieldfraction_own.models import YieldFraction

# Register your models here.
# admin.site.register(YieldFraction)
class YieldFractionAdmin(admin.ModelAdmin):
    list_display = (
        "term_name", "annot_id", "term_source_version_responsible",
        "term_source_version_update", "term_source_version",
        "term_id", "term_ok"
    )
    save_on_top = True
    readonly_fields = (
        "term_source_version_update", "term_source_version",
        "term_id", "term_ok"
    )
    search_fields = (
        "term_name", "term_id", "annot_id"
    )
    actions = [
        "delete_selected",
        "download_origin_json",
        "download_backup_json",
        "download_origin_tsv",
        "download_backup_tsv"
    ]

    ### json ###
    # action download origin vocabulary json
    def download_origin_json(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=vocabulary.origin&choice=apponyieldfraction_own"
        ))
    download_origin_json.short_description = "Download original vocabulary as json file (item selection irrelevant)"

    # action download backup vocabulary json
    def download_backup_json(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=vocabulary.backup&choice=apponyieldfraction_own"
        ))
    download_backup_json.short_description = "Download backup vocabulary as json file (item selection irrelevant)"

    ### tsv ###
    # action download origin vocabulary tsv
    def download_origin_tsv(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=vocabulary.origin&choice=apponyieldfraction_own"
        ))
    download_origin_tsv.short_description = "Download original vocabulary as tsv file (item selection irrelevant)"

    # action download backup vocabulary tsv
    def download_backup_tsv(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=vocabulary.backup&choice=apponyieldfraction_own"
        ))
    download_backup_tsv.short_description = "Download backup vocabulary as tsv file (item selection irrelevant)"

# register
admin.site.register(YieldFraction, YieldFractionAdmin)
