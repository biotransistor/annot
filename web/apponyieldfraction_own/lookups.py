# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponyieldfraction_own.models import YieldFraction


# code
class YieldFractionLookup(ModelLookup):
    model = YieldFraction
    search_fields = ('annot_id__icontains',)
registry.register(YieldFractionLookup)
