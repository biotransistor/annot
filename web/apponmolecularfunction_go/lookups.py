# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponmolecularfunction_go.models import MolecularFunction

# code
class MolecularFunctionLookup(ModelLookup):
    model = MolecularFunction
    search_fields = ('annot_id__icontains',)
registry.register(MolecularFunctionLookup)
