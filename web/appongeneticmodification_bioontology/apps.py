from django.apps import AppConfig


class AppongeneticmodificationBioontologyConfig(AppConfig):
    name = 'appongeneticmodification_bioontology'
