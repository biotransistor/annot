from django.db import models

# constants
TERM_SOURCE_NAME = "BAO"
TERM_SOURCE_FILE = "http://www.bioassayontology.org/"
TERM_SOURCE_DESCRIPTION = "Extract from the BAO ontology. The BioAssay Ontology (BAO) describes chemical biology screening assays and their results including high-throughput screening (HTS) data for the purpose of categorizing assays and data analysis. BAO is an extensible, knowledge-based, highly expressive (currently SHOIQ(D)) description of biological assays making use of descriptive logic based features of the Web Ontology Language (OWL). BAO currently has over 1000 classes and also makes use of several other ontologies. It describes several concepts related to biological screening, including Perturbagen, Format, Meta Target, Design, Detection Technology, and Endpoint. Perturbagens are perturbing agents that are screened in an assay; they are mostly small molecules. Assay Meta Target describes what is known about the biological system and / or its components interrogated in the assay (and influenced by the Perturbagen). Meta target can be directly described as a molecular entity (e.g. a purified protein or a protein complex), or indirectly by a biological process or event (e.g. phosphorylation). Format describes the biological or chemical features common to each test condition in the assay and includes biochemical, cell-based, organism-based, and variations thereof. The assay Design describes the assay methodology and implementation of how the perturbation of the biological system is translated into a detectable signal. Detection Technology relates to the physical method and technical details to detect and record a signal. Endpoints are the final HTS results as they are usually published (such as IC50, percent inhibition, etc.). BAO has been designed to accommodate multiplexed assays. All main BAO components include multiple levels of sub-categories and specification classes, which are linked via object property relationships forming an expressive knowledge-based representation."
# rest source
REST_URL = "http://data.bioontology.org"
REST_FILENAME = "bioontologybufferhttp.json"
REST_FORMAT = "http"
CLASS_URL = "http%3A%2F%2Fwww.bioassayontology.org%2Fbao%23BAO_0000238"  # url of the start term e.g. cell line modification method
GET_BRANCHCUT = False
# build
SWAP_TERMID = False
PK_STRING_CASE = str.lower

# Create your models here.
class GeneticModification(models.Model):
    term_name = models.CharField(
        unique=True,
        max_length=254,
        verbose_name='Ontological term',
        help_text="Controlled vocabulary term."
    )
    term_id = models.CharField(
        blank=True,
        max_length=254,
        verbose_name='Ontology identifier',
        help_text="Controlled vocabulary identifier."
    )
    annot_id = models.SlugField(
        primary_key=True,
        max_length=254,
        verbose_name='Annot identifier',
        help_text="Internal identifier. This identifier is the primary key and should be descriptive. Choose this identifier carefully in accordance to the identifiers already in use."
    )
    term_source_version_responsible = models.CharField(
        max_length=254,
        verbose_name="Responsible person",
        help_text="Your name."
    )
    term_source_version_update = models.DateField(
        verbose_name='Version update time stamp',
        auto_now=True
    )
    term_source_version = models.CharField(
        max_length=254,
        verbose_name='Ontology source file version',
        help_text="Version of controlled vocabulary source."
    )
    term_ok = models.NullBooleanField(
        default=None,
        null=True,
        verbose_name='Ontology term status',
        help_text='A term marked true is in the most recent ontology file version used in this database. A term marked false is internally generated or deprecated. A term marked null has not been checked against the most recent ontology file version.'
    )
    def __str__(self):
        return(self.annot_id)

    class Meta:
        ordering = ['annot_id']
