# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appongeneticmodification_bioontology.models import GeneticModification

# code
class GeneticModificationLookup(ModelLookup):
    model = GeneticModification
    search_fields = ('annot_id__icontains',)
registry.register(GeneticModificationLookup)
