# import frmom django
from django.core.management.base import BaseCommand, CommandError

# import from python
import copy
import json

# import from acpipe
from acpipe_acjson import acjson as ac

# import from annot
from app3runset.models import RunSet
from apponunit_bioontology.models import Unit

# const
es_unit = set((o_unit.annot_id for o_unit in Unit.objects.all()))

### main ###
class Command(BaseCommand):
    help = "Check runset acjson file against origin acjson fiels."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument(
            "runset",
            nargs='+',
            type=str,
            help="Annot coordinate run set setname"
        )

    def handle(self, *args, **options):

        # process
        for s_runset in options["runset"]:

            # load runset obj
            lo_runset = RunSet.objects.filter(runset_name__regex=r'{}'.format(s_runset))
            if (len(lo_runset) < 1):
                raise CommandError(
                    "Error: runset argument {} does not specify any runset.".format(
                        s_runset
                    )
                )

            # for each runset obj
            for o_runset in lo_runset:

                # load acjson file
                with open (o_runset.acjson_file.path, "r") as f_acjson:
                    d_acjson = json.load(f_acjson)
                d_acpop = copy.deepcopy(d_acjson)

                # check acid, runid, runtype
                if (d_acjson["acid"] != o_runset.acjson_file.path.split("/")[-1]):
                    self.stdout.write(
                        "Warning @ acjson_runsetcheck : acid {} differs from filename {}.".format(
                            d_acjson["acid"],
                            o_runset.acjson_file.path.split("/")[-1]
                        )
                    )
                elif (d_acjson["runid"] != o_runset.runset_name):
                    self.stdout.write(
                        "Warning @ acjson_runsetcheck : runid {} differs from runset_name {}.".format(
                            d_acjson["runid"],
                            o_runset.runset_name
                        )
                    )
                elif (d_acjson["runtype"] != "annot_runset"):
                    self.stdout.write(
                        "Warning @ acjson_runsetcheck : runtype {} is not annot_runset.".format(
                            d_acjson["runtype"]
                        )
                    )


                # handle endspointset set
                for o_endpointset in o_runset.endpointsetset.all():
                    # load acjson file
                    with open (o_endpointset.acjson_file.path, "r") as f_acjson:
                        d_endpointset = json.load(f_acjson)

                    # pop endpoints from runset acjson
                    d_acjson, es_error = ac.pop_ac(
                        d_acjson=d_acpop,
                        d_acrecord=d_endpointset,
                        ts_axis=("endpoint",),
                        b_leaf_intersection=True,
                        b_recordset_major=False,
                    )
                    if (len(es_error) != 0):
                        self.stdout.write(str(es_error))

                # handle perturbationset set
                for o_perturbationset in o_runset.perturbationsetset.all():
                    # load acjson file
                    with open (o_perturbationset.acjson_file.path, "r") as f_acjson:
                        d_perturbationset = json.load(f_acjson)
                    # pop perturbation from runset acjson
                    d_acjson, es_error = ac.pop_ac(
                        d_acjson=d_acpop,
                        d_acrecord=d_perturbationset,
                        ts_axis=("perturbation",),
                        b_leaf_intersection=True,
                        b_recordset_major=False,
                    )
                    if (len(es_error) != 0):
                        self.stdout.write(str(es_error))

                # handle sampleset set
                for o_sampleset in o_runset.samplesetset.all():
                    # load acjson file
                    with open (o_sampleset.acjson_file.path, "r") as f_acjson:
                        d_sampleset = json.load(f_acjson)
                    # pop samples from runset acjson
                    d_acjson, es_error = ac.pop_ac(
                        d_acjson=d_acpop,
                        d_acrecord=d_sampleset,
                        ts_axis=("sample",),
                        b_leaf_intersection=True,
                        b_recordset_major=False,
                    )
                    if (len(es_error) != 0):
                        self.stdout.write(str(es_error))

                # handle superset set
                for o_superset in o_runset.supersetset.all():
                    # load acjson file
                    with open (o_superset.acjson_file.path, "r") as f_acjson:
                        d_superset = json.load(f_acjson)
                    # pop records from superset acjosn
                    d_acjson, es_error = ac.pop_ac(
                        d_acjson=d_acpop,
                        d_acrecord=d_sampleset,
                        ts_axis=ac.ts_ACAXIS,
                        b_leaf_intersection=True,
                        b_recordset_major=False,
                    )
                    if (len(es_error) != 0):
                        self.stdout.write(str(es_error))

                #  check if any gent left
                # bue 20180328: this have not to be an error.
                # all records from one set can be timeseried to the other sets.
                #if (ac.count_record(d_acpop) > 0):
                #    self.stdout.write("Error @ acjson_runsetcheck : at least one runset record not found in acaxisset runset set. {}".format(d_acpop))
                #else:
                print("checked {}: ok".format(o_runset.runset_name))
