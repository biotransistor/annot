from django.db import models
from django.core.management.base import CommandError

# from prj
from app1study.models import Study
from app4superset.models import SuperSet
from appacaxis.models import EndpointSet, PerturbationSet, SampleSet
from appbrxperson.models import PersonBrick
from prjannot.settings import MEDIA_ROOT #, MEDIA_ROOTQUARANTINE, MEDIA_ROOTURL, MEDIA_URL

# python
import os
import re

# Create your models here.
class RunSet(models.Model):
    annot_id = models.SlugField(
        unique=True,
        max_length=256,
        help_text="Automatically generated."
    )
    runset_name = models.SlugField(
        primary_key=True,
        max_length=256,
        default="not_yet_specified",
        help_text="Assay run barcode."
    )
    acjson_file_overwrite = models.BooleanField(
        default=False,
        verbose_name="overwrite file",
        help_text="If file with same name aready exist, should it be overwritten?"
    )
    acjson_file = models.FileField(
        upload_to="upload/runset",
        help_text="Assay run related, resulting acjson file.",
        blank=True
    )
    endpointsetset_display = models.TextField(
        help_text="Assay run related enpointsets.",
        blank=True
    )
    endpointsetset = models.ManyToManyField(
        EndpointSet,
        help_text="Choose endpointset.",
        blank=True
    )
    perturbationsetset_display = models.TextField(
        help_text="Assay run related enpointsets.",
        blank=True
    )
    perturbationsetset = models.ManyToManyField(
        PerturbationSet,
        help_text="Choose perturbationset.",
        blank=True
    )
    samplesetset_display = models.TextField(
        help_text="Assay run related samplesets.",
        blank=True
    )
    samplesetset = models.ManyToManyField(
        SampleSet,
        help_text="Choose sampleset.",
        blank=True
    )
    supersetset_display = models.TextField(
        help_text="Assay run related supersets.",
        blank=True
    )
    supersetset = models.ManyToManyField(
        SuperSet,
        help_text="Choose superset.",
        blank=True
    )
    study_display = models.TextField(
        help_text="Assay run related study.",
        blank=True
    )
    study = models.ManyToManyField(
        Study,
        help_text="Choose study.",
        blank=True
    )
    notes = models.TextField(
        help_text="Assay run related notes.",
        blank=True
    )
    def save(self, *args, **kwargs):
        # annot_id generator
        self.annot_id = re.sub(r"[^a-z0-9-]", "", self.runset_name.lower())
        # set content display
        self.endpointsetset_display = str(
            [o_endpointsetset.annot_id for o_endpointsetset in self.endpointsetset.all()]
        )
        self.perturbationsetset_display = str(
            [o_perturbationsetset.annot_id for o_perturbationsetset in self.perturbationsetset.all()]
        )
        self.samplesetset_display = str(
            [o_samplesetset.annot_id for o_samplesetset in self.samplesetset.all()]
        )
        self.supersetset_display = str(
            [o_supersetset.annot_id for o_supersetset in self.supersetset.all()]
        )
        self.study_display = str(
            [o_study.annot_id for o_study in self.study.all()]
        )
        # file handling
        if not (self.acjson_file.name is None):
            self.acjson_file.name = re.sub(r"[^a-zA-Z0-9_\-./]", "", self.acjson_file.name)
            if (os.path.isfile("{}upload/runset/{}".format(MEDIA_ROOT, self.acjson_file.name))) \
               and not (self.acjson_file_overwrite):
                raise CommandError(
                    "Error at app3runset.models: Overwrite_file is set to False and a file with name upload/runset/{} alreday exist.".format(
                        self.acjson_file.name
                    )
                )
            if (os.path.isfile("{}upload/runset/{}".format(MEDIA_ROOT, self.acjson_file.name))):
                os.remove("{}upload/runset/{}".format(MEDIA_ROOT, self.acjson_file.name))
            self.acjson_file_overwrite = False
        # save
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.runset_name)

    __repr__ = __str__

    class Meta:
        ordering =["annot_id"]
        verbose_name_plural = "Assay Runs"
