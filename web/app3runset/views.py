## appacaxis ###
# import form django
from django.http import HttpResponse
from django.contrib import messages

# import from python
import datetime
import io
import json
import os
import re
import zipfile

# import from annot
from app3runset.models import RunSet
from prjannot.settings import MEDIA_ROOTQUARANTINE

# Create your views here.
# index
def index(request):
    """ hello world """
    return HttpResponse("Hello world! You're at the appacaxis.view index page")

# export
def export_runset2study(request):
    #return HttpResponse("Hello world! You're at the appacaxis.view export_jsontsv page")
    """
    description:
        make model realed json and tsv realted files downloadable.
    """

    # read out RunSet model
    d_study2run = {}
    for o_run in RunSet.objects.all():
        for o_study in o_run.study.all():
            try:
                ls_run = d_study2run[o_study.study_name]
                ls_run.append(o_run.runset_name)
            except:
                ls_run = [o_run.runset_name]
            d_study2run.update({o_study.study_name : ls_run})

    # Handle input
    s_filetype = request.GET.get("filetype")

    # set variables
    s_datetime = datetime.datetime.today().isoformat().split(".")[0].replace(":", "").replace("-", "")
    s_filezip = "{}_annot_{}_download.zip".format(s_datetime, s_filetype)
    s_pathfilezip = "{}{}".format(MEDIA_ROOTQUARANTINE, s_filezip)

    # open zip file handle
    with zipfile.ZipFile(s_pathfilezip, "w") as zipper:

        # stdout handle
        o_stdout = io.StringIO()

        # zip
        if (s_filetype in {"tsv"}):
            s_file = "annotCoordinate_study2runset.tsv"
            s_pathfile = "{}{}".format(MEDIA_ROOTQUARANTINE, s_file)
            with open(s_pathfile, "w") as f_tsv:
                for s_study, ls_run in d_study2run.items():
                    ls_run.sort()
                    s_out = "{}\t{}\n".format(s_study, ";".join(ls_run))
                    f_tsv.write(s_out)
            zipper.write(s_pathfile, arcname=s_file)
            os.remove(s_pathfile)

        else: # json
            s_file = "annotCoordinate_study2runset.json"
            s_pathfile = "{}{}".format(MEDIA_ROOTQUARANTINE, s_file)
            with open(s_pathfile, "w") as f_json:
                json.dump(d_study2run, f_json, sort_keys=True)
            zipper.write(s_pathfile, arcname=s_file)
            os.remove(s_pathfile)

    # push save as zip
    response = HttpResponse(content_type="application/zip")
    response["Content-Disposition"] = "attachment; filename={}".format(s_filezip)
    with open(s_pathfilezip, "rb") as f:
        response.write(f.read())

    # delete zip file
    os.system("rm {}".format(s_pathfilezip))

    # put stdout as message
    s_out = o_stdout.getvalue().strip()
    if (s_out == ""):
        messages.success(
            request,
            "{} runset2study mapping # successfully zipped.".format(s_filetype)
        )
    elif re.match(".*Error.*", s_out):
        messages.error(
            request,
            "{} runset2study mapping # {}".format(s_filetype, s_out)
        )
    elif re.match(".*Warning.*", s_out):
        messages.warning(
            request,
            "{} runset2study mapping # {}".format(s_filetype, s_out)
        )
    else:
        messages.info(
            request,
            "{} runset2study mapping # {}".format(s_filetype, s_out)
        )
    storage = messages.get_messages(request)

    # return
    return(response)
