from django import forms
from django.contrib import admin
from django.contrib import messages
from django.core import management
from django.http import HttpResponseRedirect
from app3runset.models import RunSet

# import from django selectable prj
from selectable.forms import AutoComboboxSelectWidget, AutoComboboxSelectMultipleWidget

# import from python
import io
import json
import re

# import form annot
from app1study.lookups import StudyLookup
from app4superset.lookups import SuperSetLookup
from appacaxis.lookups import EndpointSetLookup, PerturbationSetLookup, SampleSetLookup

# Register your models here.
#admin.site.register(RunSet)
class RunSetForm(forms.ModelForm):
    class Meta:
        model = RunSet
        fields = [
            "endpointsetset",
            "perturbationsetset",
            "samplesetset",
            "supersetset",
            "study",
         ]
        widgets = {
            "endpointsetset": AutoComboboxSelectMultipleWidget(EndpointSetLookup),
            "perturbationsetset": AutoComboboxSelectMultipleWidget(PerturbationSetLookup),
            "samplesetset": AutoComboboxSelectMultipleWidget(SampleSetLookup),
            "supersetset": AutoComboboxSelectMultipleWidget(SuperSetLookup),
            "study": AutoComboboxSelectMultipleWidget(StudyLookup),
         }

class RunSetAdmin(admin.ModelAdmin):
    form = RunSetForm
    search_fields = (
        "annot_id",
        "acjson_file",
        "endpointsetset__annot_id",
        "perturbationsetset__annot_id",
        "samplesetset__annot_id",
        "supersetset__annot_id",
        "study__annot_id",
        "notes",
     )
    list_display = (
        "annot_id",
        "runset_name",
        "acjson_file",
        "endpointsetset_display",
        "perturbationsetset_display",
        "samplesetset_display",
        "supersetset_display",
        "study_display",
        "notes",
    )
    save_on_top = True
    fieldsets = [
        ("Identity", { "fields" : [
            "annot_id",
            "study_display",
            "study",
            "runset_name"
        ]}),
        ("Acjson", { "fields" : [
            "acjson_file_overwrite",
            "acjson_file",
        ]}),
        ("Sets of set", { "fields" : [
            "endpointsetset_display",
            "endpointsetset",
            "perturbationsetset_display",
            "perturbationsetset",
            "samplesetset_display",
            "samplesetset",
            "supersetset_display",
            "supersetset",
        ]}),
        ("Notes", { "fields" : [
            "notes",
        ]}),
    ]
    readonly_fields = (
        "annot_id",
        "endpointsetset_display",
        "perturbationsetset_display",
        "samplesetset_display",
        "supersetset_display",
        "study_display",
    )
    actions = [
        "delete_selected",
        "download_json",
        "download_runset2study_json",
        "download_brick_json",
        "download_acjson",
        "download_tsv",
        "download_runset2study_tsv",
        "download_brick_tsv",
        "download_brick_lincs",
        "download_layouttsv_short",
        "download_layouttsv_long",
        "download_dataframetsv_tidy",
        "download_dataframetsv_unstacked",
        "resave",
        "check_acjsonrunid_against_setname",
        "check_acjson_against_setset",
        "download_acpipe_template_py",
    ]

    ### json download action ###
    def download_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=generic&choice=app3runset.runset"
        ))
    download_json.short_description = "Download this page as json file (item selection irrelevant)"

    def download_runset2study_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/app3runset/export_runset2study?filetype=json"
        ))
    download_runset2study_json.short_description = "Download runset study mapping as json file (item selection irrelevant)"

    def download_brick_json(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=runset&filetype=json&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_json.short_description = "Download selected set releated bricks as json files"

    def download_acjson(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_acjson?model=runset&filetype=acjson&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_acjson.short_description = "Download selected sets as acjson file"

    ### tsv download action ###
    def download_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=generic&choice=app3runset.runset"
        ))
    download_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

    def download_runset2study_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/app3runset/export_runset2study?filetype=tsv"
        ))
    download_runset2study_tsv.short_description = "Download runset study mapping as tsv file (item selection irrelevant)"

    def download_brick_tsv(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=runset&filetype=tsv&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_tsv.short_description = "Download selected set releated bricks as tsv files"

    def download_brick_lincs(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=runset&filetype=lincs&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_lincs.short_description = "Download selected set releated bricks as lincs data standard csv files"

    def download_layouttsv_short(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=runset&filetype=layoutshort&choice={}&user={}".format(
                "!".join(ls_choice),
                request.user.id
            )
        ))
    download_layouttsv_short.short_description = "Download selected sets as tsv_short layout file"

    def download_layouttsv_long(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=runset&filetype=layoutlong&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_layouttsv_long.short_description = "Download selected sets as tsv_long layout file"

    def download_dataframetsv_tidy(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=runset&filetype=dataframetidy&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_dataframetsv_tidy.short_description = "Download selected sets as tsv tidy dataframe files"

    def download_dataframetsv_unstacked(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=runset&filetype=dataframeunstacked&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_dataframetsv_unstacked.short_description = "Download selected sets as tsv unstacked dataframe file"

    ### resave action ###
    def resave(self, request, queryset):
        # process
        ls_save = []
        for o_record in RunSet.objects.all():
            o_record.save()
            ls_save.append(o_record.runset_name)
        # message
        messages.success(
            request,
            "{} # resaved".format(ls_save),
        )
    resave.short_description = "Resave all record (item selection irrelevant)"

    ### acpipe ###
    # action check acjson runid against set name
    def check_acjsonrunid_against_setname(self, request, queryset):
        s_out = ""
        for o_record in queryset:
            # load acjson file
            with open (o_record.acjson_file.path, "r") as f_acjson:
                d_acjson = json.load(f_acjson)
            # check
            if (d_acjson["runid"] != o_record.runset_name):
                s_out += "Warning : runid {} differs from runset_name {}. ".format(
                    d_acjson["runid"],
                    o_record.runset_name
                )
        # put s_out as message
        if (s_out == ""):
            messages.success(
                request,
                "{} # successfully checked".format(list(queryset)),
            )
        elif re.match(".*Error.*", s_out.strip()):
            messages.error(
                request,
                "{} # {}".format(ls_choice, s_out.strip()),
            )
        elif re.match(".*Warning.*", s_out):
            messages.warning(
                request,
                "{} # {}".format(ls_choice, s_out.strip()),
            )
        else:
            messages.info(
                request,
                "{} # {}".format(ls_choice, s_out.strip()),
            )
    check_acjsonrunid_against_setname.short_description = "Check runset acjson file runid against runset_name"

    # action check acpipe generated acjson against set of sets acjson
    def check_acjson_against_setset(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        for s_choice in ls_choice:
            # stdout handle
            o_out = io.StringIO()
            # command call
            management.call_command(
                "acjson_runsetcheck",
                s_choice,
                stdout=o_out,
                verbosity=0
            )
        # put stdout as message
        s_out = o_out.getvalue().strip()
        if (s_out == ""):
            messages.success(
                request,
                "{} # successfully checked".format(ls_choice),
            )
        elif re.match(".*Error.*", s_out):
            messages.error(
                request,
                "{} # {}".format(ls_choice, s_out),
            )
        elif re.match(".*Warning.*", s_out):
            messages.warning(
                request,
                "{} # {}".format(ls_choice, s_out),
            )
        else:
            messages.info(
                request,
                "{} # {}".format(ls_choice, s_out),
            )
    check_acjson_against_setset.short_description = "Check runset acjson file against set of set acjson"

    # action generate python3 acpipe template script
    def download_acpipe_template_py(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_pytemplate?axis=runset&set={}".format(
                "!".join(ls_choice)
            )
        ))
    download_acpipe_template_py.short_description = "Download python3 acpipe template script"

admin.site.register(RunSet, RunSetAdmin)
