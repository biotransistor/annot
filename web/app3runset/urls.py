# import form djnago
from django.conf.urls import url
# import from appsaontology
from app3runset import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^export_runset2study/$', views.export_runset2study, name="export_runset2study"),
]
