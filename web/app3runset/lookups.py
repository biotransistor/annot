# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from app3runset.models import RunSet

# code
class RunSetLookup(ModelLookup):
    model = RunSet
    search_fields = (
        'annot_id__icontains',
    )
registry.register(RunSetLookup)
