# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from app4superset.models import AcPipe, SuperSetFile, SuperSet

# code
class AcPipeLookup(ModelLookup):
    model = AcPipe
    search_fields = (
        'module__icontains',
    )
registry.register(AcPipeLookup)

class SuperSetFileLookup(ModelLookup):
    model = SuperSetFile
    search_fields = (
        'superset_file__icontains',
    )
registry.register(SuperSetFileLookup)

class SuperSetLookup(ModelLookup):
    model = SuperSet
    search_fields = (
        'annot_id__icontains',
    )
registry.register(SuperSetLookup)
