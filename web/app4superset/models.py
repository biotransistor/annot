from django.db import models
from django.core.management.base import CommandError

# from prj
from appacaxis.models import EndpointSet, PerturbationSet, SampleSet
from appbrxperson.models import PersonBrick
from prjannot.settings import MEDIA_ROOT #, MEDIA_ROOTQUARANTINE, MEDIA_ROOTURL, MEDIA_URL

# from python
import os
import re

# Create your models here.
# acpipe module model
class AcPipe(models.Model):
    # annot_id
    annot_id = models.SlugField(
        unique=True,
        max_length=32,
        help_text="Automatically generated."
    )
    # file
    module = models.CharField(
        primary_key=True,
        default="acpipe_nop",
        max_length = 32,
        help_text="module name.",
        verbose_name="Python3 module",
    )
    # command
    import_command = models.CharField(
        default="from acpipe_nop import nop",
        max_length = 256,
        help_text="Module import command.",
    )
    def save(self, *args, **kwargs):
        # annot_id generator
        self.module = re.sub(r"[^a-zA-Z0-9_\.-.]", "", self.module)
        self.annot_id = re.sub(r"[^a-z0-9-]", "", self.module.lower())
        self.import_command = self.import_command.strip()
        # save
        super().save(*args, **kwargs)

    # set content display
    def __str__(self):
        return(self.module)

    __repr__ = __str__

    class Meta:
        ordering =["annot_id"]
        verbose_name_plural = "Python3 acpipe modules"


# file model
class SuperSetFile(models.Model):
    annot_id = models.SlugField(
        primary_key=True,
        max_length=256,
        default="not_yet_specified",
        help_text="Automatically generated."
    )
    superset_file_overwrite = models.BooleanField(
        default=False,
        verbose_name="overwrite file",
        help_text="If file with same name aready exist, should it be overwritten?"
    )
    superset_file = models.FileField(
        upload_to="upload/supersetfile/",
        help_text="A superset related file.",
    )
    # code
    code = models.TextField(
        default=None,
        null=True,
        blank=True,
        help_text="Enter file realted python3 template code.",
    )
    def save(self, *args, **kwargs):
        # annot_id generator
        self.annot_id = re.sub(r"[^a-zA-Z0-9-]", "", self.superset_file.name.split("/")[-1].lower())
        # file handling
        self.superset_file.name = re.sub(r"[^a-zA-Z0-9_\-./]", "", self.superset_file.name)
        if (os.path.isfile("{}upload/supersetfile/{}".format(MEDIA_ROOT, self.superset_file.name))) \
           and not (self.superset_file_overwrite):
            raise CommandError(
                "Error at app4superset.models: Overwrite_file is set to False and a file with name upload/supersetfile/{} alreday exist.".format(
                    self.superset_file.name
                )
            )
        if (os.path.isfile("{}upload/supersetfile/{}".format(MEDIA_ROOT, self.superset_file.name))):
            os.remove("{}upload/supersetfile/{}".format(MEDIA_ROOT, self.superset_file.name))
        self.superset_file_overwrite = False
        # save
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.superset_file.name.split("/")[-1])

    __repr__ = __str__

    class Meta:
        ordering =["annot_id"]
        verbose_name_plural = "Supeset Files"


# superset model
class SuperSet(models.Model):
    annot_id = models.SlugField(
        unique=True,
        max_length=256,
        help_text="Automatically generated.",
        blank=True
    )
    superset_name = models.SlugField(
        primary_key=True,
        max_length=256,
        default="not_yet_specified",
        help_text="Super set name."
    )
    notes = models.TextField(
        help_text="Any additional information worth to mention.",
        blank=True
    )
    acjson_file_overwrite = models.BooleanField(
        default=False,
        verbose_name="overwrite file",
        help_text="If file with same name aready exist, should it be overwritten?"
    )
    acjson_file = models.FileField(
        upload_to="upload/superset/",
        help_text="Superset related, resulting acjson file.",
        blank=True
    )
    acpipemodule_display = models.TextField(
        help_text="Superset related python3 acpipe modules.",
        blank=True
    )
    acpipemodule = models.ManyToManyField(
        AcPipe,
        help_text="Choose python3 acpipe modules.",
        blank=True,
    )
    # endpoint
    endpointsetset_display = models.TextField(
        help_text="Superset related enpointsets.",
        blank=True
    )
    endpointsetset = models.ManyToManyField(
        EndpointSet,
        help_text="Choose endpointset.",
        blank=True
    )
    # perturbation
    perturbationsetset_display = models.TextField(
        help_text="Superset related enpointsets.",
        blank=True
    )
    perturbationsetset = models.ManyToManyField(
        PerturbationSet,
        help_text="Choose perturbationset.",
        blank=True
    )
    # sampleset
    samplesetset_display = models.TextField(
        help_text="Superset related samplesets.",
        blank=True
    )
    samplesetset = models.ManyToManyField(
        SampleSet,
        help_text="Choose sampleset.",
        blank=True
    )
    # super set
    supersetset_display = models.TextField(
        help_text="Superset related supersets.",
        blank=True
    )
    supersetset = models.ManyToManyField(
        "self",
        help_text="Choose superset.",
        blank=True
    )
    # superset file
    supersetfile_display = models.TextField(
        help_text="This superset related files.",
        blank=True
    )
    supersetfile_display = models.TextField(
        help_text="This superset related files.",
        blank=True
    )
    supersetfile = models.ManyToManyField(
        SuperSetFile,
        help_text="Choose file.",
        blank=True,
    )
    responsible = models.ForeignKey(
        PersonBrick,
        default="not_yet_specified",
        help_text="The wetlab scientist who put this set together."
    )
    def save(self, *args, **kwargs):
        # annot_id generator
        self.annot_id = re.sub(r"[^a-z0-9-]", "", self.superset_name.lower())
        # setset content display
        self.endpointset_display = str(
            [o_endpointsetset.annot_id for o_endpointsetset in self.endpointsetset.all()]
        )
        self.perturbationsetset_display = str(
            [o_perturbationsetset.annot_id for o_perturbationsetset in self.perturbationsetset.all()]
        )
        self.samplesetset_display = str(
            [o_samplesetset.annot_id for o_samplesetset in self.samplesetset.all()]
        )
        self.supersetset_display = str(
            [o_supersetset.annot_id for o_supersetset in self.supersetset.all()]
        )
        self.supersetfile_display = str(
            [o_supersetfile.annot_id for o_supersetfile in self.supersetfile.all()]
        )
        self.acpipemodule_display = str(
            [o_acpipemodule.annot_id for o_acpipemodule in self.acpipemodule.all()]
        )
        # file handling
        if not (self.acjson_file.name is None):
            self.acjson_file.name = re.sub(r"[^a-zA-Z0-9_\-./]", "", self.acjson_file.name)
            if (os.path.isfile("{}upload/superset/{}".format(MEDIA_ROOT, self.acjson_file.name))) \
               and not (self.acjson_file_overwrite):
                raise CommandError(
                    "Error at app4superset.models: Overwrite_file is set to False and a file with name upload/superset/{} alreday exist.".format(
                        self.acjson_file.name
                    )
                )
            if (os.path.isfile("{}upload/superset/{}".format(MEDIA_ROOT, self.acjson_file.name))):
                os.remove("{}upload/superset/{}".format(MEDIA_ROOT, self.acjson_file.name))
            self.acjson_file_overwrite = False
        # save
        super().save(*args, **kwargs)

    def __str__(self):
        return(self.superset_name)

    __repr__ = __str__

    class Meta:
        ordering =["annot_id"]
        verbose_name_plural = "Superset"
