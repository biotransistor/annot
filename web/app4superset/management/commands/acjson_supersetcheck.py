# import frmom django
from django.core.management.base import BaseCommand, CommandError

# import from python
import copy
import json

# import from acpipe
from acpipe_acjson import acjson as ac

# import from annot
from app4superset.models import SuperSet
from apponunit_bioontology.models import Unit

# const
es_unit = set((o_unit.annot_id for o_unit in Unit.objects.all()))


### main ###
class Command(BaseCommand):
    help = "Check superset acjson file against origin acjson fiels."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument(
            "superset",
            nargs='+',
            type=str,
            help="Annot coordinate super set setname"
        )

    def handle(self, *args, **options):

        # process
        for s_superset in options["superset"]:

            # load superset obj
            lo_superset = SuperSet.objects.filter(superset_name__regex=r'{}'.format(s_superset))
            if (len(lo_superset) < 1):
                raise CommandError(
                    "Error: superset argument {} does not specify any superset.".format(
                        s_superset
                    )
                )

            # for each superset obj
            for o_superset in lo_superset:

                # load acjson file
                with open (o_superset.acjson_file.path, "r") as f_acjson:
                    d_acjson = json.load(f_acjson)
                d_acpop = copy.deepcopy(d_acjson)

                # check acid, runid, runtype
                if (d_acjson["acid"] != o_superset.acjson_file.path.split("/")[-1]):
                    self.stdout.write(
                        "Warning @ acjson_supersetcheck : acid {} differs from filename {}.".format(
                            d_acjson["acid"],
                            o_superset.acjson_file.path.split("/")[-1]
                        )
                    )
                elif (d_acjson["runid"] != o_superset.superset_name):
                    self.stdout.write(
                        "Warning @ acjson_supersetcheck : runid {} differs from superset_name {}.".format(
                            d_acjson["runid"],
                            o_superset.superset_name
                        )
                    )
                elif (d_acjson["runtype"] != "annot_superset"):
                    self.stdout.write(
                        "Warning @ acjson_supersetcheck : runtype {} is not annot_superset.".format(
                            d_acjson["runtype"]
                        )
                    )

                # this is what we track
                self.stdout.write(f"lo_superset: {str(lo_superset)} superset: {str(o_superset)} endpointset: {str(o_superset.endpointsetset.all())} perturbationset: {str(o_superset.perturbationsetset.all())} sampleset: {o_superset.samplesetset.all()} linked untracked superset: {o_superset.supersetset.all()} # ")

                # handle endspointset set
                for o_endpointset in o_superset.endpointsetset.all():
                    # load acjson file
                    with open (o_endpointset.acjson_file.path, "r") as f_acjson:
                        d_endpointset = json.load(f_acjson)

                    # pop endpoints from superset acjson
                    d_acpop, es_error = ac.pop_ac(
                        d_acjson=d_acpop,
                        d_acrecord=d_endpointset,
                        ts_axis=("endpoint",),
                        b_leaf_intersection=True,
                        b_recordset_major=False,
                    )
                    self.stdout.write(str(f" !!!{o_endpointset}!!! "))
                    if (len(es_error) != 0):
                        self.stdout.write(str(es_error))

                # handle perturbationset set
                for o_perturbationset in o_superset.perturbationsetset.all():
                    # load acjson file
                    with open (o_perturbationset.acjson_file.path, "r") as f_acjson:
                        d_perturbationset = json.load(f_acjson)
                    # pop perturbation from superset acjson
                    d_acpop, es_error = ac.pop_ac(
                        d_acjson=d_acpop,
                        d_acrecord=d_perturbationset,
                        ts_axis=("perturbation",),
                        b_leaf_intersection=True,
                        b_recordset_major=False,
                    )
                    self.stdout.write(str(f" !!!{o_perturbationset}!!! "))
                    if (len(es_error) != 0):
                        self.stdout.write(str(es_error))

                # handle sampleset set
                for o_sampleset in o_superset.samplesetset.all():
                    # load acjson file
                    with open (o_sampleset.acjson_file.path, "r") as f_acjson:
                        d_sampleset = json.load(f_acjson)
                    # pop samples from sampleset acjson
                    d_acpop, es_error = ac.pop_ac(
                        d_acjson=d_acpop,
                        d_acrecord=d_sampleset,
                        ts_axis=("sample",),
                        b_leaf_intersection=True,
                        b_recordset_major=False,
                    )
                    self.stdout.write(str(f" !!!{o_sampleset}!!! "))
                    if (len(es_error) != 0):
                        self.stdout.write(str(es_error))

                # handle superset set
                # bue 20191030: not track because of its recursive nature
                '''
                for o_superset in o_superset.supersetset.all():
                    # load acjson file
                    with open (o_superset.acjson_file.path, "r") as f_acjson:
                        d_superset = json.load(f_acjson)
                    # pop records from superset acjosn
                    d_acpop, es_error = ac.pop_ac(
                        d_acjson=d_acpop,
                        d_acrecord=d_superset,
                        ts_axis=ac.ts_ACAXIS,
                        b_leaf_intersection=True,
                        b_recordset_major=False,
                    )
                    self.stdout.write(str(f" !!!{o_superset}!!! "))
                    if (len(es_error) != 0):
                        self.stdout.write(str(es_error))
                '''

                #  check if any gent left
                # bue 20180328: this have not to be an error.
                # all records from one set can be timeseried to the other sets.
                #if (ac.count_record(d_acpop) > 0):
                #    self.stdout.write("Error @ acjson_supersetcheck : at least one superset record not found in acaxisset superset set. {}".format(d_acpop))
                #else:
                print("checked {}: ok".format(o_superset.superset_name))
