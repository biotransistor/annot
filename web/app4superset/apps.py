from django.apps import AppConfig


class App4SupersetConfig(AppConfig):
    name = 'app4superset'
