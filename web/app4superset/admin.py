from django import forms
from django.contrib import admin
from django.contrib import messages
from django.core import management
from django.http import HttpResponseRedirect
from app4superset.models import SuperSetFile, SuperSet, AcPipe

# import from django selectable prj
from selectable.forms import AutoComboboxSelectWidget, AutoComboboxSelectMultipleWidget

# import from python
import io
import json
import re

# form annot
# import form annot
from app4superset.lookups import AcPipeLookup, SuperSetFileLookup, SuperSetLookup
from appacaxis.lookups import EndpointSetLookup, PerturbationSetLookup, SampleSetLookup
from appbrxperson.lookups import PersonBrickLookup

# Register your models here.
#admin.site.register(AcPipe)
class AcPipeAdmin(admin.ModelAdmin):
    search_fields = (
        "annot_id",
        "module",
        "import_command",
    )
    list_display = (
        "annot_id",
        "module",
        "import_command",
    )
    save_on_top = True
    fieldsets = [
        ("Identity", { "fields" : ["annot_id"]}),
        ("Module", { "fields" : [
            "module",
            "import_command",
        ]}),
    ]
    readonly_fields = ("annot_id",)
    actions = [
        "delete_selected",
        "download_json",
        "download_tsv",
    ]

    ### json download action ###
    def download_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=generic&choice=app4superset.acpipe"
        )
    )
    download_json.short_description = "Download this page as json file (item selection irrelevant)"

    ### tsv download action ###
    def download_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=generic&choice=app4superset.acpipe"
        ))
    download_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

admin.site.register(AcPipe, AcPipeAdmin)


#admin.site.register(SuperSetFile)
class SuperSetFileAdmin(admin.ModelAdmin):
    search_fields = (
        "annot_id",
        "superset_file",
    )
    list_display = (
        "annot_id",
        "superset_file",
        "code",
    )
    save_on_top = True
    fieldsets = [
        ("Identity", { "fields" : ["annot_id"]}),
        ("File", { "fields" : [
            "superset_file_overwrite",
            "superset_file",
            "code",
        ]}),
    ]
    readonly_fields = ("annot_id",)
    actions = [
        "delete_selected",
        "download_json",
        "download_tsv",
        "download_original",
    ]

    ### json download action ###
    def download_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=generic&choice=app4superset.supersetfile"
        )
    )
    download_json.short_description = "Download this page as json file (item selection irrelevant)"

    ### tsv download action ###
    def download_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=generic&choice=app4superset.supersetfile"
        ))
    download_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

    ### original file download action ###
    def download_original(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_acjson?model=supersetfile&filetype=original&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_original.short_description = "Download selected superset file"

admin.site.register(SuperSetFile, SuperSetFileAdmin)



#admin.site.register(SuperSet)
class SuperSetForm(forms.ModelForm):
    class Meta:
        model = SuperSet
        fields = [
            "acpipemodule",
            "endpointsetset",
            "perturbationsetset",
            "samplesetset",
            "supersetset",
            "supersetfile",
            "responsible",
        ]
        widgets = {
            "acpipemodule": AutoComboboxSelectMultipleWidget(AcPipeLookup),
            "endpointsetset": AutoComboboxSelectMultipleWidget(EndpointSetLookup),
            "perturbationsetset": AutoComboboxSelectMultipleWidget(PerturbationSetLookup),
            "samplesetset": AutoComboboxSelectMultipleWidget(SampleSetLookup),
            "supersetset": AutoComboboxSelectMultipleWidget(SuperSetLookup),
            "supersetfile": AutoComboboxSelectMultipleWidget(SuperSetFileLookup),
            "responsible": AutoComboboxSelectWidget(PersonBrickLookup),
        }

class SuperSetAdmin(admin.ModelAdmin):
    form = SuperSetForm
    search_fields = (
        "annot_id",
        "superset_name",
        "acjson_file",
        "acpipemodule__annot_id",
        "endpointsetset__annot_id",
        "perturbationsetset__annot_id",
        "samplesetset__annot_id",
        "supersetset__annot_id",
        "supersetfile__annot_id",
        "responsible__annot_id",
        "notes",
    )
    list_display = (
        "annot_id",
        "superset_name",
        "acjson_file",
        "acpipemodule_display",
        "endpointsetset_display",
        "perturbationsetset_display",
        "samplesetset_display",
        "supersetset_display",
        "supersetfile_display",
        "notes",
        "responsible",
    )
    save_on_top = True
    fieldsets = [
        ("Identity", { "fields" : [
            "annot_id",
            "superset_name",
        ]}),
        ("Acjson", { "fields" : [
            "acjson_file_overwrite",
            "acjson_file",
        ]}),
        ("Acpipe module", { "fields" : [
            "acpipemodule_display",
            "acpipemodule",
        ]}),
        ("Set of set", { "fields" : [
            "endpointsetset_display",
            "endpointsetset",
            "perturbationsetset_display",
            "perturbationsetset",
            "samplesetset_display",
            "samplesetset",
            "supersetset_display",
            "supersetset",
        ]}),
        ("Superset file", { "fields" : [
            "supersetfile_display",
            "supersetfile",
        ]}),
        ("Notes", { "fields" : [
            "notes",
            "responsible",
        ]}),
    ]
    readonly_fields = (
        "annot_id",
        "acpipemodule_display",
        "endpointsetset_display",
        "perturbationsetset_display",
        "samplesetset_display",
        "supersetset_display",
        "supersetfile_display",
    )
    actions = [
        "delete_selected",
        "download_json",
        "download_brick_json",
        "download_acjson",
        "download_tsv",
        "download_brick_tsv",
        "download_brick_lincs",
        "download_layouttsv_short",
        "download_layouttsv_long",
        "download_dataframetsv_tidy",
        "download_dataframetsv_unstacked",
        "resave",
        "check_acjsonrunid_against_setname",
        "check_acjson_against_set",
        "download_acpipe_template_py",
    ]

    ### json download action ###
    def download_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=generic&choice=app4superset.superset"
        ))
    download_json.short_description = "Download this page as json file (item selection irrelevant)"

    def download_brick_json(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=superset&filetype=json&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_json.short_description = "Download selected set releated bricks as json files"

    def download_acjson(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_acjson?model=superset&filetype=acjson&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_acjson.short_description = "Download selected sets as acjson file"

    ### tsv download action ###
    def download_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=generic&choice=app4superset.superset"
        ))
    download_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

    def download_brick_tsv(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=superset&filetype=tsv&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_tsv.short_description = "Download selected set releated bricks as tsv files"

    def download_brick_lincs(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_brick?model=superset&filetype=lincs&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_brick_lincs.short_description = "Download selected set releated bricks as lincs data standard csv files"

    def download_layouttsv_short(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=superset&filetype=layoutshort&choice={}&user={}".format(
                "!".join(ls_choice),
                request.user.id
            )
        ))
    download_layouttsv_short.short_description = "Download selected sets as tsv_short layout file"

    def download_layouttsv_short(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=superset&filetype=layoutshort&choice={}&user={}".format(
                "!".join(ls_choice),
                request.user.id
            )
        ))
    download_layouttsv_short.short_description = "Download selected sets as tsv_short layout file"

    def download_layouttsv_long(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=superset&filetype=layoutlong&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_layouttsv_long.short_description = "Download selected sets as tsv_long layout file"

    def download_dataframetsv_tidy(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=superset&filetype=dataframetidy&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_dataframetsv_tidy.short_description = "Download selected sets as tsv tidy dataframe files"

    def download_dataframetsv_unstacked(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_dftsv?model=superset&filetype=dataframeunstacked&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_dataframetsv_unstacked.short_description = "Download selected sets as tsv unstacked dataframe file"

    ### resave action ###
    def resave(self, request, queryset):
        # process
        ls_save = []
        for o_record in SuperSet.objects.all():
            o_record.save()
            ls_save.append(o_record.superset_name)
        # message
        messages.success(
            request,
            "{} # resaved".format(ls_save),
        )
    resave.short_description = "Resave all record (item selection irrelevant)"

    ### acpipe ###
    # action check acjson runid against set name
    def check_acjsonrunid_against_setname(self, request, queryset):
        s_out = ""
        for o_record in queryset:
            # load acjson file
            with open (o_record.acjson_file.path, "r") as f_acjson:
                d_acjson = json.load(f_acjson)
            # check
            if (d_acjson["runid"] != o_record.superset_name):
                s_out += "Warning : runid {} differs from superset_name {}. ".format(
                    d_acjson["runid"],
                    o_record.superset_name
                )
        # put s_out as message
        if (s_out == ""):
            messages.success(
                request,
                "{} # successfully checked".format(list(queryset)),
            )
        elif re.match(".*Error.*", s_out.strip()):
            messages.error(
                request,
                "{} # {}".format(ls_choice, s_out.strip()),
            )
        elif re.match(".*Warning.*", s_out):
            messages.warning(
                request,
                "{} # {}".format(ls_choice, s_out.strip()),
            )
        else:
            messages.info(
                request,
                "{} # {}".format(ls_choice, s_out.strip()),
            )
    check_acjsonrunid_against_setname.short_description = "Check superset acjson file runid against superset_name"

    # action check acpipe generated acjson against set acjson
    def check_acjson_against_set(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        for s_choice in ls_choice:
            # stdout handle
            o_out = io.StringIO()
            # command call
            management.call_command(
                "acjson_supersetcheck",
                s_choice,
                stdout=o_out,
                verbosity=0
            )
        # put stdout as message
        s_out = o_out.getvalue().strip()
        if (s_out == ""):
            messages.success(
                request,
                "{} # successfully checked".format(ls_choice),
            )
        elif re.match(".*Error.*", s_out):
            messages.error(
                request,
                "{} # {}".format(ls_choice, s_out),
            )
        elif re.match(".*Warning.*", s_out):
            messages.warning(
                request,
                "{} # {}".format(ls_choice, s_out),
            )
        else:
            messages.info(
                request,
                "{} # {}".format(ls_choice, s_out),
            )
    check_acjson_against_set.short_description = "Check superset acjson file against set of set acjson"

    # action generate python3 acpipe template script
    def download_acpipe_template_py(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appacaxis/export_pytemplate?axis=superset&set={}".format(
                "!".join(ls_choice)
            )
        ))
    download_acpipe_template_py.short_description = "Download python3 acpipe template script"

admin.site.register(SuperSet, SuperSetAdmin)
