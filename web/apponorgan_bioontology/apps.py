from django.apps import AppConfig


class ApponorganBioontologyConfig(AppConfig):
    name = 'apponorgan_bioontology'
