echo
echo "*** BEGIN con job ***"

# set docker-machine usr environment variables
echo "$(date +%Y-%m-%d_%T) set docker environment..."
echo "PATH: ${PATH}"
echo "whoami: $(whoami)"
docker-machine ls
eval "$(docker-machine env mymachine)"  # bue: adjust to your docker-machine name!
docker-machine ls
docker ps
echo "ok"

# set pull day variabe
echo
echo "$(date +%Y-%m-%d_%T) set pull date ..."
let "dday=$(date +%Y%m%d)" # $(date +%Y%m%d)-1
echo "pull day: ${dday}"
echo "ok"

# pull annot backups to host
# vocabulary
echo
echo "$(date +%Y-%m-%d_%T) handle annot vocabulary..."
# bue 20180412: origin not backuped. can be pulled form the web if needed.
#docker cp annot_web_1:/usr/src/media/vocabulary/\
#${dday}_json_origin_latestvoci /path/on/host/to/store/backup/
docker cp annot_web_1:/usr/src/media/vocabulary/\
${dday}_json_backup_latestvoci /path/on/host/to/store/backup/
echo "ok"

# brick
echo
echo "$(date +%Y-%m-%d_%T) handle annot bricks..."
docker cp annot_web_1:/usr/src/media/brick/\
${dday}_json_latestbrick /path/on/host/to/store/backup/
docker cp annot_web_1:/usr/src/media/brick/\
${dday}_tsv_latestbrick /path/on/host/to/store/backup/
echo "ok"

# experiment (and upload)
echo
echo "$(date +%Y-%m-%d_%T) handle annot experiment..."
docker cp annot_web_1:/usr/src/media/experiment/\
${dday}_json_latestexperiment /path/on/host/to/store/backup/
echo "ok"
echo "$(date +%Y-%m-%d_%T) handle annot upload ..."
docker cp annot_web_1:/usr/src/media/experiment/\
${dday}_acjson_latestexperiment /path/on/host/to/store/backup/
echo "ok"

# log
echo
echo "$(date +%Y-%m-%d_%T) handle annot log..."
docker cp annot_web_1:/usr/src/media/log/\
${dday}cron.log /path/on/host/to/store/backup/
echo "ok"

echo "*** END cron job ***"
echo
