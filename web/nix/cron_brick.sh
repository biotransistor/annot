# load brick
python /usr/src/app/manage.py brick_load

# backup json and tsvhuman brick reagent
python /usr/src/app/manage.py antibody1_db2json
python /usr/src/app/manage.py antibody1_db2tsv
python /usr/src/app/manage.py antibody2_db2json
python /usr/src/app/manage.py antibody2_db2tsv
python /usr/src/app/manage.py compound_db2json
python /usr/src/app/manage.py compound_db2tsv
python /usr/src/app/manage.py cstain_db2json
python /usr/src/app/manage.py cstain_db2tsv
python /usr/src/app/manage.py protein_db2json
python /usr/src/app/manage.py protein_db2tsv
python /usr/src/app/manage.py proteinset_db2json
python /usr/src/app/manage.py proteinset_db2tsv
# backup json and tsvhuman brick sample
python /usr/src/app/manage.py human_db2json
python /usr/src/app/manage.py human_db2tsv
# backup json brick person
python /usr/src/app/manage.py person_db2json

# pkg bricks
python /usr/src/app/manage.py brick_pkgjson
python /usr/src/app/manage.py brick_pkgtsv
