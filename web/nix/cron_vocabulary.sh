# backup vocabulary
python /usr/src/app/manage.py vocabulary_backup apponantibodypart_bioontology
python /usr/src/app/manage.py vocabulary_backup apponbiologicalprocess_go
python /usr/src/app/manage.py vocabulary_backup apponcelltype_bioontology
python /usr/src/app/manage.py vocabulary_backup apponcellularcomponent_go
python /usr/src/app/manage.py vocabulary_backup apponclonality_bioontology
python /usr/src/app/manage.py vocabulary_backup apponcompound_ebi
python /usr/src/app/manage.py vocabulary_backup appondisease_bioontology
python /usr/src/app/manage.py vocabulary_backup appondye_own
python /usr/src/app/manage.py vocabulary_backup apponethnicity_bioontology
python /usr/src/app/manage.py vocabulary_backup appongene_ensembl
python /usr/src/app/manage.py vocabulary_backup appongeneontology_go
python /usr/src/app/manage.py vocabulary_backup appongeneticmodification_bioontology
python /usr/src/app/manage.py vocabulary_backup appongrowthproperty_bioontology
python /usr/src/app/manage.py vocabulary_backup apponhealthstatus_own
python /usr/src/app/manage.py vocabulary_backup apponimmunologyisotype_bioontology
python /usr/src/app/manage.py vocabulary_backup apponmolecularfunction_go
python /usr/src/app/manage.py vocabulary_backup apponorgan_bioontology
python /usr/src/app/manage.py vocabulary_backup apponorganism_bioontology
python /usr/src/app/manage.py vocabulary_backup apponprotein_uniprot
python /usr/src/app/manage.py vocabulary_backup apponprovider_own
python /usr/src/app/manage.py vocabulary_backup apponsample_cellosaurus
python /usr/src/app/manage.py vocabulary_backup apponsampleentity_own
python /usr/src/app/manage.py vocabulary_backup apponsex_bioontology
python /usr/src/app/manage.py vocabulary_backup appontissue_bioontology
python /usr/src/app/manage.py vocabulary_backup appontransientmodification_bioontology
python /usr/src/app/manage.py vocabulary_backup apponunit_bioontology
python /usr/src/app/manage.py vocabulary_backup apponverificationprofile_own
python /usr/src/app/manage.py vocabulary_backup apponyieldfraction_own
# onw vacabulary backup 2 orignal
python /usr/src/app/manage.py vocabulary_backup2origin appondye_own
python /usr/src/app/manage.py vocabulary_backup2origin apponhealthstatus_own
python /usr/src/app/manage.py vocabulary_backup2origin apponprovider_own
python /usr/src/app/manage.py vocabulary_backup2origin apponsampleentity_own
python /usr/src/app/manage.py vocabulary_backup2origin apponverificationprofile_own
python /usr/src/app/manage.py vocabulary_backup2origin apponyieldfraction_own
# pkg vocabulary
# python /usr/src/app/manage.py vocabulary_pkgjsonorigin  # bue: not packed can be pulled for the web if needed
python /usr/src/app/manage.py vocabulary_pkgjsonbackup
