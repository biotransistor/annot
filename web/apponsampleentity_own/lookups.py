# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponsampleentity_own.models import SampleEntity

# code
class SampleEntityLookup(ModelLookup):
    model = SampleEntity
    search_fields = ('annot_id__icontains',)
registry.register(SampleEntityLookup)
