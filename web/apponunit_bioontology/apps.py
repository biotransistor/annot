from django.apps import AppConfig


class ApponunitBioontologyConfig(AppConfig):
    name = 'apponunit_bioontology'
