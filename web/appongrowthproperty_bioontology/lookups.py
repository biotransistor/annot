# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from appongrowthproperty_bioontology.models import GrowthProperty

# code
class GrowthPropertyLookup(ModelLookup):
    model = GrowthProperty
    search_fields = ('annot_id__icontains',)
registry.register(GrowthPropertyLookup)

