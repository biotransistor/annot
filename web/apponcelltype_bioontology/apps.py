from django.apps import AppConfig


class ApponcelltypeBioontologyConfig(AppConfig):
    name = 'apponcelltype_bioontology'
