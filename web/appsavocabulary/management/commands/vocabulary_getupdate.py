import django
from django.core.management.base import BaseCommand, CommandError

# import python
import csv
import datetime
import ftplib
import glob
import importlib
import json
import os
import re
import requests
#import sys
import time
import urllib.parse
import urllib.request

# import annot
from appsavocabulary.models import SysAdminVocabulary
from appsavocabulary.structure import argsvocabulary, appendrecordvocabulary
from prjannot import crowbar  # annot passkey library
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest

# constants
s_pathquarantine = "{}quarantine/".format(MEDIA_ROOT)

### ontology formats to an! json  ###
class ontologyformat2json():
    """
    description:
        transform any common ontology format file (at media/quarantine/)
        into and annot appon compatible json file (at media/vocabulary/origin/)
    """
    def pknamingconvention(
            self,
            s_term,
            s_identifier,
            o_pkstringcase=None  # str.lower
        ):
        """
        input:
            s_term: original ontology term
            s_identifier: original ontology identifier
            o_pkstringcase: string case method None, str.lower, str.upper

        output:
            generates naming convention compatible primary key string

        description:
            function generates naming convention compatible
            primary key string based on original term and identifier.
        """
        # manipulate overflow term
        if (len(s_term) > 128):
            s_term = "longterm"
        # manipulate identifier
        s_ident = re.sub(r"[^a-zA-Z0-9]", "", s_identifier)
        # build annot id primary key
        s_pk = "{}_{}".format(s_term, s_ident)
        s_pk = re.sub(r"[^a-zA-Z0-9]", "_", s_pk)
        s_pk = re.sub(r"_+", "_", s_pk)
        if not (o_pkstringcase is None):
            s_pk = o_pkstringcase(s_pk)
        # output
        return(s_pk)


    # gtf format; ensembl gene
    def gtf2json(
            self,
            s_filedownload,
            s_vocabulary,
            s_model,
            s_release,
            s_version,
            o_pkstringcase=str.lower,
            b_swaptermid=False
        ):
        """"
        input:
            s_filedownload: path to downloaded file in media/quarantine
            s_vocabulary: voci_source name (apponname without appon)
            s_model: annot appon* model
            s_release: release date in YYYY-MM-DD format
            s_version: version number
            o_pkstringcase: primary key string case. str.lower, str.upper and None.
            b_swaptermid: swap term and id?

        output:
            json formatted output file

        description:
            transforms gft format to json format.
        """
        # empty output
        ld_genome = []
        ds_genome = {}
        # processing
        with open(s_filedownload, newline="") as f_in:
            reader = csv.reader(f_in, delimiter="\t")
            for ls_tuple in reader:
                # get row
                if (len(ls_tuple) >= 9):
                    # get input string
                    s_extract = ls_tuple[8]
                    # get input string into list
                    ls_extract = s_extract.split(";")
                    # get input string into dict
                    ds_extract = {}
                    for s_xtract in ls_extract:
                        ls_fractal = s_xtract.split('"')
                        if (len(ls_fractal) > 1):
                            ds_extract.update(
                                {ls_fractal[0].strip(): ls_fractal[1].strip()}
                            )
                    # update genome dict and json compatible genome list of dict
                    try:
                        s_found = ds_genome[ds_extract["gene_id"]]
                        if not(s_found == ds_extract["gene_name"]):
                            raise CommandError(
                                "Error vocabulary_getupdate: ensg gene id {} owns more then one gene name {} and {}.".format(
                                    ds_extract["gene_id"],
                                    s_found,
                                    ds_extract["gene_name"]
                                )
                            )
                    except KeyError:
                        # update ds_genome
                        ds_genome[ds_extract["gene_id"]] = ds_extract["gene_name"]
                        # store record by updating ld_genome
                        s_pk = self.pknamingconvention(
                            s_term=ds_extract["gene_name"],
                            s_identifier=ds_extract["gene_id"],
                            o_pkstringcase=o_pkstringcase
                        )
                        appendrecordvocabulary(
                            ld_json=ld_genome,
                            s_model=s_model,
                            s_term=ds_extract["gene_name"],
                            s_id=ds_extract["gene_id"],
                            s_pk=s_pk,
                            s_responsible="term_from_origin_source",
                            s_release=s_release,
                            s_version=s_version,
                            s_termok=True,
                            b_swaptermid=b_swaptermid
                        )
        # write to file
        s_ofile = "{}vocabulary/origin/{}_{}_{}_origin.json".format(
            MEDIA_ROOT,
            s_vocabulary,
            s_release.replace("-",""),
            s_version
        )
        with open(s_ofile, "w") as f_out:  # open file handle
            json.dump(ld_genome, f_out, sort_keys=True)  # indent=4


    # bioontology branch
    def jsonbioontology2json(
            self,
            s_filedownload,
            s_vocabulary,
            s_model,
            s_release,
            s_version,
            o_pkstringcase=str.lower,
            b_swaptermid=False
        ):
        """
        input:
            s_filedownload: path to downloaded file in media/quarantine
            s_vocabulary: voci_source name (apponname without appon)
            s_model: annot appon* model
            s_release: release date in YYYY-MM-DD format
            s_version: version number
            o_pkstringcase: primary key string case. str.lower, str.upper and None.
            b_swaptermid: swap term and id?

        output:
            json formatted output file

        description:
            transforms pulled json branch format
            into vocabulary origin json format.
        """
        # pack branch django fixture compatible
        ld_branch = []

        # load d_branch form media/quarantine bioontology.json file
        with  open(s_filedownload, "r") as f_in: # open file handle
            d_branch = json.load(f_in)

        # get packed
        for s_key in d_branch:
            d_entry = {}
            d_field = {}
            # build primary key
            s_pk = self.pknamingconvention(
                s_term=d_branch[s_key],
                s_identifier=s_key,
                o_pkstringcase=o_pkstringcase
            )
            # store entry by populating list of dictionary branch
            appendrecordvocabulary(
                ld_json=ld_branch,
                s_model=s_model,
                s_term=d_branch[s_key],
                s_id=s_key,
                s_pk=s_pk,
                s_responsible="term_from_origin_source",
                s_release=s_release,
                s_version=s_version,
                s_termok=True,
                b_swaptermid=b_swaptermid
            )
        # write to file
        s_ofile = "{}vocabulary/origin/{}_{}_{}_origin.json".format(
            MEDIA_ROOT,
            s_vocabulary,
            s_release.replace("-",""),
            s_version
        )
        with open(s_ofile, "w") as f_out:  # open file handle
            json.dump(ld_branch, f_out, sort_keys=True)  # indent=4


    # uniprot api
    def jsonuniprot2json(
            self,
            s_filedownload,
            s_vocabulary,
            s_model,
            s_release,
            s_version,
            o_pkstringcase=str.lower,
            b_swaptermid=False
        ):
        """
        input:
            s_filedownload: path to downloaded file in media/quarantine
            s_vocabulary: voci_source name (apponname without appon)
            s_model: annot appon* model
            s_release: release date in YYYY-MM-DD format
            s_version: version number
            o_pkstringcase: primary key string case. str.lower, str.upper and None.
            b_swaptermid: swap term and id?

        output:
            json formatted output file

        description:
            transforms pulled json branch format
            into vocabulary origin json format.
        """
        # empty output
        ld_vocabulary = []

        # load lds_page form json file
        with  open(s_filedownload, "r") as f_in: # open file handle
            lds_page = json.load(f_in)

        # get packed
        for ds_page in lds_page:

            # primary gene name
            ls_gene = ds_page["Gene names"].split(" ")
            if (len(ls_gene) >= 1):
                s_gene = ls_gene[0]
                s_gene = re.sub("[^a-zA-Z0-9]","", s_gene)
            else:
                s_gene = "None"
            # primary protein name
            s_protein = ds_page["Protein names"]
            s_protein = s_protein.strip()

            # check for isoform
            s_isoform = ds_page["Alternative products (isoforms)"]
            o_isoform = re.search("Named isoforms=([0-9]*)", s_isoform)

            # protein with isoform
            lt_isoform = []
            if (o_isoform):
                i_isoform = int(
                    re.search("Named isoforms=([0-9]*)", s_isoform).groups()[0]
                )
                ls_fractal = s_isoform.split(";")
                s_isoname = None
                s_isoid = None
                for s_fractal in ls_fractal:
                    if (s_isoname == None):
                        o_found = re.search("Name=([a-zA-Z0-9- ]+)", s_fractal)
                        if (o_found):
                            s_isoname = o_found.groups()[0]
                            s_isoname = s_isoname.replace(" ","")
                            s_isoname = s_isoname.replace("-","")
                    if (s_isoname != None) and (s_isoid == None):
                        o_found = re.search("IsoId=([a-zA-Z0-9-]+)", s_fractal)
                        if (o_found):
                            s_isoid = o_found.groups()[0]
                    if (s_isoname != None) and (s_isoid != None):
                        lt_isoform.append((s_isoname,s_isoid))  # add tuple
                        s_isoname = None
                        s_isoid = None
                if (i_isoform != len(lt_isoform)):
                    # Error
                    raise CommandError(
                        "Error vocabulary_getupdate: UniProt protein for gene {} should have {} isoforms. But different amount found <<{}>>.".format(
                            s_gene,
                            i_isoform,
                            s_isoform
                        )
                    )
                for t_isoform in lt_isoform:
                    # primary key
                    s_isoname = re.sub("[^a-zA-Z0-9]","",t_isoform[0])
                    s_uniprot = t_isoform[1].replace("-","|")
                    s_annotid = "{}|{}_{}".format(s_gene, s_isoname, s_uniprot)
                    #print("An!_ID:", s_annotid)
                    # store record
                    appendrecordvocabulary(
                        ld_json=ld_vocabulary,
                        s_model=s_model,
                        s_term=s_protein,
                        s_id=s_uniprot,
                        s_pk=s_annotid,
                        s_responsible="term_from_origin_source",
                        s_release=s_release,
                        s_version=s_version,
                        s_termok=True,
                        b_swaptermid=b_swaptermid
                    )
            # protein without isoform
            else:
                s_uniprot = ds_page["Entry"]
                s_uniprot = s_uniprot.strip()
                # annot id
                s_annotid = "{}_{}".format(s_gene, s_uniprot)
                #print("An!_ID:", s_annotid)
                # store record
                appendrecordvocabulary(
                    ld_json=ld_vocabulary,
                    s_model=s_model,
                    s_term=s_protein,
                    s_id=s_uniprot,
                    s_pk=s_annotid,
                    s_responsible="term_from_origin_source",
                    s_release=s_release,
                    s_version=s_version,
                    s_termok=True,
                    b_swaptermid=b_swaptermid
                )

        # write ld_vocabulary to json file
        s_ofile = "{}vocabulary/origin/{}_{}_{}_origin.json".format(
            MEDIA_ROOT,
            s_vocabulary,
            s_release.replace("-",""),
            s_version
        )
        with open(s_ofile, "w") as f_out: # open file handle
            json.dump(ld_vocabulary, f_out, sort_keys=True)  # indent=4


    # obo format; chebi; cellosaurus; go
    def obo2json(
            self,
            s_filedownload,
            s_vocabulary,
            s_model,
            s_release,
            s_version,
            o_pkstringcase=str.lower,
            b_swaptermid=False
        ):
        """
        input:
            s_filedownload: path to downloaded file in media/quarantine
            s_vocabulary: voci_source name (apponname without appon)
            s_model: annot appon* model
            s_release: release date in YYYY-MM-DD format
            s_version: version number
            o_pkstringcase: primary key string case. str.lower, str.upper and None.
            b_swaptermid: swap term and id?

        output:
            json formatted putout file

        description:
            transforms obo format to json format.
            handles obo format-version 1.2.
        """
        # set empty
        ld_out = []
        ds_record = {}
        b_term = False
        b_record = False
        b_maybeobsolet = True
        # read out file
        with open(s_filedownload, "r") as f_in:
            for s_line in f_in:
                s_entry = s_line.strip()
                # set term and record flags
                if (re.search(r"^\[Term]", s_entry) != None):
                    b_term = True
                    b_maybeobsolet = False
                    # erase record
                    s_term = None
                    s_identifier = None
                    s_pk = None
                    if (s_vocabulary in [
                            "biologicalprocess_go",
                            "cellularcomponent_go",
                            "molecularfunction_go"
                    ]):
                        b_namespace = False
                    else:
                        b_namespace = True
                elif (re.search(r"^is_obsolete.*:.*true", s_entry) != None):
                    # obsolet term record
                    if b_maybeobsolet:
                        ld_out.pop(-1)
                    b_term = False
                    b_maybeobsolet = False
                elif (len(s_entry) == 0):
                    # end term record
                    b_term = False
                    b_maybeobsolet = False

                # if term flag true
                if b_term:
                    # load record
                    ls_entry = s_entry.split(": ")
                    # analyze split
                    if (ls_entry[0] in ["name","id","namespace"]) \
                       and (len(ls_entry) > 2):
                        print(
                            "\nWarning : entry '{}' Contains more then a 'key: value' pair.".format(
                                s_entry
                            )
                        )
                        ls_value = ls_entry[1:]
                        s_value = ":".join(ls_value)
                        s_key = ls_entry[0]
                        ls_entry = [s_key, s_value]
                        print("Check out this fix: {}".format(ls_entry))
                    # get term
                    if (ls_entry[0].strip() == "name"):
                        s_term = ls_entry[1].strip()
                    # get identifier
                    elif (ls_entry[0].strip() == "id"):
                        s_identifier = ls_entry[1].strip()
                    # get namespace (gene ontology)
                    elif (ls_entry[0].strip() == "namespace"):
                        s_namespace = ls_entry[1].strip()
                        # gene ontology domains
                        if not (s_namespace in [
                                "biological_process",
                                "cellular_component",
                                "molecular_function"
                            ]):
                            print(
                                "\nWarning : at {} non basic geneontology namespace {} for id {} found.".format(
                                    s_vocabulary,
                                    s_namespace,
                                    s_identifier
                                )
                            )
                        if ((s_namespace == "biological_process") \
                            and (s_vocabulary == "biologicalprocess_go")):
                            b_namespace = True
                        elif ((s_namespace == "cellular_component") \
                              and (s_vocabulary == "cellularcomponent_go")):
                            b_namespace = True
                        elif ((s_namespace == "molecular_function") \
                              and (s_vocabulary == "molecularfunction_go")):
                            b_namespace = True
                        else:
                            pass
                    # get nothing
                    else:
                        pass
                    # set record flag
                    if ((s_term is not None) \
                        & (s_identifier is not None) \
                        & b_namespace):
                        b_record = True

                # put record
                if b_record:  # & (b_term == False)
                    # set record flag back
                    b_record = False
                    # store record
                    s_pk = self.pknamingconvention(
                        s_term=s_term,
                        s_identifier=s_identifier,
                        o_pkstringcase=o_pkstringcase
                    )
                    appendrecordvocabulary(
                        ld_json=ld_out,
                        s_model=s_model,
                        s_term=s_term,
                        s_id=s_identifier,
                        s_pk=s_pk,
                        s_responsible="term_from_origin_source",
                        s_release=s_release,
                        s_version=s_version,
                        s_termok=True,
                        b_swaptermid=b_swaptermid
                    )
                    # set obsolete and term flag
                    b_maybeobsolet = True
                    b_term = False

        # write to file
        s_ofile = "{}vocabulary/origin/{}_{}_{}_origin.json".format(
            MEDIA_ROOT,
            s_vocabulary,
            s_release.replace("-",""),
            s_version
        )
        with open(s_ofile, "w") as f_out:  # open file handle
            json.dump(ld_out, f_out, sort_keys=True)  # indent=4


    # umls format; ncbitaxon
    def umls2json(
            self,
            s_filedownload,
            s_vocabulary,
            s_model,
            s_release,
            s_version,
            o_pkstringcase=str.lower,
            b_swaptermid=False
        ):
        """
        input:
            s_filedownload: path to downloaded file in media/quarantine
            s_vocabulary: voci_source name (apponname without appon)
            s_model: annot appon* model
            s_release: release date in YYYY-MM-DD format
            s_version: version number
            o_pkstringcase: primary key string case. str.lower, str.upper and None.
            b_swaptermid: swap term and id?

        output:
            json formatted output file

        description:
            transforms ulms format to json format.
        """
        # set empty
        ld_out = []
        s_term = None
        s_identifier = None
        b_term = False
        b_record = False
        # read out file
        with open(s_filedownload, "r") as f_in:
            for s_line in f_in:
                s_entry = s_line.strip()
                # set term and record flags
                # r"^<http://purl.bioontology.org/ontology/NCBITAXON/[0-9]*> a owl:Class ;"
                if (re.search(
                        r"^<http://.*/NCBITAXON/.*> a owl:Class ;",
                        s_entry
                    ) != None):
                    b_term = True
                    # erase record
                    s_term = None
                    s_identifier = None
                    s_pk = None
                elif (re.search(r"^.*\.$", s_entry) != None):
                    b_term = False

                # if term flag true
                if b_term:
                    # load record
                    ls_entry = s_entry.split('"""')
                    # get term
                    if (ls_entry[0].strip() == "skos:prefLabel"):
                        s_term = ls_entry[1].strip()
                    # get identifier
                    elif (ls_entry[0].strip() == "skos:notation"):
                        s_identifier = ls_entry[1].strip()
                # get nothing
                else:
                    pass
                # set record flag
                if (not(s_term is None) and not(s_identifier is None)):
                    b_record = True

                # put record
                if(b_record and not(b_term)):
                    # set b_record flag back
                    b_record = False
                    # store record
                    s_pk = self.pknamingconvention(
                        s_term=s_term,
                        s_identifier=s_identifier,
                        o_pkstringcase=o_pkstringcase
                    )
                    appendrecordvocabulary(
                        ld_json=ld_out,
                        s_model=s_model,
                        s_term=s_term,
                        s_id=s_identifier,
                        s_pk=s_pk,
                        s_responsible="term_from_origin_source",
                        s_release=s_release,
                        s_version=s_version,
                        s_termok=True,
                        b_swaptermid=b_swaptermid
                    )
        # write to file
        s_ofile = "{}vocabulary/origin/{}_{}_{}_origin.json".format(
            MEDIA_ROOT,
            s_vocabulary,
            s_release.replace("-",""),
            s_version
        )
        with open(s_ofile, "w") as f_out: # open file handle
            json.dump(ld_out, f_out, sort_keys=True)  # indent=4


### ontology sources ###
class ontologysource2handle():
    """
    description:
        source2get_version: get online release data and version dictionary.
        source2get_file: original source ontology file download to media/quarantine/
    """
    ## bioontology.org ##
    def __bioontologyorg2get_jsonrecord__(self, s_url, s_apikey):
        """
        input:
            s_url: bioontology.org api base url
            s_apikey: bioontology.org apikey

        output:
            d_record dictionary

        source:
            http://data.bioontology.org/documentation
            https://github.com/ncbo/ncbo_rest_sample_code

        description:
            internal method.
            get from bioontology url record via api
        """
        d_record = {}
        opener = urllib.request.build_opener()
        opener.addheaders = [
            ("Authorization", "apikey token={}".format(s_apikey))
        ]
        s_record = opener.open(s_url).read().decode("utf-8")  # type bytes turn into unicode
        d_record = json.loads(s_record)
        return(d_record)

    def __bioontologyorg2extract_term__(self, d_record, d_extract):
        """
        input:
            d_record: dictionary
            d_extract: dictionary

        output:
            d_extract: updated dictionary
            s_urlnext: url to next term

        description:
            internal method
            extract term form bioontologyorg2get_jsonrecord d_record output
        """
        # put extract in normal form
        try:
            l_record = d_record["collection"]
            s_urlnext = d_record["links"]["nextPage"]
        except(KeyError):
            l_record = [d_record]
            s_urlnext = None
        # extract info
        for d_record in l_record:
            s_identifier = d_record["@id"]
            ls_identifier = s_identifier.split("/")
            s_onidentifier = ls_identifier[-1]
            s_onterm = d_record["prefLabel"]
            # output
            if ((s_onidentifier is not None) & (s_onterm is not None)):
                # d_extract.update({s_onterm: s_onidentifier})
                d_extract.update({s_onidentifier: s_onterm})  # the identifier will be more unique as the term
        # return
        return(d_extract, s_urlnext)


    def bioontology2get_version(self, s_url, s_apikey):
        """
        input:
            s_url: bioontology.org api base url
            s_apikey: bioontology.org apikey

        output:
            ds_extract dictionary

        description:
            get from bioontology url latest ontology version online
            via api called over internal bioontologyorg2get_jsonrecord method
        """
        # get json
        ds_extract = {}
        d_record = self.__bioontologyorg2get_jsonrecord__(
            s_url=s_url,
            s_apikey=s_apikey
        )
        # extraction
        s_version = d_record["version"]
        s_version = re.sub(r"[^a-zA-Z0-9]", "-", s_version)
        s_date = d_record["released"]
        s_date = s_date[0:10]
        i_date = int(s_date.replace("-",""))
        ds_extract.update({"s_version": s_version})
        ds_extract.update({"s_date": s_date})
        ds_extract.update({"i_date": i_date})
        # output
        return(ds_extract)


    def bioontology2get_file(
            self,
            s_filedownload,
            s_format,
            s_url,
            s_apikey,
            b_getbranchcut,
            s_vocabulary,
            s_model,
            s_release,
            s_version,
            o_pkstringcase=str.lower,
            b_swaptermid=False
        ):
        """
        input:
            # rest
            s_filedownload: downloaded file name to give
            s_format: download format for this ontology. http, obo, umls.
            s_url: bioontology.org api base url
            s_apikey: bioontology.org apikey
            b_getbranchcut: should the cut it self be a ctrl vocabulary term or not?
            # build
            s_vocabulary: voci_source name (apponname without appon)
            s_model: annot appon* model
            s_release: release date in YYYY-MM-DD format
            s_version: version number
            o_pkstringcase: primary key string case. str.lower, str.upper and None.
            b_swaptermid: swap term and id?

        output:
            filedownload to /media/quarantine
            ontologyformat2json class call

        description:
            calls iteratively internal methods
            bioontologyorg2get_jsonrecord and bioontologyorg2extract_term
            to get whole ontology branch.
            save branch as json
        """
        o_trafo = ontologyformat2json()
        # http branch
        if (s_format == "http"):
            d_branch = {}
            # get cut
            if (b_getbranchcut):
                d_record = self.__bioontologyorg2get_jsonrecord__(
                    s_url=s_url,
                    s_apikey=s_apikey
                )
                (d_branch, s_urlnop) = self.__bioontologyorg2extract_term__(
                    d_record=d_record,
                    d_extract=d_branch
                )
            # get whole branch by iterate through the pages
            s_urlnext = "{}/descendants".format(s_url)
            while not (s_urlnext is None):
                d_record = self.__bioontologyorg2get_jsonrecord__(
                    s_url=s_urlnext,
                    s_apikey=s_apikey
                )
                (d_branch, s_urlnext) = self.__bioontologyorg2extract_term__(
                    d_record=d_record,
                    d_extract=d_branch
                )
            # output vocabulary json file
            with open(s_filedownload, "w") as f_out:
                json.dump(d_branch, f_out, sort_keys=True)  # indent=4
            # call ontologyformat2json
            o_trafo.jsonbioontology2json(
                s_filedownload=s_filedownload,
                s_vocabulary=s_vocabulary,
                s_model=s_model,
                s_release=s_release,
                s_version=s_version,
                o_pkstringcase=o_pkstringcase,
                b_swaptermid=b_swaptermid
            )
        # obo
        elif (s_format == "obo"):
            # http request
            # s_url = "http://data.bioontology.org/ontologies/CHEBI/download"
            opener = urllib.request.build_opener()
            opener.addheaders = [
                ("Authorization", "apikey token={}".format(s_apikey))
            ]
            s_record = opener.open(s_url).read().decode("utf-8")  # type bytes turn into unicode
            with open(s_filedownload, "w") as f_out:
                f_out.write(s_record)
            # call ontologyformat2json
            o_trafo.obo2json(
                s_filedownload=s_filedownload,
                s_vocabulary=s_vocabulary,
                s_model=s_model,
                s_release=s_release,
                s_version=s_version,
                o_pkstringcase=o_pkstringcase,
                b_swaptermid = b_swaptermid
            )
        # umls
        elif (s_format == "umls"):
            # http request
            # s_url = "http://data.bioontology.org/ontologies/NCBITAXON/download"
            opener = urllib.request.build_opener()
            opener.addheaders = [
                ("Authorization", "apikey token={}".format(s_apikey))
            ]
            s_record = opener.open(s_url).read().decode("utf-8")  # type bytes turn into unicode
            with open(s_filedownload, "w") as f_out:
                f_out.write(s_record)
            # call ontologyformat2json
            o_trafo.umls2json(
                s_filedownload=s_filedownload,
                s_vocabulary=s_vocabulary,
                s_model=s_model,
                s_release=s_release,
                s_version=s_version,
                o_pkstringcase=o_pkstringcase,
                b_swaptermid = b_swaptermid
            )
        # error
        else:
            raise CommandError(
                "Error vocabulary_getupdate: unknown vocabulary source format {}.".format(
                    m_appon.models.FORMAT
                )
            )
        # delete s_filedownload
        os.remove(s_filedownload)


    ## ftp cellosaurus; chebi; ebi ##
    def ftp2get_version(self, s_url, s_query, s_filedownload):
        """
        input:
            s_url: ftp url
            s_query: ftp subdirectory where the ontology file can be found
            s_filedownload: the ontology file name

        description:
            get latest file release date form ftp server

        ftp commands:
            https://en.wikipedia.org/wiki/List_of_FTP_commands
        """
        # bue 20161020: rewritten cause chebi ftp can not handle the mlsd command
        # reset output
        d_onlineversion = {}
        # get ls form ftp server
        # ftp://ftp.expasy.org/databases/cellosaurus/
        #s_url = "ftp.expasy.org"
        #s_query = "/databases/cellosaurus/"
        #s_filedownload = "cellosaurus.obo"
        #with ftplib.FTP(s_url) as ftp:
        ftp = ftplib.FTP(s_url)
        s_login = ftp.login()
        try:
            s_mdtm = ftp.sendcmd("MDTM {}{}".format(s_query, s_filedownload))
            b_found = True
        except:
            b_found = False
        ftp.quit()
        # extract release
        if (b_found):
            s_idate = s_mdtm[-14:-6]
            s_date = "{}-{}-{}".format(s_idate[0:4], s_idate[4:6], s_idate[6:8])
            d_onlineversion.update({"i_date" : int(s_idate)})
            d_onlineversion.update({"s_date" : s_date})
            d_onlineversion.update({"s_version" : None})
        else:
            raise CommandError(
                "Error vocabulary_getupdate: at ftp ulr {} could not find ontology file {}. update apponmmm.models.py file".format(
                    s_url,
                    s_filedownload
                )
            )
        # output
        return(d_onlineversion)


    def ftpobo2get_file(
            self,
            s_url,
            s_query,
            s_filedownload,
            s_vocabulary,
            s_model,
            s_release,
            s_version,
            o_pkstringcase=str.lower,
            b_swaptermid=False
        ):
        """
        input:
            # rest
            s_url: ftp url
            s_query: ftp subdirectory where the ontology file can be found
            s_filedownload: annot media/quarantine path and ontology file name
            # build
            s_vocabulary: voci_source name (apponname without appon)
            s_model: annot appon* model
            s_release: release date in YYYY-MM-DD format
            s_version: version number
            o_pkstringcase: primary key string case. str.lower, str.upper and None.
            b_swaptermid: swap term and id?

        output:
            filedownload to /media/quarantine
            ontologyformat2json class call

        description:
            download latest obo ontology file.
            extract and transform ontology into annot compatible
            controlled vocabulary.
        """
        # download file from ftp
        s_ftpfile = "{}{}".format(s_query, s_filedownload.split("/")[-1])
        with  ftplib.FTP(s_url) as ftp:
            ftp.login()
            ftp.retrbinary(
                "RETR {}".format(s_ftpfile),
                open(s_filedownload, "wb").write
            )
            ftp.quit()
        # extract release
        with open(s_filedownload, "r") as f_in:
            for s_line in f_in:
                if ("data-version" in s_line):
                    ls_line = s_line.split(":")
                    s_version = ls_line[1].strip()
                    s_version = re.sub(r"[^a-zA-Z0-9]", "-", s_version)
                    break
        # ontologyformat2json
        o_trafo = ontologyformat2json()
        o_trafo.obo2json(
            s_filedownload=s_filedownload,
            s_vocabulary=s_vocabulary,
            s_model=s_model,
            s_release=s_release,
            s_version=s_version,
            o_pkstringcase=o_pkstringcase,
            b_swaptermid = b_swaptermid
        )
        # delete downloaded file
        os.remove(s_filedownload)
        return(s_version)


    ## ensembl.org ##
    def ensembl2get_version(self, s_url, s_organism):
        """
        input:
            s_url: ensembl api url
            s_organism: organism written same as in the filename on the ensembl server

        description:
            get from the latest genome for the requested organism from ensembl
            extract version.
        """
        d_latest = {}

        # reset output
        s_assembly = None
        i_release = None
        # request version1
        #s_url = "http://rest.ensembl.org/info/species?content-type=application/json"
        #o_request = urllib.request.Request(s_url)
        #o_request.add_header("Content-Type", "application/json")
        #o_httpresponse = urllib.request.urlopen(o_request)
        #o_header = o_httpresponse.info()
        #s_header = str(o_header)
        #s_page = o_httpresponse.read().decode("utf-8")
        #d_page = json.loads(s_page)
        #ld_page = d_page["species"]

        # requeste version2
        # s_url = "http://rest.ensembl.org/info/species"
        d_param = {"content-type" : "application/json"}
        r = requests.get(url=s_url, params=d_param)
        ld_page = r.json()["species"]

        # json hack
        #print(ld_page)
        # get s_assembly = "GRCh38"
        # get i_release = "78"
        for d_page in ld_page:
            print("Detected: {}\t{}\t{}".format(
                d_page["name"],
                d_page["assembly"],
                d_page["release"]
            ))
            if (d_page["name"] == s_organism):
                print("Found.")
                s_assembly = d_page["assembly"]  # assembly
                i_release = d_page["release"]  # release
                break
        # nothing found
        if (s_assembly == None) or (i_release == None):
            raise CommandError(
                "Error vocabulary_getupdate: {} species not found.".format(
                    s_organism
                )
            )
        # handle output
        s_version = "{}{}".format(i_release, s_assembly)
        s_load = "*.{}.{}.gtf.gz".format(s_assembly, i_release)
        s_url = "ftp://ftp.ensembl.org/pub/release-{}/gtf/{}/{}".format(
            i_release,
            s_organism,
            s_load
        )
        # wget ftp://ftp.ensembl.org/pub/release-78/gtf/homo_sapiens/Homo_sapiens.GRCh38.78.gtf.gz
        # download file if not already downloaded
        s_cwd = os.getcwd()
        os.chdir(s_pathquarantine)
        os.system("wget -c {}".format(s_url))
        # find file name
        ls_file = glob.glob(s_load)
        if not(len(ls_file) == 1):
            raise CommandError(
                "Error vocabulary_getupdate: more then one or non downloaded extracted ensembl gft file from the requested release found. Globed for {}.".format(
                    s_load
                )
            )
        s_file = ls_file[0]
        os.chdir(s_cwd)
        # get release date
        s_date = time.strftime(
            "%Y-%m-%d",
            time.gmtime(
                os.path.getmtime("{}{}".format(s_pathquarantine, s_file))
            )
        )
        # pack output
        d_latest["s_version"] = s_version
        d_latest["s_date"] = s_date
        d_latest["i_date"] = int(s_date.replace("-",""))
        d_latest["s_filedownload"] = s_file
        # return
        return(d_latest)


    def ensembl2get_file(
            self,
            s_filedownload,
            s_vocabulary,
            s_model,
            s_release,
            s_version,
            o_pkstringcase=str.lower,
            b_swaptermid=False
        ):
        """
        input:
            # rest
            s_filedownload: downloaded file name to give
            # build
            s_vocabulary: voci_source name (apponname without appon)
            s_model: annot appon* model
            s_release: release date in YYYY-MM-DD format
            s_version: version number
            o_pkstringcase: primary key string case. str.lower, str.upper and None.
            b_swaptermid: swap term and id?

        output:
            filedownload to /media/quarantine
            ontologyformat2json class call

        description:
            get_version already downloaded the file.
            call ontologyformat2json class
        """
        # unzip source file
        os.system("gunzip -kfv {}".format(s_filedownload))
        s_file = re.sub(".gz$", "", s_filedownload)
        # call ontologyformat2json
        o_trafo = ontologyformat2json()
        o_trafo.gtf2json(
            s_filedownload = s_file,
            s_vocabulary = s_vocabulary,
            s_model = s_model,
            s_release = s_release,
            s_version = s_version,
            o_pkstringcase = o_pkstringcase,
            b_swaptermid = b_swaptermid
        )
        # delete unziped s_filedownload
        os.remove(s_file)


    ## uniprot.org ##
    def __uniprotorg2get_httpresponse__(self, s_url, s_query, s_contact):
        """
        *internal method*

        input:
            s_url: basic uniprot api url
            s_query: url query string
            s_contact: email address

        source:
            http://www.uniprot.org/help/programmatic_access

        description:
            get data from uniprot side
        """
        # handle parameter
        d_params = {
            "query": s_query,
            "format": "tab",
            "columns": "organism,organism-id,id,genes,protein names,comment(ALTERNATIVE PRODUCTS)",
            # "include":,
            # "compress":,
            #"limit": 16,
            # "offset":,
        }
        s_params = urllib.parse.urlencode(d_params)
        # print(str(s_params))
        # build request
        o_request = urllib.request.Request("{}?{}".format(s_url, s_params))
        o_request.add_header("User-Agent", "Python/3.x ({})".format(s_contact))
        # print(o_request)
        # get response
        o_httpresponse = urllib.request.urlopen(o_request)
        # print(o_httpresponse)
        return(o_httpresponse)


    def uniprot2get_version(self, s_url, s_query, s_contact):
        """
        input:
            s_url: basic uniprot api url
            s_query: url query string
            s_contact: email address

        source:
            http://www.uniprot.org/help/programmatic_access

        description:
            extract proteom version form self._uniprotorg2get_httpresponse__
        """
        # empty output
        d_latest = {}
        # processing
        o_httpresponse = self.__uniprotorg2get_httpresponse__(
            s_url=s_url,
            s_query=s_query,
            s_contact=s_contact
        )
        ds_info = {}
        s_info = str(o_httpresponse.info())
        ls_info = s_info.split("\n")
        for s_info in ls_info:
            # print(s_info)
            ls_entry = s_info.split(": ")
            if (len(ls_entry) >= 2):
                ds_info.update({ls_entry[0]: ls_entry[1]})
        # fix version and date
        s_version = ds_info["X-UniProt-Release"]
        s_version = re.sub(r"[^a-zA-Z0-9]", "-", s_version)
        s_date = "{}-01".format(s_version)
        # pack output
        d_latest["s_version"] = s_version
        d_latest["s_date"] = s_date
        d_latest["i_date"] = int(s_date.replace("-", ""))
        # output
        return(d_latest)


    def uniprot2get_file(
            self,
            s_url,
            s_query,
            s_contact,
            s_filedownload,
            s_vocabulary,
            s_model,
            s_release,
            s_version,
            o_pkstringcase=str.lower,
            b_swaptermid=False
        ):
        """
        input:
            # rest
            s_filedownload: downloaded file name to give
            s_url: basic uniprot api url
            s_query: url query string
            s_contact: email address
            # build
            s_vocabulary: voci_source name (apponname without appon)
            s_model: annot appon* model
            s_release: release date in YYYY-MM-DD format
            s_version: version number
            o_pkstringcase: primary key string case. str.lower, str.upper and None.
            b_swaptermid: swap term and id?

        output:
            filedownload to /media/quarantine
            ontologyformat2json class call

        description:
            extract proteom content form self._uniprotorg2get_httpresponse__
        """
        # empty output
        lds_response = []
        # get content
        o_httpresponse = self.__uniprotorg2get_httpresponse__(
            s_url=s_url,
            s_query=s_query,
            s_contact=s_contact
        )
        s_page = o_httpresponse.read().decode("utf-8")
        # handle header
        ls_row = s_page.split("\n")
        s_header = ls_row.pop(0)
        ls_header = s_header.split("\t")
        # handle data rows
        for s_row in ls_row:
            ds_response = {}
            ls_row = s_row.split("\t")
            if (len(ls_row) >= len(ls_header)):
                i = 0
                for s_header in ls_header:
                    ds_response.update({s_header: ls_row[i]})
                    i += 1
                lds_response.append(ds_response)
        # save as jsonuniprot json file
        with open(s_filedownload, "w") as f_out:
            json.dump(lds_response, f_out, sort_keys=True)  # indent=4
        # call ontologyformat2json.jsonuniprot2json
        o_trafo = ontologyformat2json()
        o_trafo.jsonuniprot2json(
            s_filedownload=s_filedownload,
            s_vocabulary=s_vocabulary,
            s_model=s_model,
            s_release=s_release,
            s_version=s_version,
            o_pkstringcase=o_pkstringcase,
            b_swaptermid=b_swaptermid)
        # delete s_filedownload
        os.remove(s_filedownload)


### main ###
class Command(BaseCommand):
    help = "Check for vocabulary updates."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument(
            "appon_source",
            nargs='*',
            type=str,
            help="Annot ontology django apps <apponmmm_source apponmmm_source ...>"
        )

    def handle(self, *args, **options):
        # process vocabulary list
        for o_appon in argsvocabulary(options["appon_source"]):
            # extract vocabulary, model and source form app name
            s_appon = str(o_appon.app)
            s_vocabulary = s_appon.replace("appon", "")
            s_voci = s_vocabulary.split("_")[0]
            s_model = "{}.{}".format(s_appon, s_voci)
            s_source = s_vocabulary.split("_")[1]
            self.stdout.write(
                "\nProcessing vocabulary get update: {}".format(s_appon)
            )

            # get vocabulary object
            m_appon = importlib.import_module(s_appon)

            # get already downloaded vocabulary source files
            s_apponpath = "{}vocabulary/origin/{}_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_*origin.json".format(
                MEDIA_ROOT,
                s_vocabulary
            )
            ls_apponlocal = glob.glob(s_apponpath)
            s_apponlocal = annotfilelatest(ls_apponlocal, b_verbose=False)

            # if no vocabulary source file found
            if (s_apponlocal is None):
                # set local date and version
                s_localdate = "19551105"
                s_localversion = "nop"
                # generat seed file
                s_apponlocal = "{}vocabulary/origin/{}_{}_{}_origin.json".format(
                    MEDIA_ROOT,
                    s_vocabulary,
                    s_localdate,
                    s_localversion
                )
                open(s_apponlocal, "w").close()
            # if vocabulary source file found
            else:
                # extract local data and version
                s_localdate = (s_apponlocal.split("/")[-1]).split("_")[2]
                s_localversion = ((s_apponlocal.split("/")[-1]).split(".")[-2]).split("_")[3]
            # get integer date
            i_localdate = int(s_localdate)

            # final value
            # main message
            self.stdout.write(
                "Local {} ontology source files found: {} | {} | {}.".format(
                    s_appon,
                    s_vocabulary,
                    s_localdate,
                    s_localversion
                )
            )

            # check against latest online version, download if newer version is available
            ## own source ##
            if (s_source == "own"):
                self.stdout.write("{} is own source.\nNOP".format(s_appon))
                d_onlineversion = {}
                s_date = "{}-{}-{}".format(
                    s_localdate[0:4],
                    s_localdate[4:6],
                    s_localdate[6:8]
                )
                d_onlineversion.update({"s_version" : s_localversion})
                d_onlineversion.update({"s_date" : s_date})

            ## bioontology.org source ##
            elif (s_source == "bioontology"):
                # get online vocabulary version from http://data.bioontology.org
                o_bioontology = ontologysource2handle()
                s_url = "{}/ontologies/{}/latest_submission".format(
                    m_appon.models.REST_URL,
                    m_appon.models.TERM_SOURCE_NAME
                )
                d_onlineversion = o_bioontology.bioontology2get_version(
                    s_url=s_url,
                    s_apikey=crowbar.APIKEY_BIOONTOLOGY)
                if not (d_onlineversion["i_date"] > i_localdate):
                    self.stdout.write(
                        "Online ontology release found: {} | {} | {}.\nNOP".format(
                            s_vocabulary,
                            d_onlineversion["i_date"],
                            d_onlineversion["s_version"]
                        )
                    )
                else:
                    self.stdout.write(
                        "Download and handle latest online released: {} | {} | {}.".format(
                            s_vocabulary,
                            d_onlineversion["i_date"],
                            d_onlineversion["s_version"]
                        )
                    )
                    # download newer version
                    s_filedownload = "{}{}".format(
                        s_pathquarantine,
                        m_appon.models.REST_FILENAME
                    )
                    s_format = m_appon.models.REST_FORMAT
                    # jsonbioontology type dependent url
                    if (s_format == "http"):
                        s_url = "{}/ontologies/{}/classes/{}".format(
                            m_appon.models.REST_URL,
                            m_appon.models.TERM_SOURCE_NAME,
                            m_appon.models.CLASS_URL
                        )
                        b_getbranchcut = m_appon.models.GET_BRANCHCUT
                    # obo or umls
                    elif (s_format == "obo") or (s_format == "umls"):
                        s_url = "{}/ontologies/{}/download".format(
                            m_appon.models.REST_URL,
                            m_appon.models.TERM_SOURCE_NAME
                        )
                        b_getbranchcut=None
                    # error
                    else:
                        raise CommandError(
                            "Error vocabulary_getupdate: unknown vocabulary source format {}.".format(
                                m_appon.models.FORMAT
                            )
                        )
                    #run
                    o_bioontology.bioontology2get_file(
                        s_filedownload=s_filedownload,
                        s_format=s_format,
                        s_url=s_url,
                        s_apikey=crowbar.APIKEY_BIOONTOLOGY,
                        b_getbranchcut=b_getbranchcut,
                        s_vocabulary=s_vocabulary,
                        s_model=s_model,
                        s_release=d_onlineversion["s_date"],
                        s_version=d_onlineversion["s_version"],
                        o_pkstringcase=m_appon.models.PK_STRING_CASE,
                        b_swaptermid=m_appon.models.SWAP_TERMID
                    )

            ## cellosaurus source ##
            elif (s_source == "cellosaurus"):
                # get online version
                o_cellosaurus = ontologysource2handle()
                d_onlineversion = o_cellosaurus.ftp2get_version(
                    s_url=m_appon.models.REST_URL,
                    s_query=m_appon.models.REST_QUERY,
                    s_filedownload=m_appon.models.REST_FILENAME
                )
                if not (d_onlineversion["i_date"] > i_localdate):
                    d_onlineversion["s_version"] = s_localversion
                    self.stdout.write(
                        "Online ontology release found: {} | {} | {}.\nNOP".format(
                            s_vocabulary,
                            d_onlineversion["i_date"],
                            d_onlineversion["s_version"]
                        )
                    )
                else:
                    self.stdout.write(
                        "Download and handle latest online released: {} | {} | {}.".format(
                            s_vocabulary,
                            d_onlineversion["i_date"],
                            d_onlineversion["s_version"]
                        )
                    )
                    # download and transform newer version
                    d_onlineversion["s_version"] = o_cellosaurus.ftpobo2get_file(
                        s_url=m_appon.models.REST_URL,
                        s_query=m_appon.models.REST_QUERY,
                        s_filedownload="{}{}".format(
                            s_pathquarantine,
                            m_appon.models.REST_FILENAME
                        ),
                        #s_format=m_appon.models.REST_FORMAT,
                        s_vocabulary=s_vocabulary,
                        s_model=s_model,
                        s_release=d_onlineversion["s_date"],
                        s_version=None,
                        o_pkstringcase=m_appon.models.PK_STRING_CASE,
                        b_swaptermid=m_appon.models.SWAP_TERMID
                    )

            ## chebi source ##
            elif (s_source == "ebi"):
                # get online version
                o_chebi = ontologysource2handle()
                d_onlineversion = o_chebi.ftp2get_version(
                    s_url=m_appon.models.REST_URL,
                    s_query=m_appon.models.REST_QUERY,
                    s_filedownload=m_appon.models.REST_FILENAME)
                if not (d_onlineversion["i_date"] > i_localdate):
                    d_onlineversion["s_version"] = s_localversion
                    self.stdout.write(
                        "Online ontology release found: {} | {} | {}.\nNOP".format(
                            s_vocabulary,
                            d_onlineversion["i_date"],
                            d_onlineversion["s_version"]
                        )
                    )
                else:
                    self.stdout.write(
                        "Download and handle latest online released: {} | {} | {}.".format(
                            s_vocabulary,
                            d_onlineversion["i_date"],
                            d_onlineversion["s_version"]
                        )
                    )
                    # download and transform newer version
                    d_onlineversion["s_version"] = o_chebi.ftpobo2get_file(
                        s_url=m_appon.models.REST_URL,
                        s_query=m_appon.models.REST_QUERY,
                        s_filedownload="{}{}".format(
                            s_pathquarantine,

                            m_appon.models.REST_FILENAME
                        ),
                        #s_format=m_appon.models.REST_FORMAT,
                        s_vocabulary=s_vocabulary,
                        s_model=s_model,
                        s_release=d_onlineversion["s_date"],
                        s_version=None,
                        o_pkstringcase=m_appon.models.PK_STRING_CASE,
                        b_swaptermid=m_appon.models.SWAP_TERMID
                    )

            ## ensembl source ##
            elif (s_source == "ensembl"):
                # get online version
                o_ensembl = ontologysource2handle()
                d_onlineversion = o_ensembl.ensembl2get_version(
                    s_url=m_appon.models.REST_URL,
                    s_organism = m_appon.models.REST_ORGANISM)
                if not (d_onlineversion["i_date"] > i_localdate):
                    self.stdout.write(
                        "Online ontology release found: {} | {} | {}.\nNOP".format(
                            s_vocabulary,
                            d_onlineversion["i_date"],
                            d_onlineversion["s_version"]
                        )
                    )
                else:
                    self.stdout.write(
                        "Unpack and handle latest online released: {} | {} | {}.".format(
                            s_vocabulary,
                            d_onlineversion["i_date"],
                            d_onlineversion["s_version"]
                        )
                    )
                    # download newer version and transform to an! json
                    o_ensembl.ensembl2get_file(
                        s_filedownload="{}{}".format(
                            s_pathquarantine,
                            d_onlineversion["s_filedownload"]
                        ),
                        #s_format=m_appon.models.REST_FORMAT,
                        s_vocabulary=s_vocabulary,
                        s_model = s_model,
                        s_release=d_onlineversion["s_date"],
                        s_version=d_onlineversion["s_version"],
                        o_pkstringcase=m_appon.models.PK_STRING_CASE,
                        b_swaptermid=m_appon.models.SWAP_TERMID
                    )

            ## gene ontology source ##
            elif (s_source == "go"):
                # get online version
                o_go = ontologysource2handle()
                d_onlineversion = o_go.ftp2get_version(
                    s_url=m_appon.models.REST_URL,
                    s_query=m_appon.models.REST_QUERY,
                    s_filedownload=m_appon.models.REST_FILENAME)
                if not (d_onlineversion["i_date"] > i_localdate):
                    d_onlineversion["s_version"] = s_localversion
                    self.stdout.write(
                        "Online ontology release found: {} | {} | {}.\nNOP".format(
                            s_vocabulary,
                            d_onlineversion["i_date"],
                            d_onlineversion["s_version"]
                        )
                    )
                else:
                    self.stdout.write(
                        "Download and handle latest online released: {} | {} | {}.".format(
                            s_vocabulary,
                            d_onlineversion["i_date"],
                            d_onlineversion["s_version"]
                        )
                    )
                    # download and transform newer version
                    d_onlineversion["s_version"] = o_go.ftpobo2get_file(
                        s_url=m_appon.models.REST_URL,
                        s_query=m_appon.models.REST_QUERY,
                        s_filedownload="{}{}".format(
                            s_pathquarantine,
                            m_appon.models.REST_FILENAME
                        ),
                        #s_format=m_appon.models.REST_FORMAT,
                        s_vocabulary=s_vocabulary,
                        s_model=s_model,
                        s_release=d_onlineversion["s_date"],
                        s_version=None,
                        o_pkstringcase=m_appon.models.PK_STRING_CASE,
                        b_swaptermid=m_appon.models.SWAP_TERMID
                    )


            ## uniprot ##
            elif (s_source == "uniprot"):
                # get online version
                o_uniprot = ontologysource2handle()
                d_onlineversion = o_uniprot.uniprot2get_version(
                    s_url=m_appon.models.REST_URL,  # http://www.uniprot.org/uniprot/
                    s_query=m_appon.models.REST_QUERY, # "(organism:9606) AND (keyword:Reference proteome)"  # latest homo sapiens reference proteom
                    s_contact = crowbar.CONTACT)
                if not (d_onlineversion["i_date"] > i_localdate):
                    self.stdout.write(
                        "Online ontology release found: {} | {} | {}.\nNOP".format(
                            s_vocabulary,
                            d_onlineversion["i_date"],
                            d_onlineversion["s_version"]
                        )
                    )
                else:
                    self.stdout.write(
                        "Download and handle latest online released: {} | {} | {}.".format(
                            s_vocabulary,
                            d_onlineversion["i_date"],
                            d_onlineversion["s_version"]
                        )
                    )
                    # download and transform newer version
                    o_uniprot.uniprot2get_file(
                        s_filedownload = "{}{}".format(  # s_pathquarantine + "uniproteom_homosapines.csv"
                            s_pathquarantine,
                            m_appon.models.REST_FILENAME
                        ),
                        s_url=m_appon.models.REST_URL,
                        s_query=m_appon.models.REST_QUERY,
                        s_contact = crowbar.CONTACT,
                        #s_format = m_appon.models.REST_FORMAT,
                        s_vocabulary=s_vocabulary,
                        s_model=s_model,
                        s_release=d_onlineversion["s_date"],
                        s_version=d_onlineversion["s_version"],
                        o_pkstringcase=m_appon.models.PK_STRING_CASE,
                        b_swaptermid=m_appon.models.SWAP_TERMID
                    )

            ## unknown source ##
            else:
                raise CommandError(
                    "Error vocabulary_getupdate: unknown source {}. Adapt appsavocabulary/management/commands/vocabulary_getupdate.py.".format(
                        s_source
                    )
                )

            ## output appsavocabulary ##
            self.stdout.write("Update appsavocabulary status")
            obj_n = SysAdminVocabulary.objects.get(app=s_appon)
            # obj_n.app
            # obj_n.vocabulary
            obj_n.ontology = m_appon.models.TERM_SOURCE_NAME
            obj_n.url = m_appon.models.TERM_SOURCE_FILE
            # downloaded from the online source
            obj_n.version_latest = d_onlineversion["s_version"]
            i_year = int(d_onlineversion["s_date"][0:4])
            i_month = int(d_onlineversion["s_date"][5:7])
            i_day = int(d_onlineversion["s_date"][8:10])
            obj_n.version_latest_date = datetime.date(i_year, i_month, i_day)
            # uploaded into annot database
            # obj_n.version_loaded
            # obj_n.version_loaded_date
            obj_n.up_to_date = obj_n.version_loaded_date >= obj_n.version_latest_date
            obj_n.save()
