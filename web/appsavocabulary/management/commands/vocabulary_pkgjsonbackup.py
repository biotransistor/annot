# import frmom django
from django.core.management.base import BaseCommand  #,CommandError

# import from python
from datetime import datetime
import glob
import os
import shutil

# import from annot
from appsavocabulary.structure import argsvocabulary
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest

### main ###
class Command(BaseCommand):
    help = "Pack latest backuped vocabularies into YYYYMMDD_json_backup_latestvoci folder."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument(
            "appon_source",
            nargs='*',
            type=str, help="Annot ontology django apps <apponmmm_source apponmmm_source ...>"
        )

    def handle(self, *args, **options):
        ### generate latest out folder path ###
        s_outputpath = "{}vocabulary/{}_json_backup_latestvoci".format(
            MEDIA_ROOT,
            datetime.now().strftime("%Y%m%d")
        )

        ### remove _own_*_None_backup.json ###
        # bue 20181023: hack beause this files can have a newer date then the real most complete origin
        os.system("rm {}vocabulary/backup/*_own_*_None_backup.json".format(MEDIA_ROOT))

        ### process each record  ###
        for o_queryset in argsvocabulary(options["appon_source"]):
            self.stdout.write(
                "\nProcessing vocabulary pkg json backup: {}".format(
                    o_queryset.vocabulary
                )
            )

            ### get latest fs json ###
            # bue 20160226: internal function should be put as methods into the Command class.
            s_latesregex = "{}vocabulary/backup/{}_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_*_backup.json".format(
                MEDIA_ROOT,
                o_queryset.vocabulary
            )
            ls_annotfile = glob.glob(s_latesregex)
            s_latestpath = annotfilelatest(
                ls_annotfile=ls_annotfile,
                b_verbose=False
            )
            if not(s_latestpath is None):
                ## make output path ##
                if (options['verbosity'] > 0):
                    self.stdout.write(
                        "Copy {} to {}.".format(s_latestpath, s_outputpath)
                    )
                if not(os.path.exists(s_outputpath)):
                    os.mkdir(s_outputpath)
                ## copy in to output path ##
                shutil.copy(s_latestpath, s_outputpath, follow_symlinks=False)
