# import form django
from django.core import management
from django.core.management.base import BaseCommand, CommandError

# import python
import datetime
import glob
import importlib
import inspect
import json
import re

# import annot
from appsavocabulary.models import SysAdminVocabulary
from appsavocabulary.structure import argsvocabulary
from prjannot.settings import MEDIA_ROOT


class Command(BaseCommand):
    help = "Calls vocabulary_getupdate. Then loads latest origin vocabularies into into annot."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument(
            "appon_source",
            nargs='*',
            type=str,
            help="Annot ontology django apps <apponmmm_source apponmmm_source ...>"
        )

    def handle(self, *args, **options):
        # process vocabulary app
        for o_appon in argsvocabulary(options["appon_source"]):

            if ("_own" in str(o_appon.app)):
                # python manage.py vocabulary_backup2origin o_appon.app
                management.call_command(
                    "vocabulary_backup2origin",
                    o_appon.app
                )
            else:
                # python manage.py vocabulary_getupdate o_appon.app
                try:
                    management.call_command(
                        "vocabulary_getupdate",
                        o_appon.app
                    )
                except:
                    self.stdout.write(
                        "\nError @ vocabulary_loadupdate : annot could not check against the latest {} online vocabulary version because of what ever reason. preceding with the latest locally available origin file.".format(o_appon.app)
                    )

            # off we go
            self.stdout.write(
                "\nProcessing vocabulary load update: {}".format(o_appon.app)
            )

            # load latest database version
            try:
                m_appon = importlib.import_module(
                    "{}.models".format(o_appon.app)
                )
                for s_name, obj in inspect.getmembers(m_appon):
                    if inspect.isclass(obj):
                        s_apponmodels = s_name
                        c_apponmodels = obj  # equivalent to json model
                        lo_record = c_apponmodels.objects.all()
                        if (options['verbosity'] > 0):
                            self.stdout.write(
                                "Load database vocabulary: {}".format(
                                    s_apponmodels
                                )
                            )

            except ImportError:
                raise CommandError(
                    "Annot vocabulary app {} does not exist.".format(
                        o_appon.app
                    )
                )

            # load related files
            obj_n = SysAdminVocabulary.objects.get(app=o_appon.app)
            s_vocabulary = obj_n.vocabulary

            # load term filter if existent
            ls_fterm = []
            s_ffterm = "{}/filter_{}_term.txt".format(o_appon.app, s_vocabulary)
            try:
                f_fterm = open(s_ffterm, 'r')  # open file handle
                if (options['verbosity'] > 0):
                    self.stdout.write(
                        "Load term filter file: ".format(s_ffterm))
                for s_line in f_fterm:
                    s_term = s_line.split("#")[0].strip()
                    if (len(s_term) > 0):
                        ls_fterm.append(s_term)
                f_fterm.close()
            except:
                if (options['verbosity'] > 0):
                    self.stdout.write(
                        "No term filter file found: {}".format(s_ffterm))

            # load identifier filter if existent
            ls_ffier = []
            s_fffier = "{}/filter_{}_identifier.txt".format(
                o_appon.app,
                s_vocabulary
            )
            try:
                with open(s_fffier, 'r')  as f_ffier: # open file handle
                    if (options['verbosity'] > 0):
                        self.stdout.write(
                            "Load identifier filter file: {}".format(s_fffier)
                        )
                    for s_line in f_ffier:
                        s_fier = s_line.split("#")[0].strip()
                        if (len(s_fier) > 0):
                            ls_ffier.append(s_fier)
            except:
                if (options['verbosity'] > 0):
                    self.stdout.write(
                        "No identifier filter file found: {}".format(s_fffier)
                    )

            # load latest json file version
            # bue 20160226: internal function should be put as methods into the Command class.
            i_termcount = 0
            s_version_latest = obj_n.version_latest
            o_version_latest_date = obj_n.version_latest_date
            s_version_latest_date = str(o_version_latest_date)
            s_version_latest_date = s_version_latest_date.replace('-', '')
            s_filefixture = "{}vocabulary/origin/{}_{}_{}_origin.json".format(
                MEDIA_ROOT,
                s_vocabulary,
                s_version_latest_date,
                s_version_latest
            )
            if (options['verbosity'] > 0):
                self.stdout.write(
                    "Load latest json file version: {}".format(s_filefixture)
                )
            try:
                # open file handle
                f_fixture = open(s_filefixture, 'r')  # open file handle
                # update
                ld_json = json.load(f_fixture)
                i_termcount = len(ld_json)
                # pack list of dictionary into dictionary of dictionary where the term_name is the key
                dd_json = {}
                for d_json in ld_json:
                    s_term_file = d_json["fields"]["term_name"]
                    dd_json.update({s_term_file: d_json})
                # special case protein identifier
                if (o_appon.app == "apponprotein_uniprot"):
                    # update simple uniprot identifier to isoform specific uniprot identifiers
                    if (len(ls_fterm) > 0):
                        ls_fterm_explicite = []
                        ls_fterm_all = list(dd_json.keys())
                        for s_fterm_explicite in ls_fterm_all:
                            for s_fterm in ls_fterm:
                                if re.search(s_fterm, s_fterm_explicite) :
                                    ls_fterm_explicite.append(s_fterm_explicite)
                        ls_fterm = ls_fterm_explicite
                        if (options['verbosity'] > 0):
                            self.stdout.write(
                                "UniProt explicite: {}".format(ls_fterm)
                            )
                # filter empty
                if ((len(ls_fterm) == 0) and (len(ls_ffier) == 0)):
                    ls_fterm = list(dd_json.keys())

                # loop through loaded database version
                if (options['verbosity'] > 0):
                    self.stdout.write("Update database version term status.")
                for o_record in lo_record:
                    s_term_record = o_record.term_name
                    # check every vocabulary term if it is a member of the latest json file version
                    try:
                        d_json = dd_json[s_term_record]
                        # o_record.term_name
                        o_record.term_id = d_json["fields"]["term_id"]
                        # o_record.annot_id is equivalent to json pk or pwn
                        o_record.term_source_version_responsible = "term_from_origin_source"
                        s_date = d_json["fields"]["term_source_version_update"]
                        ls_date = s_date.split('-')
                        i_year = int(ls_date[0])
                        i_month = int(ls_date[1])
                        i_day = int(ls_date[2])
                        o_record.term_source_version_update = datetime.date(i_year, i_month, i_day)
                        o_record.term_source_version = d_json["fields"]["term_source_version"]
                        o_record.term_ok = True  # set status
                        o_record.save()
                    except KeyError:
                        o_record.term_ok = False  # reset status
                        o_record.save()

                # loop through json file version
                if (options['verbosity'] > 0):
                    self.stdout.write(
                        "Load latest json file version into the database.")
                for d_json in ld_json:
                    s_term_name = d_json["fields"]["term_name"]
                    s_term_id = d_json["fields"]["term_id"]
                    # print("prefilter name and identifier", s_term_name, s_term_id)
                    # check if json file entry term passes the filters
                    if ((s_term_name in ls_fterm) or (s_term_id in ls_ffier)):
                        try:
                            # check if json file entry term already in database
                            c_apponmodels.objects.get(term_name=s_term_name)
                        except c_apponmodels.DoesNotExist:
                            # write missing json file entry into database
                            s_date = d_json["fields"]["term_source_version_update"]
                            ls_date = s_date.split('-')
                            i_year = int(ls_date[0])
                            i_month = int(ls_date[1])
                            i_day = int(ls_date[2])
                            o_newrecord = c_apponmodels(
                                term_name=s_term_name,
                                term_id=s_term_id,
                                annot_id=d_json["pk"],
                                term_source_version_responsible="term_from_origin_source",
                                term_source_version_update=datetime.date(i_year, i_month, i_day),
                                term_source_version=d_json["fields"]["term_source_version"],
                                term_ok=True
                            )
                            o_newrecord.save()

            except FileNotFoundError:
                # non existing vocabulary file
                self.stdout.write(
                    "Warning: ontology file is non existent. This can happen if this is a completely new own vocabulary with yet no a single terms defined."
                )
            except ValueError:
                # empty vocabulary file
                self.stdout.write(
                    "Warning: ontology file is empty. This can happen if this is a completely new own vocabulary with yet no a single terms defined."
                )

            # load not_yet_specified and not_specified term
            if (options['verbosity'] > 0):
                self.stdout.write(
                    "Load not_yet_specified and not_specified term."
                )
            try:
                # check if json file entry term already in database
                c_apponmodels.objects.get(term_name="not available")
            except c_apponmodels.DoesNotExist:
                # write missing json file entry into database
                o_newrecord = c_apponmodels(
                    term_name="not available",
                    term_id="not_available",
                    annot_id="not_available",
                    term_source_version_responsible="annot_basic_term"
                )
                o_newrecord.save()
            try:
                # check if json file entry term already in database
                c_apponmodels.objects.get(term_name="not yet specified")
            except c_apponmodels.DoesNotExist:
                # write missing json file entry into database
                o_newrecord = c_apponmodels(
                    term_name="not yet specified",
                    term_id="not_yet_specified",
                    annot_id="not_yet_specified",
                    term_source_version_responsible="annot_basic_term"
                )
                o_newrecord.save()

            # update vocabulary status
            if (options['verbosity'] > 0):
                self.stdout.write("Update vocabulary status")
            obj_n = SysAdminVocabulary.objects.get(app=o_appon.app)
            # obj_n.app
            # obj_n.vocabulary
            # obj_n.ontology
            # obj_n.url
            # obj_n.version_latest  # download from the online source
            # obj_n.version_latest_date  # download from the online source
            obj_n.version_loaded = s_version_latest  # upload into annot database
            obj_n.version_loaded_date = o_version_latest_date  # upload into annot database
            obj_n.origin_term_count = i_termcount
            obj_n.up_to_date = obj_n.version_loaded_date >= obj_n.version_latest_date  # upload into annot database
            obj_n.save()
