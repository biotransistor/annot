# import from django
from django.core import management
from django.core.management.base import BaseCommand  #,CommandError

# import from python
import glob
import os
import re
import sys
import json

# import annot
from appsavocabulary.models import SysAdminVocabulary
from appsavocabulary.structure import argsvocabulary, appendrecordvocabulary
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest


class Command(BaseCommand):
    help = "Transform latest controlled vocabulary backup into an \
           origin vocabulary version. This code will work only on own \
           controlled vocabulary."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument(
            "appon_source",
            nargs='*',
            type=str,
            help="Annot ontology django apps <apponmmm_source apponmmm_source ...>"
        )

    def handle(self, *args, **options):

        ### remove _own_*_None_backup.json and  _own_*_None_origin.json ###
        # bue 20181023: hack beause this files can have a newer date then the real most complete origin
        os.system("rm {}vocabulary/backup/*_own_*_None_backup.json".format(MEDIA_ROOT))
        os.system("rm {}vocabulary/origin/*_own_*_None_origin.json".format(MEDIA_ROOT))

        # process vocabulary app
        for o_appon in argsvocabulary(options["appon_source"]):
            if (options['verbosity'] > 0):
                self.stdout.write(
                    "\nProcess vocabulary backup2origin: {}...".format(
                        o_appon.app
                    )
                )

            # initiate
            b_flag = False

            # own vocabulary
            if (re.search(r"_own$", o_appon.app) != None):

                # find latest json file backup version
                obj_n = SysAdminVocabulary.objects.get(app=o_appon.app)
                s_vocabulary = obj_n.vocabulary
                s_regexfixture = (
                    "{}vocabulary/backup/{}_*_backup.json".format(
                        MEDIA_ROOT,
                        s_vocabulary
                    )
                )
                ls_backuppathfile = glob.glob(s_regexfixture)
                s_backuplatest = annotfilelatest(ls_backuppathfile)

                # if backup found
                if not (s_backuplatest is None):

                    # load latest backup
                    with open(s_backuplatest, 'r') as f_backup:
                        ld_backup = json.load(f_backup)
                    # no need for : term_source_version_update, term_source_version
                    [d_backup["fields"].pop("term_source_version_update") for d_backup in ld_backup]
                    [d_backup["fields"].pop("term_source_version") for d_backup in ld_backup]

                    # find latest json file origin version
                    s_originlatest = None
                    s_regexfixture = (
                        "{}vocabulary/origin/{}*_origin.json".format(
                            MEDIA_ROOT,
                            s_vocabulary
                        )
                    )
                    ls_originpathfile = glob.glob(s_regexfixture)
                    s_originlatest = annotfilelatest(ls_originpathfile)

                    # if origin found
                    if not ((s_originlatest is None) \
                            or re.match(
                                "(.*)_own_19551105_nop_origin.json$",
                                s_originlatest
                            )
                        ):
                        # load latest origin
                        with open(s_originlatest, 'r') as f_origin:
                            ld_origin = json.load(f_origin)
                        # no need for : term_source_version_update, term_source_version
                        [d_origin["fields"].pop("term_source_version_update") for d_origin in ld_origin]
                        [d_origin["fields"].pop("term_source_version") for d_origin in ld_origin]

                        # compare latest backup with latest origin
                        if (options['verbosity'] > 0):
                            self.stdout.write(
                                "Compare latest json file backup version to latest json file origin version"
                            )

                        # go through backup, check if everything is in origin
                        if not (b_flag):
                            for d_backup in ld_backup:
                                if not (d_backup in ld_origin):
                                    b_flag = True
                                    if (options['verbosity'] > 0):
                                        self.stdout.write(
                                            "Conclusion: {} found in backup but not in origin.".format(
                                                d_backup
                                            )
                                        )
                                    break

                        # go through origin, check if everything is in backup
                        if not (b_flag):
                            for d_origin in ld_origin:
                                if not (d_origin in ld_backup):
                                    b_falg = True
                                    if (options['verbosity'] > 0):
                                        self.stdout.write(
                                            "Conclusion: {} found in origin but not in backup: {}".format(
                                                d_origin
                                            )
                                        )
                                    break


                        # latest backup same as latest origin
                        if not (b_flag):
                            # main message
                            self.stdout.write(
                                "Conclusion: {} no operation. Latest backup file same as latest origin file.".format(
                                    o_appon.app
                                )
                            )

                    # backup file found but no origin file found
                    else:
                        b_flag = True
                        # main message
                        self.stdout.write(
                            "Conclusion: {} found just backup file but no origin file.".format(
                                o_appon.app
                            )
                        )

                # if no backup found
                else:
                    # main message
                    self.stdout.write(
                        "Conclusion: {} no operation. No backup file found.".format(
                            o_appon.app
                        )
                    )

            # not own vocabulary
            else:
                # main message
                self.stdout.write(
                    "Conclusion: {} no operation. Only a own vocabulary can be transformed from a backup version to an origin release.".format(
                        o_appon.app
                    )
                )

            # generate file
            if (b_flag):
                if (options['verbosity'] > 0):
                    self.stdout.write("Generate origin release file.")

                # generate origin release path file name
                s_ifile = s_backuplatest.split('/')[-1]
                s_iversion = s_ifile.split('_')[-2]
                s_oversion = s_ifile.split('_')[-3]
                s_odate = "{}-{}-{}".format(
                    s_oversion[0:4],
                    s_oversion[4:6],
                    s_oversion[6:8]
                )
                s_ofile = s_ifile.replace(
                    "{}_backup".format(s_iversion),
                    "{}_origin".format(s_oversion)
                )
                s_opathfile = "{}vocabulary/origin/{}".format(
                    MEDIA_ROOT,
                    s_ofile
                )

                # read input
                # input is latest backup file content

                # populate output
                ld_out = []
                for d_in in ld_backup:
                    appendrecordvocabulary(
                        ld_json=ld_out,
                        s_model=d_in["model"],
                        s_term=d_in["fields"]["term_name"],
                        s_id=d_in["pk"],
                        s_pk=d_in["pk"],
                        s_responsible="term_from_origin_source",
                        s_release=s_odate,
                        s_version=s_oversion,
                        s_termok=True
                    )
                # write output
                with open(s_opathfile, 'w') as f_out:
                    json.dump(ld_out, f_out, sort_keys=True)  # indent=4
                f_out.close()
                # main message
                self.stdout.write("Written {}".format(s_opathfile))

                # load new original and backup
                # bue 20180505: this is not working, becasue it calles it self recursively
                #management.call_command("vocabulary_loadupdate", o_appon.app)
                #management.call_command("vocabulary_backup", o_appon.app)
