# import form django
from django.core import management
from django.core.management.base import BaseCommand, CommandError
# import from python
import datetime
import glob
import importlib
import inspect
import json
import os
# import from annot
from appsavocabulary.models import SysAdminVocabulary
from appsavocabulary.structure import argsvocabulary
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest


class Command(BaseCommand):
    help = "Calls vocabulary_getupdate, vocabulary_loadupdate, then backups controlled vocabulary."

    def add_arguments(self, parser):
        # --verbosity argument is default management command option
        # positional arguments
        parser.add_argument(
            "appon_source",
            nargs='*',
            type=str,
            help="Annot ontology django apps <apponmmm_source apponmmm_source ...>"
        )

    def handle(self, *args, **options):
        # initiate
        b_flag = False

        # process vocabulary app
        for o_appon in argsvocabulary(options["appon_source"]):

            # python manage.py vocabulary_getupdate
            management.call_command("vocabulary_loadupdate", o_appon.app)

            # begin backup handle
            if (options['verbosity'] > 0):
                self.stdout.write(
                    "\nProcessing vocabulary backup: {}".format(o_appon.app)
                )

            # load latest database version
            try:
                m_appon = importlib.import_module(
                    "{}.models".format(o_appon.app)
                )
                for s_apponmodels, c_apponmodels in inspect.getmembers(m_appon):
                    if inspect.isclass(c_apponmodels):
                        lo_record = c_apponmodels.objects.all()
                        if (options['verbosity'] > 0):
                            self.stdout.write(
                                "Load database version: {}".format(
                                    s_apponmodels
                                )
                            )
                            break

            except ImportError:
                raise CommandError(
                    "Annot vocabulary app {} does not exist.".format(
                        o_appon.app
                    )
                )

            ### remove _own_*_None_backup.json ###
            # bue 20181023: hack beause this files can have a newer date then the real most complete origin
            os.system("rm {}vocabulary/backup/*_own_*_None_backup.json".format(MEDIA_ROOT))


            # find latest json file backup version
            obj_n = SysAdminVocabulary.objects.get(app=o_appon.app)
            ls_backuppathfile = glob.glob(
                "{}vocabulary/backup/{}_*_{}_backup.json".format(
                    MEDIA_ROOT,
                    obj_n.vocabulary,
                    obj_n.version_latest
                )
            )
            s_backuplatest = annotfilelatest(ls_backuppathfile)

            # backup file found?
            if (s_backuplatest is None):
                b_flag = True
                if (options['verbosity'] > 0):
                    self.stdout.write("No earlier backup.")

            if not (b_flag):
                # compare database content to latest back up content
                if (options['verbosity'] > 0):
                    self.stdout.write(
                        "Compare database content to latest backup file content ..."
                    )

                # load latest json file backup version
                if (options['verbosity'] > 0):
                    self.stdout.write(
                        "Load file: {}".format(s_backuplatest)
                    )
                with open(s_backuplatest, 'r')  as f_backuplatest: # file handle
                    ld_json = json.load(f_backuplatest)
                [d_json.pop("model") for d_json in ld_json]
                [d_json["fields"].pop("term_source_version_update") for d_json in ld_json]

                # check every database version term
                # if term is member of latest json file version
                for o_record in lo_record:
                    d_record = {}
                    #d_record.update({"model": }) popped!
                    d_record.update({"pk": o_record.annot_id})
                    d_record.update({"fields": {}})
                    d_record["fields"].update({"term_name": o_record.term_name})
                    d_record["fields"].update({"term_id": o_record.term_id})
                    d_record["fields"].update({"term_source_version_responsible": o_record.term_source_version_responsible})
                    #d_record["fields"].update({"term_source_version_update": })  popped!
                    d_record["fields"].update({"term_source_version": o_record.term_source_version})
                    d_record["fields"].update({"term_ok": o_record.term_ok})

                    if not (d_record in ld_json):
                        b_flag = True  # some records not found
                        if (options['verbosity'] > 0):
                            self.stdout.write(
                                "Database record missing in backup. will break! {}".format(
                                    d_record
                                )
                            )
                        break

            if not (b_flag):
                # check every latest json file version term
                # if term is member of the database version
                for d_json in ld_json:
                    try:
                        o_record = c_apponmodels.objects.get(
                            annot_id=d_json["pk"]
                        )
                    except AttributeError:
                        b_flag = True
                        if (options['verbosity'] > 0):
                            self.stdout.write(
                                "Backup term  missing in database. will break! {}".format(
                                    d_json["pk"]
                                )
                            )
                        break

            # main messages
            self.stdout.write(
                "Conclusion: {} backup needed? {}.".format(o_appon.app, b_flag)
            )

            # generate backup file
            if (b_flag):
                # generate future backup path file name
                s_today = str(datetime.date.today())
                s_today = s_today.replace("-", "")
                s_backuptoday ="{}vocabulary/backup/{}_{}_{}_backup.json".format(
                    MEDIA_ROOT,
                    obj_n.vocabulary,
                    s_today,
                    obj_n.version_latest
                )
                # python manage.py dumpdata s_backuptoday
                self.stdout.write(
                    "Generate backup file: {}".format(s_backuptoday)
                )
                f_stream = open(s_backuptoday, 'w')  # encoding="utf-8"
                management.call_command(
                    "dumpdata",
                    o_appon.app,
                    format='json',
                    stdout=f_stream
                )  # indent=4
                f_stream.close()
            # or don't generate backup file
            else:
                # main message
                self.stdout.write(
                    "Latest backup version is equivalent to the database version. Backup skipped."
                )
