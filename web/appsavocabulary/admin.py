# import form django
from django.contrib import admin
from django.contrib import messages
from django.http import HttpResponseRedirect
#from django.http import HttpResponse

# python
import os

# import from annot
from appsavocabulary.models import SysAdminVocabulary


# Register your models here.
#admin.site.register(SysAdminVocabulary)
class SysAdminVocabularyAdmin(admin.ModelAdmin):
    # view
    search_fields = ("app","vocabulary","ontology","url")
    list_display = (
        "app","vocabulary","ontology","url",
        "version_latest","version_latest_date",
        "version_loaded","version_loaded_date",
        "origin_term_count","up_to_date"
    )
    list_display_links = ("vocabulary",)
    readonly_fields = (
        "app","vocabulary","ontology","url",
        "version_latest","version_latest_date",
        "version_loaded","version_loaded_date",
        "origin_term_count","up_to_date"
    )
    actions = [
        "download_pipeelement_json",
        "download_origin_json",
        "download_backup_json",
        "download_pipeelement_tsv",
        "download_origin_tsv",
        "download_backup_tsv",
        "cron_start",
        "cron_stop",
        "cron_status",
    ]

    ### json ###
    # action download pipe element json
    def download_pipeelement_json(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=generic&choice=appsavocabulary.sysadminvocabulary"
        ))
    download_pipeelement_json.short_description = "Download this page as json file (item selection irrelevant)"

    # action download origin vocabulary json
    def download_origin_json(self,  request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=vocabulary.origin&choice={}".format(
                "!".join(ls_choice)
            )
        ))
    download_origin_json.short_description = "Download original vocabulary as json file"

    # action download backup vocabulary json
    def download_backup_json(self,  request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=json&layer=vocabulary.backup&choice={}".format(            
                "!".join(ls_choice))
        ))
    download_backup_json.short_description = "Download backup vocabulary as json file"

    ### tsv ###
    # action download pipe element tsv
    def download_pipeelement_tsv(self, request, queryset):
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=generic&choice=appsavocabulary.sysadminvocabulary"
        ))
    download_pipeelement_tsv.short_description = "Download this page as tsv file (item selection irrelevant)"

    # action download origin vocabulary tsv
    def download_origin_tsv(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=vocabulary.origin&choice={}".format(
                "!".join(ls_choice))
        ))
    download_origin_tsv.short_description = "Download original vocabulary as tsv file"

    # action download backup vocabulary tsv
    def download_backup_tsv(self, request, queryset):
        ls_choice = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return(HttpResponseRedirect(
            "/appsavocabulary/export?filetype=tsv&layer=vocabulary.backup&choice={}".format(
                "!".join(ls_choice))
        ))
    download_backup_tsv.short_description = "Download backup vocabulary as tsv file"

    ### cron ###
    def cron_start(self, request, queryset):  # self = modeladmin
        s_out = os.popen("/etc/init.d/cron start").read().strip()
        self.message_user(
            request,
            s_out,
            level=messages.INFO
        )
    cron_start.short_description = "/etc/init.d/cron start (item selection irrelevant)"

    def cron_stop(self, request, queryset):  # self = modeladmin
        s_out = os.popen("/etc/init.d/cron stop").read().strip()
        self.message_user(
            request,
            s_out,
            level=messages.INFO
        )
    cron_stop.short_description = "/etc/init.d/cron stop (item selection irrelevant)"

    def cron_status(self, request, queryset):  # self = modeladmin
        s_out = os.popen("/etc/init.d/cron status").read().strip()
        if s_out.endswith("running."):
            self.message_user(
                request,
                s_out,
                level=messages.SUCCESS
            )
        elif s_out.endswith("failed!"):
            self.message_user(
                request,
                s_out,
                level=messages.ERROR
            )
        else:
            self.message_user(
                request,
                s_out,
                level= messages.WARNING  # messages.INFO
            )
    cron_status.short_description = "/etc/initd/cron status (item selection irrelevant)"

# register
admin.site.register(SysAdminVocabulary, SysAdminVocabularyAdmin)
