# annot project vocabulary specific functions

# import djnago
from django.core.management.base import CommandError
# import python
import glob
import os
# import annot
from appsavocabulary.models import SysAdminVocabulary
from prjannot.settings import BASE_DIR


def argsvocabulary(o_args):
    """
    description:
        internal function called by vocabulary management commands.
        get vocabulary django apps from the filesystem
        and populates the app SysAdminVocabulary model.
    """
    # set variables
    lo_queryset = []

    # get appvocabulary file system
    os.chdir(BASE_DIR)
    ls_appon_fs = glob.glob("appon*")

    # get appvocabulary obj
    ls_appon_obj = []
    queryset = SysAdminVocabulary.objects.all()  # get queryset
    ls_appon_obj = [str(o_appon.app) for o_appon in queryset]

    # treat argument specified vocabulary
    if (len(o_args) > 0):

        # populate querysetlist
        for obj_n in o_args:
            # obj_n is either obj or str
            if (type(obj_n) == str):
                s_obj_n = obj_n
            else:
                s_obj_n = str(obj_n.app)

            # check against file system
            if not(s_obj_n in ls_appon_fs):
                raise CommandError(
                    "Voci {} not found on file system.".format(s_obj_n)
                )

            # check against objs
            if not(s_obj_n in ls_appon_obj):
                # populate
                s_vocabulary = s_obj_n.replace("appon", "")
                obj_i = SysAdminVocabulary(
                    app=s_obj_n,
                    vocabulary=s_vocabulary
                )
                obj_i.save()

            # get complete list of queryset obj
            o_queryset = SysAdminVocabulary.objects.get(app=s_obj_n)
            lo_queryset.append(o_queryset)

    # treat all vocabularies
    else:
        # loop through file system found appon
        for s_appon_fs in ls_appon_fs:
            # check if file system found appon in obj table
            if not(s_appon_fs in ls_appon_obj):
                # populate
                s_vocabulary = s_appon_fs.replace("appon", "")
                obj_i = SysAdminVocabulary(
                    app=s_appon_fs,
                    vocabulary=s_vocabulary
                )
                obj_i.save()

        # loop through appon obj in table
        for s_appon_obj in ls_appon_obj:
            # check if found obj on file system
            if not(s_appon_obj in ls_appon_fs):
                # delete obj out of table
                obj_i = SysAdminVocabulary.objects.get(app=s_appon_obj)
                obj_i.delete()

        # get complete list of queryset obj
        queryset = SysAdminVocabulary.objects.all()
        lo_queryset = list(queryset)

    # output
    return(lo_queryset)


def appendrecordvocabulary(
        ld_json,
        s_model,
        s_term,
        s_id,
        s_pk,
        s_responsible,
        s_release,
        s_version,
        s_termok,
        b_swaptermid=False
    ):
    """
    description:
        internal function called by vocabulary management commands.
        appends record to the specific vocabulary model related json object.
    """
    # set variables
    d_record = {}
    d_fields = {}
    # field
    if (b_swaptermid):
        d_fields.update({"term_name": s_id})
        d_fields.update({"term_id": s_term})
    else:
        d_fields.update({"term_name": s_term})
        d_fields.update({"term_id": s_id})
    d_fields.update({"term_source_version_responsible": s_responsible})
    d_fields.update({"term_source_version_update": s_release})
    d_fields.update({"term_source_version": s_version})
    d_fields.update({"term_ok": s_termok})
    # record
    d_record.update({"fields": d_fields})
    d_record.update({"pk": s_pk})
    d_record.update({"model": s_model})
    # output
    ld_json.append(d_record)
    #return(ld_json)
