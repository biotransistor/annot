from django.apps import AppConfig


class AppsavocabularyConfig(AppConfig):
    name = 'appsavocabulary'
