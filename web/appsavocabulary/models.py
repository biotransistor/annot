from django.db import models

# Create your models here.
class SysAdminVocabulary(models.Model):
    app = models.SlugField(
        primary_key=True
    )
    vocabulary = models.SlugField(
        unique=True
    )
    ontology = models.SlugField()
    url = models.URLField()
    version_latest = models.CharField(
        max_length=255,
        null=True,
        default=None
    )
    version_latest_date = models.DateField(
        default="1955-11-05"
    )
    version_loaded = models.CharField(
        max_length=255,
        null=True,
        default=None
    )
    version_loaded_date = models.DateField(
        default="1955-11-05"
    )
    origin_term_count = models.PositiveIntegerField(
        default=None,
        null=True,
        blank=True
    )
    up_to_date = models.NullBooleanField(
        default=None
    )
    def __str__(self):
       return(self.app)

    class Meta:
        verbose_name_plural = "sys admin ctrl vocabularies"
        ordering = ["app"]
