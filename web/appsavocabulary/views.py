## appsabrick ##
# import for django
from django.shortcuts import render
from django.http import HttpResponse
from django.core import management
from django.contrib import messages

# import from python
import csv
from datetime import datetime
import glob
import io
import json
import os
import re
import zipfile

# import from annot
from prjannot.settings import MEDIA_ROOT
from prjannot.structure import annotfilelatest, dumpdatajson2tsvhuman, dumpdatajson2jsonhuman

# Create your views here.
# index
def index(request):
    """ hello world """
    return HttpResponse("Hello, world. You're at the appsavocabulary index.")

# export
def export(request):
    """
    description:
        make vocabulary layer and above downloadable in json and tsv file format
    """
    # extract info from get url request call
    s_filetype = request.GET.get("filetype")
    s_layer = request.GET.get("layer")
    s_choice = request.GET.get("choice")

    # set zip file variable
    s_pathzip = "{}quarantine/".format(MEDIA_ROOT)
    s_filezip = "{}_annot_{}{}_download.zip".format(
        s_filetype,
        datetime.now().strftime("%Y%m%d"),
        datetime.now().strftime("_%H%M%S"),
    )
    s_pathfilezip = "{}{}".format(s_pathzip, s_filezip)

    # open zip file handle
    with zipfile.ZipFile(s_pathfilezip, "w") as zipper:

        #################
        ## brick layer ##
        #################
        if (s_layer == "brick"):
            # get stdout
            o_out = io.StringIO()

            # for each vocabulary
            ls_choice = s_choice.split("!")
            for s_bricktype in ls_choice:

                # all bricks but appbrxperson
                if (s_bricktype != "person"):
                    # s_managepycommand
                    if (s_filetype == "tsv"):
                        s_fileext = "txt"
                        s_fileclass = "human"
                        s_managepycommand = "{}_db2tsv".format(s_bricktype)
                    else:
                        s_filetype = "json"
                        s_fileext = "json"
                        s_fileclass = "oo"
                        s_managepycommand = "{}_db2json".format(s_bricktype)

                    # produce latest version by python manage.py command
                    management.call_command(
                        s_managepycommand,
                        stdout=o_out,
                        verbosity=0
                    )

                    # get latest file version
                    s_pathoutput = "{}brick/{}/".format(MEDIA_ROOT, s_fileclass)
                    s_annotfileregex = "{}{}_brick_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]_{}.{}".format(
                        s_pathoutput,
                        s_bricktype,
                        s_fileclass,
                        s_fileext
                    )
                    ls_annotfile = glob.glob(s_annotfileregex)
                    s_pathfileoutput = annotfilelatest(ls_annotfile)
                    s_fileoutput = s_pathfileoutput.replace(s_pathoutput, "")

                # person brick handle only
                else:
                    # produce latest version by python manage.py command
                    management.call_command(
                        "person_db2json",
                        verbosity=0
                    )

                    # get latest file version
                    s_pathdatadump = "{}brick/oo/".format(MEDIA_ROOT)
                    s_annotfileregex = "{}person_brick_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]_oo.json".format(
                        s_pathdatadump,
                    )
                    ls_annotfile = glob.glob(s_annotfileregex)
                    s_pathfiledatadump = annotfilelatest(ls_annotfile)
                    s_filedatadump = s_pathfiledatadump.replace(s_pathdatadump, "")
                    with open(s_pathfiledatadump, "r") as f_json:
                        d_jsonrecord = json.load(f_json)

                    # tsv manipulation
                    if (s_filetype == "tsv"):
                        s_fileoutput = s_filedatadump.replace(".json", "_human.txt")
                        s_pathfileoutput = "{}{}".format(s_pathzip, s_fileoutput)
                        llo_tsvrecord = dumpdatajson2tsvhuman(d_jsonrecord)
                        with open(s_pathfileoutput, "w", newline="") as f_tsv:
                            writer = csv.writer(f_tsv, delimiter="\t")
                            writer.writerows(llo_tsvrecord)

                    # json manipulation
                    else:
                        s_fileoutput = s_filedatadump.replace(".json", "_human.json")
                        s_pathfileoutput = "{}{}".format(s_pathzip, s_fileoutput)
                        ddo_jsonrecord = dumpdatajson2jsonhuman(d_jsonrecord)
                        with open(s_pathfileoutput, "w") as f_json:
                            json.dump(
                                ddo_jsonrecord,
                                f_json,
                                indent=4,
                                sort_keys=True
                            )

                # pack latest version into zip
                zipper.write(s_pathfileoutput, arcname=s_fileoutput)

            # screen message
            s_out = o_out.getvalue()
            s_out = s_out.strip()

        ######################
        ## vocabulary layer ##
        ######################
        elif (s_layer.split(".")[0] == "vocabulary"):
            if (s_layer.split(".")[-1] == "origin"):
                s_managepycommand = "vocabulary_loadupdate"
            else:
                s_managepycommand = "vocabulary_backup"

            # stdout handle
            o_out = io.StringIO()

            # for each vocabulary
            ls_choice = s_choice.split("!")
            for s_apponvocabulary in ls_choice:

                # produce latest version by python manage.py command
                management.call_command(
                    s_managepycommand,
                    s_apponvocabulary,
                    stdout=o_out,
                    verbosity=0
                )

                # get latest file version
                s_vocabularysource = s_apponvocabulary.replace("appon", "")
                s_pathjson = "{}vocabulary/{}/".format(MEDIA_ROOT, s_layer.split(".")[-1])
                s_annotfileregex = "{}{}_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_*_{}.json".format(
                    s_pathjson,
                    s_vocabularysource,
                    s_layer.split(".")[-1]
                )
                ls_annotfile = glob.glob(s_annotfileregex)
                s_pathfilejson = annotfilelatest(ls_annotfile)
                s_filejson = s_pathfilejson.replace(s_pathjson, "")
                with open(s_pathfilejson, "r") as f_json:
                    d_jsonrecord = json.load(f_json)

                # tsv manipulation
                if (s_filetype == "tsv"):
                    s_filehuman = s_filejson.replace(".json", "_human.txt")
                    s_pathfilehuman = "{}{}".format(s_pathzip, s_filehuman)
                    llo_tsvrecord = dumpdatajson2tsvhuman(d_jsonrecord)
                    with open(s_pathfilehuman, "w", newline="") as f_tsvhuman:
                        writer = csv.writer(f_tsvhuman, delimiter="\t")
                        writer.writerows(llo_tsvrecord)

                # json manipulation
                else:
                    s_filehuman = s_filejson.replace(".json", "_human.json")
                    s_pathfilehuman = "{}{}".format(s_pathzip, s_filehuman)
                    ddo_jsonrecord = dumpdatajson2jsonhuman(d_jsonrecord)
                    with open(s_pathfilehuman, "w") as f_jsonhuman:
                        json.dump(
                            ddo_jsonrecord,
                            f_jsonhuman,
                            indent=4,
                            sort_keys=True
                        )

                # zip
                zipper.write(s_pathfilehuman, arcname=s_filehuman)
                os.remove(s_pathfilehuman)

            # screen message
            s_out = o_out.getvalue()
            s_out = s_out.strip()


        ###############################################
        ## generic json and tsv djnago db table dump ##
        ###############################################
        else:
            # get stdout
            o_out = io.StringIO()

            #  managepy command
            management.call_command("dumpdata", s_choice, stdout=o_out, verbosity=0)

            # handle manage py output
            s_jsonrecord = o_out.getvalue()
            s_jsonrecord = s_jsonrecord.strip()
            d_jsonrecord = json.loads(s_jsonrecord)
            if (s_filetype == "tsv"):
                s_file = "{}_dumpdata_{}{}_human.txt".format(
                    s_choice.split(".")[-1],
                    datetime.now().strftime("%Y%m%d"),
                    datetime.now().strftime("_%H%M%S"),
                )
                s_pathfile = "{}{}".format(s_pathzip, s_file)
                llo_tsvrecord = dumpdatajson2tsvhuman(d_jsonrecord)
                with open(s_pathfile, "w", newline="") as f_tsvhuman:
                    writer = csv.writer(f_tsvhuman, delimiter="\t")
                    writer.writerows(llo_tsvrecord)
            else:
                s_file = "{}_dumpdata_{}{}_oo.json".format(
                    s_choice.split(".")[-1],
                    datetime.now().strftime("%Y%m%d"),
                    datetime.now().strftime("_%H%M%S"),
                )
                s_pathfile = "{}{}".format(s_pathzip, s_file)
                ddo_jsonrecord = dumpdatajson2jsonhuman(d_jsonrecord)
                with open(s_pathfile, "w") as f_jsonhuman:
                    json.dump(ddo_jsonrecord, f_jsonhuman, indent=4, sort_keys=True)

            # pack dump into zip
            zipper.write(s_pathfile, arcname=s_file)
            os.remove(s_pathfile)

            # screen message
            s_out = ""


    # push latest version save as zip
    response = HttpResponse(content_type="application/zip")   # apploication/json
    response["Content-Disposition"] = "attachment; filename={}".format(
        s_filezip
    )
    with open(s_pathfilezip, "rb") as f:
        response.write(f.read())
    # delete zip file
    os.system("rm {}".format(s_pathfilezip))


    # put message
    if (s_out == ""):
        messages.success(
            request,
            "{} {} # successfully zipped.".format(s_filetype, s_choice)
        )
    elif re.match(".*Error.*", s_out):
        messages.error(
            request,
            "{} {} # {}".format(s_filetype, s_choice, s_out)
        )
    elif re.match(".*Warning.*", s_out):
        messages.warning(
            request,
            "{} {} # {}".format(s_filetype, s_choice, s_out)
        )
    else:
        messages.info(
            request, "{} {} # {}".format(s_filetype, s_choice, s_out)
        )
    storage = messages.get_messages(request)

    # return
    return(response)
    # return HttpResponse("Hello, world. You're at the appsavocabulary export page.")
