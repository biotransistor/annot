# import form django selectable prj
from selectable.base import ModelLookup
from selectable.registry import registry

# import from annot
from apponcompound_ebi.models import Compound

# code
class CompoundLookup(ModelLookup):
    model = Compound
    search_fields = ('annot_id__icontains',)
registry.register(CompoundLookup)

