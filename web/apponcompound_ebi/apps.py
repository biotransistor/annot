from django.apps import AppConfig


class ApponcompoundEbiConfig(AppConfig):
    name = 'apponcompound_ebi'
