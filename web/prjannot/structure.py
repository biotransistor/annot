# import for django
# import from python
import re
import sys


# import from annot
def retype(o_value):
    """
    routine to merely transfom string to type
    """
    if not((type(o_value) == bool) or (o_value == None)):
        # if not a number
        if not((type(o_value) == float) \
               or (type(o_value) == int) \
               or (type(o_value) == complex)):
            # strip
            o_value = str(o_value)
            o_value = o_value.strip()

            # number
            if re.fullmatch("[-|+|0-9|.|e|E|j|(|)]+", o_value):
                # complex
                if re.search("j", o_value):
                    try:
                        o_value = complex(o_value)
                    except ValueError:
                        pass  # still a string
                # float
                elif re.search("[.|e|E]", o_value):
                    try:
                        o_value = float(o_value)
                    except ValueError:
                        pass  # still a string
                # integer
                else:
                    try:
                        o_value = int(o_value)
                    except ValueError:
                            pass  # still a string
            # boolean
            elif (o_value in ('TRUE','True','true')):
                o_value = True
            elif (o_value in ('FALSE','False','false')):
                o_value = False
            elif (o_value in ('NONE','None','none','Null','Null','null')):
                o_value = None
            # a real string, complex, float or integer
            else:
                pass
    # output
    return(o_value)


# annot project whide utilised metodes
def annotfilelatest(ls_annotfile, b_verbose=True):
    if (b_verbose):
        print("Search latest file version... {}".format(ls_annotfile))
    # initiate values
    s_annotfilelatest = None
    i_datelatest = 0
    s_versionrelease = None

    # for each file
    for s_annotfile in ls_annotfile:
        # get value form file
        s_date = (s_annotfile.split("/")[-1]).split("_")[2]
        i_date = int(s_date)
        s_version = ((s_annotfile.split("/")[-1]).split(".")[-2]).split("_")[3]

        # check for lates release date
        if (i_datelatest < i_date):
            # set datelatest
            i_datelatest = i_date
            # overwrite final value
            i_daterelease = i_date
            s_versionrelease = s_version
            s_annotfilelatest = s_annotfile

        # check for latest version
        elif (i_datelatest == i_date):
            try:
                i_version = int(s_version)
                i_versionrelease = int(s_versionrelease)
                if (i_versionrelease < i_version):
                    s_versionreleaset = s_version
                    s_annotfilelatest = s_annotfile
            except ValueError:
                sys.exit(
                    "CONFLICT:\nFor this vocabulary {} at this day more then one backup version exist. Unable to gess the laest version. Delete the superfluos versions.".format(
                        s_annotfile
                    )
                )

    # output
    if (b_verbose):
        print("Found file: {}".format(s_annotfilelatest))
    return(s_annotfilelatest)


# generic djnago dumpdata json to human tsv function
def dumpdatajson2tsvhuman(ld_json, es_filter=None):
    ls_header = None
    llo_out = []
    for d_json in ld_json:
        if  (es_filter == None) or (d_json["pk"] in es_filter):
            # handle fields
            if (ls_header == None):
                ls_filed = list(d_json["fields"].keys())
                # pop some overlong fields
                try:
                    ls_filed.pop(ls_filed.index("acjson"))
                except ValueError:
                    pass
                ls_filed.sort()
                # handle header
                ls_header = ["model","pk"] + ls_filed
            # for each record
            lo_record = [d_json["model"], d_json["pk"]]
            llo_record = [lo_record]
            for s_filed in ls_filed:
                o_entry = d_json["fields"][s_filed]
                if (type(o_entry) == dict):
                    sys.exit(
                        "Error: django dumpdata json should not contain a dictionary in fields. {}".format(
                            o_entry
                        )
                    )
                if (type(o_entry) == list):
                    o_entry = str(o_entry)
                    o_entry = re.sub(r"'","", o_entry)
                    o_entry = re.sub(r'"','', o_entry)
                if (type(o_entry) == str):
                    o_entry = re.sub(r"\s"," ", o_entry)
                # append
                [lo_record.append(o_entry) for lo_record in llo_record]
            # fuse records
            llo_out.extend(llo_record)
    # output
    if (ls_header != None):
        llo_out.insert(0, ls_header)
    return(llo_out)


# generic djnago dumpdata json to human json function
def dumpdatajson2jsonhuman(ld_json, es_filter=None):
    ls_field = None
    ddo_record = {}
    for d_json in ld_json:
        if  (es_filter == None) or (d_json["pk"] in es_filter):
            # handle fields
            if (ls_field == None):
                ls_filed = list(d_json["fields"].keys())
                # pop some overlong fields
                try:
                    ls_filed.pop(ls_filed.index("acjson"))
                except ValueError:
                    pass
            # for each record
            do_record = {}
            do_record.update({"model" : d_json["model"]})
            ls_keys = list(do_record.keys())
            for s_key in ls_keys:
                if (type(do_record[s_key]) == str):
                    s_entry = re.sub(r"\s"," ", do_record[s_key])
                    do_record.update({s_key : s_entry})
            for s_filed in ls_filed:
                do_record.update({s_filed : d_json["fields"][s_filed]})
            # fuse records
            ddo_record.update({d_json["pk"] : do_record})
    # output
    return(ddo_record)
